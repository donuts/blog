title: 10 years of collecting Tor directory data
---
pub_date: 2014-05-15
---
author: karsten
---
tags:

metrics
directory authority
archive
---
categories:

metrics
network
---
_html_body:

<p>Today is the 10th anniversary of collecting Tor directory data!</p>

<p>As the <a href="https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf" rel="nofollow">2004 Tor design paper</a> says, "As of mid-May 2004, the Tor network consists of 32 nodes (24 in the US, 8 in Europe), and more are joining each week as the code matures."</p>

<p>In fact, we still have the original relay lists from back then.  The <a href="https://people.torproject.org/~karsten/directory-2004-05-15-07-31-04" rel="nofollow">first archived Tor directory</a> dates back to May 15, 2004.  It starts with the following lines which are almost human-readable:</p>

<p>signed-directory<br />
published 2004-05-15 07:30:57<br />
recommended-software 0.0.6.1,0.0.7pre1-cvs<br />
running-routers moria1 moria2 tor26 incognito jap dizum<br />
  cassandra metacolo poblano ned TheoryOrg Tonga<br />
  peertech hopey tequila triphop moria4 anize rot52<br />
  randomtrash</p>

<p>As of today, May 15, 2014, there are about 4,600 relays in the Tor network and another 3,300 bridges.  In these 10 years, we have collected a total of 212 GiB of bz2-compressed tarballs containing Tor directory data.  That's more than 600 GiB of uncompressed data.  And of course, the full archive is publicly <a href="https://metrics.torproject.org/data.html" rel="nofollow">available for download</a>.</p>

<p><img src="https://extra.torproject.org/blog/2014-05-15-10-years-collecting-tor-directory-data/tor-directory-data.png" /></p>

<p>Here's a small selection of what people do with this fine archive:</p>

<ul>
<li>The metrics portal shows graphs of <a href="https://metrics.torproject.org/network.html" rel="nofollow">network growth over time</a> and <a href="https://metrics.torproject.org/users.html" rel="nofollow">estimates of users derived from directory activity</a>.</li>
<li>The <a href="https://exonerator.torproject.org/" rel="nofollow">ExoneraTor service</a> allows people to look up whether a given IP address was part of the Tor network in the past.</li>
<li>The websites <a href="https://atlas.torproject.org/" rel="nofollow">Atlas</a>, <a href="https://globe.torproject.org/" rel="nofollow">Globe</a>, and <a href="https://compass.torproject.org/" rel="nofollow">Compass</a> let users explore how specific relays or bridges contribute to the Tor network.</li>
<li>The <a href="https://shadow.github.io/" rel="nofollow">Shadow Simulator</a> uses archived Tor directory data to generate network topologies that match the real Tor network as close as possible.</li>
<li>The <a href="https://torps.github.io/" rel="nofollow">Tor Path Simulator</a> uses Tor directory archive data to simulate the effect of changes to Tor's path selection algorithm.</li>
</ul>

<p>If people want to use the Tor directory archive for their <a href="https://research.torproject.org/" rel="nofollow">research</a> or for building new applications, or want to help out with the projects listed above, don't hesitate to contact us!</p>

<p>Happy 10th birthday, Tor directory archive!</p>

---
_comments:

<a id="comment-59834"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-59834" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2014</p>
    </div>
    <a href="#comment-59834">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-59834" class="permalink" rel="bookmark">Happy 10th birthday</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Happy 10th birthday</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-60032"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60032" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
    <a href="#comment-60032">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60032" class="permalink" rel="bookmark">Yes, it does indeed call for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, it does indeed call for a joyous celebration....but...wait...</p>
<p>Of the Tor directory data, how many bridges, nodes and relays are run by the NSA, FBI, CIA, GCHQ (UK) and the other 3 "eyes"? Or is such information top secret?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-60046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60046" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  karsten
  </article>
    <div class="comment-header">
      <p class="comment__submitted">karsten said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-60032" class="permalink" rel="bookmark">Yes, it does indeed call for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-60046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60046" class="permalink" rel="bookmark">Ah, that one is easy:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ah, that one is easy: <a href="https://globe.torproject.org/#/search/query=nsa" rel="nofollow">https://globe.torproject.org/#/search/query=nsa</a> (17 relays, 5 bridges), <a href="https://globe.torproject.org/#/search/query=fbi" rel="nofollow">https://globe.torproject.org/#/search/query=fbi</a> (1 bridge), no results for the others.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-60171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60171" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to karsten</p>
    <a href="#comment-60171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60171" class="permalink" rel="bookmark">Ha. I like Karsten&#039;s answer.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ha. I like Karsten's answer. But if you want a second answer, I encourage you to 1) look through all the threads at <a href="https://blog.torproject.org/blog/tor-nsa-gchq-and-quick-ant-speculation" rel="nofollow">https://blog.torproject.org/blog/tor-nsa-gchq-and-quick-ant-speculation</a>, and 2) watch our 30c3 talk from December: <a href="https://events.ccc.de/congress/2013/Fahrplan/events/5423.html" rel="nofollow">https://events.ccc.de/congress/2013/Fahrplan/events/5423.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-60203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60203" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to karsten</p>
    <a href="#comment-60203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60203" class="permalink" rel="bookmark">17 relays and 5 bridges</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>17 relays and 5 bridges hosted by the NSA? When was that? What about this year?</p>
<p>Has Tor developers done anything to prevent NSA, FBI, CIA and GCHQ from running rogue relays and bridges?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-60254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60254" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-60203" class="permalink" rel="bookmark">17 relays and 5 bridges</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-60254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60254" class="permalink" rel="bookmark">Karsten&#039;s response was a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Karsten's response was a joke (but a funny one). He responded to the "which relays are run by nsa" by showing you which relays have the substring "nsa" in their nickname. It is funny on the surface because of course anybody could put, or not put, that substring in their nickname. And funny underneath because it underlines how hard it is to actually answer a question like that for sure.</p>
<p>But to give more concrete answers, as far as we know there have never been any relays or bridges run by the NSA. But that shouldn't make you happy, because the attack I worry about is having their surveillance cover somebody else's perfectly honest relay. Why should they run their own relays when they can watch yours, and get almost all of the benefits with fewer risks? But here I am repeating all of my statements from the gchq-quick-ant blog post, so I'm going to stop doing that here and invite you again to go read all of them there.</p>
<p>(Oh, and there *were* some relays run on amazon ec2 by gchq. Most of them were tiny and only for a week. Go check out the remation 2 documents.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-60065"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60065" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
    <a href="#comment-60065">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60065" class="permalink" rel="bookmark">SO there you go
I mean these</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>SO there you go<br />
I mean these guys are getting paid so you do not something for nothing these days</p>
<p>tim</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-60172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60172" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-60065" class="permalink" rel="bookmark">SO there you go
I mean these</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-60172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60172" class="permalink" rel="bookmark">Huh? I&#039;m afraid you&#039;ll have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Huh? I'm afraid you'll have to be a little bit less subtle in what you're saying.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-60208"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60208" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-60065" class="permalink" rel="bookmark">SO there you go
I mean these</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-60208">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60208" class="permalink" rel="bookmark">As I understand it, the US,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As I understand it, the US, Canada, Europe, Australia and NZ are trying their best to change and upgrade their economies from manufacturing to IT-based.</p>
<p>Let's face it: most manufacturing jobs are outsourced to China, Vietnam, Bangladesh, Brazil, etc.... which provide jobs to millions of people.</p>
<p>Those manufacturing jobs that are still in the US, Europe, Australia, etc.. are few and far in between. Take for example, how many jobs can manufacturers of aircraft, military drones, satellites, etc...offer to job seekers?</p>
<p>The US has since realized that the only way to create more jobs for its citizens is to heighten the sense of insecurity among the world's population leading to the need for more online surveillance.</p>
<p>The US plans to recruit more than 6,000 people next year. The excuse is cyber security.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-60244"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60244" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2014</p>
    </div>
    <a href="#comment-60244">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60244" class="permalink" rel="bookmark">Could you put a graph on Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you put a graph on Tor Metrics Portal for number of Pluggable Transport users out of all bridge users and the number of users for each indivdual PT ( obfs3, fte and flash proxy )?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-60267"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60267" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  karsten
  </article>
    <div class="comment-header">
      <p class="comment__submitted">karsten said:</p>
      <p class="date-time">May 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-60244" class="permalink" rel="bookmark">Could you put a graph on Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-60267">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60267" class="permalink" rel="bookmark">There are graphs on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are graphs on the number of bridge users by pluggable transport on the metrics website: <a href="https://metrics.torproject.org/users.html#userstats-bridge-transport" rel="nofollow">https://metrics.torproject.org/users.html#userstats-bridge-transport</a>.  (And there's an open ticket to add another graph for all pluggable transport users combined, so that should be available at some point.)  In the meantime, if you want to play with the numbers yourself, here's the CSV file: <a href="https://metrics.torproject.org/stats.html#clients" rel="nofollow">https://metrics.torproject.org/stats.html#clients</a></p>
<p>But don't trust those numbers too much, because they may not be as accurate as you'd hope.  See this FAQ at the bottom of the page: "Q: Why are there so few bridge users that are not using the default OR protocol or that are using IPv6? A: Very few bridges report data on transports or IP versions yet, and by default we consider requests to use the default OR protocol and IPv4. Once more bridges report these data, the numbers will become more accurate."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-60269"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60269" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2014</p>
    </div>
    <a href="#comment-60269">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60269" class="permalink" rel="bookmark">Wasn&#039;t there a FF security</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wasn't there a FF security update yesterday? is there gonna be a new TBB release/update soon?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-60599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-60599" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2014</p>
    </div>
    <a href="#comment-60599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-60599" class="permalink" rel="bookmark">Just a thought: With almost</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just a thought: With almost complete data collection, “forever” storage, and quantum exploitation on the horizon, I think it would be only prudent to continually upgrade Tor encryption to the strongest available. And maybe audit its implementations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-61486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2014</p>
    </div>
    <a href="#comment-61486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61486" class="permalink" rel="bookmark">me very try tor at iran</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>me very try tor at iran sorry no work for me</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-61595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-61595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 25, 2014</p>
    </div>
    <a href="#comment-61595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-61595" class="permalink" rel="bookmark">You should add</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You should add <a href="https://tor.stackexchange.com/" rel="nofollow">https://tor.stackexchange.com/</a> and <a href="https://tails.boum.org" rel="nofollow">https://tails.boum.org</a> to the default bookmarks in TorBrowser</p>
</div>
  </div>
</article>
<!-- Comment END -->
