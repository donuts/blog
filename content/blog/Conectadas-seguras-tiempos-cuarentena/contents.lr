title: Conectadas y seguras en tiempos de cuarentena
---
pub_date: 2020-03-23
---
author: gaba
---
tags:

castellano
seguridad personal
violencia doméstica
tor
---
categories: network
---
summary: Nuestras recomendaciones para que estés conectadx de forma segura, puedas trabajar remotamente y proteger tu privacidad cuando estás en cuarentena.
---
_html_body:

<p><em>View this post in <a href="https://blog.torproject.org/remote-work-personal-safety">English (en)</a> | <a href="https://blog.torproject.org/trabalho-remoto-seguranca-pessoal">Portuguese (pt)</a>.</em></p>
<hr />
<p>Estamos en una situación nueva y problemática para todxs alrededor del mundo. Al ser Tor una organización totalmente remota e internacional que desarrolla herramientas para seguridad digital, nos gustaría compartir algunas recomendaciones sobre como trabajar desde tu casa, y al mismo tiempo mantener tus derechos a privacidad y libertad de expresión.</p>
<h2>Trabajo remoto</h2>
<p><a href="https://www.torproject.org/about/history/">Desde 2006</a>, el projecto Tor y su comunidad ha funcionado de forma remota. Siempre que sea posible, usamos herramientas libres que compartan nuestro compromiso con proteger los derechos humanos de privacidad y libertad de expresión en Internet. Estas son las herramientas que estamos usando para estar conectadas:</p>
<p><strong>IRC.</strong> La mayor parte de nuestras conversaciones en línea sucede en canales abiertos de IRC en el servidor irc.oftc.net, como #tor-project, #tor-dev y #tor-www <a href="https://support.torproject.org/get-in-touch/irc-help/">entre otros</a>. Si quieres hablar con alguna de nosotras sobre Tor, puedes encontrarnos en #tor. Sin importar que herramienta de chat usa tu organización, te recomendamos crear un canal para temas off-topic. Es invaluable la oportunidad de compartir noticias o información sin interferir con los canales principales. No tienes que compartir tu correo electrónico ni ninguna información personal para registrarte o usar IRC.</p>
<p><strong><a href="https://pad.riseup.net/">Bloc de notas de Riseup</a>.</strong> Las usamos para las agendas de reuniones, para tomar notas y para coordinar borradores de blogposts. No se guardan para siempre, por lo que es para un uso temporario y no para guardar documentos. No se necesita un registro o cuenta, solamente un navegador web.</p>
<p><em>Para quienes estén trabajando con población de riesgo o con información sensible, esto sera de ayuda:</em></p>
<p><a href="torproject.org/download"><strong>Tor Browser</strong></a>. Cuando usas el navegador web Tor para búsquedas, para loguearte en cuentas o para colaborar, te estás protegiendo de rastreadores en sitios web, vigilancia de tu proveedor de Internet o de cualquier persona monitoreando tu red, y de censura de tu proveedor de Internet o del gobierno. Si eres un proveedor de salud o de primeros auxilios y estás buscando información sensible que pueda ser conectada con personas que te estén consultando, el navegador de Tor es una herramienta que te puede proteger a ti y a la gente que tú atiendes.</p>
<blockquote><p>"Soy un doctor en un pueblo muy politizado. Cuando tengo que hacer una investigacion en enfermedades o tratamientos o buscar cosas especificas en la historia de mis pacientes, estoy bien al tanto que mi historial de búsquedas puede correlacionarse con visitas de pacientes y esto puede filtrar información sobre su salud, familias y vida personal. Yo uso Tor para hacer la mayor parte de mi investigación cuando pienso que hay riesgo de que se correlacione con las visitas de mis pacientes."</p>
<p>- Usuaria Anónima de Tor</p>
</blockquote>
<p><strong><a href="https://www.signal.org/">Signal</a>.</strong> Para enviar mensajes de persona a persona y en grupos pequeños de chat, usamos una aplicación de software libre llamada Signal. La comunicación es encriptada punto a punto (desde que mandas el mensaje en tu dispositivo hasta donde llega al final) y puedes hacer que mensajes en particular que tengan información sensible expiren y desaparezcan luego despues de cierto tiempo. Jitsi Meet. Para reuniones que requieran audio y video, usamos Jitsi Meet. Es software libre y no se necesita ningún registro para usarlo. Se puede elegir la dirección web que vas a usar, por ejemplo , <a href="https://meet.jit.si/onionsforall">https://meet.jit.si/onionsforall</a> y compartir enlace. Intenta usarlo en lugar de <a href="https://www.accessnow.org/access-now-urges-transparency-from-zoom-on-privacy-and-security/">Zoom, que ya ha sido criticado por su falta de transparencia</a>.</p>
<p><strong><a href="onionshare.org">OnionShare</a>.</strong> OnionShare te permite compartir de forma segura y anónima un archivo de cualqueir tamaño sin involucrar a terceros. Si necesitas compartir recursos críticos para individuos o grupos, <a href="https://blog.torproject.org/new-version-onionshare-makes-it-easy-anyone-publish-anonymous-uncensorable-websites-0">la última versión de OnionShare</a> también te permite crear un sitio Onion solo accesible por la red de Tor.</p>
<p><strong><a href="http://share.riseup.net/">share.riseup.net</a>.</strong> Esta es una herramienta en la web para compartir pequenos archivos (no mas de 50mb) de forma rapida. Usualmente usamos enlaces de share.riseup.net para compartir fotos o impresiones de pantalla en nuestros canales de IRC.</p>
<p>Si aun no encuentras las herramientas correctas para lo que necesitas, puedes leer las recomendaciones que el sysadmin del Tor Project tiene en <a href="https://anarc.at/blog/2020-03-15-remote-tools/"><em>Remote presence tools for social distancing</em> (en inglés)</a>.</p>
<h2>Seguridad Personal</h2>
<p>El hogar no es siempre el lugar más seguro para todas. Si estás buscando ayuda o estás en contacto con alguien que necesita ayuda, te recomendamos usar el navegador Tor para buscar información o asistencia sin dejar ninguna huella de tu búsqueda o tu historial de navegación. <a href="https://www.techsafety.org/digital-services-during-public-health-crises">La Red Nacional para Terminar con la Violencia Doméstica (en EEUU) </a>tiene recomendaciones adicionales que puedes seguir.</p>
<p>Lo mismo va para cualquiera que esté investigando temas con información sensible, tanto sea recursos para salud de mujeres, como información sobre condiciones de salud mental o médica: Usar <a href="http://torproject.org/download">el navegador Tor</a> en combinación con motores de búsqueda como <a href="http://duckduckgo.com/">DuckDuckGo</a>, puede ayudarte mantener tu información personal protegida, habilitarte a poder compartir solo lo que tú quieras, y permitir acceso a información crítica y recursos que pudieran estar censurados o bajo vigilancia.</p>
<blockquote><p>"Uso el navegador de Tor para investigar sobre enfermedades mentales, como por ejemplo depresión, que puedan suceder en tu familia. No quiero que nadie que yo no quiera sepa sobre estas enfermedades. Por esto es que uso Tor para investigar sobre lo que sea relacionado a estas enfermedades"</p>
<p>- Usuaria Anónima de Tor</p>
</blockquote>
<p>Muchas de las herramientas que mencionamos antes, incluidas Jitsi, Signal y OnionShare, pueden ayudarte a comunicar de forma más segura en circunstancias difíciles.</p>
<h2>Mantente Conectada</h2>
<p>Vivimos en tiempos inciertos, y es fundamental que nos mantengamos conectadas y hagamos nuestra parte en mantenernos unas a las otras seguras. Si tienes alguna pregunta sobre Tor, o cualquier otra herramienta que te pueda ayudar, únete al canal #tor en irc.oftc.net</p>
<p>Si quieres contribuir con nuestro trabajo, te invitamos a unirte a nuestra comunidad. Somos una pequeña organización sin fines de lucro con un objectivo muy claro: hacer que la privacidad y la libertad sean el modo por defecto en Internet, y nuestro trabajo se hace posible gracias a las contribuciones invaluables de voluntarixs alrededor del mundo. Nuestra segunda DocsHackaton, un evento totalmente remoto, comenzó ayer <a href="https://blog.torproject.org/docshackathon-2020">Domingo 22 de Marzo</a>, y agradecemos tu presencia, tiempo y contribución. Esperamos que te nos unas.</p>
<p>Somos sólo una de muchas comunidades en linea donde tu podrias hacer un impacto, por lo que si no encuentras el lugar correcto para ti, sigue explorando.</p>

