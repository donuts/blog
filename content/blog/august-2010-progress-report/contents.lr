title: August 2010 Progress Report
---
pub_date: 2010-09-13
---
author: phobos
---
tags:

progress report
translations
alpha release
scalability
---
categories:

localization
releases
reports
---
_html_body:

<p><strong>Releases</strong></p>

<ul>
<li>On August 21st we released Tor Browser Bundle 1.0.10 for GNU/Linux. See <a href="https://blog.torproject.org/blog/tor-browser-bundle-1010-gnulinux-released" rel="nofollow">https://blog.torproject.org/blog/tor-browser-bundle-1010-gnulinux-relea…</a></li>
<li>On August 18, we released Tor 0.2.2.15-alpha.  It fixes a big bug in hidden service availability, fixes a variety of other bugs that were preventing performance experiments from moving forward, fixes several bothersome memory leaks, and generally closes a lot of smaller bugs that have been filling up trac lately. See <a href="https://blog.torproject.org/blog/tor-02215-alpha-released" rel="nofollow">https://blog.torproject.org/blog/tor-02215-alpha-released</a></li>
<li>Released two new versions of Orbot, Tor for Android.<br />
<blockquote><p>
Version 1.0.2<br />
- added "check" yes/no dialog prompt<br />
- debugged iptables/transprox settings on Android 1.6 and 2.2<br />
- added proxy settings help screen and fixed processSettings() NPE</p>
<p>Version 1.0.1<br />
- found and fixed major bug in per-app trans proxying; list of apps was being cached and iptables rules were not properly updated as the user changed the selection in the list
</p></blockquote>
</li>
<li>On August 6, we released Libevent 2.0.6-rc, the first release<br />
candidate of the new Libevent 2.0 series. The main new feature that<br />
we want from Libevent 2.0 is its support for buffer-based (rather than<br />
socket-based) network abstractions, which will let us support Windows the<br />
way it wants to be supported. The new Libevent includes a wide variety of<br />
other features that will make our lives easier too, ranging from making<br />
it easier to support multi-threaded crypto operations to making it easier<br />
to modularly change Tor's transport from TLS-over-TCP to other options. See <a href="http://levent.git.sourceforge.net/git/gitweb.cgi?p=levent/levent;a=blob;f=ChangeLog;hb=fe008ed656766266b93cdf2083f5b8bc50e6aad3" rel="nofollow">http://levent.git.sourceforge.net/git/gitweb.cgi?p=levent/levent;a=blob…</a></li>
</ul>

<p><strong>Funding</strong><br />
We finished the first year of our NSF grant, and wrote up the annual<br />
(interim) report on what metrics work we've done and what research<br />
projects we've helped with. We were then awarded the second year of the<br />
two-year grant.</p>

<p><strong>Design, develop, and implement enhancements that make Tor a better<br />
tool for users in censored countries.</strong></p>

<p>Continuing research into China's Great Firewall shows bridges are surviving for 1-2 weeks before being blocked.  We're working to improve the bridge database such that it only gives out bridges that work in a requested country.  Right now, we hand out a random selection of 3 bridges regardless of potential country of usage.  In China, bridges and relays are still blocked by IP address and TCP port combinations. </p>

<p><strong>Architecture and technical design docs for Tor enhancements<br />
related to blocking-resistance.</strong></p>

<p>We've brainstormed future tasks that need to be done for Tor, broken down<br />
into two sets by upcoming priority:<br />
<a href="https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorD/March2011" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorD/March20…</a><br />
and<br />
<a href="https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorD/June2011" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorD/June2011</a></p>

<p>We wrote up project sketches of how to tackle some of these tasks, such as:</p>

<ul>
<li>Bridge relay images for the cloud. <a href="https://trac.torproject.org/projects/tor/ticket/1853" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1853</a></li>
<li>Bundling the http request and headers with the initial begin cell, to save a round-trip and improve client preformance. <a href="https://trac.torproject.org/projects/tor/ticket/1849" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1849</a></li>
<li>A performance experiment to raise the minimum bandwidth for being a relay, thus reducing the overall capacity of the network but improving the average performance of each relay. <a href="https://trac.torproject.org/projects/tor/ticket/1854" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1854</a></li>
<li>A preliminary design for UDP transport between Tor relays. <a href="https://trac.torproject.org/projects/tor/ticket/1855" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1855</a></li>
<li>Tor clients should remember bridge relay information and statistics across restarts. <a href="https://trac.torproject.org/projects/tor/ticket/1852" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1852</a></li>
<li>We should have bridge relays automatically monitor whether they can reach websites like Baidu, to give us early warnings when the bridges become blocked. <a href="https://trac.torproject.org/projects/tor/ticket/1851" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/1851</a></li>
</ul>

<p><strong>Outreach and Advocacy</strong></p>

<ul>
<li>Roger met with the University of California - San Diego research group to help them better understand the research challenges in Tor.  The full trip report is available at <a href="https://blog.torproject.org/blog/trip-report-ucsd" rel="nofollow">https://blog.torproject.org/blog/trip-report-ucsd</a>.   Roger's presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/slides-ucsd10.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/slides-ucsd10.pdf</a>.</li>
<li>Roger met with a researcher from The Cooperative Association for Internet Data Analysis (CAIDA) to discuss Tor, metrics, and analyzing our growing collection of data about the Tor network.</li>
<li>Roger attended the "Workshop on Cyber Security Data for Experimentation" organized by the National Science Foundation (NSF).  The premise of the workshop was that many academics need real-world data sets to solve problems, whereas industry is the place with the real-world data sets and they don't have any real reason to share. By getting the academics and the industry people talking, with government funders nearby, they hoped to better understand the problems and maybe move things forward.<br />
Roger was there (and on the first panel) because of Tor's work on gathering Tor network snapshots, performance data, and user statistics. Tor's approach represents one way out of the trap where researchers never quite get the data they want, or if they do it isn't open enough (which hinders whether anybody else can reproduce their results).  The full trip report is available at <a href="https://blog.torproject.org/blog/trip-report-nsf-data-workshop" rel="nofollow">https://blog.torproject.org/blog/trip-report-nsf-data-workshop</a>.</li>
<li>Roger did a talk to a half dozen NSF program managers, to bring them up to speed on what Tor is up to and what sort of measurement and research projects we're working on and should work on next. The presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/slides-nsf10.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/slides-nsf10.pdf</a>.</li>
<li>Erinn discussed Tor and free software at DebConf 2010 in New York City. <a href="http://debconf10.debconf.org/" rel="nofollow">http://debconf10.debconf.org/</a>.</li>
<li>Andrew and Paul presented at The International Conference on Cyber Security.  <a href="http://www.iccs.fordham.edu/" rel="nofollow">http://www.iccs.fordham.edu/</a>.  Andrew's presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/2010-iccs-tor-overview.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/2010-iccs-tor-ove…</a>.  Paul presented the current attacks on Tor's design from a research perspective, as well as giving a briefing on current topics of research into trusted routing within Tor.</li>
<li>Ian, Roger, and others attended Usenix Security 2010, <a href="http://www.usenix.org/events/sec10/index.html" rel="nofollow">http://www.usenix.org/events/sec10/index.html</a>.</li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<ul>
<li>Torbrowser Bundle for GNU/Linux version 1.0.10 released. See above for the details.</li>
<li>Torbrowser Bundle for OSX works on OS X 10.4 and 10.5.  10.6 users continue to experience issues with the launching of the Firefox browser.  Erinn and Steven are continuing to debug the 10.6 issues.</li>
</ul>

<p><strong>Bridge relay and bridge authority work.</strong></p>

<p>We're developing designs to better handle bridge distribution.  Part of this is to enable Tor clients to become bridges and then relays by default.  The other part of this is for Tor clients to always have a set of working bridges, requesting new bridges in advance, or be able to track bridge address changes and update accordingly.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<ul>
<li>Accepted Proposal 174 for "Optimistic Data for Tor: Server Side" from Ian Goldberg.  This change will save one OP-Exit round trip (down to one from two). There are still two SOCKS Client-OP round trips (negligible time) and two Exit-Server round trips.  Depending on the ratio of the Exit-Server (Internet) RTT to the OP-Exit (Tor) RTT, this will decrease the latency by 25 to 50 percent.  Experiments validate these predictions. [Goldberg, PETS 2010 rump session; see <a href="https://thunk.cs.uwaterloo.ca/optimistic-data-pets2010-rump.pdf" rel="nofollow">https://thunk.cs.uwaterloo.ca/optimistic-data-pets2010-rump.pdf</a>] The full proposal can be read at <a href="https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposals/174-optimistic-data-server.txt" rel="nofollow">https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposa…</a>.</li>
<li>Mike Perry fixed a number of bugs in the bandwidth authority and exit scanner codebases.  The exit scanner codebase is updated with the work of a Google Summer of Code student.</li>
<li>A Google Summer of Code student, Harry Bock, worked on improving the Tor DNSEL codebase.  TorDNSEL is a DNSBL-style interface for querying information about Tor exit nodes, to be more thorough, more usable, and more maintainable. Out of this effort came TorBEL, a set of specifications and Python tools that try to address this problem.  The full writeup on the new software can be found on our blog at <a href="https://blog.torproject.org/blog/torbel-tor-bulk-exit-list-tools" rel="nofollow">https://blog.torproject.org/blog/torbel-tor-bulk-exit-list-tools</a>.</li>
</ul>

<p><strong>Translations</strong><br />
Runa implemented a change to the publishing of translated website pages.  We now only publish a non-English page to the website if the text is 80% translated or more.  This has cleared out hundreds of older, untranslated pages from the website that were misinforming and confusing readers.</p>

<p>Updated translations for all documents in Arabic, Persian, German, French, Polish, Romanian, Russian, Norwegian, Mandarin Chinese, and Turkish.</p>

---
_comments:

<a id="comment-7675"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7675" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 13, 2010</p>
    </div>
    <a href="#comment-7675">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7675" class="permalink" rel="bookmark">Can you guys make a Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you guys make a Tor Browser Bundle without Firefox? It would make LiveCD usage so much better in that closing the browser does not close Tor. I made this LiveCD (test it using VirtualBox) using Midori and XChat &amp; different config directories so that one shortcut will use the default network, and the other will use Tor.</p>
<p>42.7MB <a href="http://www.mediafire.com/file/3uxbsu795ud3rbk/slitaz-tor-0.1.iso" rel="nofollow">http://www.mediafire.com/file/3uxbsu795ud3rbk/slitaz-tor-0.1.iso</a></p>
<p>Screenshot: <a href="http://imgur.com/pAbNf.png" rel="nofollow">http://imgur.com/pAbNf.png</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7682" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7675" class="permalink" rel="bookmark">Can you guys make a Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7682" class="permalink" rel="bookmark">You can always make your own</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can always make your own torbrowser bundle.  The instructions for doing so are at <a href="https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/DEPLOYMENT" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/DEPLOYMENT</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7684"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7684" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-7684">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7684" class="permalink" rel="bookmark">I downloaded</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded vidalia-0.2.9.tar.gz. I'm not a coder but removing more than 50 instances of the words "browser" and "firefox" from the source code seems like a bad idea.</p>
<p>However: I might be able to just remove the dialog error box that appears when the browser isn't found. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7693" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7684" class="permalink" rel="bookmark">I downloaded</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7693" class="permalink" rel="bookmark">So, you just want to run</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So, you just want to run vidalia/tor from a usb drive?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-7695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7695" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7675" class="permalink" rel="bookmark">Can you guys make a Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7695" class="permalink" rel="bookmark">Perhaps what you really want</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Perhaps what you really want is TAILS, not some cobbled together livecd with tor added into it.  See <a href="http://amnesia.boum.org/about/" rel="nofollow">http://amnesia.boum.org/about/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-7679"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7679" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
    <a href="#comment-7679">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7679" class="permalink" rel="bookmark">Yeah, I agree. The fact that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, I agree. The fact that closing Firefox also closes Tor is annoying especially for relay operators.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7685"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7685" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
    <a href="#comment-7685">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7685" class="permalink" rel="bookmark">&quot;Libevent ... support for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><i>"Libevent ... support for buffer-based (rather than socket-based) network abstractions..."</i></p>
<p>I don't really understand what Libevent is, is it to be plumbed into a Tor package at some point in the future?  I keep running into the "ENOBUFS - Out of memory" "WSAENOBUFS" "Error 10050" problem here which is getting a bit tiring.  I gather this change should help cure that if I read it right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7694"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7694" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7685" class="permalink" rel="bookmark">&quot;Libevent ... support for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7694">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7694" class="permalink" rel="bookmark">Yes, this is exactly what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, this is exactly what Libevent 2.0 is fixing, see <a href="https://trac.torproject.org/projects/tor/ticket/98" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/98</a> for the sordid story of busy tor relays on windows.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-7707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7707" class="permalink" rel="bookmark">Very good, so does</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very good, so does 0.2.2.15-alpha contain the new Libevent method then, as I still encounter the same problem with it;</p>
<p>Sep 20 07:40:11.890 [warn] write() failed: WSAENOBUFS. Not enough ram?</p>
<p>I wouldn't have thought RAM was an issue here, unless it's some obscure system buffer tucked away somewhere defined (and limited) by Windows (probably).</p>
<p>These were the stats during the failed state;</p>
<p>Ram Total 2,096,620 KB<br />
Available 1,380,376 KB<br />
System Cache 248,356 KB<br />
Kernel Memory Total 315,592 KB<br />
Kernel Memory Paged 55,496 KB<br />
Kernel Memory Nonpaged 260,088 KB<br />
Tor.exe Mem Usage 39,856 KB<br />
Tor.exe VM Size 24,692 KB</p>
<p>In tests prior a netstat hasn't revealed anything either; no masses of ports left open in a limbo state or anything, only what one would expect to see.  So far I haven't been able to figure out just what to query where on the system to monitor whatever it is that's running out.</p>
<p>Whatever is going on it seems pretty clear that the Tor process isn't releasing some resource it's consuming, something I can't say I've experienced with other (similarly heavy) applications I've used in the past (which causes me to ask what it is that Tor does differently).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7710"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7710" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7707" class="permalink" rel="bookmark">Very good, so does</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7710">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7710" class="permalink" rel="bookmark">We have not built packages</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have not built packages for windows which include libevent 2.0 yet.  Libevent 2.0 is in release candidate.  We're going to wait until libevent 2.0 is stable and start building the -alpha packages with it.</p>
<p>Bug 98 has the details of the problem, <a href="https://trac.torproject.org/projects/tor/ticket/98" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/98</a>.  The core problem is the way Windows handles sockets, which is not the proper way to do heavy network i/o on Windows.  See <a href="http://msdn.microsoft.com/en-us/library/ms819739" rel="nofollow">http://msdn.microsoft.com/en-us/library/ms819739</a>.  </p>
<p>The real answer is buffer events, and this code is in libevent 2.0.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-7686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7686" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
    <a href="#comment-7686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7686" class="permalink" rel="bookmark">I was able to make</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was able to make TorBrowser standalone, to a degree. Just search for "unable to start" in /vidalia-0.2.9/src/vidalia/MainWindow.cpp and remove the 4 lines so that "MainWindow::onBrowserFailed(QString errmsg)" looks like this:</p>
<p>{<br />
  Q_UNUSED(errmsg);<br />
}</p>
<p>and compile Vidalia from there. After that, you can simply replace the executable in ./App/.</p>
<p>A link to version 1.0.10 without Firefox &amp; the modified Vidalia for those that can't compile: <a href="http://www.mediafire.com/file/zsx4mst75vdkj32/Torbundle-linux-i386-1.0.10-NO_BROWSER.tar.gz" rel="nofollow">http://www.mediafire.com/file/zsx4mst75vdkj32/Torbundle-linux-i386-1.0…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
