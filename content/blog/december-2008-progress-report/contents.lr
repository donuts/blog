title: December 2008 Progress Report
---
pub_date: 2009-02-02
---
author: phobos
---
tags:

progress report
anonymity advocacy
bug fixes
security fixes
alpha release
---
categories:

advocacy
releases
reports
---
_html_body:

<p><strong>Releases</strong><br />
Tor 0.2.1.8-alpha (released December 8) fixes some crash bugs in earlier alpha releases, builds better on unusual platforms like Solaris and old OS X, and fixes a variety of other issues.<br />
<a href="http://archives.seul.org/or/talk/Dec-2008/msg00129.html" rel="nofollow">http://archives.seul.org/or/talk/Dec-2008/msg00129.html</a></p>

<p>Tor Browser Bundle 1.1.6 (released December 2) and 1.1.7 (released December 12) update Tor to 0.2.1.8-alpha, include a new version of Firefox, and attempt to wrestle with the "AllowMultipleInstances=false" design that could allow us to run Tor Browser Bundle alongside a normal Firefox.<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/README" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/README</a></p>

<p>Tor 0.2.1.9-alpha (released December 25) fixes many more bugs, some of them security-related.<br />
<a href="http://archives.seul.org/or/talk/Jan-2009/msg00029.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2009/msg00029.html</a></p>

<p><strong>Bug fixes</strong><br />
Security fixes in the Tor 0.2.1.8-alpha release:<br />
  - When the client is choosing entry guards, now it selects at most one guard from a given relay family. Otherwise we could end up with all of our entry points into the network run by the same operator. Suggested by Camilo Viecco. Fix on 0.1.1.11-alpha.<br />
  - The "ClientDNSRejectInternalAddresses" config option wasn't being consistently obeyed: if an exit relay refuses a stream because its exit policy doesn't allow it, we would remember what IP address the relay said the destination address resolves to, even if it's an internal IP address. Bugfix on 0.2.0.7-alpha; patch by rovv.<br />
  - The "User" and "Group" config options did not clear the supplementary group entries for the Tor process. The "User" option is now more robust, and we now set the groups to the specified user's primary group. The "Group" option is now ignored. For more detailed logging on credential switching, set CREDENTIAL_LOG_LEVEL in common/compat.c to LOG_NOTICE or higher. Patch by Jacob Appelbaum and Steven Murdoch. Bugfix on 0.0.2pre14. Fixes bug 848.</p>

<p>Performance scalability fixes from the Tor 0.2.1.9-alpha ChangeLog:<br />
  - Clip the MaxCircuitDirtiness config option to a minimum of 10 seconds. Warn the user if lower values are given in the configuration. Bugfix on 0.1.0.1-rc. Patch by Sebastian.<br />
  - Clip the CircuitBuildTimeout to a minimum of 30 seconds. Warn the user if lower values are given in the configuration. Bugfix on 0.1.1.17-rc. Patch by Sebastian.</p>

<p>Relay stability fixes from the Tor 0.2.1.9-alpha ChangeLog:<br />
  - Fix a logic error that would automatically reject all but the first configured DNS server. Bugfix on 0.2.1.5-alpha. Possible fix for part of bug 813/868. Bug spotted by coderman.<br />
  - When we can't initialize DNS because the network is down, do not automatically stop Tor from starting. Instead, retry failed dns_init() every 10 minutes, and change the exit policy to reject *:* until one succeeds. Fixes bug 691.</p>

<p>Karsten discovered a bug where some directory authorities would take many minutes to send out a network status, because they were rate limiting too low. The short-term fix is to get those authorities to set<br />
  "MaxAdvertisedBandwidth 10 KB"<br />
in their torrc, so they don't spend as much of their bandwidth relaying ordinary Tor traffic.<br />
<a href="https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=847" rel="nofollow">https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=847</a><br />
We need to consider longer-term solutions too, where clients actually recover more gracefully from this situation.</p>

<p><strong>Advocacy</strong><br />
We finally made our 3-year development roadmap public:<br />
<a href="https://blog.torproject.org/blog/our-three-year-development-roadmap-published" rel="nofollow">https://blog.torproject.org/blog/our-three-year-development-roadmap-pub…</a></p>

<p>Jillian York continued blogging for us about the good uses of Tor:<br />
<a href="http://www.knightpulse.org/blog/tor" rel="nofollow">http://www.knightpulse.org/blog/tor</a></p>

<p>"Syria: Using Tor for Censorship Resistance", Dec 1<br />
<a href="http://www.knightpulse.org/blog/08/12/01/syria-using-tor-censorship-resistance" rel="nofollow">http://www.knightpulse.org/blog/08/12/01/syria-using-tor-censorship-res…</a></p>

<p>"Australia Addresses Internet Circumvention", Dec 19<br />
<a href="http://www.knightpulse.org/blog/08/12/19/australia-addresses-internet-circumvention" rel="nofollow">http://www.knightpulse.org/blog/08/12/19/australia-addresses-internet-c…</a></p>

<p>Howcast produced a quick video for the masses on how to circumvent censorship. We were technical consultants for this video. It's tough to talk about Tor, when the first question you're trying to answer is "What is a proxy? And why do I care?" Howcast did a great job for a high-level overview of circumvention technologies in four minutes.<br />
<a href="https://blog.torproject.org/blog/how-circumvent-internet-proxy-howcast" rel="nofollow">https://blog.torproject.org/blog/how-circumvent-internet-proxy-howcast</a></p>

<p>Wendy was a panelist at a conference organized by Paul Ohm and others at Colorado U at the beginning of December on law, wiretapping, and research-oriented data collection: "The Law and Ethics of Network Monitoring":<br />
<a href="http://www.silicon-flatirons.org/events.php?id=544" rel="nofollow">http://www.silicon-flatirons.org/events.php?id=544</a></p>

<p>Roger, Karsten, Sebastian, Steven, Jacob, Mike, Peter, Wendy, Frank, Christian, and others attended the 25C3 conference in Berlin, Dec 27-30.<br />
Roger gave a talk there, similar to the DC08 talk but focusing entirely on 'present' and 'future': "Security and anonymity vulnerabilities in Tor: past, present, and future"<br />
<a href="http://freehaven.net/~arma/slides-25c3.pdf" rel="nofollow">http://freehaven.net/~arma/slides-25c3.pdf</a></p>

<p>There was a workshop after Roger's talk on Germany and data retention.  Sebastian Hahn was really great at representing Tor there, particularly because it was right after Roger's talk so he missed half of it, and because it was mostly in German. Roger tried to add the points that a) he really still does want to do Tor talks for German law enforcement (we got a few leads), and b) the major German Tor relay busts were in 2006-2007, not 2008, and maybe we're finally making progress.</p>

<p>Jacob was among the presenters at 25C3 on a talk about how they had managed to forge a root SSL certificate. In short, this meant that they could pretend to be any https site on the Internet, and no browser would complain. Nick wrote up a response explaining how it works and how it can affect Tor users:<br />
"The MD5 certificate collision attack, and what it means for Tor"<br />
<a href="http://blog.torproject.org/blog/md5-certificate-collision-attack%2C-and-what-it-means-tor" rel="nofollow">http://blog.torproject.org/blog/md5-certificate-collision-attack%2C-and…</a></p>

<p><strong>New features</strong><br />
New feature from the Tor 0.2.1.8-alpha ChangeLog:<br />
  - New DirPortFrontPage option that takes an html file and publishes it as "/" on the DirPort. Now relay operators can provide a disclaimer without needing to set up a separate webserver. There's a sample disclaimer in contrib/tor-exit-notice.html.</p>

<p>We continued work on Thandy (our secure updater) this month.</p>

<p>Thandy itself is working smoothly at this point -- it can contact the central repository, check all the keys, look in the registry and compare the currently installed version to the new choices, fetch the right packages, check all the signatures, and launch the install.</p>

<p>We also now have a branch of Vidalia that has the GUI components for our updater in and working. It launches the updater to check for updates periodically, and there's a "check now" button. It does the update via Tor if Tor is up and running, and via direct connection otherwise.</p>

<p>We had hoped to be able to get away with patching our current .nsi Windows installer, but it turns out that "nsi silent (non-GUI) install" and "Vista" are not compatible concepts: Vista only likes MSI-based silent installs, due to that whole permissions thing that Vista gets so excited about.</p>

<p>So we now have a shiny new wxs-based msi installer for Tor on Windows:<br />
<a href="https://svn.torproject.org/svn/tor/trunk/contrib/tor.wxs.in" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/contrib/tor.wxs.in</a><br />
with buildbot-style output here:<br />
<a href="https://data.peertech.org/torbld" rel="nofollow">https://data.peertech.org/torbld</a></p>

<p>The new installer has been tested for install, upgrade, repair and removal. But that's just Tor, and our recommended download bundle contains four components: Tor, Vidalia (the GUI), Torbutton (our Firefox extension), and either Privoxy or Polipo (an http proxy configured to use Tor -- we're migrating from Privoxy to Polipo).</p>

<p>So, the next step is to work on MSI installer files for the other three, plus a meta-msi file for the bundle. We're aiming to have a first go of that at the beginning of January. That way we can give a simpler demo of "download this bundle, then it will automatically notice that it should upgrade Tor, and it will fetch the new package and upgrade."</p>

<p>In other news, Roger had a long chat with Justin Cappos in early December. Justin did his PhD thesis on security of package managers, and is now a post-doc at UW working on (among other things) auto-update frameworks.  See the beginning of a thread here:<br />
<a href="http://archives.seul.org/or/dev/Dec-2008/msg00010.html" rel="nofollow">http://archives.seul.org/or/dev/Dec-2008/msg00010.html</a></p>

<p><strong>Translations</strong><br />
We have our translation server up and online:<br />
<a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a><br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>We continued enhancements to the Chinese and Russian Tor website translations. Our Farsi translation from this summer is slowly becoming obsolete; we should solve that at some point.</p>

