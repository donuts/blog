title: EFF's Panopticlick and Torbutton
---
pub_date: 2010-01-29
---
author: mikeperry
---
tags:

tor
torbutton
online anonymity
privacy
EFF
panopticlick
---
categories:

applications
network
partners
---
_html_body:

<p>The EFF has recently released a <a href="http://panopticlick.eff.org/" rel="nofollow">browser fingerprinting test suite</a> that they call <a href="http://panopticlick.eff.org/about.php" rel="nofollow">Panopticlick</a>. The idea is that in normal operation, your browser leaks a lot of information about its configuration which <a href="https://www.eff.org/deeplinks/2010/01/tracking-by-user-agent" rel="nofollow">can be used to uniquely fingerprint you</a> independent of your cookies.</p>

<p>Because of how EFF's testing tool functions, it has created some confusion and concern among Tor users, so I wanted to make a few comments to try to clear things up.</p>

<p>First off, Torbutton has defended against <a href="https://www.torproject.org/torbutton/design/#attacks" rel="nofollow">these and other types of attacks</a> since the 1.2.0 series began. We make the User Agent of all Torbutton users uniform, we block all plugins both to prevent proxy bypass conditions and to block subtler forms of plugin tracking, we round screen resolution down to 50 pixel multiples, we set the timezone to GMT, and we clear and disable DOM Storage.</p>

<p>In fact, based on <a href="https://www.torproject.org/torbutton/design/#id2530601" rel="nofollow">my display resolution calculations</a>, we should only be presenting just over 7 bits of information to fingerprint Tor users, and this is only in the form of window size, which for most users either changes from day to day, or is set to a common maximized display size.</p>

<p>Why then does EFF's page tend to tell Tor users that they are unique amongst the hundreds of thousands of users that have been fingerprinted so far? The answer has largely to do with selection bias. The majority of visitors to EFF's site are likely not Tor users. Thus Torbutton's protection mechanisms tend to make Tor users stand out as unique amongst the rest of the web. This is not as bad as it seems. Torbutton's protection mechanisms are only meant to make all Tor users look uniform amongst themselves, not to make them look like the rest of web users. After all, Tor users are already identifiable because they appear from <a href="https://www.torproject.org/tordnsel/" rel="nofollow">easily recognizable Tor exit node IP addresses</a>. </p>

<p>What's more is that these protections are of course not enabled while Tor is disabled. In fact, one of <a href="https://www.torproject.org/torbutton/design/#requirements" rel="nofollow">Torbutton's design requirements</a> is to not provide any evidence that you are a Tor user during normal operation.</p>

<p>I'd like to commend the EFF for bringing these web fingerprinting details to the public eye in a way that I unfortunately was unable to do when I first developed protections for them.</p>

<p>However, I wish that they also included or at least referenced <a href="http://whattheinternetknowsaboutyou.com/" rel="nofollow">url history disclosure information</a> with their tool. After all, if you have history enabled (and you haven't set Torbutton to block history reads during Non-Tor usage), each URL you visit adds <a href="https://www.eff.org/deeplinks/2010/01/primer-information-theory-and-privacy" rel="nofollow">another bit</a> to the set that can be used to fingerprint you. Often bits that are extremely sensitive, such as which diseases and genetic conditions you have based on your <a href="http://en.wikipedia.org/wiki/Category:Diseases_and_disorders" rel="nofollow">Wikipedia</a> or <a href="https://www.google.com/health/" rel="nofollow">Google Health</a> url history. I am convinced that it is only a matter of time before the ad networks begin mining this data to provide targeted ads for over-the-counter and prescription medications and to sell this data to other marketing and insurance firms, if they don't do it already.</p>

---
_comments:

<a id="comment-4199"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4199" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 31, 2010</p>
    </div>
    <a href="#comment-4199">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4199" class="permalink" rel="bookmark">I noticed that Torbutton</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I noticed that Torbutton sets the window size to be equal to the screen size, but this is very unusual. Wouldn't it be better to set the screen size to be a standard screen size, as close to the window size as possible?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4285"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4285" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">February 07, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4199" class="permalink" rel="bookmark">I noticed that Torbutton</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4285">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4285" class="permalink" rel="bookmark">This is likely to leak</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is likely to leak information in the form of the amount of overhead you need. Different platforms and devices will have different needs for decoration overhead in terms of this differential. Better to behave as if this overhead is always 0. My feeling was that webapps really only need to know the total size available to the render window, and should behave as if this is the maximal size available for them to work with anyways. I've always hated websites that try to increase the window size to utilize more of your available desktop, which seems to be the only use case for this information that I can think of. They should be working with the space you have given them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4216"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4216" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2010</p>
    </div>
    <a href="#comment-4216">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4216" class="permalink" rel="bookmark">I wonder whether it would be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wonder whether it would be possible to plug holes opened up by plugins, eg font enumeration via flash?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4286"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4286" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">February 07, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4216" class="permalink" rel="bookmark">I wonder whether it would be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4286">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4286" class="permalink" rel="bookmark">Yes it is possible, but not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes it is possible, but not really feasible because you have to operate with these plugins on a binary level, not on a javascript or even programmable API level. I have written prototype code to instrument flash on Windows to prevent it from making its own socket calls for example. It is even possible to do this relatively cross-platform - ie to abstract the hooking procedures for different binary formats. However, making this stable and not run afoul of antivirus software on various platforms is a daunting task, not to mention working with plugins that operate partially out of process, like adobe's acroread, for example.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15492" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 09, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to mikeperry</p>
    <a href="#comment-15492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15492" class="permalink" rel="bookmark">Would you mind publishing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would you mind publishing this prototype code? I am interest in this subject.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-4220"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4220" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2010</p>
    </div>
    <a href="#comment-4220">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4220" class="permalink" rel="bookmark">WinXp Pro and Firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>WinXp Pro and Firefox 3.6</p>
<p>Downloaded the latest inst. bundle of Tor for windows:<br />
<a href="http://www.torproject.org/easy-download.html.en" rel="nofollow">http://www.torproject.org/easy-download.html.en</a></p>
<p>Polipo.exe causing difficulties to get to internet though Tor and Vidalia seem to work ok and connecting properly.<br />
I unchecked in Torbutton Polipo and found everything seemed to work OK.<br />
Wondering if do not use Polipo at all, is there any disadvantage?<br />
How can I get Polipo working. </p>
<p>In earlier version of Tor there was Privoxy and it was giving no problems.</p>
<p>Sorry if this is not a correct site, but I could not find anything else.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4668"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4668" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4220" class="permalink" rel="bookmark">WinXp Pro and Firefox</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4668">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4668" class="permalink" rel="bookmark">IIRC Firefox used to have a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>IIRC Firefox used to have a DNS leaking problem and an additional proxy took care of that. Polipo is chosen instead of Privoxy for speed, also I think later versions of Privoxy had also some leaking problems.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4241" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 02, 2010</p>
    </div>
    <a href="#comment-4241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4241" class="permalink" rel="bookmark">Panopticlick didn&#039;t include</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Panopticlick didn't include the CSS site history thing because Peter felt that visited URL history changes too frequently (e.g. when users visit new sites, or when old visited sites expire from their history) to be a reliable and stable identifier.  This is different from other browser properties which don't change under normal day-to-day browsing behavior but only when the user makes a configuration change to their computer.</p>
<p>However, another researcher has been looking into how reliably site history can be used with fuzzy matching techniques so there should be some preliminary experiments and data about this soon.  There is obviously some kind of trade-off between the false positive rate and the false negative rate in recognizing users from their fingerprints based on what data sources you integrate into the fingerprint and how you match them.  Using visited site history would probably decrease false positives at the cost of increasing false negatives, but it will be interesting to see data that shows whether it might be worth it.</p>
<p>The Panopticlick site also explains that Panopticlick is not aiming to catalogue every metric that might be used for device or browser fingerprinting, but just to gather some hard data on the distribution of some specific browser features to make their tracking potential more concrete.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4287"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4287" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">February 07, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4241" class="permalink" rel="bookmark">Panopticlick didn&#039;t include</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4287">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4287" class="permalink" rel="bookmark">Yes - interesting. I suppose</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes - interesting. I suppose the EFF was right in that they have plenty of bits to work with as it is..</p>
<p>I've always suspected that the best way to do this is to make a bit vector space and to classify users by their nearest neighbour in the space, but I suppose it is a difficult problem when to create a new user entry vs classifying them as being close to an existing user.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4272"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4272" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 06, 2010</p>
    </div>
    <a href="#comment-4272">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4272" class="permalink" rel="bookmark">Between this and Ccleaner</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Between this and Ccleaner (Piriform) (keeping everthing clean/empty) im not concerned bout the media part lol</p>
</div>
  </div>
</article>
<!-- Comment END -->
