title: February 2009 Progress Report
---
pub_date: 2009-03-10
---
author: phobos
---
tags:

progress report
translation
anonymity advocacy
---
categories:

advocacy
localization
reports
---
_html_body:

<p><strong>New releases, new hires, new funding</strong><br />
On February 8, we released versions 0.2.0.34-stable and 0.2.1.12-alpha.  </p>

<p>Tor 0.2.0.34 features several more security-related fixes. You should upgrade, especially if you run an exit relay (remote crash) or a directory authority (remote infinite loop), or you're on an older (pre-XP) or not-recently-patched Windows (remote exploit).</p>

<p>This release marks end-of-life for Tor 0.1.2.x. Those Tor versions have many known flaws, and nobody should be using them. You should upgrade. If you're using a Linux or BSD and its packages are obsolete, stop using those packages and upgrade anyway.</p>

<p><strong>Enhancements</strong><br />
In Tor 0.2.1.12-alpha, if we're using bridges and our network goes away, be more willing to forgive our bridges and try again when we get an application  request. Bugfix on 0.2.0.x.</p>

<p>Continued to develop research and coding items for improving Tor's performance using a number of techniques.  We're focusing on six main reasons for slow performance:  congestion control, tcp backoff, wrong window sizes at start, lack of priority for circuit control cells, and user load from peer to peer bulk data transfers.</p>

<p>We've implemented KDE Marble as an alternate visualization of the world into Vidalia.  The first phase is to get a better 3-D globe for clients.  The next phase is to enable “click to exit” so users can choose their country of preference for exit nodes.  More on this coming in a future blog post.</p>

<p><strong>Outreach</strong><br />
Andrew and Roger attended an Open Society Institute Forum on, “The Future of Freedom and Control in the Internet Age”, <a href="http://www.soros.org/initiatives/fellowship/events/freedom_20090210" rel="nofollow">http://www.soros.org/initiatives/fellowship/events/freedom_20090210</a>.  Rebecca MacKinnon and Evgeny Morozov both mentioned Tor and its positive uses multiple times during the talk and subsequent Q&amp;A.  </p>

<p>Andrew attended Mobile Activism 4 Change barcamp on February 21.  This generated some citizen media press about security, privacy, and anonymity in reference to the mobile activist world.  You can read more at <a href="http://barcamp.org/MobileTechForSocialChangeNewYork" rel="nofollow">http://barcamp.org/MobileTechForSocialChangeNewYork</a>.  </p>

<p>Jacob attended the InfoActivism camp, <a href="http://www.informationactivism.org/" rel="nofollow">http://www.informationactivism.org/</a>, in Bangalore, India.  He gave 20 presentations, trainings, and lectures on Tor.  </p>

<p>Produced a guide to Tor and circumvention with the Center for Human Rights and Democracy in Saudi Arabia, <a href="http://cdhr.info" rel="nofollow">http://cdhr.info</a>.</p>

<p>Worked with Global Voices (<a href="http://globalvoicesonline.org/" rel="nofollow">http://globalvoicesonline.org/</a>) to update their guide to anonymous blogging with Tor and Wordpress; <a href="http://advocacy.globalvoicesonline.org/projects/guide/" rel="nofollow">http://advocacy.globalvoicesonline.org/projects/guide/</a>.  We recommend the Tor Browser Bundle by default, and provide clearer instructions and more pictures to assist users in getting configured quickly and securely.</p>

<p>There was a talk at BlackHat from Xinwen Fu. Our official response and thoughts on the topic are available at <a href="https://blog.torproject.org/blog/one-cell-enough" rel="nofollow">https://blog.torproject.org/blog/one-cell-enough</a></p>

<p>From Feb 6-9, Roger, Nick, Wendy, and Andrew attended ShmooconV, <a href="http://shmoocon.org/" rel="nofollow">http://shmoocon.org/</a>, in February.  Discussed Tor present and futures with many of the attendees.  </p>

<p>End of Feb, Steven and Roger went to Financial Crypto 2009. We talked more with economics and "economics of information security" professors and researchers to get a better intuition about how to balance usability and load on the network. Steven also did a lightning talk on the "TLS footprint" arms race question: should we wait to fix known flaws, to slow down the arms race, or should we fix everything asap to discourage the censors from even trying?</p>

<p>Feb 17, Roger did a guest lecture on Tor in Drexel's senior-level computer<br />
security class.</p>

<p>In Feb we also met with the Freedom House (<a href="http://www.freedomhouse.org/" rel="nofollow">http://www.freedomhouse.org/</a>), to help them understand how Tor works and to try to help with the trainings they're organizing.</p>

<p>Jillian C. York continued her blogging for Tor at KnightPulse with “From Tunisia to Japan: Anonymity Everywhere”, <a href="http://www.knightpulse.org/blog/09/02/25/tunisia-japan-anonymity-everywhere" rel="nofollow">http://www.knightpulse.org/blog/09/02/25/tunisia-japan-anonymity-everyw…</a></p>

<p><strong>Tor bundles for USB drives or LiveCDs</strong><br />
On February 18, we released Tor Browser Bundle 1.1.9 with an updated Tor version to 0.2.1.12-alpha, Vidalia updated to 0.1.11, and Firefox 3.0.6.  Andrew has taken over building the bundle to reduce the time between tor releases and bundles which include it.  This should make PETER from the blog happy. </p>

<p>Updated the Incognito LiveCD TODO list to provide some more direction and tasks for the near future, <a href="http://archives.seul.org/or/cvs/Feb-2009/msg00056.html" rel="nofollow">http://archives.seul.org/or/cvs/Feb-2009/msg00056.html</a></p>

<p>We continued development and enhancement of TorVM with software updates to libevent, openwrt, vidalia, openvpn, tor, and win pcap.  Enhanced the self-extraction and build scripts for easier creation by less technical users.</p>

<p><strong>Scalability, load balancing, directory overhead, and efficiency</strong><br />
We wrote up a summary of directory overhead work here:<br />
<a href="https://blog.torproject.org/blog/overhead-directory-info%3A-past%2C-present%2C-future" rel="nofollow">https://blog.torproject.org/blog/overhead-directory-info%3A-past%2C-pre…</a></p>

<p>Csaba Kiraly has been doing research on how to reduce the overall load on the Tor network, while also reducing latency for clients: <a href="http://archives.seul.org/or/dev/Feb-2009/msg00000.html" rel="nofollow">http://archives.seul.org/or/dev/Feb-2009/msg00000.html</a></p>

<p><strong>Alternate acquisition methods</strong><br />
Updated our get-tor email auto-responder to include more languages, added in the English version of the tor browser bundle, tested gmail download and resuming interrupted downloads, and fleshed out the design for easier localization of the message text and commands.</p>

<p><strong>Translation</strong><br />
We had a combined 113 commits across Polish, Chinese, Italian, German, Spanish, Russian, Argentinian, Brazilian Portuguese, and Romanian languages.  41 of these commits were through our translation portal.</p>

---
_comments:

<a id="comment-785"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-785" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2009</p>
    </div>
    <a href="#comment-785">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-785" class="permalink" rel="bookmark">Wordle of this post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I made one for the heck of it, <a href="http://www.wordle.net/gallery/wrdl/633076/Tor" rel="nofollow">http://www.wordle.net/gallery/wrdl/633076/Tor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">March 11, 2009</p>
    </div>
    <a href="#comment-787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-787" class="permalink" rel="bookmark">thanx phobos for the TBB</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanx phobos for the TBB updates</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-793" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-787" class="permalink" rel="bookmark">thanx phobos for the TBB</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-793" class="permalink" rel="bookmark">enjoy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you're welcome!  Thanks for keeping on us about updating TBBs.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-788"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-788" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2009</p>
    </div>
    <a href="#comment-788">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-788" class="permalink" rel="bookmark">Can someone tell me why Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can someone tell me why Tor doesn't work for me anymore? I don't know where to go for support. The hidden services stopped working, and then, thinking it might solve the problem, upgraded Tor to the newer version and then Tor stopped working entirely.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-791" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 12, 2009</p>
    </div>
    <a href="#comment-791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-791" class="permalink" rel="bookmark">finding support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/faq#SupportMail" rel="nofollow">https://www.torproject.org/faq#SupportMail</a></p>
<p>It's also clear we need a better way to provide support than blog comments and email.</p>
</div>
  </div>
</article>
<!-- Comment END -->
