title: February 2010 Progress Report
---
pub_date: 2010-03-24
---
author: phobos
---
tags:

progress report
advocacy
performance enhancements
---
categories:

advocacy
reports
---
_html_body:

<p><strong>New releases</strong></p>

<ul>
<li>On February 13, we released a new stable version of Tor, 0.2.1.23. Tor 0.2.1.23 fixes a huge client-side performance bug, makes Tor work again on the latest OS X, and updates the location of a directory authority.</li>
<li>On February 21st, we released an update Tor stable in 0.2.1.24. Tor 0.2.1.24 makes Tor work again on the latest OS X – this time for sure!</li>
<li>On February 22, we released the latest in the -alpha series, 0.2.2.9-alpha.</li>
<li>On February 15th, we released an updated Tor Browser Bundle; version 1.3.2.</li>
<li>On February 27th, we released an updated Tor Browser Bundle, version 1.3.3.</li>
<li>On February 18th, Tor for the Nokia Maemo mobile platform was announced. <a href="https://blog.torproject.org/blog/tor-nokia-n900-maemo-gsm-telephone" rel="nofollow">https://blog.torproject.org/blog/tor-nokia-n900-maemo-gsm-telephone</a>.</li>
<li>On February 7th, volunteers released a new beta of the Amnesia LiveCD, version 0.4.2. Amnesia is the merging of two projects, one of which is the Incognito LiveCD.</li>
</ul>

<p><strong>Design, develop, and implement enhancements that make<br />
Tor a better tool for users in censored countries.</strong></p>

<p>Work continues to improve the Tor ports for Android, Maemo, and iPhone.</p>

<p>We worked with Ian Goldberg at University of Waterloo to come up with a plan for one of his grad students to continue working on “Nymbler”, which is a framework they’re working on that will allow Tor users to remain anonymous yet prove to websites like Wikipedia and Slashdot that they are not jerks (or at least, not yet known to be jerks). This anonymous authentication approach will hopefully be a step toward letting Tor users post to Wikipedia again; but it is still in its very early stages.</p>

<p>Along these same lines, the Freenode IRC channel has been experimenting with a new way to allow Tor users to interact in their chat rooms while still being able to contain the abuse potential: <a href="http://blog.freenode.net/2010/01/connecting-to-freenode-using-tor-sasl/" rel="nofollow">http://blog.freenode.net/2010/01/connecting-to-freenode-using-tor-sasl/</a>.</p>

<p><strong>Architecture and technical design docs for Tor enhancements related to blocking-resistance.</strong></p>

<p>Roger, Karsten, Steven met with Paul Syverson and Aaron Johnson at UT Austin to continue basic research on designs to let Tor users take advantage of local knowledge of how safe various Tor relays are in order to build safer paths through the network. The first goal is to answer questions like “If I believe that these relays are monitored by the Chinese government, then avoiding them will improve my security, but avoiding them could also stand out because I behave differently than other Tor users; what’s the right balance?” The second goal is to figure out how path selection should work when the user runs one of the relays herself, and thus knows it’s more trusted. The third goal is to come up with intuitive interfaces for letting users specify which parts of the network they trust more, while at the same time explaining the security implications of each choice.</p>

<p>Roger and Karsten also met with Vitaly Shmatikov to learn more about his recent work on “differential privacy”, which is an academic approach to making sure that numbers in databases do not leak too much identifying information. This question needs more attention because of the way Tor is computing and publishing “sanitized” user statistics on its metrics portal.</p>

<p>   Roger also finished the first draft of his “Ten things to look for in tools that circumvent Internet censorship” document that we hope will eventually be a useful primer for policy people getting involved in this space: <a href="https://svn.torproject.org/svn/projects/articles/circumvention-features.html" rel="nofollow">https://svn.torproject.org/svn/projects/articles/circumvention-features…</a></p>

<p><strong>Grow the Tor network and user base. Outreach.</strong></p>

<ul>
<li>Roger spoke to the Philadelphia Linux Users’ Group about Tor and censorship. Several of the members are now looking at volunteering on Tor development. Roger also did a talk on Tor for undergraduates in Drexel University’s security class.</li>
<li>Andrew joined EDRI, <a href="http://www.edri.org" rel="nofollow">http://www.edri.org</a>, in a discussion with Members of European Parliament. and their staff along with senior staff from the European Commission Directorate-General - Justice, Freedom, Security about censorship, data retention, and online privacy.</li>
<li>Andrew gave a Tor talk to around 500 people at FOSDEM, <a href="http://www.fosdem.org" rel="nofollow">http://www.fosdem.org</a>.</li>
<li>Andrew, Roger, and Karen met with Access, <a href="http://accessnow.org" rel="nofollow">http://accessnow.org</a>, to discuss a bridges4tor campaign to increase the number of Tor Bridges, <a href="https://www.torproject.org/bridges" rel="nofollow">https://www.torproject.org/bridges</a>, for users in censored countries.</li>
<li>Steven spoke to around 80 people at Secure Application Development 2010, <a href="http://secappdev.org/" rel="nofollow">http://secappdev.org/</a>, in Groot Begijnhof, Leuven, Belgium.</li>
<li>We worked with Susan Landau to help her better understand Tor in the context of freedom, privacy, and circumvention tools, so that her upcoming book on the subject can be more accurate.</li>
<li>We worked with Dave Dittrich and other researchers in the botnet community to investigate a set of suspicious Tor relays that appeared to be associated with a bot network the researchers were tracking. We eventually decided to cut these suspicious relays out of the Tor network.</li>
<li>We talked a little bit with the fellow writing a circumvention tool called Puff. On the one hand, it looks like yet another centralized proxy where the operator could screw the users but promises not to. On the other hand, he seems technically savvy and he seems to really care about doing the right thing. We should talk with him more to help him improve the safety that his service can provide.</li>
</ul>

<p><strong>Bridge relay and bridge authority work.</strong><br />
We’re currently researching how to turn every tor client into a bridge by default, if the client finds itself reachable externally. This will dramatically increase the available bridges. There are some new attacks and challenges to overcome before this can be deployed as part of a -stable release, but we expect by Q3 2010 to have this into -alpha releases.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong><br />
From the 0.2.2.9-alpha changelog:</p>

<blockquote><p>We were selecting our guards uniformly at random, and then weighting which of our guards we’d use uniformly at random. This imbalance meant that Tor clients were severely limited on throughput (and probably latency too) by the first hop in their circuit. Now we select guards weighted by currently advertised bandwidth. We also automatically discard guards picked using the old algorithm. Fixes bug 1217; bugfix on 0.2.1.3-alpha. Found by Mike Perry.</p></blockquote>

<p><strong>More reliable (e.g. split) download mechanism.</strong><br />
Enhanced the metrics portal with numbers from the get-tor email autoresponder, <a href="http://metrics.torproject.org/gettor-graphs.html" rel="nofollow">http://metrics.torproject.org/gettor-graphs.html</a>.</p>

<p><strong>Footprints from Tor Browser Bundle.</strong><br />
We’ve picked up the work towards a Tor Browser Bundle for OS X and Linux, and hope to have experimental bundles for at least one of those platforms ready in March. Soon after they’re ready for testing, we’ll want to start looking at how footprints from running the bundle differ on each platform.</p>

<p><strong>Translation work, ultimately a browser-based approach.</strong><br />
Translated content updates for Torbutton, Tor Browser, Website, General Documentation, Vidalia interface, and TorCheck in Chinese, German, French, Italian, Dutch, Norwegian, Polish, Russian, Arabic, Farsi, Burmese, Spanish, Swedish, and Turkish.</p>

---
_comments:

<a id="comment-4756"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4756" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2010</p>
    </div>
    <a href="#comment-4756">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4756" class="permalink" rel="bookmark">Good news. I can&#039;t wait to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good news. I can't wait to try the new bridge-default client :P</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2010</p>
    </div>
    <a href="#comment-4757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4757" class="permalink" rel="bookmark">Good news, I can&#039;t wait to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good news, I can't wait to try the new bridge-default client :P</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4771"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4771" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2010</p>
    </div>
    <a href="#comment-4771">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4771" class="permalink" rel="bookmark">Hi!!!!!
I made a Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!!!</p>
<p>I made a Tor Browser bundle for Linux!!!<br />
<strong><a href="http://honeybeenet.altervista.org/factorbee/" rel="nofollow">http://honeybeenet.altervista.org/factorbee/</a></strong><br />
I'll update it also within this week or on the next one, to add new features; to improve it a lot!!!!!!!</p>
<p>~bee!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4810"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4810" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4771" class="permalink" rel="bookmark">Hi!!!!!
I made a Tor Browser</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4810">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4810" class="permalink" rel="bookmark">Perhaps you&#039;ve missed that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Perhaps you've missed that Tor is working on the same thing for the six months past?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4821"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4821" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4810" class="permalink" rel="bookmark">Perhaps you&#039;ve missed that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4821">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4821" class="permalink" rel="bookmark">Hi!!!
Well, my one is much</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!<br />
Well, my one is much better than theirs!!!! The official Tor Browser Bundle for Linux sucks!!!!! they just did a copy of the one they've made for Windows, without adding nothing of new!!!!!!!!!! They worked on it for 6 months to make a poor product!!!! I worked on it for only one month and i did something of much better!!!!!!!!!!! It's your turn to think what's the better `Browser Bundle', i think my one is the greatest!!!!!!!! if you try it, you'll understand why!!!!!!! ~bee!!!!!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4825" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4821" class="permalink" rel="bookmark">Hi!!!
Well, my one is much</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4825" class="permalink" rel="bookmark">It&#039;s more than &quot;just a copy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's more than "just a copy of the one they've made for Windows", see <a href="http://archives.seul.org/or/talk/Mar-2010/msg00212.html" rel="nofollow">http://archives.seul.org/or/talk/Mar-2010/msg00212.html</a> and <a href="http://archives.seul.org/or/talk/Mar-2010/msg00214.html" rel="nofollow">http://archives.seul.org/or/talk/Mar-2010/msg00214.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-4793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4793" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2010</p>
    </div>
    <a href="#comment-4793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4793" class="permalink" rel="bookmark">Hi!!!!! I would be careful</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!!! I would be careful about using security software from just anywhere!!!!!! Because it could grab my credit card details and send them to someone in China!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4793" class="permalink" rel="bookmark">Hi!!!!! I would be careful</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4804" class="permalink" rel="bookmark">Hi!!!!!!!!!! your post is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!!!!!!!! your post is right after my one and this is why i'm thinking that you were addressing it to me!!!!!<br />
Well, what you said is very right and i agree with it!!!!!! This is why i believe that you aren't one (<em>hypocritical</em>), WINDOWS or MAC OSX, user!!!!!!!!! Indeed you are a GNU/LINUX user, and you can try FactorBee too!!!!!! Factorbee is safe because <strong>it's open source!!!!!!!</strong> Look at the source-code and compile/build it yourself!!!!! along with the source-code, in the same bzip2 package, you'll find the build instructions!!!! bye!!!!! ~bee!!!!!!!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4801"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4801" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
    <a href="#comment-4801">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4801" class="permalink" rel="bookmark">The proxy server is refusing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The proxy server is refusing connections</p>
<p>Firefox is configured to use a proxy server that is refusing connections.</p>
<p>    *   Check the proxy settings to make sure that they are correct.</p>
<p>    *   Contact your network administrator to make sure the proxy server is<br />
          working.</p>
<p>The above is what I get when using this.<br />
Any help?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
    <a href="#comment-4802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4802" class="permalink" rel="bookmark">The proxy server is refusing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The proxy server is refusing connections</p>
<p>Firefox is configured to use a proxy server that is refusing connections.</p>
<p>    *   Check the proxy settings to make sure that they are correct.</p>
<p>    *   Contact your network administrator to make sure the proxy server is<br />
          working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
    <a href="#comment-4805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4805" class="permalink" rel="bookmark">http://xqz3u5drneuzhaeo.onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://xqz3u5drneuzhaeo.onion/users/mytorideaz/" rel="nofollow">http://xqz3u5drneuzhaeo.onion/users/mytorideaz/</a></p>
<p>SUGGESTION: ALERT BRIDGE OPERATORS WHEN THEY ARE GETTING BLOCKED<br />
The current bridge strategy focuses on the TOR-user communication of bridges, and is basically a rationing strategy on user's ability to find out bridges. It works but there's an inbuilt limit on its effectiveness: the adversary typically provides the end user's connection, so can always in principle find out bridges by monitoring traffic.</p>
<p>It would be nice to also try to address this at the bridge provider end. Specifically, can you warn a bridge provider that their bridge is likely being blocked in some locations?</p>
<p>You could add a notification mechanism so when there is evidence a bridge is being systematically blocked in any location, the bridge operator gets a popup warning. A significant fraction of bridge operators will be using ADSL connections where to get a new IP address all they need do is switch the modem off and on again, making the bridge useful again.</p>
<p>Two possibilities here:<br />
*The bridge operator's TOR installation itself monitors the number of incoming connections, and when it notices a significant drop from a particular location, pops up an alert.<br />
*A centralized service does the monitoring and informs the bridge operator's TOR installation, which alerts the user.</p>
<p>Is it possible? It would be a useful feature as it would mean that at least some bridges are very very hard to block, and would reduce the fraction of bridges that are dead at any one time. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4806" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4805" class="permalink" rel="bookmark">http://xqz3u5drneuzhaeo.onion</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4806" class="permalink" rel="bookmark">I was going to post a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was going to post a similar suggestion  recently when the new blocks arrived but thought I'd wait and see if the Tor team move towards making accessible Tor client users active as bridges by default.  I think this might be the better way really. .......................................................................................................................<br />
I agree with the problem though.  Running a bridge here I only became aware of the blocks after implementing a particular system that monitors for particular conditions that arrive when a block occurs that I threw together after China initiated the late December blocks.  I don't imagine many people would do this though so gather there could be a significant problem with bridge ops not ever becoming aware of restricted access to the service short of them noticing a bandwidth usage drop or something.<br />
.......................................................................................................................<br />
I'm hoping the Tor-users-default-to-bridge ideas pan out.  I see a fair amount more activity of the bridge here recently so I'm guessing there might be a shortage of bridge capacity out there at the moment.  Many more bridges sounds like a good answer to me.<br />
.......................................................................................................................<br />
Also, captcha is really hard... 1+1=???????????</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4826" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4805" class="permalink" rel="bookmark">http://xqz3u5drneuzhaeo.onion</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4826" class="permalink" rel="bookmark">Something like this is on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Something like this is on the todo list. The idea is the bridge authorities will notice when a bridge is blocked in country X and stop giving it out to users of that country.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4851" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-4851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4851" class="permalink" rel="bookmark">Hi Phobos,  actually I don&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Phobos,  actually I don't think this is the same as what the other commenter and myself are suggesting. We're suggesting that bridge operators are informed asap when they're being blocked, so they can take action and change their IP (if they're able to, which many operators will be), thus bringing their bridge back into play.  Your idea, although useful, will do nothing to stop a bridge operator continuing to run their bridge on a blocked IP address, possibly for weeks or months. The operator will not realize that the bridge is not making a useful contribution to the Tor community, which is a pity. The two ideas are complementary and it would be nice to see both implemented. *** <a href="http://xqz3u5drneuzhaeo.onion/users/mytorideaz/" rel="nofollow">http://xqz3u5drneuzhaeo.onion/users/mytorideaz/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4859"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4859" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 28, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4851" class="permalink" rel="bookmark">Hi Phobos,  actually I don&#039;t</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4859">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4859" class="permalink" rel="bookmark">Ok.  Just because a bridge</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ok.  Just because a bridge is blocked by China, doesn't mean it is useless the world over.  People in other countries will greatly appreciate that a bridge is stable and exists over a long period of time.</p>
<p>I understand your point and wish and it needs some thought, design, and coding.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4881"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4881" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-4881">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4881" class="permalink" rel="bookmark">Not useless, no, but my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not useless, no, but my experience has shown China peers to be by far the most prolific users of a bridge, maybe even up around 95% or more.  If as has happened now you find many bridges blocked, say 50%, then the remaining 50% would have to take the load of the ~95%.  Better than nothing, and certainly better not to waste the China user's time with blocked bridge addresses, but not a great solution on it's own I don't think.  I think it would be more ideal if bridge operators could be made aware of the situation in some way to allow for them to correct the problem short of there being a better solution (like many more bridges in general). *(I'm assuming here that bridge bandwidth capacity is actually an issue)*</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-4841"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4841" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
    <a href="#comment-4841">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4841" class="permalink" rel="bookmark">De FOSDEM praten was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>De FOSDEM praten was geweldig. Andrew behandeld hecklers het vrij goed aan het eind. Ik waardeer de eerlijkheid van het Tor mensen als geheel. Ze lijken echt geloven in wat ze doen, ik hoop dat dit hen niet in de problemen komen.</p>
<p>Andrew bracht 10 minuten uitleg over de meest elementaire begrippen van Tor mij na het gesprek. Tannenbaum blies me met een antwoord op een woord en een blik van "jij idioot, niet mijn tijd verspillen." Echt geweldig om mensen die de zorg over gebruikers in de wereld, maakt niet uit hoe beroemd.</p>
<p>-De vraag meisje van FOSDEM</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4897"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4897" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 30, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4841" class="permalink" rel="bookmark">De FOSDEM praten was</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4897">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4897" class="permalink" rel="bookmark">If google translate is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If google translate is accurate, I believe the correct answer is, you're welcome.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4845"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4845" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
    <a href="#comment-4845">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4845" class="permalink" rel="bookmark">Murphy&#039;s law saved of all</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Murphy's law saved of all torizens.</p>
<p>Murphy's law was confirmed: the compensation of bugs of each other with an even numbers of those was happened. In conjunction of Tor and openssl, bug CVE-2010-0740 was compensated with lack of a proper mechanism of session's closure, SSL_shutdown() has not been used by Tor in practice at all.</p>
<p>Long live the score on the lack of specs and docs. SbO.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4923"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4923" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 03, 2010</p>
    </div>
    <a href="#comment-4923">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4923" class="permalink" rel="bookmark">Great work, like always,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great work, like always, thanks for all</p>
<p>SwissTorExit</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5457"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5457" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 22, 2010</p>
    </div>
    <a href="#comment-5457">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5457" class="permalink" rel="bookmark">my tor is not connecting its</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>my tor is not connecting its stopping at establishing network circuit.....i use MTN nigeria pls help hope MTN is not blocking <a href="mailto:me....kadex4real2002@yahoo.com" rel="nofollow">me....kadex4real2002@yahoo.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5697"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5697" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2010</p>
    </div>
    <a href="#comment-5697">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5697" class="permalink" rel="bookmark">Thanks a lot. It works for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks a lot. It works for me. I'm using it in Nigeria and it is damn fast. Hope you will send me the updates of the software at <a href="mailto:emtexonline@yahoo.com" rel="nofollow">emtexonline@yahoo.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-6150"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6150" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 14, 2010</p>
    </div>
    <a href="#comment-6150">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6150" class="permalink" rel="bookmark">Mine is working well.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mine is working well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
