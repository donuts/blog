title: five minutes to speak
---
pub_date: 2011-02-13
---
author: phobos
---
tags:

censorship circumvention
policy makers
policy
technology
cyberutopian
peer to peer
---
categories: circumvention
---
_html_body:

<p>I was asked to give a five minute speech to open a panel in front of a number of policy makers and advisors in Washington, DC in the past few weeks.  The discussion was under <a href="https://secure.wikimedia.org/wikipedia/en/wiki/Chatham_House_Rule" rel="nofollow">Chatham House Rule</a>.  A number of people have encouraged me to publish the speech notes as a blog post, it is as follows.</p>

<blockquote><p>Here I am, a technologist in a room full of policy people.  I'll stick<br />
to what I know and try not to put anyone to sleep in the next five<br />
minutes.</p>
<p>Technology is agnostic, who uses and how they use it matters.  Roads,<br />
cars, phones, email, websites are all technologies used for good and bad.<br />
In the 1930s, the feds and police warned of mass chaos if the interstate<br />
highway system was built in the US.  The ability for criminals to quickly<br />
transit between cities was of grave concern. Crime would spread faster,<br />
further, and this would hasten the breakdown of the very fabric of the<br />
American society, community. Time has shown the benefits vastly outweighed<br />
the costs.  This same principle has shown to be true of the internet<br />
and computer technology.  Sure, we may have new kinds of crime with botnets,<br />
zombies, phishing, but do we really?  Lying, impersonation, and tricking<br />
someone into doing your work are the same crimes they have been for the<br />
past few millenia.  It's just that the substrate that is used, has changed.<br />
What are some of the largest companies in the world?  GE, IBM, Apple,<br />
Microsoft, Google.  What one should or should not do is policy and law,<br />
what one can actually do or not do is technology.</p>
<p>Circumvention, anonymity, and privacy tools used in a free world can be<br />
a minor annoyance, i.e. wikileaks used wikis, ssl, email, and yes, tor,<br />
but in the end, it's an annoyance.  We don't have people in the streets<br />
rioting trying to overthrow our govt. Wikipedia uses the same technology<br />
in wikis, ssl, and email. Everyone loves Wikipedia and considers it a net positive.</p>
<p>The same circumvention, anonymity, and privacy tools are deadly to<br />
repressive regimes.  The free flow of information and education are of<br />
great concern to a regime trying to control the horizontal and vertical<br />
of every day life. The tactics a regime can use are legal, technical,<br />
and physical.  The regime can switch between tactics, generally<br />
depending upon what's economical and most effective.</p>
<p>Roughly 1 billion people are online in some way.  Berkman did a study<br />
that found roughly 2% of that billion know what a proxy is, or even that<br />
technology exists to circumvent internet censorship.  98% of the world<br />
accepts that facebook, google, cnn, and the bbc, are blocked and doesn't<br />
try to find ways around it. This doesn't even broach the topic of online<br />
privacy relating to commercial entities nor law enforcement and<br />
intelligence agencies trying to learn the who, what, where, and how of your Internet activities.</p>
<p>Arguing about which proxy technology should get all of the funding and<br />
attention is silly.  The budgets and adversaries vastly outweigh the<br />
funding and research into proxies.  It's not a zero-sum game, and<br />
the different technologies take very different approaches to success;<br />
freegate/ultrasurf, vpns, psiphon, and web proxies play a game of cat<br />
and mouse with ip addresses and sometimes encryption; tor uses the<br />
strategy of R&amp;D and protecting ones anonymity and privacy first, the<br />
secondary effects of this are well-suited to circumvention too.  Tor,<br />
freegate, psiphon, and vpns sum up to roughly $50m in funding from the US govt<br />
of the past few years.  Only a very small fraction of that total has made it to actual technology.  Compare that to the billions spent on snakeoil<br />
black box technology by the DoD and intelligence agencies preparing for<br />
a cyberwar arms race, much like the nuclear arms race, to deter other<br />
nations from attacking us.</p>
<p>I talked to a member of a terrorist organization in Vietnam.  He's been<br />
stalked, harassed, and had everything confiscated multiple times by his<br />
government.  You know his organization as Deutsche Welle.  He's a<br />
reporter.  He had no idea how his plans, documents, and contacts were<br />
being discovered and used against him. His ability to understand the<br />
differences between Tor, JAP, and Freegate was like asking which tires<br />
are best for gravel, snow, or tarmac.  The question he didn't even know<br />
to ask is, "What are safe and secure computing and online practices?"<br />
to use my analogy, "what car do I want for those tires?  the answer is<br />
a rally car."   I spent 4 hours going over how the internet works,<br />
how to think about adversaries online, what is ssl, what it means, what<br />
are phishing, viruses, botnets, and state-sponsored malware.  By the<br />
end of the 4th hour, he understood how tor is different than a simple<br />
vpn or proxy server, and when to use tor and when it isn't needed. 3.5h<br />
of that discussion was basic operational, computer, and online security<br />
and safe practices.</p>
<p>So where does this leave us?  It leaves us with a mix of education,<br />
technology, and many, many unanswered questions.  This is a young field<br />
overall.  As the censorship providers and technologies get better, so<br />
will those circumvention technologies.  Educating users about internet<br />
safety, risks, and making the tools vastly easier and safer to use<br />
should be a goal.</p>
<p>Tor published a "10 things to think about circumvention tools" paper to<br />
try to distill what we've learned over the past 10 years of doing this.<br />
In a few of these areas, tor is not the best choice, for now.</p>
<p>What about technology?  Isn't it going to save us all?  Currently,<br />
freegate/ultrasurf, vpns, and web proxies are looking for money to fund<br />
their growing infrastructure costs. The more users you have, the more<br />
servers, more bandwidth, and more costs you incur.  Its a quick way to<br />
spend lots of money and get lots of users.  However becoming the ISP for<br />
the world gets very expensive, very quickly as you scale up to hundreds<br />
of millions of users.  Look at the infrastructures of google, facebook,<br />
yahoo, and microsoft to see the challenges that lie ahead for these tools.</p>
<p>Tor and "distributed tools" look to improve the research and development<br />
and rely on the scaling of users to both provide the circumvention and<br />
grow to become a self-sustaining ISP to the world.  We have only begun<br />
to see the power and effects of these technologies with bittorrent, jap,<br />
skype, freenet, i2p, and tor.</p></blockquote>

---
_comments:

<a id="comment-9248"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9248" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2011</p>
    </div>
    <a href="#comment-9248">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9248" class="permalink" rel="bookmark">&quot;&quot;&quot;98% of the world
accepts</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"""98% of the world<br />
accepts that facebook, google, cnn, and the bbc, are blocked and doesn't<br />
try to find ways around it."""</p>
<p>I don't blame them !!!</p>
<p>:-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9280"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9280" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
    <a href="#comment-9280">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9280" class="permalink" rel="bookmark">Why didn&#039;t you talk about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why didn't you talk about how the vast majority of Tor users outside of.China only use it.to.distribute videos of little children being raped and other child porn materials?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9287"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9287" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9280" class="permalink" rel="bookmark">Why didn&#039;t you talk about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9287">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9287" class="permalink" rel="bookmark">Please provide data to back</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please provide data to back up this statement.  The vast majority of Tor users outside China, and inside China, use Tor to do boring, normal stuff online.  From speaking to law enforcement officers around the world, crimes through Tor pale in comparison to the general population and to botnets.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9288"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9288" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9280" class="permalink" rel="bookmark">Why didn&#039;t you talk about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9288">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9288" class="permalink" rel="bookmark">It takes one to know one,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It takes one to know one, pervert.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9284"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9284" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
    <a href="#comment-9284">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9284" class="permalink" rel="bookmark">The free flow of information</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><cite>The free flow of information and education are of<br />
great concern to a regime trying to control the horizontal and vertical<br />
of every day life.</cite></p>
<p>Meanwhile, the EU is trying again to implement mandatory internet censorship.  For the children, of course.  At least for now :-/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9286"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9286" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9284" class="permalink" rel="bookmark">The free flow of information</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9286">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9286" class="permalink" rel="bookmark">And it seems the EU is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And it seems the EU is succeeding in their attempts to censor the Internet, as is the US as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9289"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9289" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
    <a href="#comment-9289">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9289" class="permalink" rel="bookmark">Next time ur in dc, look me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Next time ur in dc, look me up, i'd like to donate money and panties to you.</p>
<p>-bravia</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9292"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9292" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2011</p>
    </div>
    <a href="#comment-9292">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9292" class="permalink" rel="bookmark">Have you seen this report? </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have you seen this report?  <a href="http://lugar.senate.gov/record.cfm?id=331192&amp;" rel="nofollow">http://lugar.senate.gov/record.cfm?id=331192&amp;</a></p>
<p>They want to support freegate and ultrasurf only.</p>
<p>Also, tor got 1.6million over three years, while psiphon got 2.9 million.  even freegate/ultrasurf got 1.2million in a year.  you need to sell more snakeoil to the govt.</p>
<p>also, hire some lobbyists to fight for tor, you are losing the battle in the US government to idiots with more money less morals.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9317"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9317" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9292" class="permalink" rel="bookmark">Have you seen this report? </a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9317">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9317" class="permalink" rel="bookmark">Yes, at least I&#039;ve read that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, at least I've read that report.  I learned World Expo's are still popular in many places and that they make the assumption that becoming the ISP for the world is a worthwhile goal.  The last two paragraphs of my speech attempted to address this as an incomplete solution.  It's a short term solution to give money to places like AT&amp;T, Level 3, and Verizon, you're just doing it through the circumvention provider.  As soon as the money stop flowing, people cannot use the tool anymore.</p>
<p>Distributed, or peer to peer-like tools, need funding for research and development to make their tools better.  This is a longer return on investment, but it has a huge potential to be self-sustaining and vastly larger than any one infrastructure and ISP.  Look at the economics of Skype and Bittorrent networks as compared to centralized models. Vastly cheaper overall, and the capacity for a more distributed and diverse userbase globally.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9312" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2011</p>
    </div>
    <a href="#comment-9312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9312" class="permalink" rel="bookmark">Can you email a copy of Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you email a copy of Tor software? Hotmail is now allowing a maximum attachment size of 25MB, and my Tor bundle is less than that, I think.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9316"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9316" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9312" class="permalink" rel="bookmark">Can you email a copy of Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9316">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9316" class="permalink" rel="bookmark">Yes,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, <a href="http://www.torproject.org/projects/gettor.html.en" rel="nofollow">http://www.torproject.org/projects/gettor.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9314"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9314" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2011</p>
    </div>
    <a href="#comment-9314">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9314" class="permalink" rel="bookmark">read</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>read this,<br />
<a href="http://arstechnica.com/tech-policy/news/2011/02/black-ops-how-hbgary-wrote-backdoors-and-rootkits-for-the-government.ars/" rel="nofollow">http://arstechnica.com/tech-policy/news/2011/02/black-ops-how-hbgary-wr…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9344"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9344" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2011</p>
    </div>
    <a href="#comment-9344">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9344" class="permalink" rel="bookmark">Thank you very much for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you very much for posting this. Out of curiosity, could you provide some citations to support this:  </p>
<p>"Tor, freegate, psiphon, and vpns sum up to roughly $50m in funding from the US govt<br />
of the past few years."</p>
<p>I'm researching anonymity tools and I see that Tor's most recent funding report is from 2008.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9368" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 24, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9344" class="permalink" rel="bookmark">Thank you very much for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9368" class="permalink" rel="bookmark">You need to finish that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You need to finish that sentence for it to make sense.  The USG has provided up to $50m for censorship circumvention technology via grants and contracts.  However, roughly 2% of that has gone to actual technology over the years.  </p>
<p>All of Tor's financial reports are at <a href="https://www.torproject.org/about/financials" rel="nofollow">https://www.torproject.org/about/financials</a>.  We publish them as filed with the IRS and Dept of Commerce.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9373"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9373" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 24, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-9373">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9373" class="permalink" rel="bookmark">to answer the direct</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>to answer the direct question, the $50m total comes from the State Dept's own RFPs:</p>
<p>in 2008, $15m for circumvention technology, training, education, distribution</p>
<p>in 2010, $5m for mobile circumvention tech, training, education, distribution</p>
<p>in 2011, $30m for circumvention tech, training, education, distribution and evaluations.  See <a href="http://www.state.gov/g/drl/p/127829.htm" rel="nofollow">http://www.state.gov/g/drl/p/127829.htm</a></p>
<p>And from <a href="http://www.newsweek.com/2010/01/25/up-against-tehran-s-firewall.html" rel="nofollow">http://www.newsweek.com/2010/01/25/up-against-tehran-s-firewall.html</a>, see<br />
<cite>Tehran's worries don't get much sympathy in Washington. In the past three years, Congress has budgeted no less than $50 million for the State Department to sponsor programs that provide unmonitored and uncensored access to the Internet for users living in closed societies. </cite></p>
<p>And you can dig through grants.gov to find the State DRL programs around Internet Freedom to find the individual grants.  I can't find the older State grants on their website.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-9403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2011</p>
    </div>
    <a href="#comment-9403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9403" class="permalink" rel="bookmark">i try to play a video but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i try to play a video but it's say </p>
<p>'You need the latest version of Adobe Flash Player to view this video. Click here to download.<br />
You see this message either because your Flash Player is outdated or because you disabled JavaScript in your browser'</p>
<p>I do have the latest version of Adobe Flash Player and java script is enable</p>
<p>and when I turn Tor off, all the videos is working</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9541" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2011</p>
    </div>
    <a href="#comment-9541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9541" class="permalink" rel="bookmark">hello, its AKASH MAHAJAN</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello, its AKASH MAHAJAN from India.  whenever there is any problem in displaying the web page the browser reads standard india time by... sir i cant understand that how it can be detected that i am from India.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9541" class="permalink" rel="bookmark">hello, its AKASH MAHAJAN</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13628" class="permalink" rel="bookmark">Hey Akash,
Tor utilizes in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey Akash,</p>
<p>Tor utilizes in my experience, tor checks the system time, see the message log if you have maintain a Indian time zone and manipulated the clock it wouldn't connect.</p>
<p>Change your time zone from control panel</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
