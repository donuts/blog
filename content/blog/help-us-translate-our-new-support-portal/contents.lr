title: Help Us Translate Our New Support Portal
---
pub_date: 2018-07-31
---
author: ggus
---
tags: translations
---
categories: localization
---
summary:

We are in the final stages of finalizing our new support portal, just one portal of many new sites to come in our website redesign. In order to finish it, we need to translate it from English into more languages. At the very least, we want to translate it to Farsi, Russian, Simplified Chinese, Portuguese, German, Korean, Turkish, Italian, Arabic, French, and Spanish. If you are knowledgeable about another language not listed here, we'd be grateful for your help as well. 
---
_html_body:

<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid1327"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">Over <a href="https://metrics.torproject.org/userstats-relay-country.html">2 million people around the world</a> rely on Tor for private access to the open web everyday, and we want to make sure that Tor and our resources are localized for as many of our users as possible. That's why we need your help.</span></div>
<div class="ace-line" id="magicdomid944"> </div>
<div class="ace-line" id="magicdomid1485"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">We are in the final stages of finalizing our new <a href="https://support.torproject.org">support portal</a>, just one portal of many new sites to come in our website redesign. </span></div>
<div class="ace-line"> </div>
<div class="ace-line"><img alt="Tor support website screenshots in different languages: english, french and spanish." height="397" src="/static/images/blog/inline-images/Artboard.png" width="794" /></div>
<div class="ace-line"> </div>
<div class="ace-line"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">In order to finish it, w</span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">e </span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">need to translate it from English into more languages. <strong>At the very least, we </strong></span><strong><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">want to translate it to Farsi, Russian, Simplified Chinese, Portuguese, German, Korean, Turkish, Italian, Arabic, French</span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">, </span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">and Spanish.</span></strong><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx"> If you are knowledgeable about another language not listed here, we'd be grateful for your help as well. </span></div>
<div class="ace-line" id="magicdomid86"> </div>
<div class="ace-line" id="magicdomid1500"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">Here's how you can get started:</span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z"> </span></div>
<div class="ace-line" id="magicdomid1133"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">    </span></div>
<div class="ace-line" id="magicdomid1521"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">    - C</span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">reate an account </span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">at</span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx"> </span><a href="https://www.transifex.com"><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">Transife</span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">x</span></a></div>
<div class="ace-line" id="magicdomid1601"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">    - Join your language team on</span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx"> <a href="https://www.transifex.com/otf/torproject/">the Tor Project's page</a></span></div>
<div class="ace-line" id="magicdomid1606"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">    - Visit our</span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z"> </span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx"> </span><a href="https://www.transifex.com/otf/torproject/support-portal/"><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">support portal </span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">project page</span></a></div>
<div class="ace-line" id="magicdomid1561"> </div>
<div class="ace-line" id="magicdomid1600"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">Voilà! Now you are ready to </span><span class="author-a-z89ziz67zz73zmz86zz82z4xz70zkhstz67zz83z">add and adjust translations</span><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">. </span></div>
<div class="ace-line" id="magicdomid479"> </div>
<div class="ace-line" id="magicdomid1651"><span class="author-a-z78zqz76z7z70zsz75zh6z80zc16bz72zx">We appreciate your help! Feel free to add any comments or questions on Transifex along the way. </span></div>

---
_comments:

<a id="comment-276276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bernd (not verified)</span> said:</p>
      <p class="date-time">July 31, 2018</p>
    </div>
    <a href="#comment-276276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276276" class="permalink" rel="bookmark">Registering on a site that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Registering on a site that is <a href="https://framapic.org/HWV2jxdpF2Qh/LtfOaRprRIqc.png" rel="nofollow">ridden with trackers</a>? No, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276277"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276277" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">July 31, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276276" class="permalink" rel="bookmark">Registering on a site that…</a> by <span>Bernd (not verified)</span></p>
    <a href="#comment-276277">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276277" class="permalink" rel="bookmark">As noted on IRC we…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As noted on IRC we appreciate your feedback.</p>
<p>If you can find a "FLOSS and privacy friendly" (Bernd's words) translation service that has an existing diverse user base, maybe Tor would like to hear about it.</p>
<p>Tor has used other translation services before and found that it didn't get much help if there weren't already many users on the platform. If the goal is to get as much help translating as possible, you go where the people are.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276286"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276286" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 01, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-276286">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276286" class="permalink" rel="bookmark">Well, at least you can now…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, at least you can now be sure that you won't get help from people who care for their privacy - which is kind of the target group of tor.<br />
Tor users are here. If you link them to the translation site they will get there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276294"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276294" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>a contributor (not verified)</span> said:</p>
      <p class="date-time">August 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-276294">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276294" class="permalink" rel="bookmark">I&#039;ve heard Tor plans to move…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've heard Tor plans to move to Weblate and leave Transifex, which is proprietary and tracks users. It's such a contradiction that Tor doesn't care much for their contributors privacy and work.<br />
The assumption that 'users on the platform' and 'go where people are' is not correct for translation work. You want someone who is genuinely interested in Tor, so the person will dedicate time and knowledge to the project. Making a right choice on platform translation, consistent with privacy care, will keep the right people contributing. You don't need a thousand of one time/one string of inconsistent translation, you need a few people who care about what they are doing. Be open to contributors is not the same as use random data provided by one string contributors. This is the commercial web model, Tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276315"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276315" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bob (not verified)</span> said:</p>
      <p class="date-time">August 04, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-276315">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276315" class="permalink" rel="bookmark">Doesn&#039;t Mozilla use Pontoon…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Doesn't Mozilla use Pontoon?<br />
<a href="https://github.com/mozilla/pontoon" rel="nofollow">https://github.com/mozilla/pontoon</a><br />
Pretty sure that would be a better option.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-276300"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276300" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 03, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276276" class="permalink" rel="bookmark">Registering on a site that…</a> by <span>Bernd (not verified)</span></p>
    <a href="#comment-276300">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276300" class="permalink" rel="bookmark">Thankfully there&#039;s something…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thankfully there's something called the Tor Browser that protects exactly against that type of thing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-276289"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276289" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 01, 2018</p>
    </div>
    <a href="#comment-276289">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276289" class="permalink" rel="bookmark">I hope this blog is part of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope this blog is part of the redesign thing because it really needs one. Previous version was much better.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276291"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276291" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>emmapeel (not verified)</span> said:</p>
      <p class="date-time">August 02, 2018</p>
    </div>
    <a href="#comment-276291">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276291" class="permalink" rel="bookmark">Bernd: Feel free to submit…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bernd:<br />
Feel free to submit patches to the tor-dev mailing list with your translations, based on our repo <a href="https://git.torproject.org/project/web/support.git" rel="nofollow">https://git.torproject.org/project/web/support.git</a><br />
The translation files are located on the i18n folder: <a href="https://gitweb.torproject.org/project/web/support.git/tree/i18n" rel="nofollow">https://gitweb.torproject.org/project/web/support.git/tree/i18n</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276292"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276292" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>victorhck (not verified)</span> said:</p>
      <p class="date-time">August 02, 2018</p>
    </div>
    <a href="#comment-276292">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276292" class="permalink" rel="bookmark">Hi!
Spreading the word in my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!<br />
Spreading the word in my blog for reach spanish speakers that maybe can help and join the project:<br />
- <a href="https://victorhckinthefreeworld.com/2018/08/02/colabora-en-traducir-el-portal-de-ayuda-de-tor/" rel="nofollow">https://victorhckinthefreeworld.com/2018/08/02/colabora-en-traducir-el-…</a></p>
<p>Happy hacking</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276299"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276299" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 03, 2018</p>
    </div>
    <a href="#comment-276299">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276299" class="permalink" rel="bookmark">I can&#039;t wait for the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't wait for the official web discussion forums to arrive!</p>
<p>I mean.... HAHAHAHAHAHAHAHAHAHAHHAHHAH!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276306"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276306" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Your name (not verified)</span> said:</p>
      <p class="date-time">August 03, 2018</p>
    </div>
    <a href="#comment-276306">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276306" class="permalink" rel="bookmark">use google-translate, ne?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>use google-translate, ne?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276326"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276326" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ButterflyOfFire (not verified)</span> said:</p>
      <p class="date-time">August 06, 2018</p>
    </div>
    <a href="#comment-276326">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276326" class="permalink" rel="bookmark">Hi,
Sorry, I&#039;ve deleted my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
Sorry, I've deleted my Transifex account. Is there any Weblate instance ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276347"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276347" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Diana (not verified)</span> said:</p>
      <p class="date-time">August 09, 2018</p>
    </div>
    <a href="#comment-276347">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276347" class="permalink" rel="bookmark">Hi,
I will host an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
I will host an unconference 20-26 August and we want to do a group effort to translate Tor to Dutch, but I am also an open source privacy fan, so I am reluctant to use transifex as well. Any other solution you can suggest?<br />
:)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276375"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276375" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Diana (not verified)</span> said:</p>
      <p class="date-time">August 14, 2018</p>
    </div>
    <a href="#comment-276375">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276375" class="permalink" rel="bookmark">I organise a hackathon to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I organise a hackathon to translate tor to Dutch next week. Is there any way to do it without transifex?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276968"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276968" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kordycepts (not verified)</span> said:</p>
      <p class="date-time">September 07, 2018</p>
    </div>
    <a href="#comment-276968">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276968" class="permalink" rel="bookmark">I asked to participate in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I asked to participate in the translation to pt-br and I was not accepted yet :/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-283496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Phalicy (not verified)</span> said:</p>
      <p class="date-time">August 19, 2019</p>
    </div>
    <a href="#comment-283496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283496" class="permalink" rel="bookmark">Welsh? ..Cymraeg 
 I was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>Welsh? ..Cymraeg </p></blockquote>
<p> I was wondering whether Welsh support has been addressed? Probably  too niche a language,  but surely a language where<br />
</p>
<blockquote>MORON (carrot) &amp; POPTY-PING (microwave)</blockquote>
<p> are but 2 of the amazing words in WELSH!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
