title: Introducing PrivChat: the Tor Project’s live event series
---
pub_date: 2020-07-14
---
author: alsmith
---
tags: privchat
---
summary: PrivChat is a brand-new fundraising event series held to raise donations for the Tor Project. Through PrivChat, we will bring you important information related to what is happening in tech, human rights, and internet freedom by convening experts for a chat with our community.
---
_html_body:

<p><strong>PrivChat </strong>is a brand-new fundraising event series held to raise donations for the Tor Project. Through PrivChat, we will bring you important information related to what is happening in tech, human rights, and internet freedom by convening experts for a chat with our community.</p>
<p>Our goal with PrivChat is to build a two-way support system. You will get access to information from leading minds thinking about and working on privacy, technology, and human rights. And with your support, the Tor Project will be more agile in our development, allowing us to respond more rapidly to increasing surveillance and censorship threats (and host more PrivChats)! PrivChats are free to attend, but if you enjoy these events we encourage you to become a <a href="https://donate.torproject.org/monthly-giving">monthly donor</a>.</p>
<p>We held our first PrivChat on June 23, and brought together Carmela Troncoso, Assistant Professor at EPFL (Switzerland); Daniel Kahn Gillmor, Senior Staff Technologist for ACLU’s Speech, Privacy, and Technology Project; and Matt Mitchell, hacker and Tech Fellow at the Ford Foundation, to chat with us about privacy in the context of the COVID-19 pandemic, contact tracing, privacy, and the uprising in the U.S. against systemic racism. Watch the the first edition of PrivChat:</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube-nocookie.com/embed/gSyDvG4Z308" width="560"><br />
</iframe></p>
<p>One of the Tor Project’s priorities over the last several years (and for the foreseeable future) is to grow our pool of unrestricted funds and improve or launch programs that help us raise these kinds of donations. One of these campaigns is our annual Year-End Campaign (<a href="https://blog.torproject.org/take-back-internet-us">here’s 2019’s</a>) and our <a href="https://blog.torproject.org/tors-bug-smash-fund-help-tor-smash-all-bugs">Bug Smash Campaign</a>, which takes place every August. PrivChat is part of this strategy.</p>
<p>We plan to hold our next PrivChat in August. If you’re interested in staying up to date about PrivChat, you can: <a href="https://newsletter.torproject.org/">subscribe to our newsletter</a>, <a href="https://blog.torproject.org/events/month">watch our events page</a>, <a href="https://twitter.com/torproject">follow us on Twitter</a>, or <a href="https://torproject.org/privchat">bookmark the PrivChat page</a>.</p>

---
_comments:

<a id="comment-288718"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288718" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2020</p>
    </div>
    <a href="#comment-288718">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288718" class="permalink" rel="bookmark">How is it possible to use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How is it possible to use privchat video in Tor browser? Tor isn't allowing flash player or hmtl5 player add in plugin.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288732"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288732" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288718" class="permalink" rel="bookmark">How is it possible to use…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288732">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288732" class="permalink" rel="bookmark">If you&#039;re talking about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're talking about YouTube, video won't play unless you set Tor Browser's security level to Safer or Standard. Then, videos that you try to play will be covered by a translucent yellow box containing text that begins with "". That means the NoScript add-on in Tor Browser blocked the video or audio. Click the "" text to allow media, or go into NoScript's options and allow "media" on that website. Pro tip: Open the browser's main menu, and click on "Customize ..." to find NoScript's toolbar icon and drag it into the browser's toolbar. It looks like a blue "S" in a white circle.</p>
<p>On other websites, if the video is embedded not from the first-party host but from a third-party host, then allow "media" on the first-party website **and** the third-party website.</p>
<p>MODERATORS: Please add to support.torproject.org help to allow video media. There is no help for it, and it's a frequent question.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288753"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288753" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288732" class="permalink" rel="bookmark">If you&#039;re talking about…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288753">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288753" class="permalink" rel="bookmark">text that begins with &quot;&quot;
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>text that begins with ""</p>
<p>should be: text that begins with "[MEDIA]" but the square brackets [ ] are less-than, greater-than symbols, <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">&lt;</span> <span style="color: #339933;">&gt;</span></code></span> &lt; &gt;</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-288719"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288719" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2020</p>
    </div>
    <a href="#comment-288719">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288719" class="permalink" rel="bookmark">I wish it was simulcast or…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wish it was simulcast or simul-hosted on a platform that isn't proprietary (YouTube) and crawling with anti-privacy trackers. The world should be ostracizing those business models.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288759"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288759" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288719" class="permalink" rel="bookmark">I wish it was simulcast or…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288759">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288759" class="permalink" rel="bookmark">To watch the recordings, you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To watch the recordings, you can use <a href="https://invidio.us" rel="nofollow">https://invidio.us</a>, which extracts the .mp4 from Youtube and displays it in an HTML video tag, so it works completely without JS. I don't know if it supports live streaming events, so you'll have to try it and see.</p>
<p>Please use a .onion address to reduce the load on the exit nodes! You can find them at:<br />
<a href="https://instances.invidio.us" rel="nofollow">https://instances.invidio.us</a></p>
<p>See also:<br />
<a href="https://github.com/omarroth/invidious" rel="nofollow">https://github.com/omarroth/invidious</a></p>
<p>&gt; crawling with anti-privacy trackers<br />
Not to mention most of the exit nodes are blocked by Youtube, so you either have to fill out captchas or keep hitting new identity every time you want to watch a video or use any Google site for that matter.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>(ACCEPTINGTOR) (not verified)</span> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
    <a href="#comment-288725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288725" class="permalink" rel="bookmark">https://www.eff.org/press…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.eff.org/press/releases/eff-asks-supreme-court-rule-violating-terms-service-isnt-crime-under-cfaa" rel="nofollow">https://www.eff.org/press/releases/eff-asks-supreme-court-rule-violatin…</a></p>
<p><a href="https://www.eff.org/document/van-buren-eff-security-researchers-amicus-brief" rel="nofollow">https://www.eff.org/document/van-buren-eff-security-researchers-amicus-…</a></p>
<p><a href="https://www.eff.org/cases/van-buren-v-united-states/security-researcher-amici" rel="nofollow">https://www.eff.org/cases/van-buren-v-united-states/security-researcher…</a></p>
<p><a href="https://www.eff.org/deeplinks/2013/01/these-are-critical-fixes-computer-fraud-and-abuse-act" rel="nofollow">https://www.eff.org/deeplinks/2013/01/these-are-critical-fixes-computer…</a></p>
<p><a href="https://www.reddit.com/r/technology/comments/16njr9/im_rep_zoe_lofgren_im_introducing_aarons_law_to/" rel="nofollow">https://www.reddit.com/r/technology/comments/16njr9/im_rep_zoe_lofgren_…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288731"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288731" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
    <a href="#comment-288731">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288731" class="permalink" rel="bookmark">Audrey Tang&#039;s perspectives…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Audrey Tang's perspectives on privacy would be interesting. Other than her portfolio of civic technology and open-source governance work including recently in response to COVID-19, people who are transgender tend to be very concerned and careful to make certain of privacy.</p>
<p>Similar to her work on vTaiwan in particular, other developers of civic technology platforms such as NYSenate.gov, DemocracyOS, Consul project, Decidm, and CiviWiki also struggle with securing the privacy of citizen participants.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288733"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288733" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
    <a href="#comment-288733">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288733" class="permalink" rel="bookmark">&quot;follow us on Twitter&quot; ... …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"follow us on Twitter" ... "use the hashtag"</p>
<p>When you talk about Twitter, talk about Mastodon.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288735"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288735" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288733" class="permalink" rel="bookmark">&quot;follow us on Twitter&quot; ... …</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288735">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288735" class="permalink" rel="bookmark">Good point, you can also…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good point, you can also follow us there: <a href="https://mastodon.social/@torproject" rel="nofollow">https://mastodon.social/@torproject</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288734"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288734" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
    <a href="#comment-288734">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288734" class="permalink" rel="bookmark">Can donors opt to not be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can donors opt to not be sent swag? It draws attention themselves and could compromise their safety. For example, receiving mail or flashing a Tor sticker at an airport can land you in detention.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288737" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">July 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288734" class="permalink" rel="bookmark">Can donors opt to not be…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288737" class="permalink" rel="bookmark">Yes, donors can opt out of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, donors can opt out of swag. On <a href="https://donate.torproject.org" rel="nofollow">https://donate.torproject.org</a>, you can check the " No thanks, I don't want a gift. I would prefer 100% of my donation to go to the Tor Project's work," box and not receive swag. There are also some other ways to donate anonymously, the donate page and subsequent FAQ outline various methods.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288741"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288741" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>joe smith (not verified)</span> said:</p>
      <p class="date-time">July 18, 2020</p>
    </div>
    <a href="#comment-288741">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288741" class="permalink" rel="bookmark">please make a web forum
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>please make a web forum</p>
<p>!remindme 5,000,000 years</p>
<p>i know you're reading these and not approving them ^_^</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>UglyAntifa (not verified)</span> said:</p>
      <p class="date-time">July 19, 2020</p>
    </div>
    <a href="#comment-288747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288747" class="permalink" rel="bookmark">@ TP: I love you, but in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ TP: I love you, but in this case IMO you are not helping by encouraging Surveillance Capitalism.</p>
<p>I urge TP to discuss in a future post one of the worst problems dissidents face, given the current entire lack of a privacy industry: there are currently no secure (audited) platform-independent</p>
<p>o real time messaging platforms (Signal tries but will be rendered illegal if LAEDA or EARN-IT pass; Signal has stated they will leave the  US market if this happens),</p>
<p>o anonymized email or mailing list software</p>
<p>o online meeting platforms</p>
<p>The answer is not to take the path of least resistance by adopting some of the worst privacy abusers in the Surveillance Capitalism model, such as</p>
<p>o Youtube</p>
<p>o Facebook</p>
<p>o Google and other Alphabet platforms</p>
<p>o Mailchimp</p>
<p>o Zoom</p>
<p>all of which have suffered repeatedly huge security failures, some of which have also repeatedly been caught breaking their word by secretly selling user data to the worst elements of the  govt and corporate world.</p>
<p>The correct response is to publicly call for a genuine privacy industry.</p>
<p>The need is clear, the demand is there, but potential entrepreneurs are too frightened to even talk about supplying what we need.  Even such simple items as Faraday sleeves for phones and smart cards are difficult or impossible to obtain, not to mention properly secured SOHO routers or devices designed to</p>
<p>o monitor the local WiFi/audio environment for "out of band" signaling by the various user devices we believe (perhaps falsely) we own ourselves,</p>
<p>o detect cameras hidden in "smart TVs" (preferably in the showroom),</p>
<p>o detect EM litter abusable by Tempest violations, </p>
<p>o warn of dangerous data sharing by in-vehicle surveillance systems which are factory installed in most (all?) new US vehicles,</p>
<p>o warn of the presence of nearby cell-site simulators, </p>
<p>o warn us when we are targeted with thru-wall WiFi motion detectors or thru-wall radars, </p>
<p>o warn of timing vulnerabilities in our device CPUs,</p>
<p>o warn of the presence of nearby mystery drones or spyplanes, </p>
<p>o guard against unconstitutional indiscriminate "targeted"  USG (mainly FBI/CIA) cyberattacks on personal computers of US citizens, etc.</p>
<p>The 14th Amendment was meant to counter these kinds of abuses but in 1866 politicians could not have anticipated the kind of technologies confronting modern citizens who are trying to speak out against government abuses.</p>
<p>Not to be alarmist, but recent events in Portland and Seattle have raised legitimate concerns about unidentified paramilitaries kidnapping citizens.  The political will to curb such outrageous, self-defeating, and flagrantly unconstitutional abuses is plainly lacking (only Sen. Wyden of OR appears perturbed by paramilitary kidnappings), so our only hope is to try to coopt capitalism by persuading ambitious tech entrepreneurs that they can do good and make (some) money at the same time by providing the kinds of personal cybersecurity/privacy enhancing consumer devices we all so desperately need.  I believe that such devices can work well with FOSS software projects like Tor Project and Tails Project.</p>
<p>truthout.org<br />
Secret Federal Police Deployed by Trump Snatch Protesters Off Portland Streets<br />
Police face demonstrators as Black Lives Matter supporters demonstrate in Portland, Oregon, on Jake Johnson, Common Dreams<br />
17 Jul 2020</p>
<p>truthout.org<br />
Trump’s Federal Police Are Kidnapping and Brutalizing Protesters in Portland<br />
Tatiana Cozzarelli, Left Voice<br />
18 Jul 2020</p>
<p>truthout.org<br />
Oregon Senator Vows Amendment to Bar Trump From Sending Paramilitary Squads Onto US Streets<br />
Jake Johnson, Common Dreams<br />
19 Jul 2020</p>
<p>theguardian.com<br />
Portland: protesters' outrage grows over federal officers' 'blatant abuse'<br />
Trump tweets ‘we are trying to help Portland, not hurt it’ but the presence of militarized federal agents has prompted shock and anger<br />
Hallie Golden<br />
19 Jul 2020</p>
<p>theatlantic.com<br />
The Supreme Court has betrayed the promise of equal citizenship by allowing police to arrest and kill Americans at will.<br />
David H. Gans</p>
</div>
  </div>
</article>
<!-- Comment END -->
