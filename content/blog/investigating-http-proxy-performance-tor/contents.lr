title: Investigating http proxy performance with Tor
---
pub_date: 2009-08-19
---
author: phobos
---
tags:

vidalia bundle
tor browser bundle
privoxy
polipo
performance
socks5
caching
faster tor
faster firefox
open research
---
categories:

applications
network
research
---
_html_body:

<p>A while ago there was a thread on <a href="http://archives.seul.org/or/talk/" rel="nofollow">OR-TALK</a> that devolved into<br />
</p>

<blockquote>"why does Tor still ship ancient privoxy?"</blockquote>

<p> and<br />
</p>

<blockquote>"why are you shipping polipo with the Tor Browser Bundle instead of current privoxy?"</blockquote>

<p>For those interested, the thread is here, <a href="http://archives.seul.org/or/talk/Jul-2009/msg00063.html" rel="nofollow">http://archives.seul.org/or/talk/Jul-2009/msg00063.html</a>.</p>

<p>Scott had a good argument for why we should update the bundles to the latest privoxy, and I agree, we should.  But then I started thinking about why we needed a proxy at all.  Almost all browsers support socks5 direct, isn't that faster than a middleman proxy?</p>

<p>This got me thinking about why polipo is in the TBB, but not the other packages.  The TBB "feels faster" when using Tor than using the installed Tor, Vidalia, and Privoxy.  However, I couldn't find any actual testing of performance of polipo vs. privoxy vs. socks5 direct.  </p>

<p>So I did it myself, in a loose manner.  I wanted to quantify "feels faster".</p>

<p>The raw data from all the testing is :</p>

<ul>
<li>Tamper Data as xml,</li>
<li>proxy config files, </li>
<li>and results in a spreadsheet.</li>
</ul>

<p>This is all contained in <a href="http://freehaven.net/~phobos/polipo-v-privoxy.tar.gz" rel="nofollow">http://freehaven.net/~phobos/polipo-v-privoxy.tar.gz</a> {.asc).  There is a README as well.  And yes, the ruby script is a quick and dirty hack.</p>

<p>I tested a few scenarios:</p>

<p>1) native <a href="http://www.pps.jussieu.fr/~jch/software/polipo/" rel="nofollow">polipo</a> and <a href="http://www.privoxy.org/" rel="nofollow">privoxy</a> without using Tor.<br />
2) polipo and privoxy forwarding to Tor localhost:9050.<br />
3) firefox socks5 direct to Tor via localhost:9050.</p>

<p>The summary of results:</p>

<ul>
<li>Native polipo is 54.5% faster on average than native privoxy.  This could be due to polipo's caching, http 1.1 pipelining, and it can serve bits as fast as they come in from the network.  Privoxy needs to load the whole page, scan it, and then send it to the client.  Even if privoxy filtering is disabled, it still works the same way.</li>
<li>Polipo caching shines with Tor usage.  Performance is still about 40% faster with polipo than privoxy.  Common images are cached, and served from the memory cache in single-digit millisecond ranges.  Privoxy needs to wait for Tor to wholly deliver the bits.  Caching is faster, this we know already.  However, from a user perspective, it's just faster to load pages.</li>
<li>socks5 in Firefox 3.5.2 did better than I expected.  It was about twice as slow as polipo, but still twice as fast as privoxy.  I chalk this up to the tor circuit variability more than anything else.</li>
<li>I tried testing a click to a second page to see how much polipo caching helps people reading different pages on the same site.  It helps, but not as much as I expected.  Polipo ranged from slower than privoxy to 40-100% faster. Too much variability to make a real determination.</li>
</ul>

<p>Caveats:  Testing under tor is highly variable.  I used the same circuits for both the polipo and privoxy tests to minimize variability.  However, I can't control node load and congestion.</p>

<p>Out of 23 get requests for the Torproject.org/index.html.en, 17 are for the country flags.   Perhaps we should load these last at the bottom of the page, or do something else to speed up the torproject page load.</p>

<p>As I was doing this, I kept thinking of other ways to do it better;</p>

<ul>
<li>time requests and bits between tor, the http proxy, and the browser.  How long does each request take to get from the browser, to the proxy, to tor and back across each layer?  how much latency does each piece of software add to the request and delivery?</li>
<li>automate testing and let it run on a normal tor client over weeks. This will average out tor network variability and show "typical" user experience.</li>
<li>Pick a sampling of the top 100 websites by visits worldwide and measure their performance with the three methods, fully instrumented as in #1.</li>
<li>Do user experience measurements.  Pay/ask/bribe people to sit in front of a computer, video record their browsing and feedback, and ask for a rating of each configuration (socks5, polipo, privoxy, and a placebo).</li>
<li>re-run #2 and run gcov to watch the code paths used in each piece of software, and figure out what can be optimized for performance.</li>
<li>test various "private browsing modes" through tor to see which browser is faster; firefox, safari, chromium, torfox, or torora.</li>
<li>how can we better tune polipo caching dynamically based on system ram config?  Does having 1GB of cache provide significant benefits over the default?</li>
</ul>

<p>I'm sure there are lots of things wrong with my measurements, minimal analysis, and results.</p>

<p>Constructive criticism is welcome.</p>

---
_comments:

<a id="comment-2174"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2174" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2009</p>
    </div>
    <a href="#comment-2174">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2174" class="permalink" rel="bookmark">&quot;Out of 23 get requests for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Out of 23 get requests for the Torproject.org/index.html.en, 17 are for the country flags. Perhaps we should load these last at the bottom of the page, or do something else to speed up the torproject page load."</p>
<p>That would be very easy to optimise. Use a single image with all the flags in it, then use an image map to set the links on it.<br />
Alternatively CSS sprites could be used (single file and links/flags displayed using an offset).<br />
The image map is a nicer solution in my opinion.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2177"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2177" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2009</p>
    </div>
    <a href="#comment-2177">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2177" class="permalink" rel="bookmark">Firefox is buggy with socks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Firefox is buggy with socks proxy directly.<br />
It has has a hard-coded 10 second timeout, and it blocks when waiting, so if you have one tab waiting to connect to socks, you cant use the other tabs.</p>
<p>Polipo sucks please stop using it, watch what its doing with a packet sniffer.<br />
Completely non-standard stuff there that breaks alot of sites, converting GET requests into HEAD etc which show blank pages!<br />
Its annoying to be on a forum and click refresh only to get a blank page over and over, because polipo is sending HEAD requests to the server instead of GET which of course results in blank page as there is no response body to a HEAD request.<br />
WTH is polipo thinking modifying a request, its supposed to be pass-through only.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2183" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2177" class="permalink" rel="bookmark">Firefox is buggy with socks</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2183" class="permalink" rel="bookmark">re: polipo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have you reported this GET to HEAD request transformation to the polipo list as a bug?</p>
<p>If you know of an open source, freely licensed caching proxy that works on windows, linuxes, unixes, and macs, then point it out.  I'll look into it.</p>
<p>And yes, Firefox is buggy with SOCKS.  In fact, there's a 3-year old bug about the hard-coded timer waiting for someone at Mozilla to address it.  At one point, we even offered to pay someone at Mozilla to fix it.   The bug being, <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=280661" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=280661</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2186"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2186" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-2186">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2186" class="permalink" rel="bookmark">Nope I didn&#039;t report it.
I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nope I didn't report it.<br />
I was under the impression that Polipo was not in active development.<br />
Is that not true?</p>
<p>Is there any point in caching?<br />
I can see the advantage if multiple PC's share the same cache, but browsers already have their own caching anyway.</p>
<p>I prefer to make connections via SSL anyway, and proxy's cant cache those.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-2185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2009</p>
    </div>
    <a href="#comment-2185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2185" class="permalink" rel="bookmark">I thought the main reason to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I thought the main reason to use Privoxy was not to leak DNS by using Socks 4a. Is Socks 5 no longer leaking DNS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2185" class="permalink" rel="bookmark">I thought the main reason to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2187" class="permalink" rel="bookmark">Yes socks 5 still leaks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes socks 5 still leaks DNS.<br />
Socks 4a is the only socks that supports connecting to a host name, socks 4/5 only support IP, so you have to resolve the DNS to IP which leaks.</p>
<p>Most apps don't support socks 4a, but HTTP proxy is very common.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2193" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2187" class="permalink" rel="bookmark">Yes socks 5 still leaks</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2193" class="permalink" rel="bookmark">False. Socks 5 works</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>False. Socks 5 works fine.<br />
Easy way to test: Have the vidalia Tor Network page open, and then load a page in Firefox. The connection will appear in the Tor Network page, and you can see it named the same way it's passed to Tor. If the browser passes an IP, you'll see it there, but if it passes the hostname, you'll also see it there. I always see hostnames there (except for when I'm purposely connecting to a specific IP).</p>
<p>BTW The CAPTCHA here sucks, it's taken me a lot tries. You should use something like reCAPTCHA, works nicely for me on my site.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2205"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2205" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 24, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2193" class="permalink" rel="bookmark">False. Socks 5 works</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2205">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2205" class="permalink" rel="bookmark">re:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>firefox and socks5 with torbutton enabled works correctly.  Not all socks5 compatible applications will do name resolution via the socks proxy.</p>
<p>Using recaptcha means trusting a 3rd party to serve up the captcha, and not log requests for the captcha.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-2201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Soulmech (not verified)</span> said:</p>
      <p class="date-time">August 24, 2009</p>
    </div>
    <a href="#comment-2201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2201" class="permalink" rel="bookmark">If Polipo works better than</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If Polipo works better than the other two, why change that? Just work with it, optimize it if you can, leave it at that. Unless there is some really good reason to switch, it's probably best to leave it alone.</p>
<p>I use the Tor Browser Bundle, and it's always been pretty good for me. I've never had serious issues or lag with it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2210" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>MathFox (not verified)</span> said:</p>
      <p class="date-time">August 25, 2009</p>
    </div>
    <a href="#comment-2210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2210" class="permalink" rel="bookmark">Cookies, flash and such</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What will be the effect of removing a filtering proxy on the privacy of the user with respect to tracking cookies and locally stored data by flash objects?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2219"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2219" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2210" class="permalink" rel="bookmark">Cookies, flash and such</a> by <span>MathFox (not verified)</span></p>
    <a href="#comment-2219">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2219" class="permalink" rel="bookmark">re: </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If the user is using torbutton defaults, then the cookies and such are wiped on toggle or restart of the browser.  Torbutton disables plugins, like flash, so one shouldn't have flash cookies from browsing with Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2213"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2213" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SwissTorExit (not verified)</span> said:</p>
      <p class="date-time">August 25, 2009</p>
    </div>
    <a href="#comment-2213">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2213" class="permalink" rel="bookmark">re:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i have tested firefox.3.5.2 with socks5 and work perfectly if we use only the browser but if we try to use java or a few application, sometime the DNS can be leaked.</p>
<p>with privoxy, so far only flash leak DNS and bypass proxy, with last java and config it to not save data on system and use privoxy, it don't seem leaks DNS anymore. Flash stay very dangerous and bypass anyway the proxy.</p>
<p>well my conculsion are:</p>
<p>For me are possible yet to use java if the new nocript are installed with cs lite( cookie prevention) , so if someone need java for a specific site , it can be used safty.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2238"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2238" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2009</p>
    </div>
    <a href="#comment-2238">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2238" class="permalink" rel="bookmark">Polipo is good but not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Polipo is good but not stable, It has annoying issues when uploading big files.<br />
and its development is inactive. but indeed its a promising candidate. Some people used to chain up squid and privoxy to speed up browsing. If there is a mini squid which is easy to setup i would dump polipo.</p>
<p>but yeah, polipo has another advantage. If polipo is integrated into Tor Bundle(I never use this pack so I dont know). You can create a wizard to help set up a hidden service using polipo as a mini http server</p>
<p>Off topic:<br />
Will there be torIM plugin for Pidgin? I guess chatting through hidden service would be safer than chatting though 3rd party servers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2995"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2995" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 03, 2009</p>
    </div>
    <a href="#comment-2995">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2995" class="permalink" rel="bookmark">Throwing Anonymity Away?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Privoxy was used to shild against possible attacks which could compromise anonymity.</p>
<p>It seems that users aren't protected any more.</p>
<p>There is no hint that polipo can do the same; There has even been some discussion, that current Firefox 3.5 isn't as "solid" against such threats as FF 3.0 was (which will be discontinued). I think that investigating "performance" is not enough. Tor was not meant to enhance "performance" but anonymity.</p>
<p>Privoxy, or any other (even tor build-in) shielding solution shold be part of the software package.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3432"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3432" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2009</p>
    </div>
    <a href="#comment-3432">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3432" class="permalink" rel="bookmark">There is always other</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is always other reverse proxies like<br />
<a href="http://www.delegate.org/delegate/" rel="nofollow">http://www.delegate.org/delegate/</a> (windows/linux)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4277"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4277" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 06, 2010</p>
    </div>
    <a href="#comment-4277">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4277" class="permalink" rel="bookmark">Since Polipo was substituted</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since Polipo was substituted for Privoxy, tor has become much less useful. </p>
<p>BUG: timing out martian query</p>
<p>Configured to measure directory request statistics, but no GeoIP database found!<br />
Configured to measure entry node statistics, but no GeoIP database found!<br />
Bridge status changed.  Forgetting GeoIP stats.<br />
%s\geoip<br />
Unable to parse state in "%s". Moving it aside to "%s".  This could be a bug in Tor; please tell the developers.<br />
Uh oh.  We couldn't even validate our own default state. This is a bug in Tor.<br />
Initialized state<br />
Unparseable bandwidth history state: %s<br />
Unable to parse state in "%s"; too many saved bad state files to move aside. Discarding the old state file.<br />
Bridges cannot be configured to measure additional GeoIP statistics as entry guards.<br />
Unable to read state file "%s"<br />
Error in accounting options<br />
Error loading rendezvous service keys<br />
Error creating cookie authentication file.<br />
Error parsing already-validated policy options.<br />
Previously validated hidden services line could not be added!<br />
Weirdly, I couldn't even move the state aside. The OS gave an error of %s<br />
Error initializing keys; exiting</p>
</div>
  </div>
</article>
<!-- Comment END -->
