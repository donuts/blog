title: January 2009 Progress Report
---
pub_date: 2009-02-23
---
author: phobos
---
tags:

progress report
translations
bug fixes
releases
security fixes
---
categories:

localization
releases
reports
---
_html_body:

<p><strong>New releases, new hires, new funding</strong></p>

<p>Tor 0.2.1.10-alpha (released January 6) fixes two major bugs in bridge<br />
relays (one that would make the bridge relay not so useful if it had<br />
DirPort set to 0, and one that could let an attacker learn a little bit<br />
of information about the bridge's users), and a bug that would cause your<br />
Tor relay to ignore a circuit create request it can't decrypt (rather<br />
than reply with an error). It also fixes a wide variety of other bugs.<br />
<a href="http://archives.seul.org/or/talk/Jan-2009/msg00078.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2009/msg00078.html</a></p>

<p>Tor 0.2.1.11-alpha (released Jan 20) finishes fixing the "if your Tor is<br />
off for a week it will take a long time to bootstrap again" bug. It also<br />
fixes an important security-related bug reported by Ilja van Sprundel. You<br />
should upgrade. (We'll send out more details about the bug once people<br />
have had some time to upgrade.)<br />
<a href="http://archives.seul.org/or/talk/Jan-2009/msg00171.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2009/msg00171.html</a></p>

<p>Tor 0.2.0.33 (released Jan 21) fixes a variety of bugs that were making<br />
relays less useful to users. It also finally fixes a bug where a relay or<br />
client that's been off for many days would take a long time to bootstrap.<br />
<a href="http://archives.seul.org/or/announce/Jan-2009/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/Jan-2009/msg00000.html</a></p>

<p>Tor Browser Bundle 1.1.8 (released Jan 22) updates Tor to 0.2.1.11-alpha<br />
(security update), updates OpenSSL to 0.9.8j (security update), updates<br />
Firefox to 3.0.5, updates Pidgin to 2.5.4, and updates libevent to 1.4.9.<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/README" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/README</a></p>

<p>This month we also hired three new people: Martin Peck is working on<br />
Tor VM, a new way of packaging Tor on Windows that will let people use<br />
Youtube safely again; Mike Perry is working on Torbutton maintenance<br />
and development and on Torflow, a set of scripts to do measurements on<br />
the Tor network; and Andrew Lewman is our new executive director.</p>

<p><strong>Enhancements</strong><br />
Major bugfixes in the Tor 0.2.1.10-alpha and 0.2.0.33 releases:<br />
- If the cached networkstatus consensus is more than five days old,<br />
  discard it rather than trying to use it. In theory it could be useful<br />
  because it lists alternate directory mirrors, but in practice it just<br />
  means we spend many minutes trying directory mirrors that are long<br />
  gone from the network. Helps bug 887 a bit; bugfix on 0.2.0.x.</p>

<p>Tor 0.2.1.10-alpha contains cleanups that let Tor build on Google's<br />
Android phone:<br />
- Change our header file guard macros to be less likely to conflict<br />
  with system headers. Adam Langley noticed that we were conflicting<br />
  with log.h on Android.</p>

<p>Major bugfixes in the Tor 0.2.1.11-alpha and 0.2.0.33 releases:<br />
- Discard router descriptors as we load them if they are more than<br />
  five days old. Otherwise if Tor is off for a long time and then<br />
  starts with cached descriptors, it will try to use the onion<br />
  keys in those obsolete descriptors when building circuits. Bugfix<br />
  on 0.2.0.x. Fixes bug 887.</p>

<p>Security bugfixes in the Tor 0.2.1.11-alpha and 0.2.0.33 releases:<br />
- Fix a heap-corruption bug that may be remotely triggerable on<br />
  some platforms. Reported by Ilja van Sprundel.</p>

<p>Circuit-building speedups in Tor 0.2.1.10-alpha:<br />
- When a relay gets a create cell it can't decrypt (e.g. because it's<br />
  using the wrong onion key), we were dropping it and letting the<br />
  client time out. Now actually answer with a destroy cell. Fixes<br />
  bug 904. Bugfix on 0.0.2pre8.</p>

<p>Scalability fixes from the Tor 0.2.0.33 ChangeLog:<br />
- Clip the MaxCircuitDirtiness config option to a minimum of 10 seconds,<br />
  and the CircuitBuildTimeout to a minimum of 30 seconds. Warn the user if<br />
  lower values are given in the configuration. These fixes prevent a user<br />
  from rebuilding circuits too often, which can be a denial-of-service<br />
  attack on the network.<br />
- When a stream at an exit relay is in state "resolving" or<br />
  "connecting" and it receives an "end" relay cell, the exit relay<br />
  would silently ignore the end cell and not close the stream. If<br />
  the client never closes the circuit, then the exit relay never<br />
  closes the TCP connection. Bug introduced in Tor 0.1.2.1-alpha;<br />
  reported by "wood".<br />
- When sending CREATED cells back for a given circuit, use a 64-bit<br />
  connection ID to find the right connection, rather than an addr:port<br />
  combination. Now that we can have multiple OR connections between<br />
  the same ORs, it is no longer possible to use addr:port to uniquely<br />
  identify a connection.</p>

<p>Bootstrapping speedups in Tor 0.2.1.11-alpha:<br />
- When our circuit fails at the first hop (e.g. we get a destroy<br />
  cell back), avoid using that OR connection anymore, and also<br />
  tell all the one-hop directory requests waiting for it that they<br />
  should fail. Bugfix on 0.2.1.3-alpha.</p>

<p><strong>Architecture</strong><br />
Proposal 158 ("Clients download consensus + microdescriptors") suggests a<br />
new way forward for reducing directory overhead for clients, and replaced<br />
part of proposal 141. Rather than modifying the circuit-building protocol<br />
to fetch a server descriptor inline at each circuit extend, we instead put<br />
all of the information that clients need either into the consensus itself,<br />
or into a new set of data about each relay called a microdescriptor.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/158-microdescriptors.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/158-microde…</a></p>

<p>From the 0.2.0.33 ChangeLog:<br />
- Never use OpenSSL compression: it wastes RAM and CPU trying to compress<br />
  cells, which are basically all encrypted, compressed, or both. It also<br />
  made us stand out from other applications on the wire.</p>

<p><strong>Advocacy</strong><br />
Jillian York continued blogging for us about the good uses of Tor:<br />
<a href="http://www.knightpulse.org/blog/tor" rel="nofollow">http://www.knightpulse.org/blog/tor</a></p>

<p>"Federico Heinz advocates anonymous browsing in Argentina", Jan 8<br />
<a href="http://www.knightpulse.org/blog/09/01/08/federico-heinz-advocates-anonymous-browsing-argentina" rel="nofollow">http://www.knightpulse.org/blog/09/01/08/federico-heinz-advocates-anony…</a></p>

<p>"Human Rights Organizations in Argentina welcome anonymous browsing", Jan 25<br />
<a href="http://www.knightpulse.org/blog/09/01/25/human-rights-organizations-argentina-welcome-anonymous-browsing" rel="nofollow">http://www.knightpulse.org/blog/09/01/25/human-rights-organizations-arg…</a></p>

<p>"Watch how you get around", Jan 30<br />
<a href="http://www.knightpulse.org/blog/09/01/30/watch-how-you-get-around" rel="nofollow">http://www.knightpulse.org/blog/09/01/30/watch-how-you-get-around</a></p>

<p><strong>Pre-configured bundles</strong><br />
Tor Browser Bundle 1.1.8 (released Jan 22) updates Tor to 0.2.1.11-alpha<br />
(security update), updates OpenSSL to 0.9.8j (security update), updates<br />
Firefox to 3.0.5, updates Pidgin to 2.5.4, and updates libevent to 1.4.9.<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/README" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/README</a></p>

<p>We continued work on Vidalia features to support where we want Tor<br />
Browser Bundle to go. In particular, we're changing it to be able to<br />
launch Firefox natively, rather than use the "PortableFirefox" pile of<br />
complex scripts. We hope this change will also let users run a normal<br />
Firefox alongside TBB. More on that in February.</p>

<p>We also continued work on Tor VM, a new way of packaging Tor on<br />
Windows that will (among other things) let people use Youtube safely<br />
again. Hopefully we'll have some simple instructions up about that in<br />
February too.</p>

<p><strong>Bridges</strong><br />
Major bugfixes in the Tor 0.2.1.10-alpha and 0.2.0.33 releases:<br />
- Bridge relays that had DirPort set to 0 would stop fetching<br />
  descriptors shortly after startup, and then briefly resume<br />
  after a new bandwidth test and/or after publishing a new bridge<br />
  descriptor. Bridge users that try to bootstrap from them would<br />
  get a recent networkstatus but would get descriptors from up to<br />
  18 hours earlier, meaning most of the descriptors were obsolete<br />
  already. Reported by Tas; bugfix on 0.2.0.13-alpha.<br />
- Prevent bridge relays from serving their 'extrainfo' document<br />
  to anybody who asks, now that extrainfo docs include potentially<br />
  sensitive aggregated client geoip summaries. Bugfix on<br />
  0.2.0.13-alpha.</p>

<p>Bugfixes in the Tor 0.2.1.10-alpha release:<br />
- When we made bridge authorities stop serving bridge descriptors over<br />
  unencrypted links, we also broke DirPort reachability testing for<br />
  bridges. So bridges with a non-zero DirPort were printing spurious<br />
  warns to their logs. Bugfix on 0.2.0.16-alpha. Fixes bug 709.</p>

<p>New feature in Tor 0.2.1.10-alpha:<br />
- New controller event "clients_seen" to report a geoip-based summary<br />
  of which countries we've seen clients from recently. Now controllers<br />
  like Vidalia can show bridge operators that they're actually making<br />
  a difference.<br />
Vidalia will add support for this feature in February.</p>

<p><strong>Alternate download methods</strong><br />
Our "gettor" email auto-responder is up and working:<br />
<a href="https://svn.torproject.org/svn/projects/gettor/README" rel="nofollow">https://svn.torproject.org/svn/projects/gettor/README</a><br />
<a href="https://www.torproject.org/finding-tor#Mail" rel="nofollow">https://www.torproject.org/finding-tor#Mail</a></p>

<p>Thandy itself is working smoothly at this point too -- it can contact<br />
the central repository, check all the keys, look in the registry and<br />
compare the currently installed version to the new choices, fetch the<br />
right packages, check all the signatures, and launch the install.</p>

<p>As of December we only had a new MSI-based installer for Tor, but not for<br />
Vidalia, Torbutton, or Polipo. Now we do, though it's still in testing:<br />
<a href="https://data.peertech.org/torbld" rel="nofollow">https://data.peertech.org/torbld</a></p>

<p><strong>Translations</strong><br />
Our translation server is up and online:<br />
<a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a><br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>We continued enhancements to the Chinese and Russian Tor website<br />
translations. Our Farsi translation from this summer is slowly becoming<br />
obsolete; we should solve that at some point.</p>

---
_comments:

<a id="comment-715"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-715" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>An (not verified)</span> said:</p>
      <p class="date-time">February 23, 2009</p>
    </div>
    <a href="#comment-715">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-715" class="permalink" rel="bookmark">TOR Rocks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nodoubt TOR is the best. But i would have liked it better if the TOR proxy rotations can be disabled. They change too frequently. Also it would have been better if we can select the end nodes of the proxy when connecting.</p>
<p>Is there a way to do it??</p>
<p>Let me know if there is way to tweak TOR because i didn't find it yet anywhere written.</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-723" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-715" class="permalink" rel="bookmark">TOR Rocks</a> by <span>An (not verified)</span></p>
    <a href="#comment-723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-723" class="permalink" rel="bookmark">Tor rocks so much, that it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor rocks so much, that it is tweaked by default.</p>
<p>Why should someone give away software, which isn't configured/tweaked as good as possible?!</p>
<p>BTW: It's Tor not TOR. See the logos..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-723" class="permalink" rel="bookmark">Tor rocks so much, that it</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-727" class="permalink" rel="bookmark">TOR or Tor or T-O-R, search</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR or Tor or T-O-R, search engines take it the same way.</p>
<p>And anyway, In Tor i don't like that automatic tweak feature which changes the ip address randomly all the time. If i want to use a usa ip for some time, there is no way to have it fixed and working because the next moment it uses germany ip. And i get blocked again on the websites.</p>
<p>This is the only reason why i have limited my use of Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-730"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-730" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-727" class="permalink" rel="bookmark">TOR or Tor or T-O-R, search</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-730">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-730" class="permalink" rel="bookmark">There are two ways to fix this.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1) Use TrackHostExits, so that your IP doesn't change every so many minutes.  See more details at <a href="https://www.torproject.org/tor-manual.html.en" rel="nofollow">https://www.torproject.org/tor-manual.html.en</a></p>
<p>2) # Use exit nodes in GB.<br />
    ExitNodes {gb}</p>
<p>    # Use exit nodes in countries that have won the World Cup<br />
    # recently. Also use any exit node at MIT.<br />
    ExitNodes {it}, {br}, {fr}, 18.0.0.0/8</p>
<p>    # Don't use an exit node in any country that is sinking under<br />
    # water.<br />
    ExcludeExitNodes {mv}</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-716"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-716" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Mori (not verified)</span> said:</p>
      <p class="date-time">February 23, 2009</p>
    </div>
    <a href="#comment-716">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-716" class="permalink" rel="bookmark">Farsi translation</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear Tor Project,<br />
First let me sincerely thank you for your continuous and tireless efforts to provide us with security and freedom of information on the Internet. I just love Tor, especially Tor VM that works great!<br />
I am an English teacher living in Iran. I have a BA in Farsi language and Literature from Yazd University. If you need any help with your Farsi translation, please do not hesitate to let me know. I would be glad to be of any assistance and play a small role in Tor success.<br />
Kind regards<br />
Mori</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-731"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-731" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-716" class="permalink" rel="bookmark">Farsi translation</a> by <span>Mori (not verified)</span></p>
    <a href="#comment-731">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-731" class="permalink" rel="bookmark">Great!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We'd love more Farsi translation.  You can read more about how to help at <a href="https://www.torproject.org/translation-overview.html.en" rel="nofollow">https://www.torproject.org/translation-overview.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-722"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-722" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Aleph0 (not verified)</span> said:</p>
      <p class="date-time">February 25, 2009</p>
    </div>
    <a href="#comment-722">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-722" class="permalink" rel="bookmark">MapAddress for domains?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would it be non-trivial to have the MapAddress option behave like TrackHostExits?<br />
That is, if an entry is like<br />
MapAddress .rapidshare.com .rapidshare.com.exitpoint.exit<br />
have it applied to rs1xx.rapidshare.com, rs223dt.rapidshare.com, rs28tl2.rapidshare.com... </p>
<p>I already have such a line in my TORRC file; on start TOR doesn't complain, but apparently it doesn't have any effect. The alternative would be to add 5000-odd MapAddress lines specifying every possible hostname given Rapidshare's current naming convention, but I don't know how TOR would handle it; my connections drop (flaky company proxy) often enough already...</p>
<p>I already have the TrackHostExits enabled for both rapidshare.com and .rapidshare.com, so when rapidshare.com redirects me to whatever.rapidshare.com TOR will reuse the existing circuit; however, if the connection drops and it takes longer than TrackHostExitsExpire to reestablish, TOR will open a new circuit with a different exit point (and Rapidshare will claim I've shared my account and lock it...  T_T  ).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-732"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-732" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-722" class="permalink" rel="bookmark">MapAddress for domains?</a> by <span>Aleph0 (not verified)</span></p>
    <a href="#comment-732">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-732" class="permalink" rel="bookmark">fine feature request</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this is a fine feature request for <a href="https://bugs.torproject.org/" rel="nofollow">https://bugs.torproject.org/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-722" class="permalink" rel="bookmark">MapAddress for domains?</a> by <span>Aleph0 (not verified)</span></p>
    <a href="#comment-1928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1928" class="permalink" rel="bookmark">How to block the fuc....</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How to block the fuc.... rapidshare traffic? I'm a exit node.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
