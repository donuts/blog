title: Moving Tor to a datagram transport
---
pub_date: 2011-11-07
---
author: sjmurdoch
---
tags:

tls
performance improvements
datagram
---
_html_body:

<p>Tor currently transports data over encrypted TLS tunnels between nodes, in turn carried by TCP. This has worked reasonably well, but recent research has shown that it might not be the best option, particularly for performance.</p>

<p>For example, when a packet gets dropped or corrupted on a link between two Tor nodes, TCP will cause the packet to be retransmitted eventually. However, in the meantime, all circuits passing through this pair of nodes will be stalled, not only the circuit corresponding to the packet which was dropped.</p>

<p>Also, Tor uses two levels of congestion control; TCP for hop-by-hop links, and a custom scheme for circuits. This might not be the right approach -- maybe these schemes aren't the right ones to use or maybe there should only be one level of congestion control.</p>

<p>There have been a variety of solutions proposed to fix one or both of these problems, Most end up sending data as datagrams in UDP packets, and Tor is responsible for managing congestion and recovering from packet loss, and so can (hopefully) do so in a more intelligent way. However, there are many options for what architecture to use, what building blocks to build these schemes from, and how to tweak the many parameters that result.</p>

<p>To help clarify the various options, I've written a summary of both Tor's current network stack architecture and various proposals for how it can be improved. The document discusses various tradeoffs between the approaches, including performance, security, and engineering difficulties in deploying the solution. It ends with some provisional conclusions about which options are most promising.</p>

<p>The document, “<a href="http://www.cl.cam.ac.uk/~sjm217/papers/tor11datagramcomparison.pdf" rel="nofollow">Comparison of Tor Datagram Designs</a>”, is now available (with source in <a href="https://gitweb.torproject.org/sjm217/torspec.git/tree/datagram_comparison:/proposals/ideas/xxx-datagram-comparison" rel="nofollow">git</a>), and I would welcome comments. Over the next few months I'll be working on building and evaluating prototypes for these schemes, and I will be incorporating feedback from the Tor community into the process.</p>

---
_comments:

<a id="comment-12576"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12576" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2011</p>
    </div>
    <a href="#comment-12576">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12576" class="permalink" rel="bookmark">Please have a chat with Bram</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please have a chat with Bram Cohen for his experiences designing robust high-throughput datagram transports.</p>
<p>-andy</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12596"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12596" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sjmurdoch
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sjmurdoch said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12576" class="permalink" rel="bookmark">Please have a chat with Bram</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12596">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12596" class="permalink" rel="bookmark">Indeed, I have chatted with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed, I have chatted with Bram and he had a number of good suggestions which I have yet to integrate into the report.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12577" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2011</p>
    </div>
    <a href="#comment-12577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12577" class="permalink" rel="bookmark">If tor would also support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If tor would also support UDP it would be able to exploit UDP hole punching techniques, already used with VoIP ICE/RTP, thus exposing Tor relay otherwise unreachable directly on their IP address.</p>
<p>-naif</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12597"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12597" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sjmurdoch
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sjmurdoch said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12577" class="permalink" rel="bookmark">If tor would also support</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12597">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12597" class="permalink" rel="bookmark">Yes, it should allow that,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, it should allow that, although needing a rendezvous service to coordinate would have some anonymity consequences so needs careful analysis.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12578" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2011</p>
    </div>
    <a href="#comment-12578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12578" class="permalink" rel="bookmark">Interesting paper,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting paper, especially if changing the transport protocol will allow tor to carry UDP traffic in the future. Personally, this is one of the features I miss most about tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12598"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12598" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sjmurdoch
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sjmurdoch said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12578" class="permalink" rel="bookmark">Interesting paper,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12598">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12598" class="permalink" rel="bookmark">Some of the candidate</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some of the candidate architectures allow Tor to carry UDP, but these are the ones which are the most challenging to build and with the most uncertainty as to the performance impact (positive or negative). Right now, my inclination is to not aim to carry UDP traffic, but this is still to be finalised.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12590"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12590" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2011</p>
    </div>
    <a href="#comment-12590">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12590" class="permalink" rel="bookmark">An absolute must, imho. But</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>An absolute must, imho. But not simply as an either TCP or UDP. Some things are best done with UDP, and the questions for Tor will be which things are those, and how will they be done?</p>
<p>- julie</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12591"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12591" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
    <a href="#comment-12591">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12591" class="permalink" rel="bookmark">After realization of that,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After realization of that, will it be able to use SIP and etc. VoiP-clients through Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12599" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sjmurdoch
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sjmurdoch said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12591" class="permalink" rel="bookmark">After realization of that,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12599" class="permalink" rel="bookmark">If Tor can carry UDP, then</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If Tor can carry UDP, then yes, but not all proposed architectures support this. See my answer to <a href="https://blog.torproject.org/blog/moving-tor-datagram-transport#comment-12598" rel="nofollow">a previous comment</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12593"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12593" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
    <a href="#comment-12593">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12593" class="permalink" rel="bookmark">Isn&#039;t there another way to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Isn't there another way to deal with the dropped packets (dont retransmits happen almost instantly anyway?).<br />
UDP will make it easier to detect and block tor.<br />
Some ISPs and server datacenters also limit UDP speeds, I know OVH do this.<br />
Most company and university firewalls block UDP.<br />
Also UDP has no congestion control, and doing it at both ends of the circuit doesn't work because the ISP routers on every hop in between the two nodes cant do congestion control and so will need to drop packets.<br />
This will make tor slower by wasting the bandwidth of the sender/uploader by retransmitting packets.<br />
I know from using freenet that 10% or so of my bandwidth is wasted retransmitting dropped packets.</p>
<p>Have you though about splitting the circuits over multiple TCP connections? I don't mean 1 connection per circuit, do the same combining the circuits into one TCP connection except do it over a few connections, this should improve speed also.<br />
If a connection stalls you can then retransmit over one of the other connections.</p>
<p>If Tor is going to start using UDP please at least make it optional.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12600"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12600" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sjmurdoch
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sjmurdoch said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12593" class="permalink" rel="bookmark">Isn&#039;t there another way to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12600">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12600" class="permalink" rel="bookmark">For some time, Tor nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For some time, Tor nodes will support both UDP and TCP so they can communicate with older versions of Tor, and I expect it will also be optional. For the first hop, we may permanently need to support TCP to resist blocking.</p>
<p>Whatever option we pick, emphasis will be put on choosing good congestion control, both for end-to-end and hop-by-hop. TCP isn't that great because it signals congestion through packet loss. Other alternatives being considered, such as uTP, detect congestion before packet loss by monitoring latency.</p>
<p>Yes, there is a continuum between 1 TCP session between pairs of hosts and 1 TCP session per circuit, so this option could be explored.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12602"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12602" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2011</p>
    </div>
    <a href="#comment-12602">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12602" class="permalink" rel="bookmark">Interesting paper.  If the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting paper.  If the tor network migrated to UDP, would there be native support for UDP applications, such as DNS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2011</p>
    </div>
    <a href="#comment-12747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12747" class="permalink" rel="bookmark">What about SCTP ? It&#039;s rare</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about SCTP ? It's rare but more likely to work in constrained environment.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12874"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12874" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2011</p>
    </div>
    <a href="#comment-12874">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12874" class="permalink" rel="bookmark">Hi.
Why we need to download</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi.<br />
Why we need to download 20 meg for any updating release of Tor?<br />
is it possible for you to do something that we only download few updated kilobites rather than the whole program ?<br />
thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16062"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16062" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 09, 2012</p>
    </div>
    <a href="#comment-16062">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16062" class="permalink" rel="bookmark">any update?, can i transmit</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>any update?, can i transmit udp over tor network now?</p>
</div>
  </div>
</article>
<!-- Comment END -->
