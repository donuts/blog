title: New alpha release: Tor 0.4.5.2-alpha
---
pub_date: 2020-11-23
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.5.2-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release by mid-December.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.5.2-alpha is the second alpha release in the 0.4.5.x series. It fixes several bugs present in earlier releases, including one that made it impractical to run relays on Windows. It also adds a few small safety features to improve Tor's behavior in the presence of strange compile-time options, misbehaving proxies, and future versions of OpenSSL.</p>
<h2>Changes in version 0.4.5.2-alpha - 2020-11-23</h2>
<ul>
<li>Major bugfixes (relay, windows):
<ul>
<li>Fix a bug in our implementation of condition variables on Windows. Previously, a relay on Windows would use 100% CPU after running for some time. Because of this change, Tor now require Windows Vista or later to build and run. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/30187">30187</a>; bugfix on 0.2.6.3-alpha. (This bug became more serious in 0.3.1.1-alpha with the introduction of consensus diffs.) Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (compilation):
<ul>
<li>Disable deprecation warnings when building with OpenSSL 3.0.0 or later. There are a number of APIs newly deprecated in OpenSSL 3.0.0 that Tor still requires. (A later version of Tor will try to stop depending on these APIs.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40165">40165</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (protocol, proxy support, defense in depth):
<ul>
<li>Respond more deliberately to misbehaving proxies that leave leftover data on their connections, so as to make Tor even less likely to allow the proxies to pass their data off as having come from a relay. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40017">40017</a>.</li>
</ul>
</li>
<li>Minor features (safety):
<ul>
<li>Log a warning at startup if Tor is built with compile-time options that are likely to make it less stable or reliable. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/18888">18888</a>.</li>
</ul>
</li>
<li>Minor bugfixes (circuit, handshake):
<ul>
<li>In the v3 handshaking code, use connection_or_change_state() to change the state. Previously, we changed the state directly, but this did not pass the state change to the pubsub or channel objects, potentially leading to bugs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32880">32880</a>; bugfix on 0.2.3.6-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Use the correct 'ranlib' program when building libtor.a. Previously we used the default ranlib, which broke some kinds of cross-compilation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40172">40172</a>; bugfix on 0.4.5.1-alpha.</li>
<li>Remove a duplicate typedef in metrics_store.c. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40177">40177</a>; bugfix on 0.4.5.1-alpha.</li>
<li>When USDT tracing is enabled, and STAP_PROBEV() is missing, don't attempt to build. Linux supports that macro but not the BSDs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40174">40174</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>Exit Tor on a misconfiguration when the Bridge line is configured to use a transport but no corresponding ClientTransportPlugin can be found. Prior to this fix, Tor would attempt to connect to the bridge directly without using the transport, making it easier for adversaries to notice the bridge. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/25528">25528</a>; bugfix on 0.2.6.1-alpha.</li>
<li>Fix an issue where an ORPort was compared with other kinds of ports, when it should have been only checked against other ORPorts. This bug would lead to "DirPort auto" getting ignored. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40195">40195</a>; bugfix on 0.4.5.1-alpha.</li>
<li>Fix a bug where a second non-ORPort with a variant family (ex: SocksPort [::1]:9050) would be ignored due to a configuration parsing error. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40183">40183</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (crash, relay, signing key):
<ul>
<li>Avoid assertion failures when we run Tor from the command line with `--key-expiration sign`, but an ORPort is not set. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40015">40015</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Remove trailing whitespace from control event log messages. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32178">32178</a>; bugfix on 0.1.1.1-alpha. Based on a patch by Amadeusz Pawlik.</li>
<li>Turn warning-level log message about SENDME failure into a debug- level message. (This event can happen naturally, and is no reason for concern). Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40142">40142</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay, address discovery):
<ul>
<li>Don't trigger an IP change when no new valid IP can be found. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40071">40071</a>; bugfix on 0.4.5.1-alpha.</li>
<li>When attempting to discover our IP, use a simple test circuit, rather than a descriptor fetch: the same address information is present in NETINFO cells, and is better authenticated there. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40071">40071</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix the `config/parse_tcp_proxy_line` test so that it works correctly on systems where the DNS provider hijacks invalid queries. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.4.3.1-alpha.</li>
<li>Fix unit tests that used newly generated list of routers so that they check them with respect to the date when they were generated, not with respect to the current time. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40187">40187</a>; bugfix on 0.4.5.1-alpha.</li>
<li>Fix our Python reference-implementation for the v3 onion service handshake so that it works correctly with the version of hashlib provided by Python 3.9. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.3.1.6-rc.</li>
<li>Fix the `tortls/openssl/log_one_error` test to work with OpenSSL 3.0.0. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40170">40170</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Removed features (controller):
<ul>
<li>Remove the "GETINFO network-status" controller command. It has been deprecated since 0.3.1.1-alpha. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/22473">22473</a>.</li>
</ul>
</li>
<p>Tor 0.4.5.2-alpha is the second alpha release in the 0.4.5.x series. It fixes several bugs present in earlier releases, including one that made it impractical to run relays on Windows. It also adds a few small safety features to improve Tor's behavior in the presence of strange compile-time options, misbehaving proxies, and future versions of OpenSSL.</p>
<h2>Changes in version 0.4.5.2-alpha - 2020-11-23</h2>
<ul>
<li>Major bugfixes (relay, windows):
<ul>
<li>Fix a bug in our implementation of condition variables on Windows. Previously, a relay on Windows would use 100% CPU after running for some time. Because of this change, Tor now require Windows Vista or later to build and run. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/30187">30187</a>; bugfix on 0.2.6.3-alpha. (This bug became more serious in 0.3.1.1-alpha with the introduction of consensus diffs.) Patch by Daniel Pinto.
  </li></ul>
</li><li>Minor features (compilation):
<ul>
<li>Disable deprecation warnings when building with OpenSSL 3.0.0 or later. There are a number of APIs newly deprecated in OpenSSL 3.0.0 that Tor still requires. (A later version of Tor will try to stop depending on these APIs.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40165">40165</a>.
  </li></ul>
</li></ul>
<p> </p>
<!--break--><ul>
<li>Minor features (protocol, proxy support, defense in depth):
<ul>
<li>Respond more deliberately to misbehaving proxies that leave leftover data on their connections, so as to make Tor even less likely to allow the proxies to pass their data off as having come from a relay. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40017">40017</a>.
  </li></ul>
</li><li>Minor features (safety):
<ul>
<li>Log a warning at startup if Tor is built with compile-time options that are likely to make it less stable or reliable. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/18888">18888</a>.
  </li></ul>
</li><li>Minor bugfixes (circuit, handshake):
<ul>
<li>In the v3 handshaking code, use connection_or_change_state() to change the state. Previously, we changed the state directly, but this did not pass the state change to the pubsub or channel objects, potentially leading to bugs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32880">32880</a>; bugfix on 0.2.3.6-alpha. Patch by Neel Chauhan.
  </li></ul>
</li><li>Minor bugfixes (compilation):
<ul>
<li>Use the correct 'ranlib' program when building libtor.a. Previously we used the default ranlib, which broke some kinds of cross-compilation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40172">40172</a>; bugfix on 0.4.5.1-alpha.
</li><li>Remove a duplicate typedef in metrics_store.c. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40177">40177</a>; bugfix on 0.4.5.1-alpha.
</li><li>When USDT tracing is enabled, and STAP_PROBEV() is missing, don't attempt to build. Linux supports that macro but not the BSDs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40174">40174</a>; bugfix on 0.4.5.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (configuration):
<ul>
<li>Exit Tor on a misconfiguration when the Bridge line is configured to use a transport but no corresponding ClientTransportPlugin can be found. Prior to this fix, Tor would attempt to connect to the bridge directly without using the transport, making it easier for adversaries to notice the bridge. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/25528">25528</a>; bugfix on 0.2.6.1-alpha.
</li><li>Fix an issue where an ORPort was compared with other kinds of ports, when it should have been only checked against other ORPorts. This bug would lead to "DirPort auto" getting ignored. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40195">40195</a>; bugfix on 0.4.5.1-alpha.
</li><li>Fix a bug where a second non-ORPort with a variant family (ex: SocksPort [::1]:9050) would be ignored due to a configuration parsing error. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40183">40183</a>; bugfix on 0.4.5.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (crash, relay, signing key):
<ul>
<li>Avoid assertion failures when we run Tor from the command line with `--key-expiration sign`, but an ORPort is not set. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40015">40015</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chauhan.
  </li></ul>
</li><li>Minor bugfixes (logging):
<ul>
<li>Remove trailing whitespace from control event log messages. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32178">32178</a>; bugfix on 0.1.1.1-alpha. Based on a patch by Amadeusz Pawlik.
</li><li>Turn warning-level log message about SENDME failure into a debug- level message. (This event can happen naturally, and is no reason for concern). Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40142">40142</a>; bugfix on 0.4.1.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (relay, address discovery):
<ul>
<li>Don't trigger an IP change when no new valid IP can be found. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40071">40071</a>; bugfix on 0.4.5.1-alpha.
</li><li>When attempting to discover our IP, use a simple test circuit, rather than a descriptor fetch: the same address information is present in NETINFO cells, and is better authenticated there. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40071">40071</a>; bugfix on 0.4.5.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (testing):
<ul>
<li>Fix the `config/parse_tcp_proxy_line` test so that it works correctly on systems where the DNS provider hijacks invalid queries. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.4.3.1-alpha.
</li><li>Fix unit tests that used newly generated list of routers so that they check them with respect to the date when they were generated, not with respect to the current time. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40187">40187</a>; bugfix on 0.4.5.1-alpha.
</li><li>Fix our Python reference-implementation for the v3 onion service handshake so that it works correctly with the version of hashlib provided by Python 3.9. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.3.1.6-rc.
</li><li>Fix the `tortls/openssl/log_one_error` test to work with OpenSSL 3.0.0. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40170">40170</a>; bugfix on 0.2.8.1-alpha.
  </li></ul>
</li><li>Removed features (controller):
<ul>
<li>Remove the "GETINFO network-status" controller command. It has been deprecated since 0.3.1.1-alpha. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/22473">22473</a>.
  </li></ul>
</li></ul>
</ul>

---
_comments:

<a id="comment-290486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2020</p>
    </div>
    <a href="#comment-290486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290486" class="permalink" rel="bookmark">Hey could someone look into…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey could someone look into this? I got this since recently and this never happened before! Using latest stable version of OS.</p>
<p>&gt; apt update</p>
<blockquote><p>
Err: <a href="https://deb.torproject.org/torproject.org" rel="nofollow">https://deb.torproject.org/torproject.org</a> buster InRelease<br />
  The following signatures were invalid: EXPKEYSIG 74A941BA219EC810 deb.torproject.org archive signing key<br />
W: Failed to fetch <a href="https://deb.torproject.org/torproject.org/dists/buster/InRelease" rel="nofollow">https://deb.torproject.org/torproject.org/dists/buster/InRelease</a>  The following signatures were invalid: EXPKEYSIG 74A941BA219EC810 deb.torproject.org archive signing key
</p></blockquote>
<p>&gt; dpkg --list|grep deb.torproject.org-keyring</p>
<blockquote><p>
Already have v2018.08.06
</p></blockquote>
<p>&gt; apt-key adv --keyserver keyserver.ubuntu.com --recv-key 74A941BA219EC810</p>
<blockquote><p>
Executing: /000.sh --keyserver keyserver.ubuntu.com --recv-key 74A941BA219EC810<br />
gpg: key EE8CBC9E886DDD89: "deb.torproject.org archive signing key" not changed<br />
gpg: Total number processed: 1<br />
gpg:              unchanged: 1
</p></blockquote>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290580"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290580" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290486" class="permalink" rel="bookmark">Hey could someone look into…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290580">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290580" class="permalink" rel="bookmark">Your log says EXPKEYSIG,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your log says EXPKEYSIG, short for "expired key signature". That means your copy of key 0x74A941BA219EC810 is expired. The package, <span class="geshifilter"><code class="php geshifilter-php">deb<span style="color: #339933;">.</span>torproject<span style="color: #339933;">.</span>org<span style="color: #339933;">-</span>keyring</code></span>, is hosted on deb.torproject.org whose file listings your <span class="geshifilter"><code class="php geshifilter-php">apt</code></span> is unable to import because of the expired key. Being unable to import it, your dpkg thinks your 2018 copy of the key is up to date. In the repository, its version is no longer v2018.08.06 but <a href="https://deb.torproject.org/torproject.org/dists/buster/main/binary-amd64/Packages" rel="nofollow">2020.11.18</a>.</p>
<p>Your final command, <span class="geshifilter"><code class="php geshifilter-php">apt<span style="color: #339933;">-</span><a href="http://www.php.net/key"><span style="color: #990000;">key</span></a> adv <span style="color: #339933;">...</span></code></span>, should have updated your copy of the key, but the output doesn't mention new signatures on the key. Tor Project has been saving their keys to torproject.org servers since a key-signature flooding/poisoning attack in 2019. If it isn't the fault of something on your end, it's also possible that they could have not been sending it to the keyservers lately, but they might have done so after you wrote your comment. Try to update the key now, and then run <span class="geshifilter"><code class="php geshifilter-php">apt update</code></span>. The copy of the key on <a href="https://keyserver.ubuntu.com/pks/lookup?search=0x74A941BA219EC810&amp;fingerprint=on&amp;op=index" rel="nofollow">keyserver.ubuntu.com</a> at the time I write this appears to expire on 2022-06-11.</p>
<p>Full instructions are here: <a href="https://support.torproject.org/apt/tor-deb-repo/" rel="nofollow">https://support.torproject.org/apt/tor-deb-repo/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290603"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290603" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>debianusr (not verified)</span> said:</p>
      <p class="date-time">December 05, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290580" class="permalink" rel="bookmark">Your log says EXPKEYSIG,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290603">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290603" class="permalink" rel="bookmark">i also used the deb…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i also used the deb.torproject.org-keyring package but apt broke. </p>
<blockquote><p>tor://sdscoq7snqtznauu.onion/torproject.org/dists/bullseye/InRelease failed: EXPKEYSIG 74A941BA219EC810 deb.torproject.org archive signing key</p></blockquote>
<p>Manually fixed by:<br />
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">torsocks wget <span style="color: #339933;">&lt;</span>a href<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;http://sdscoq7snqtznauu.onion/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc&quot;</span> rel<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;nofollow&quot;</span><span style="color: #339933;">&gt;</span>http<span style="color: #339933;">:</span><span style="color: #666666; font-style: italic;">//sdscoq7snqtznauu.onion/torproject.org/A3C4F0F979CAA22CDBA8F512EE…&lt;/a&gt;&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">sudo apt<span style="color: #339933;">-</span><a href="http://www.php.net/key"><span style="color: #990000;">key</span></a> add A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89<span style="color: #339933;">.</span>asc </div></li></ol></pre></div></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290625"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290625" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290603" class="permalink" rel="bookmark">i also used the deb…</a> by <span>debianusr (not verified)</span></p>
    <a href="#comment-290625">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290625" class="permalink" rel="bookmark">Yes, that&#039;s a valid…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, that's a valid modification of the <a href="https://support.torproject.org/apt/tor-deb-repo/" rel="nofollow">full instructions</a> linked earlier, and using torsocks would be a nice addition to the instructions. (<strong>Website team, I'm talking to you!</strong>) For readers following along, sdscoq7snqtznauu.onion is the <a href="https://onion.torproject.org/" rel="nofollow">official onion</a> of deb.torproject.org.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-290492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290492" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nice realease (not verified)</span> said:</p>
      <p class="date-time">November 23, 2020</p>
    </div>
    <a href="#comment-290492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290492" class="permalink" rel="bookmark">copy paste fail. the Changes…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>copy paste fail. the Changes In Version 0.4.5.2-Alpha - 2020-11-23 is duplicated</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290503"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290503" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="STUCK BOOTSTRAPPED 45%">STUCK BOOTSTRA… (not verified)</span> said:</p>
      <p class="date-time">November 25, 2020</p>
    </div>
    <a href="#comment-290503">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290503" class="permalink" rel="bookmark">Stuck at asking for relay…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Stuck at asking for relay descriptors. Was connecting before but anymore no matter trials. This for Linux connecting to H20 Att.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290592"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290592" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>cypherpunks (not verified)</span> said:</p>
      <p class="date-time">December 02, 2020</p>
    </div>
    <a href="#comment-290592">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290592" class="permalink" rel="bookmark">&gt; Exit Tor on a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Exit Tor on a misconfiguration when the Bridge line is configured to use a transport but no corresponding ClientTransportPlugin can be found.</p>
<p>$ cat torrc<br />
ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy<br />
Bridge obfs4 192.95.36.142:443 CDF2E852BF539B82BD10E27E9115A31734E378C2<br />
UseBridges 1</p>
<p>$ tor -f torrc --verify-config<br />
[notice] Tor 0.4.5.2-alpha running on Linux with Libevent 2.1.12-stable, OpenSSL 1.1.1h, Zlib 1.2.11, Liblzma 5.2.4, Libzstd 1.4.5 and Glibc 2.31 as libc.<br />
[...]<br />
[warn] Bridge line with transport obfs4 is missing a ClientTransportPlugin line<br />
[err] set_options(): Bug: Acting on config options left us in a broken state. Dying. (on Tor 0.4.5.2-alpha )<br />
[err] Reading config failed--see warnings above.</p>
<p>This also breaks the default Tor Browser configuration<br />
<a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/PTConfigs/linux/torrc-defaults-appendix" rel="nofollow">https://gitweb.torproject.org/builders/tor-browser-build.git/plain/proj…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290601"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290601" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290592" class="permalink" rel="bookmark">&gt; Exit Tor on a…</a> by <span>cypherpunks (not verified)</span></p>
    <a href="#comment-290601">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290601" class="permalink" rel="bookmark">Note about the bridge: It&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Note about the bridge: It's one of the default bridges, so it is effectively already public.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290594"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290594" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2020</p>
    </div>
    <a href="#comment-290594">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290594" class="permalink" rel="bookmark">&gt; Because of this change,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Because of this change, Tor now require Windows Vista or later to build and run.<br />
Could you make such changes AFTER the next LTS tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290623"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290623" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290594" class="permalink" rel="bookmark">&gt; Because of this change,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290623">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290623" class="permalink" rel="bookmark">For relay operators and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For the convenience of relay operators and package maintainers, here is LTS information from the <a href="https://gitlab.torproject.org/tpo/core/tor" rel="nofollow">ChangeLog</a> (ReleaseNotes doesn't include alphas):</p>
<blockquote><p>Per our support policy, we support each stable release series for nine months after its first stable release, or three months after the first stable release of the next series: whichever is longer. This means that 0.4.4.x will be supported until around June 2021--or later, if 0.4.5.x is later than anticipated.</p>
<p>Note also that support for 0.4.2.x has just ended; support for 0.4.3.x will continue until Feb 15, 2021. We still plan to continue supporting <strong>0.3.5.x, our long-term stable series, until Feb 2022.</strong></p></blockquote>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290616"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290616" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>CheckUrMailbox! (not verified)</span> said:</p>
      <p class="date-time">December 07, 2020</p>
    </div>
    <a href="#comment-290616">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290616" class="permalink" rel="bookmark">[Moderator: Fifth attempt to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[Moderator: Fifth attempt to post.  TP is asking for money but your mail address is not working!]</p>
<p>Tor Project is in the middle of a funding drive with time limited matching grant. The Board of Directors needs to immediately look into this issue:</p>
<p>USPS is claiming that the mailing address given in your website</p>
<p><a href="https://www.torproject.org/contact/" rel="nofollow">https://www.torproject.org/contact/</a></p>
<p>&gt; Send us Mail<br />
&gt;<br />
&gt; The Tor Project<br />
&gt; 217 1st Ave South #4903<br />
&gt; Seattle, WA 98194 USA</p>
<p>is invalid.  They claim someone apparently asked the private mailbox company at that street address to forward mail to a second address, but this expired more than a year ago so USPS is no longer honoring the forwarding address.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bizarre (not verified)</span> said:</p>
      <p class="date-time">December 16, 2020</p>
    </div>
    <a href="#comment-290739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290739" class="permalink" rel="bookmark">I compiled this version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I compiled this version using Libevent 2.1.12-stable, OpenSSL 1.1.1h, and zlib 1.2.11.<br />
The moment I run Tor with Bridge enabled, I receive the error below:</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Dec <span style="color: #cc66cc;">16</span> <span style="color: #cc66cc;">18</span><span style="color: #339933;">:</span><span style="color: #cc66cc;">22</span><span style="color: #339933;">:</span><span style="color:#800080;">30.447</span> <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> Problem bootstrapping<span style="color: #339933;">.</span> Stuck at <span style="color: #cc66cc;">10</span><span style="color: #339933;">%</span> <span style="color: #009900;">&#40;</span>conn_done<span style="color: #009900;">&#41;</span><span style="color: #339933;">:</span> Connected to a relay<span style="color: #339933;">.</span> <span style="color: #009900;">&#40;</span>TLS_ERROR<span style="color: #339933;">;</span> TLS_ERROR<span style="color: #339933;">;</span> <a href="http://www.php.net/count"><span style="color: #990000;">count</span></a> <span style="color: #cc66cc;">4</span><span style="color: #339933;">;</span> recommendation warn<span style="color: #339933;">;</span> host 22DEB9BEA09A1EF652777DFD987405D9314596C9 at 185<span style="color: #339933;">.</span>220<span style="color: #339933;">.</span>101<span style="color: #339933;">.</span>154<span style="color: #339933;">:</span><span style="color: #cc66cc;">19263</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Dec <span style="color: #cc66cc;">16</span> <span style="color: #cc66cc;">18</span><span style="color: #339933;">:</span><span style="color: #cc66cc;">22</span><span style="color: #339933;">:</span><span style="color:#800080;">30.448</span> <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> <span style="color: #cc66cc;">4</span> connections have failed<span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Dec <span style="color: #cc66cc;">16</span> <span style="color: #cc66cc;">18</span><span style="color: #339933;">:</span><span style="color: #cc66cc;">22</span><span style="color: #339933;">:</span><span style="color:#800080;">30.448</span> <span style="color: #009900;">&#91;</span>Warning<span style="color: #009900;">&#93;</span> <span style="color: #cc66cc;">4</span> connections died in state handshaking <span style="color: #009900;">&#40;</span>TLS<span style="color: #009900;">&#41;</span> with SSL state SSLv3<span style="color: #339933;">/</span>TLS write client hello in HANDSHAKE</div></li></ol></pre></div></p>
<p>I also had to edit some lines in the torrc file because it will not accept the previous settings.</p>
<p><strong>Old Settings</strong><br />
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Bridge obfs4 XX<span style="color: #339933;">.</span>XXX<span style="color: #339933;">.</span>XX<span style="color: #339933;">.</span>XXX<span style="color: #339933;">:</span>XXXXX EC4F9DA66F520A094E5B534AA08DFC1AB5E95B64 cert<span style="color: #339933;">=</span>OJJtSTddonrjXMCWGX97lIagsGtGiFnUI6t<span style="color: #339933;">/</span>OGFbKtpvWiFEfS0sLBnhLmHUENLoW1soeg iat<span style="color: #339933;">-</span>mode<span style="color: #339933;">=</span><span style="color: #cc66cc;">1</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">ClientTransportPlugin obfs2<span style="color: #339933;">,</span>obfs3<span style="color: #339933;">,</span>obfs4<span style="color: #339933;">,</span>scramblesuit <a href="http://www.php.net/exec"><span style="color: #990000;">exec</span></a> X<span style="color: #339933;">:</span>\PluggableTransports\obfs4proxy</div></li></ol></pre></div></p>
<p><strong>New Settings</strong><br />
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Bridge XX<span style="color: #339933;">.</span>XXX<span style="color: #339933;">.</span>XX<span style="color: #339933;">.</span>XXX<span style="color: #339933;">:</span>XXXXX EC4F9DA66F520A094E5B534AA08DFC1AB5E95B64<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">ClientTransportPlugin obfs4 <a href="http://www.php.net/exec"><span style="color: #990000;">exec</span></a> D<span style="color: #339933;">:</span>\OS1\Proxy\PluggableTransports\obfs4proxy</div></li></ol></pre></div></p>
<p>Even thought it's accepting the new lines, I'm still receiving the error above.</p>
<p>If i run Tor with Bridge disabled, I don't receive any error.</p>
<p>When I use 0.4.3.5, I don't receive any error. My OS is Windows 10 x64.<br />
I tried compiling 0.4.4.5 and 0.4.5.1-alpha, but they will not detect Libevent 2.1.12-stable.</p>
</div>
  </div>
</article>
<!-- Comment END -->
