title: New release candidate: Tor 0.4.5.4-rc
---
pub_date: 2021-01-22
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.5.4-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely around this coming Tuesday.</p>
<p>Tor 0.4.5.4-rc is the second release candidate in its series. It fixes several bugs present in previous releases.</p>
<p>We expect that the stable release will be the same, or almost the same, as this release candidate, unless serious bugs are found.</p>
<h2>Changes in version 0.4.5.4-rc - 2021-01-22</h2>
<ul>
<li>Major bugfixes (authority, IPv6):
<ul>
<li>Do not consider multiple relays in the same IPv6 /64 network to be sybils. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40243">40243</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (directory cache, performance, windows):
<ul>
<li>Limit the number of items in the consensus diff cache to 64 on Windows. We hope this will mitigate an issue where Windows relay operators reported Tor using 100% CPU, while we investigate better solutions. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/24857">24857</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor feature (build system):
<ul>
<li>New "make lsp" command to generate the compile_commands.json file used by the ccls language server. The "bear" program is needed for this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40227">40227</a>.</li>
</ul>
</li>
<li>Minor features (authority, logging):
<ul>
<li>Log more information for directory authority operators during the consensus voting process, and while processing relay descriptors. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40245">40245</a>.</li>
<li>Reject obsolete router/extrainfo descriptors earlier and more quietly, to avoid spamming the logs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40238">40238</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix another warning about unreachable fallthrough annotations when building with "--enable-all-bugs-are-fatal" on some compilers. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40241">40241</a>; bugfix on 0.4.5.3-rc.</li>
<li>Change the linker flag ordering in our library search code so that it works for compilers that need the libraries to be listed in the right order. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33624">33624</a>; bugfix on 0.1.1.0-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (config, bridge):
<ul>
<li>Don't initiate a connection to a bridge configured to use a missing transport. This change reverts an earlier fix that would try to avoid such situations during configuration chcecking, but which doesn't work with DisableNetwork. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40106">40106</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Avoid a non-fatal assertion in certain edge-cases when establishing a circuit to an onion service. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/32666">32666</a>; bugfix on 0.3.0.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>If we were unable to build our descriptor, don't mark it as having been advertised. Also remove an harmless BUG(). Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40231">40231</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
</ul>

