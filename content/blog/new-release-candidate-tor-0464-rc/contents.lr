title: New release candidate: Tor 0.4.6.4-rc
---
pub_date: 2021-05-28
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.6.4-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely next week.</p>
<p>Remember, this is a not a stable release yet: but we still hope that people will try it out and look for bugs before the official stable release comes out in June.</p>
<p>Tor 0.4.6.4-rc fixes a few bugs from previous releases. This, we hope, the final release candidate in its series: unless major new issues are found, the next release will be stable.</p>
<h2>Changes in version 0.4.6.4-rc - 2021-05-28</h2>
<ul>
<li>Minor features (compatibility):
<ul>
<li>Remove an assertion function related to TLS renegotiation. It was used nowhere outside the unit tests, and it was breaking compilation with recent alpha releases of OpenSSL 3.0.0. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40399">40399</a>.</li>
</ul>
</li>
<li>Minor bugfixes (consensus handling):
<ul>
<li>Avoid a set of bugs that could be caused by inconsistently preferring an out-of-date consensus stored in a stale directory cache over a more recent one stored on disk as the latest consensus. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40375">40375</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (control, sandbox):
<ul>
<li>Allow the control command SAVECONF to succeed when the seccomp sandbox is enabled, and make SAVECONF keep only one backup file to simplify implementation. Previously SAVECONF allowed a large number of backup files, which made it incompatible with the sandbox. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40317">40317</a>; bugfix on 0.2.5.4-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (metrics port):
<ul>
<li>Fix a bug that made tor try to re-bind() on an already open MetricsPort every 60 seconds. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40370">40370</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove unneeded code for parsing private keys in directory documents. This code was only used for client authentication in v2 onion services, which are now unsupported. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40374">40374</a>.</li>
</ul>
</li>
</ul>

