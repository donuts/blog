title: New Release: Tor 0.4.4.5
---
pub_date: 2020-09-15
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>After months of work, we have a new stable release series!<br />
If you build Tor from source, you can download the source<br />
code for 0.4.4.5 on the<br />
<a href="https://www.torproject.org/download/tor/">download page</a>.<br />
Packages should be available within the next several weeks, with a new Tor Browser by some time next week.</p>
<p>Tor 0.4.4.5 is the first stable release in the 0.4.4.x series. This series improves our guard selection algorithms, adds v3 onion balance support, improves the amount of code that can be disabled when running without relay support, and includes numerous small bugfixes and enhancements. It also lays the ground for some IPv6 features that we'll be developing more in the next (0.4.5) series.</p>
<p>Per our support policy, we support each stable release series for nine months after its first stable release, or three months after the first stable release of the next series: whichever is longer. This means that 0.4.4.x will be supported until around June 2021--or later, if 0.4.5.x is later than anticipated.</p>
<p>Note also that support for 0.4.2.x has just ended; support for 0.4.3 will continue until Feb 15, 2021. We still plan to continue supporting 0.3.5.x, our long-term stable series, until Feb 2022.</p>
<p>Below are the changes since 0.4.3.6-rc. For a complete list of changes since 0.4.4.4-rc, see the ChangeLog file.</p>
<h2>Changes in version 0.4.4.5 - 2020-09-15</h2>
<ul>
<li>Major features (Proposal 310, performance + security):
<ul>
<li>Implements Proposal 310, "Bandaid on guard selection". Proposal 310 solves load-balancing issues with older versions of the guard selection algorithm, and improves its security. Under this new algorithm, a newly selected guard never becomes Primary unless all previously sampled guards are unreachable. Implements recommendation from 32088. (Proposal 310 is linked to the CLAPS project researching optimal client location-aware path selections. This project is a collaboration between the UCLouvain Crypto Group, the U.S. Naval Research Laboratory, and Princeton University.)
  </li></ul>
</li><li>Major features (fallback directory list):
<ul>
<li>Replace the 148 fallback directories originally included in Tor 0.4.1.4-rc (of which around 105 are still functional) with a list of 144 fallbacks generated in July 2020. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40061">40061</a>.
  </li></ul>
</li></ul>
<p> </p>
<!--break--><ul>
<li>Major features (IPv6, relay):
<ul>
<li>Consider IPv6-only EXTEND2 cells valid on relays. Log a protocol warning if the IPv4 or IPv6 address is an internal address, and internal addresses are not allowed. But continue to use the other address, if it is valid. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33817">33817</a>.
</li><li>If a relay can extend over IPv4 and IPv6, and both addresses are provided, it chooses between them uniformly at random. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33817">33817</a>.
</li><li>Re-use existing IPv6 connections for circuit extends. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33817">33817</a>.
</li><li>Relays may extend circuits over IPv6, if the relay has an IPv6 ORPort, and the client supplies the other relay's IPv6 ORPort in the EXTEND2 cell. IPv6 extends will be used by the relay IPv6 ORPort self-tests in 33222. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33817">33817</a>.
  </li></ul>
</li><li>Major features (v3 onion services):
<ul>
<li>Allow v3 onion services to act as OnionBalance backend instances, by using the HiddenServiceOnionBalanceInstance torrc option. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32709">32709</a>.
  </li></ul>
</li><li>Major bugfixes (NSS):
<ul>
<li>When running with NSS enabled, make sure that NSS knows to expect nonblocking sockets. Previously, we set our TCP sockets as nonblocking, but did not tell NSS, which in turn could lead to unexpected blocking behavior. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40035">40035</a>; bugfix on 0.3.5.1-alpha.
  </li></ul>
</li><li>Major bugfixes (onion services, DoS):
<ul>
<li>Correct handling of parameters for the onion service DoS defense. Previously, the consensus parameters for the onion service DoS defenses were overwriting the parameters set by the service operator using HiddenServiceEnableIntroDoSDefense. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40109">40109</a>; bugfix on 0.4.2.1-alpha.
  </li></ul>
</li><li>Major bugfixes (stats, onion services):
<ul>
<li>Fix a bug where we were undercounting the Tor network's total onion service traffic, by ignoring any traffic originating from clients. Now we count traffic from both clients and services. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40117">40117</a>; bugfix on 0.2.6.2-alpha.
  </li></ul>
</li><li>Minor features (security):
<ul>
<li>Channels using obsolete versions of the Tor link protocol are no longer allowed to circumvent address-canonicity checks. (This is only a minor issue, since such channels have no way to set ed25519 keys, and therefore should always be rejected for circuits that specify ed25519 identities.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40081">40081</a>.
  </li></ul>
</li><li>Minor features (bootstrap reporting):
<ul>
<li>Report more detailed reasons for bootstrap failure when the failure happens due to a TLS error. Previously we would just call these errors "MISC" when they happened during read, and "DONE" when they happened during any other TLS operation. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32622">32622</a>.
  </li></ul>
</li><li>Minor features (client-only compilation):
<ul>
<li>Disable more code related to the ext_orport protocol when compiling without support for relay mode. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33368">33368</a>.
</li><li>Disable more of our self-testing code when support for relay mode is disabled. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33370">33370</a>.
</li><li>Most server-side DNS code is now disabled when building without support for relay mode. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33366">33366</a>.
  </li></ul>
</li><li>Minor features (code safety):
<ul>
<li>Check for failures of tor_inet_ntop() and tor_inet_ntoa() functions in DNS and IP address processing code, and adjust codepaths to make them less likely to crash entire Tor instances. Resolves issue <a href="https://bugs.torproject.org/tpo/core/tor/33788">33788</a>.
  </li></ul>
</li><li>Minor features (continuous integration):
<ul>
<li>Run unit-test and integration test (Stem, Chutney) jobs with ALL_BUGS_ARE_FATAL macro being enabled on Travis and Appveyor. Resolves ticket <a href="https://bugs.torproject.org/tpo/core/tor/32143">32143</a>.
  </li></ul>
</li><li>Minor features (control port):
<ul>
<li>If a ClientName was specified in ONION_CLIENT_AUTH_ADD for an onion service, display it when we use ONION_CLIENT_AUTH_VIEW. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40089">40089</a>. Patch by Neel Chauhan.
</li><li>Return a descriptive error message from the 'GETINFO status/fresh- relay-descs' command on the control port. Previously, we returned a generic error of "Error generating descriptor". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32873">32873</a>. Patch by Neel Chauhan.
  </li></ul>
</li><li>Minor features (defense in depth):
<ul>
<li>Wipe more data from connection address fields before returning them to the memory heap. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/6198">6198</a>.
  </li></ul>
</li><li>Minor features (denial-of-service memory limiter):
<ul>
<li>Allow the user to configure even lower values for the MaxMemInQueues parameter. Relays now enforce a minimum of 64 MB, when previously the minimum was 256 MB. On clients, there is no minimum. Relays and clients will both warn if the value is set so low that Tor is likely to stop working. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/24308">24308</a>.
  </li></ul>
</li><li>Minor features (developer tooling):
<ul>
<li>Add a script to help check the alphabetical ordering of option names in the manual page. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33339">33339</a>.
</li><li>Refrain from listing all .a files that are generated by the Tor build in .gitignore. Add a single wildcard *.a entry that covers all of them for present and future. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33642">33642</a>.
</li><li>Add a script ("git-install-tools.sh") to install git hooks and helper scripts. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33451">33451</a>.
  </li></ul>
</li><li>Minor features (directory authority):
<ul>
<li>Authorities now recommend the protocol versions that are supported by Tor 0.3.5 and later. (Earlier versions of Tor have been deprecated since January of this year.) This recommendation will cause older clients and relays to give a warning on startup, or when they download a consensus directory. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32696">32696</a>.
  </li></ul>
</li><li>Minor features (directory authority, shared random):
<ul>
<li>Refactor more authority-only parts of the shared-random scheduling code to reside in the dirauth module, and to be disabled when compiling with --disable-module-dirauth. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33436">33436</a>.
  </li></ul>
</li><li>Minor features (directory):
<ul>
<li>Remember the number of bytes we have downloaded for each directory purpose while bootstrapping, and while fully bootstrapped. Log this information as part of the heartbeat message. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32720">32720</a>.
  </li></ul>
</li><li>Minor features (entry guards):
<ul>
<li>Reinstate support for GUARD NEW/UP/DOWN control port events. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40001">40001</a>.
  </li></ul>
</li><li>Minor features (IPv6 support):
<ul>
<li>Adds IPv6 support to tor_addr_is_valid(). Adds tests for the above changes and tor_addr_is_null(). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33679">33679</a>. Patch by MrSquanchee.
</li><li>Allow clients and relays to send dual-stack and IPv6-only EXTEND2 cells. Parse dual-stack and IPv6-only EXTEND2 cells on relays. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33901">33901</a>.
  </li></ul>
</li><li>Minor features (linux seccomp2 sandbox, portability):
<ul>
<li>Allow Tor to build on platforms where it doesn't know how to report which syscall caused the linux seccomp2 sandbox to fail. This change should make the sandbox code more portable to less common Linux architectures. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34382">34382</a>.
</li><li>Permit the unlinkat() syscall, which some Libc implementations use to implement unlink(). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33346">33346</a>.
  </li></ul>
</li><li>Minor features (logging):
<ul>
<li>When trying to find our own address, add debug-level logging to report the sources of candidate addresses. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32888">32888</a>.
  </li></ul>
</li><li>Minor features (onion service client, SOCKS5):
<ul>
<li>Add 3 new SocksPort ExtendedErrors (F2, F3, F7) that reports back new type of onion service connection failures. The semantics of these error codes are documented in proposal 309. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32542">32542</a>.
  </li></ul>
</li><li>Minor features (onion service v3):
<ul>
<li>If a service cannot upload its descriptor(s), log why at INFO level. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33400">33400</a>; bugfix on 0.3.2.1-alpha.
  </li></ul>
</li><li>Minor features (python scripts):
<ul>
<li>Stop assuming that /usr/bin/python exists. Instead of using a hardcoded path in scripts that still use Python 2, use /usr/bin/env, similarly to the scripts that use Python 3. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33192">33192</a>; bugfix on 0.4.2.
  </li></ul>
</li><li>Minor features (testing, architecture):
<ul>
<li>Our test scripts now double-check that subsystem initialization order is consistent with the inter-module dependencies established by our .may_include files. Implements ticket <a href="https://bugs.torproject.org/tpo/core/tor/31634">31634</a>.
</li><li>Initialize all subsystems at the beginning of our unit test harness, to avoid crashes due to uninitialized subsystems. Follow- up from ticket <a href="https://bugs.torproject.org/tpo/core/tor/33316">33316</a>.
</li><li>Our "make check" target now runs the unit tests in 8 parallel chunks. Doing this speeds up hardened CI builds by more than a factor of two. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40098">40098</a>.
  </li></ul>
</li><li>Minor features (v3 onion services):
<ul>
<li>Add v3 onion service status to the dumpstats() call which is triggered by a SIGUSR1 signal. Previously, we only did v2 onion services. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/24844">24844</a>. Patch by Neel Chauhan.
  </li></ul>
</li><li>Minor features (windows):
<ul>
<li>Add support for console control signals like Ctrl+C in Windows. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/34211">34211</a>. Patch from Damon Harris (TheDcoder).
  </li></ul>
</li><li>Minor bugfixes (control port, onion service):
<ul>
<li>Consistently use 'address' in "Invalid v3 address" response to ONION_CLIENT_AUTH commands. Previously, we would sometimes say 'addr'. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40005">40005</a>; bugfix on 0.4.3.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (correctness, buffers):
<ul>
<li>Fix a correctness bug that could cause an assertion failure if we ever tried using the buf_move_all() function with an empty input buffer. As far as we know, no released versions of Tor do this. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40076">40076</a>; bugfix on 0.3.3.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (directory authorities):
<ul>
<li>Directory authorities now reject votes that arrive too late. In particular, once an authority has started fetching missing votes, it no longer accepts new votes posted by other authorities. This change helps prevent a consensus split, where only some authorities have the late vote. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/4631">4631</a>; bugfix on 0.2.0.5-alpha.
  </li></ul>
</li><li>Minor bugfixes (git scripts):
<ul>
<li>Stop executing the checked-out pre-commit hook from the pre-push hook. Instead, execute the copy in the user's git directory. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33284">33284</a>; bugfix on 0.4.1.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (initialization):
<ul>
<li>Initialize the subsystems in our code in an order more closely corresponding to their dependencies, so that every system is initialized before the ones that (theoretically) depend on it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33316">33316</a>; bugfix on 0.4.0.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (IPv4, relay):
<ul>
<li>Check for invalid zero IPv4 addresses and ports when sending and receiving extend cells. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33900">33900</a>; bugfix on 0.2.4.8-alpha.
  </li></ul>
</li><li>Minor bugfixes (IPv6, relay):
<ul>
<li>Consider IPv6 addresses when checking if a connection is canonical. In 17604, relays assumed that a remote relay could consider an IPv6 connection canonical, but did not set the canonical flag on their side of the connection. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33899">33899</a>; bugfix on 0.3.1.1-alpha.
</li><li>Log IPv6 addresses on connections where this relay is the responder. Previously, responding relays would replace the remote IPv6 address with the IPv4 address from the consensus. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33899">33899</a>; bugfix on 0.3.1.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Fix a regression on sandboxing rules for the openat() syscall. The fix for bug <a href="https://bugs.torproject.org/tpo/core/tor/25440">25440</a> fixed the problem on systems with glibc &gt;= 2.27 but broke with versions of glibc. We now choose a rule based on the glibc version. Patch from Daniel Pinto. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/27315">27315</a>; bugfix on 0.3.5.11.
</li><li>Makes the seccomp sandbox allow the correct syscall for opendir according to the running glibc version. This fixes crashes when reloading torrc with sandbox enabled when running on glibc 2.15 to 2.21 and 2.26. Patch from Daniel Pinto. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40020">40020</a>; bugfix on 0.3.5.11.
  </li></ul>
</li><li>Minor bugfixes (logging, testing):
<ul>
<li>Make all of tor's assertion macros support the ALL_BUGS_ARE_FATAL and DISABLE_ASSERTS_IN_UNIT_TESTS debugging modes. (IF_BUG_ONCE() used to log a non-fatal warning, regardless of the debugging mode.) Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33917">33917</a>; bugfix on 0.2.9.1-alpha.
</li><li>Remove surprising empty line in the INFO-level log about circuit build timeout. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33531">33531</a>; bugfix on 0.3.3.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (mainloop):
<ul>
<li>Better guard against growing a buffer past its maximum 2GB in size. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33131">33131</a>; bugfix on 0.3.0.4-rc.
  </li></ul>
</li><li>Minor bugfixes (onion service v3 client):
<ul>
<li>Remove a BUG() warning that could occur naturally. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34087">34087</a>; bugfix on 0.3.2.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (onion service, logging):
<ul>
<li>Fix a typo in a log message PublishHidServDescriptors is set to 0. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33779">33779</a>; bugfix on 0.3.2.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (onion services v3):
<ul>
<li>Avoid a non-fatal assertion failure in certain edge-cases when opening an intro circuit as a client. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34084">34084</a>; bugfix on 0.3.2.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (protocol versions):
<ul>
<li>Sort tor's supported protocol version lists, as recommended by the tor directory specification. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33285">33285</a>; bugfix on 0.4.0.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (rate limiting, bridges, pluggable transports):
<ul>
<li>On a bridge, treat all connections from an ExtORPort as remote by default for the purposes of rate-limiting. Previously, bridges would treat the connection as local unless they explicitly received a "USERADDR" command. ExtORPort connections still count as local if there is a USERADDR command with an explicit local address. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33747">33747</a>; bugfix on 0.2.5.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (refactoring):
<ul>
<li>Lift circuit_build_times_disabled() out of the circuit_expire_building() loop, to save CPU time when there are many circuits open. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33977">33977</a>; bugfix on 0.3.5.9.
  </li></ul>
</li><li>Minor bugfixes (relay, self-testing):
<ul>
<li>When starting up as a relay, if we haven't been able to verify that we're reachable, only launch reachability tests at most once a minute. Previously, we had been launching tests up to once a second, which was needlessly noisy. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40083">40083</a>; bugfix on 0.2.8.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (relay, usability):
<ul>
<li>Adjust the rules for when to warn about having too many connections to other relays. Previously we'd tolerate up to 1.5 connections per relay on average. Now we tolerate more connections for directory authorities, and raise the number of total connections we need to see before we warn. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33880">33880</a>; bugfix on 0.3.1.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (SOCKS, onion service client):
<ul>
<li>Detect v3 onion service addresses of the wrong length when returning the F6 ExtendedErrors code. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33873">33873</a>; bugfix on 0.4.3.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (tests):
<ul>
<li>Fix the behavior of the rend_cache/clean_v2_descs_as_dir when run on its own. Previously, it would exit with an error. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40099">40099</a>; bugfix on 0.2.8.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (v3 onion services):
<ul>
<li>Remove a BUG() warning that could trigger in certain unlikely edge-cases. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34086">34086</a>; bugfix on 0.3.2.1-alpha.
</li><li>Remove a BUG() that was causing a stacktrace when a descriptor changed at an unexpected time. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/28992">28992</a>; bugfix on 0.3.2.1-alpha.
  </li></ul>
</li><li>Minor bugfixes (windows):
<ul>
<li>Fix a bug that prevented Tor from starting if its log file grew above 2GB. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/31036">31036</a>; bugfix on 0.2.1.8-alpha.
  </li></ul>
</li><li>Code simplification and refactoring:
<ul>
<li>Define and use a new constant TOR_ADDRPORT_BUF_LEN which is like TOR_ADDR_BUF_LEN but includes enough space for an IP address, brackets, separating colon, and port number. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33956">33956</a>. Patch by Neel Chauhan.
</li><li>Merge the orconn and ocirc events into the "core" subsystem, which manages or connections and origin circuits. Previously they were isolated in subsystems of their own.
</li><li>Move LOG_PROTOCOL_WARN to app/config. Resolves a dependency inversion. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33633">33633</a>.
</li><li>Move the circuit extend code to the relay module. Split the circuit extend function into smaller functions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33633">33633</a>.
</li><li>Rewrite port_parse_config() to use the default port flags from port_cfg_new(). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32994">32994</a>. Patch by MrSquanchee.
</li><li>Updated comments in 'scheduler.c' to reflect old code changes, and simplified the scheduler channel state change code. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33349">33349</a>.
</li><li>Refactor configuration parsing to use the new config subsystem code. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33014">33014</a>.
</li><li>Move a series of functions related to address resolving into their own files. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33789">33789</a>.
  </li></ul>
</li><li>Documentation:
<ul>
<li>Replace most http:// URLs in our code and documentation with https:// URLs. (We have left unchanged the code in src/ext/, and the text in LICENSE.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/31812">31812</a>. Patch from Jeremy Rand.
</li><li>Document the limitations of using %include on config files with seccomp sandbox enabled. Fixes documentation bug <a href="https://bugs.torproject.org/tpo/core/tor/34133">34133</a>; bugfix on 0.3.1.1-alpha. Patch by Daniel Pinto.
  </li></ul>
</li><li>Removed features:
<ul>
<li>Our "check-local" test target no longer tries to use the Coccinelle semantic patching tool parse all the C files. While it is a good idea to try to make sure Coccinelle works on our C before we run a Coccinelle patch, doing so on every test run has proven to be disruptive. You can still run this tool manually with "make check-cocci". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40030">40030</a>.
</li><li>Remove the ClientAutoIPv6ORPort option. This option attempted to randomly choose between IPv4 and IPv6 for client connections, and wasn't a true implementation of Happy Eyeballs. Often, this option failed on IPv4-only or IPv6-only connections. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32905">32905</a>. Patch by Neel Chauhan.
</li><li>Stop shipping contrib/dist/rc.subr file, as it is not being used on FreeBSD anymore. Closes issue <a href="https://bugs.torproject.org/tpo/core/tor/31576">31576</a>.
  </li></ul>
</li><li>Testing:
<ul>
<li>Add a basic IPv6 test to "make test-network". This test only runs when the local machine has an IPv6 stack. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33300">33300</a>.
</li><li>Add test-network-ipv4 and test-network-ipv6 jobs to the Makefile. These jobs run the IPv4-only and dual-stack chutney flavours from test-network-all. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33280">33280</a>.
</li><li>Remove a redundant distcheck job. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33194">33194</a>.
</li><li>Run the test-network-ipv6 Makefile target in the Travis CI IPv6 chutney job. This job runs on macOS, so it's a bit slow. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33303">33303</a>.
</li><li>Sort the Travis jobs in order of speed. Putting the slowest jobs first takes full advantage of Travis job concurrency. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33194">33194</a>.
</li><li>Stop allowing the Chutney IPv6 Travis job to fail. This job was previously configured to fast_finish (which requires allow_failure), to speed up the build. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33195">33195</a>.
</li><li>Test v3 onion services to tor's mixed IPv4 chutney network. And add a mixed IPv6 chutney network. These networks are used in the test-network-all, test-network-ipv4, and test-network-ipv6 make targets. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33334">33334</a>.
</li><li>Use the "bridges+hs-v23" chutney network flavour in "make test- network". This test requires a recent version of chutney (mid- February 2020). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/28208">28208</a>.
</li><li>When a Travis chutney job fails, use chutney's new "diagnostics.sh" tool to produce detailed diagnostic output. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32792">32792</a>.
  </li></ul>
</li><li>Deprecated features (onion service v2):
<ul>
<li>Add a deprecation warning for version 2 onion services. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40003">40003</a>.
  </li></ul>
</li><li>Documentation (manual page):
<ul>
<li>Add cross reference links and a table of contents to the HTML tor manual page. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33369">33369</a>. Work by Swati Thacker as part of Google Season of Docs.
</li><li>Alphabetize the Denial of Service Mitigation Options, Directory Authority Server Options, Hidden Service Options, and Testing Network Options sections of the tor(1) manual page. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33275">33275</a>. Work by Swati Thacker as part of Google Season of Docs.
</li><li>Refrain from mentioning nicknames in manpage section for MyFamily torrc option. Resolves issue <a href="https://bugs.torproject.org/tpo/core/tor/33417">33417</a>.
</li><li>Updated the options set by TestingTorNetwork in the manual page. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33778">33778</a>.
  </li></ul>
</li></ul>

---
_comments:

<a id="comment-289474"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289474" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 16, 2020</p>
    </div>
    <a href="#comment-289474">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289474" class="permalink" rel="bookmark">Hi! Wonderful, thanks for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi! Wonderful, thanks for this excellent work. </p>
<p>I have a question (probably a naive one). What does it mean that the new release "improves the amount of code that can be disabled when running without relay support"? Does it mean that when I run Tor as a client only (so, not on relays and/or bridges) I can set an option in the configuration file so that some code is disabled? What is the aim? </p>
<p>Thanks a lot</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289503"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289503" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">September 21, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289474" class="permalink" rel="bookmark">Hi! Wonderful, thanks for…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289503">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289503" class="permalink" rel="bookmark">Hi!
This is about an option…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!</p>
<p>This is about an option, "--disable-relay-mode," that applies when building the Tor binaries from C; it lets people build a version of the Tor binary that doesn't have support for being a relay at all.</p>
<p>The main reason somebody might want to do this is if they are shipping Tor in an environment like low-end mobile devices, where storage or memory is very scarce, and they don't want to include any bytes of code that won't be used.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289499"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289499" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Barack (not verified)</span> said:</p>
      <p class="date-time">September 20, 2020</p>
    </div>
    <a href="#comment-289499">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289499" class="permalink" rel="bookmark">When are you going to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When are you going to exclude 0.4.1 and 0.4.2 relays from the network?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289504" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">September 21, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289499" class="permalink" rel="bookmark">When are you going to…</a> by <span>Barack (not verified)</span></p>
    <a href="#comment-289504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289504" class="permalink" rel="bookmark">Not sure.  Usually we do…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not sure.  Usually we do that once they're mostly unused, or once we find a security issue so bad that we don't want affected relays on the network.  I don't think we have a set schedule, though.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289542"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289542" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 22, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289499" class="permalink" rel="bookmark">When are you going to…</a> by <span>Barack (not verified)</span></p>
    <a href="#comment-289542">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289542" class="permalink" rel="bookmark">At present, there are more 0…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At present, there are more 0.3.5.8 relays in the network than any other obsolete or unrecommended version. Compared to the amount of 0.3.5.8 relays, there are about 100 fewer 0.4.1.6 relays, the second most common obsolete or unrecommended version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289635"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289635" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 23, 2020</p>
    </div>
    <a href="#comment-289635">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289635" class="permalink" rel="bookmark">apt list tor -a
Listing…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>apt list tor -a<br />
Listing... Done<br />
tor/unstable 0.4.4.5-1 armhf<br />
tor/stable,now 0.4.2.7-1~d10.buster+1 armhf [installed]<br />
tor/stable 0.3.5.10-1 armhf</p>
<p>Why the 0.4.4.5 is marked as unstable? Why I can't upgrade tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
