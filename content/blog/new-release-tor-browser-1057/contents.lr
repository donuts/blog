title: New Release: Tor Browser 10.5.7 (Android)
---
pub_date: 2021-09-27
---
author: sysrqb
---
tags:

tor browser
tbb-10.5
tbb
---
categories: applications
---
summary: Tor Browser 10.5.7 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.7 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.7/">distribution directory</a>.</p>
<p>This version updates Firefox's GeckoView component to 92.0 on Android. This version includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-38/">security updates</a> to Firefox.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5.6</a>:</p>
<ul>
<li>Android
<ul>
<li>Update Openssl to 1.1.1l</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40639">Bug 40639</a>: Rebase geckoview patches onto 92.0 build3</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292885"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292885" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 01, 2021</p>
    </div>
    <a href="#comment-292885">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292885" class="permalink" rel="bookmark">Is having the snowflake…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is having the snowflake browser extension useless if port forwarding is disabled/doesn't work on my router?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292886"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292886" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 01, 2021</p>
    </div>
    <a href="#comment-292886">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292886" class="permalink" rel="bookmark">OMG https://bugzilla.mozilla…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OMG <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1732388" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1732388</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292922"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292922" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 06, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292886" class="permalink" rel="bookmark">OMG https://bugzilla.mozilla…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292922">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292922" class="permalink" rel="bookmark">Mozilla&#039;s idea of an …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mozilla's idea of an "enhancement" is getting more and more out of touch.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292932"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292932" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jane (not verified)</span> said:</p>
      <p class="date-time">October 06, 2021</p>
    </div>
    <a href="#comment-292932">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292932" class="permalink" rel="bookmark">On the safer mode my install…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On the safer mode my install consistently disables all JavaScript. Is this a bug or should I be concerned with my browser? I will leave JavaScript disabled out of an abundance of caution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293102" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292932" class="permalink" rel="bookmark">On the safer mode my install…</a> by <span>Jane (not verified)</span></p>
    <a href="#comment-293102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293102" class="permalink" rel="bookmark">Yes, that was an issue that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, that was an issue that was fixed in version 10.5.9: <a href="https://blog.torproject.org/new-release-tor-browser-1059" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-1059</a>. Thank you for reporting it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
