title: New Release: Tor Browser 11.0.13
---
pub_date: 2022-05-23
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.0.13 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.13 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.13/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-19/) to Firefox.

We also updated Tor to 0.4.7.7 (the first stable Tor release with support for [congestion control](https://blog.torproject.org/congestion-contrl-047/)).

Note: the Android version 11.0.13 will be available later during the week.

The full changelog since [Tor Browser 11.0.12](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Android
  - [Bug fenix#40212](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40212): Tor Browser crashing on launch
- All Platforms
  - Update Tor to 0.4.7.7
  - [Bug tor-browser#40967](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40967): Integrate Mozilla fix for [Bug 1770137](https://bugzilla.mozilla.org/1770137)
  - [Bug tor-browser#40968](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40968): Integrate Mozilla fix for [Bug 1770048](https://bugzilla.mozilla.org/1770048)
- Build System
  - All Platforms
    - Update Go to 1.17.10
    - [Bug tor-browser-build#40319](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40319): Add build tag to downloads.json
    - [Bug tor-browser-build#40486](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40486): Add tools/signing/do-all-signing script, and other signing scripts improvements
