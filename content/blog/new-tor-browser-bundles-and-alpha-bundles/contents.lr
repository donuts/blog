title: New Tor Browser Bundles and alpha bundles
---
pub_date: 2012-10-23
---
author: erinn
---
tags:

release candidate
tor browser bundle
firefox updates
tbb
---
categories:

applications
releases
---
_html_body:

<p>The stable Tor Browser Bundles have been updated to fix a crash bug that existed in the previous version. If you were experiencing problems, please update and <a href="https://trac.torproject.org" rel="nofollow">let us know</a> if you have any further problems.</p>

<p>All alpha bundles have also been updated to Tor 0.2.3.23-rc. We've downgraded the Firefox version in the alpha Tor Browser Bundles to 10.0.9esr and will continue to keep the same version of Firefox in both bundles for the foreseeable future. In addition to that, the Linux and OS X versions have automatic port selection re-enabled, so those of you who were experiencing trouble running a concurrent system Tor on those systems should no longer have any issues.</p>

<p>All users who were using Tor 0.2.3.22-rc are strongly encouraged to upgrade.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>Further notes about Tor Browser Bundle updates:</p>

<p><strong>Tor Browser Bundle (2.2.39-4)</strong></p>

<ul>
<li>Update Firefox patches to prevent crashing (closes: #7128)</li>
<li>Update HTTPS Everywhere to 3.0.2</li>
<li>Update NoScript to 2.5.8</li>
</ul>

<p><strong>Tor Browser Bundle (2.3.23-alpha-1)</strong></p>

<ul>
<li>Update Tor to 0.2.3.23-rc</li>
<li>Update Firefox to 10.0.9esr</li>
<li>Update HTTPS Everywhere to 4.0development.1</li>
<li>Update NoScript to 2.5.8</li>
<li>Re-enable automatic Control and SOCKS port selection on Linux and OSX</li>
</ul>

---
_comments:

<a id="comment-17877"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17877" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2012</p>
    </div>
    <a href="#comment-17877">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17877" class="permalink" rel="bookmark">The new browser bundle for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The new browser bundle for mac doesn't do downloads properly - all my downloads are finishing as incomplete files.</p>
<p>Where can I download an older version of the tor browser bundle, I stupidly deleted the old one when I installed the new one.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17879"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17879" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2012</p>
    </div>
    <a href="#comment-17879">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17879" class="permalink" rel="bookmark">The Tor has been blocked in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Tor has been blocked in Iran for more than 24 days ,it seems they defeated you finally!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17884"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17884" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2012</p>
    </div>
    <a href="#comment-17884">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17884" class="permalink" rel="bookmark">Why the change to Firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why the change to Firefox ESR in the alpha bundles?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17887"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17887" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2012</p>
    </div>
    <a href="#comment-17887">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17887" class="permalink" rel="bookmark">Instead of using Tails, what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Instead of using Tails, what is a good LiveCD to use with TBB?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17890" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2012</p>
    </div>
    <a href="#comment-17890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17890" class="permalink" rel="bookmark">I tried commenting before</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I tried commenting before but for some reason it doesn't appear.</p>
<p>The new os x browser bundle ((2.2.39-4) doesn't work properly - when downloading files in the browser, they all quit prematurely.</p>
<p>The previous version worked fine. Please post how to find the previous version re-install!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17895"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17895" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2012</p>
    </div>
    <a href="#comment-17895">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17895" class="permalink" rel="bookmark">Thanks from china.
This new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks from china.<br />
This new version works on Windows 7.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17900"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17900" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2012</p>
    </div>
    <a href="#comment-17900">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17900" class="permalink" rel="bookmark">thanks from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks from china<br />
tor-browser-2.2.39-4_en-US works on Windows 7 here :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17909"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17909" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2012</p>
    </div>
    <a href="#comment-17909">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17909" class="permalink" rel="bookmark">I was using TBB version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was using TBB version 2.2.39-3. It was working OK. I quit TBB and restarted it hours later. Vidalia bootstrapped to 25% only and stuck there! It would not bootstrap further. 'Vidalia not responding' in Activity Monitor app. I deleted TBB and downloaded version 2.2.39-4. Same problem. I tried the Vidalia bundle with my own Firefox. Same problem. Unable to use TBB. (Intel Mac OSX 10.6.8). Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2012</p>
    </div>
    <a href="#comment-17916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17916" class="permalink" rel="bookmark">Hi,
do you still provide</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>do you still provide pre-built versions of the alpha software?<br />
If so, where can I download it?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17924"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17924" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2012</p>
    </div>
    <a href="#comment-17924">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17924" class="permalink" rel="bookmark">This is in the past, but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is in the past, but thanks whoever fixed that crash, if you see this :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
