title: New Tor Browser Bundles with Firefox 17.0.11esr and Tor 0.2.4.18-rc
---
pub_date: 2013-11-19
---
author: erinn
---
tags:

tor
release candidate
tor browser bundle
tor browser
tor releases
tbb
---
categories:

applications
network
releases
---
_html_body:

<p><a href="https://www.mozilla.org/en-US/firefox/17.0.11/releasenotes/" rel="nofollow">Firefox 17.0.11esr</a> has been released with several security fixes and the stable and RC Tor Browser Bundles have been updated</p>

<p>There is also a new Tor 0.2.4.18-rc release and the RC bundles have been updated to include that as well.</p>

<p><a href="https://www.torproject.org/projects/torbrowser.html.en#downloads" rel="nofollow">https://www.torproject.org/projects/torbrowser.html.en#downloads</a></p>

<p><strong>Tor Browser Bundle (2.3.25-15)</strong></p>

<ul>
<li>Update Firefox to 17.0.11esr</li>
<li>Update NoScript to 2.6.8.5</li>
<li>Fix paths so Mac OS X 10.9 can find the geoip file. Patch by David Fifield.<br />
    (closes: #10092)</li>
</ul>

<p><strong>Tor Browser Bundle (2.4.18-rc-1)</strong></p>

<ul>
<li>Update Tor to 0.2.4.18-rc</li>
<li>Update Firefox to 17.0.11esr</li>
<li>Update NoScript to 2.6.8.5</li>
<li>Remove PDF.js since it is no longer supported in Firefox 17</li>
<li>Fix paths so Mac OS X 10.9 can find the geoip file. Patch by David Fifield.<br />
    (closes: #10092)</li>
</ul>

---
_comments:

<a id="comment-38247"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38247" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
    <a href="#comment-38247">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38247" class="permalink" rel="bookmark">@ erinn
In previous posts, I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ erinn</p>
<p>In previous posts, I asked whether there was communication and co-ordination among developers of Tor and Tails. I was assured there was.</p>
<p>This post of yours is evidence that there is none. The current version of Tails, which is 0.21, does not include TBB 2.3.25-15. The next release of Tails is scheduled for December 11, 2013.</p>
<p>Tell me, erinn, what shall Tails' users do in the interim?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-38256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38256" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38247" class="permalink" rel="bookmark">@ erinn
In previous posts, I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-38256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38256" class="permalink" rel="bookmark">Oh, by communication you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh, by communication you meant that Erinn shouldn't announce her releases until Tails announces theirs? I think that's a poor plan -- the new Firefox ESR is out and public, so the clock is ticking either way.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-38258"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38258" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-38258">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38258" class="permalink" rel="bookmark">@ arma
Let me rephrase and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ arma</p>
<p>Let me rephrase and simplify my earlier post.</p>
<p>Should users <strong><i>continue</i></strong> using Tails 0.21?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38263"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38263" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-38263">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38263" class="permalink" rel="bookmark">Perhaps it would avoid</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Perhaps it would avoid confusion and preempt questions if Erinn were to add a statement like "an updated tails release will follow shortly" to the release announcements?</p>
<p>In any case thanks for the prompt Tor Browser update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38296"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38296" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-38296">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38296" class="permalink" rel="bookmark">Is it SAFE for us Tails&#039;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it <strong>SAFE</strong> for us Tails' users to continue using Tails 0.21? I use Tails every day.</p>
<p>Your <i>prompt</i> response to the above question is much appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38413"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38413" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-38413">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38413" class="permalink" rel="bookmark">@ arma
(note: I submitted a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ arma</p>
<p>(note: I submitted a reply two days ago and apparently it was deemed inappropriate and was censored. I do not see why it should not appear on this page.)</p>
<p>Let me re-phrase and simplify my earlier post as follows:</p>
<p>Should users of Tails <strong>continue</strong> using Tails 0.21, knowing that it does not contain the latest version of Iceweasel 17.0.11?</p>
<p>Or to phrase it in another way, is it <strong>SAFE</strong> for users of Tails to continue using Tails 0.21?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-38262"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38262" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
    <a href="#comment-38262">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38262" class="permalink" rel="bookmark">The vulnerabilities fixed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The vulnerabilities fixed are listed in MFSA 2013-103: <a href="https://www.mozilla.org/security/announce/2013/mfsa2013-103.html" rel="nofollow">https://www.mozilla.org/security/announce/2013/mfsa2013-103.html</a></p>
<p>See also <a href="https://www.mozilla.org/security/known-vulnerabilities/firefoxESR.html" rel="nofollow">https://www.mozilla.org/security/known-vulnerabilities/firefoxESR.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38265"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38265" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
    <a href="#comment-38265">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38265" class="permalink" rel="bookmark">How close are we to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How close are we to upgrading the main TBBs to run with Tor 2.4.x? It is likely that 2.4.x contains a higher degree of security than 2.3.x, especially with the new handshake protocols, and these TBB releases seem to work pretty well. What's the timeline of the 2.4.x release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-38504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38504" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 21, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38265" class="permalink" rel="bookmark">How close are we to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-38504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38504" class="permalink" rel="bookmark">Step one is for me to put</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Step one is for me to put out a stable Tor 0.2.4.x release. Real soon now I hope.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-38266"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38266" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2013</p>
    </div>
    <a href="#comment-38266">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38266" class="permalink" rel="bookmark">I&#039;m getting a bad signature</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm getting a bad signature when trying to verify the bundle<br />
Erinn Clark<br />
63FEE659<br />
8738 A680 B84B 3031 A630  F2DB 416F 0610 63FE E659</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38279"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38279" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2013</p>
    </div>
    <a href="#comment-38279">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38279" class="permalink" rel="bookmark">Thanks for this great work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this great work !<br />
Why we dont use firefox release 24.1.1esr from 2013-11-15 ?<br />
Im glad to see and try "Pluggable Transports Tor Browser Bundle"<br />
But Im asking why Privoxy is not inside the bundle ?</p>
<p>Best Regards<br />
MrWhite</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39350"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39350" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 29, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38279" class="permalink" rel="bookmark">Thanks for this great work</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39350">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39350" class="permalink" rel="bookmark">I don&#039;t believe Privoxy has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't believe Privoxy has ever been a part of Tor Browser Bundle, although it was once a part of the Vidalia Bundle. Something called Polipo was included when Firefox contained a bug which caused connections through Tor to timeout. That's been long since fixed, so there's no need for something like Privoxy or Polipo to use Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-38328"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38328" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2013</p>
    </div>
    <a href="#comment-38328">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38328" class="permalink" rel="bookmark">@erinn 
So glad the team is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@erinn </p>
<p>So glad the team is so responsive to propagating updates into TBB.  Would it be possible to aim to standardize this practice across all TBB releases?</p>
<p>I imagine many users understand the alpha--&gt;beta--&gt;RC--&gt;stable convention for releases and I can sympathize with the huge diversity of platforms the project supports, but the current set of builds makes the distinction a bit confusing:<br />
* TBB 0.2.3.25-15, with latest FF and NoScript but a version of Tor that has remained "stable" for a bizarrely long time despite arma and nickm's comments about its handshake making it almost functionally deprecated<br />
* TBB 0.2.4.18-rc1, which contains what's called a rc of tor that most users--and as far as I can tell, developers--are treating as stable code...especially in relation to the current "stable" build<br />
* TBB 3.0-b1, which currently contains the OLD FF/NoScript and a slightly older version of tor itself but which isn't made easily accessible to users who don't follow the project's e-mail and announcements carefully</p>
<p>For modular components like tor itself, NoScript, and Firefox, it might be ideal to propagate updates to all TBB releases simultaneously.  Encouraging as many TBB users as possible to converge on the latest FF/NoScript code in whatever flavor of TBB they use seems important from both a security standpoint and an anonymity standpoint.  Vulnerabilities fixed in 17.0.11esr could be used to exploit the minority of TBB 3.0beta1 users without those patches, but to the extent other users running updated releases can be identified as non-vulnerable, their anonymity is also potentially reduced when component updates into different TBBs are staggered.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38336"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38336" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2013</p>
    </div>
    <a href="#comment-38336">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38336" class="permalink" rel="bookmark">This new release (64 bit)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This new release (64 bit) crashes every few minutes on Debian 7.2 (64 bit)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-38503"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38503" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 21, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38336" class="permalink" rel="bookmark">This new release (64 bit)</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-38503">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38503" class="permalink" rel="bookmark">Yep. Try the newer 64-bit</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep. Try the newer 64-bit bundle she put up a little bit ago.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-38579"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38579" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2013</p>
    </div>
    <a href="#comment-38579">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38579" class="permalink" rel="bookmark">Today with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Today with 2.3.25-15_en-US.</p>
<p>Status: Connected to the Tor network!</p>
<p>Message log: the usual stuff (100%).</p>
<p>'Sorry. You are not using Tor.</p>
<p>Your IP address appears to be: 72.52.91.19'</p>
<p>Not mine, by the way.</p>
<p>And it crashes often in multi-tab-use as well as -14 does.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39042"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39042" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38579" class="permalink" rel="bookmark">Today with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39042">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39042" class="permalink" rel="bookmark">https://trac.torproject.org/p</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://trac.torproject.org/projects/tor/ticket/7342#comment:8" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/7342#comment:8</a></p>
<p>As for the crashes, you might be best served opening a trac ticket with more details. Also try TBB 3.0rc1.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39540" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 30, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-39540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39540" class="permalink" rel="bookmark">Opened a ticket for crashes.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Opened a ticket for crashes. Please add information, if you have some.</p>
<p><a href="https://trac.torproject.org/projects/tor/ticket/10254" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/10254</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-38707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2013</p>
    </div>
    <a href="#comment-38707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38707" class="permalink" rel="bookmark">Do the Tor Browser Bundles</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do the Tor Browser Bundles use Perfect Forward Secrecy ( PFS )? If no, do they need to or is PFS dependent on the individual website being viewed and not the browser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39051"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39051" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38707" class="permalink" rel="bookmark">Do the Tor Browser Bundles</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39051">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39051" class="permalink" rel="bookmark">Whether you get perfect</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whether you get perfect forward secrecy is a property of the handshake with a webserver. TBB supports it if the webserver you're talking to supports it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-38772"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38772" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 24, 2013</p>
    </div>
    <a href="#comment-38772">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38772" class="permalink" rel="bookmark">I JUST d/l&#039;d tor browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I JUST d/l'd tor browser bundle 17.0.11 and thought I'd check the NoScript "Allow Scripts Globally" default setting. uhh...guys....it looks like scripts are allowed globally by default...if I'm reading this right. I pulled a screenshot (this is for Debian 6.0.8 Linux Kernel 2.6.32-5-amd64 Gnome 2.30.2) just to show I'm not crazy. I'm sure I MUST be reading this wrong. I'll send if you like. (Do you have a preferred public key I should use?)</p>
<p>In the NoScript options window on the "General" tab at the bottom of that, I see "Scripts Globally Allowed (dangerous)" and its CHECKED on. (So I unchecked it.)</p>
<p>Is there some old setting somewhere that does this just on MY system? Does this not apply somehow?</p>
<p>What don't I understand? Is my OS broke?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39048"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39048" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38772" class="permalink" rel="bookmark">I JUST d/l&#039;d tor browser</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39048">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39048" class="permalink" rel="bookmark">https://www.torproject.org/do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/docs/faq#TBBJavaScriptEnabled" rel="nofollow">https://www.torproject.org/docs/faq#TBBJavaScriptEnabled</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39623"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39623" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-39623">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39623" class="permalink" rel="bookmark">I saw this logic earlier.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I saw this logic earlier. But it doesn't go far enough...though you're certainly right about  profiling javascript off.<br />
The trouble is that, as you know, everything is profilable using Naive Bayes and many similar. So website visits have a profile. IP exit points add to that. Absince of TOR exit adds to that. Mouse behavior/timing between clicks add to that. This is the unavoidable nature of modern machine discovery. All of us, whether we use javascript or not, have fuzzily unique profiles.<br />
Unfortunately, this logic surrenders (by default) something huge like javascript execution. You do this to buy the absence of only a single variable!! This is in a large multi-dimensional profiling analysis offered by something as simple as Naive Bayes. You increase the profiling complexity by only 10% (if that) when you leave this front door wide open!<br />
SUGGESTION: Issue a banner warning on the Tor "Congratulations" page that says "JAVASCRIPT IS ON". Then more people will be informed enough to make a truly intelligent decision.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-40339"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-40339" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-39623" class="permalink" rel="bookmark">I saw this logic earlier.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-40339">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-40339" class="permalink" rel="bookmark">noscript is a waste of time</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>noscript is a waste of time - it's well known the FH exploit bypassed it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-40355"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-40355" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 08, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-40339" class="permalink" rel="bookmark">noscript is a waste of time</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-40355">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-40355" class="permalink" rel="bookmark">No, you are confused.
It</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, you are confused.</p>
<p>It depends how your noscript was configured.</p>
<p>The trouble is that we weren't using noscript for the thing that would have helped in that case. That doesn't mean noscript itself is a waste of time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-41632"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-41632" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-40355" class="permalink" rel="bookmark">No, you are confused.
It</a> by arma</p>
    <a href="#comment-41632">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-41632" class="permalink" rel="bookmark">was it to do with some</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>was it to do with some people whitelisting?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-41634"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-41634" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-40355" class="permalink" rel="bookmark">No, you are confused.
It</a> by arma</p>
    <a href="#comment-41634">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-41634" class="permalink" rel="bookmark">It was an iframe attack.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It was an iframe attack. However I tested noscript and even with iframe enabled, javascript was not executed within the frame.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-40356"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-40356" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 08, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-39623" class="permalink" rel="bookmark">I saw this logic earlier.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-40356">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-40356" class="permalink" rel="bookmark">There is now a little thing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is now a little thing at the bottom of check.torproject.org. It remains for us to make check.tp.o a better page. Also, in TBB 3, that page isn't the homepage anymore.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-38843"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38843" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2013</p>
    </div>
    <a href="#comment-38843">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38843" class="permalink" rel="bookmark">This release of the TBB</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This release of the TBB freezes and occasionally crashes whilst visiting Amazon.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39047"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39047" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38843" class="permalink" rel="bookmark">This release of the TBB</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39047">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39047" class="permalink" rel="bookmark">Oh? If you can trigger it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh? If you can trigger it reliably, you should open a ticket with details.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-38860"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38860" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2013</p>
    </div>
    <a href="#comment-38860">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38860" class="permalink" rel="bookmark">The tor browser bundle is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The tor browser bundle is flashing an update info that's apparently not available. It's no bid deal but probably should be addressed as soon as possible. Your efforts are highly appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39054"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39054" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38860" class="permalink" rel="bookmark">The tor browser bundle is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39054">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39054" class="permalink" rel="bookmark">Well, maybe it is a big deal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, maybe it is a big deal -- what version do you have?</p>
<p>There are alas a few bugs in the TBB 2.x update notification mechanism. You might enjoy TBB 3.x.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-39278"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39278" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 28, 2013</p>
    </div>
    <a href="#comment-39278">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39278" class="permalink" rel="bookmark">NoScript updated itself to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NoScript updated itself to 2.6.8.6</p>
<p>Don't recall NS updating itself in previous TBBs.</p>
<p>Everything ok?</p>
</div>
  </div>
</article>
<!-- Comment END -->
