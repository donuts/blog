title: November 2008 Progress Report
---
pub_date: 2008-12-25
---
author: phobos
---
tags:

progress report
hidden services
translations
bug fixes
alpha release
---
categories:

localization
onion services
releases
reports
---
_html_body:

<p><strong>Bug Fixes</strong></p>

<p>Tor 0.2.1.7-alpha (released November 8) fixes a major security problem in Debian and Ubuntu packages (and maybe other packages) noticed by Theo de Raadt, fixes a smaller security flaw that might allow an attacker to access local services, adds better defense against DNS poisoning attacks on exit relays, further improves hidden service performance, and fixes a variety of other issues.<br />
<a href="http://archives.seul.org/or/talk/Nov-2008/msg00229.html" rel="nofollow">http://archives.seul.org/or/talk/Nov-2008/msg00229.html</a></p>

<p>Tor 0.2.0.32 (released November 20) fixes a major security problem in Debian and Ubuntu packages (and maybe other packages) noticed by Theo de Raadt, fixes a smaller security flaw that might allow an attacker to access local services, further improves hidden service performance, and fixes a variety of other issues.<br />
<a href="http://archives.seul.org/or/announce/Dec-2008/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/Dec-2008/msg00000.html</a></p>

<p>Vidalia 0.1.10 (released November 2) fixes some presentation bugs and some bugs in the Windows installer.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.10/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.10/CHA…</a></p>

<p>In the Vidalia 0.1.10 stable release:<br />
  - Add a prettier dialog for prompting people for their control port password that also includes a checkbox for whether the user wants Vidalia to remember the entered password, a Help button, and a Reset button (Windows only).<br />
  - Fix a crash bug that occurred when the user clicks 'Clear' in the message log toolbar followed by 'Save All'.<br />
  - Uncheck the Torbutton options by default in the Windows bundle installer if Firefox is not installed.<br />
  - Add a Windows bundle installer page that warns the user that they should install Firefox, if it looks like they haven't already done so.</p>

<p>Security fixes in the Tor 0.2.1.7-alpha release:<br />
  - The "ClientDNSRejectInternalAddresses" config option wasn't being consistently obeyed: if an exit relay refuses a stream because its exit policy doesn't allow it, we would remember what IP address the relay said the destination address resolves to, even if it's an internal IP address. Bugfix on 0.2.0.7-alpha; patch by rovv.<br />
  - The "User" and "Group" config options did not clear the supplementary group entries for the Tor process. The "User" option is now more robust, and we now set the groups to the specified user's primary group. The "Group" option is now ignored. For more detailed logging on credential switching, set CREDENTIAL_LOG_LEVEL in common/compat.c to LOG_NOTICE or higher. Patch by Jacob Appelbaum and Steven Murdoch. Bugfix on 0.0.2pre14. Fixes bug 848.</p>

<p><strong>Design Work</strong><br />
We have a preliminary proposal that suggests we use only one destination port per circuit. This came out of a discussion between Roger and Robert Hogan about how making an AIM connection through your circuit, and then also web browsing through it, can link the web browsing to your AIM login and you may not want that.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/ideas/xxx-separate-streams-by-port.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/ideas/xxx-s…</a></p>

<p>We picked up the "proposal 141, clients do less directory downloading" design discussion again:<br />
<a href="http://archives.seul.org/or/dev/Nov-2008/msg00000.html" rel="nofollow">http://archives.seul.org/or/dev/Nov-2008/msg00000.html</a><br />
<a href="http://archives.seul.org/or/dev/Nov-2008/msg00001.html" rel="nofollow">http://archives.seul.org/or/dev/Nov-2008/msg00001.html</a><br />
<a href="http://archives.seul.org/or/dev/Nov-2008/msg00007.html" rel="nofollow">http://archives.seul.org/or/dev/Nov-2008/msg00007.html</a><br />
It looks like we have a plausible new direction to go, but nobody to write up the design proposal or implement it. I'm going to do the first go at the next design proposal in January, and hopefully somebody will have time to build it from there.</p>

<p>We continued extensive work on Thandy this month.</p>

<p>We have a Thandy repository up at<br />
<a href="http://updates.torproject.org/thandy/" rel="nofollow">http://updates.torproject.org/thandy/</a><br />
and its keys and location ship with the thandy client.</p>

<p>(The current repository is still for testing only, and we'll discard the keys and generate new ones when we want to put it up for real. We'll also get an ssl cert for it.)</p>

<p>The client-side of Thandy (teaching it how to decide which packages and bundles are out of date, and teaching it to download new files and check all the right signatures) exists now too. It supports download resuming, doing the download over Tor, etc.</p>

<p>The big picture is that thandy will remember what versions of each package and bundle are installed. Vidalia will periodically launch thandy-client so it can check for updates. When there are new packages, thandy will tell Vidalia (via stdout currently, since Vidalia launched it). Then when the time is right, Vidalia will launch thandy-client with a --install option, and thandy will know how to run the installers for each type of package (currently "rpm", "win32", and "none" are supported):<br />
<a href="https://svn.torproject.org/svn/updater/trunk/doc/interface.txt" rel="nofollow">https://svn.torproject.org/svn/updater/trunk/doc/interface.txt</a></p>

<p>The long-term plan is to have every platform have a package system that is capable of answering "What version of the software is installed?" On Windows, that would either be the new MSI installer file we're working on:<br />
<a href="https://svn.vidalia-project.net/svn/vidalia/trunk/pkg/win32/vidalia.wxs.in" rel="nofollow">https://svn.vidalia-project.net/svn/vidalia/trunk/pkg/win32/vidalia.wxs…</a><br />
or our current NSI installer, with a new registry key patch we're working on.</p>

<p>If an upgrade attempt fails (due to a broken package, broken system, sudden power loss, etc), thandy will try again the next time you tell it to install. With luck, it will work later, or an upgraded version of the package that _does_ work will come to be, and thandy will fetch and install that one instead.</p>

<p>We're working on patching our current Windows installer so it knows how to answer what version is installed. Then it will be easier for all the components to work together.</p>

<p>In short: many more components of our auto updater are coming together, but they aren't all together yet.</p>

<p>We've started to think about moving the Tor Browser Bundle from Firefox 2 to Firefox 3. This will mean we should measure new traces. We'll do it once Torbutton is known to be more stable on Firefox 3, which should happen in early 2009</p>

<p><strong>Translation</strong><br />
We have our translation server up and online:<br />
<a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a><br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>We now have a Romanian translation.</p>

<p>We continued enhancements to the Chinese and Russian Tor website translations. Our Farsi translation from this summer is slowly becoming obsolete; we should solve that at some point.</p>

---
_comments:

<a id="comment-461"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-461" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2008</p>
    </div>
    <a href="#comment-461">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-461" class="permalink" rel="bookmark">what should i do about the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what should i do about the bug in firefox 3, it writes to history even if i dont want it to</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-480" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 30, 2008</p>
    </div>
    <a href="#comment-480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-480" class="permalink" rel="bookmark">bugs.torproject.org</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you could open a bug at <a href="https://bugs.torproject.org" rel="nofollow">https://bugs.torproject.org</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
