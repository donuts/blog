title: Pluggable transports bundles 2.4.12-alpha-2-pt1 with Firefox 17.0.6esr
---
pub_date: 2013-06-02
---
author: dcf
---
tags:

obfsproxy
pluggable transports
flashproxy
obfsbundle
---
categories: circumvention
---
_html_body:

<p>We've updated the Pluggable Transports Tor Browser Bundles with Firefox 17.0.6esr and Tor 0.2.4.11-alpha. These correspond to the <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-1706esr" rel="nofollow">Tor Browser Bundle release</a> of May 14.</p>

<ul>
<li><a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.12-alpha-2-pt1_en-US.exe" rel="nofollow">Windows</a> (<a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.12-alpha-2-pt1_en-US.exe.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.12-alpha-2-pt1-osx-i386-en-US.zip" rel="nofollow">Mac OS X</a> (<a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.12-alpha-2-pt1-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.12-alpha-2-pt1-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 32-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.12-alpha-2-pt1-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.12-alpha-2-pt1-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 64-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.12-alpha-2-pt1-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<p>These bundles contain contain flash proxy and obfsproxy configured to run by default. Flash proxy has a new faster registration method, <a href="https://trac.torproject.org/projects/tor/ticket/8860" rel="nofollow">flashproxy-reg-appspot</a>. The existing flashproxy-reg-email and flashproxy-reg-http will be tried if flashproxy-reg-appspot doesn't work.</p>

<p>If you want to use flash proxy, you will have to take the extra steps listed in the <a href="https://trac.torproject.org/projects/tor/wiki/FlashProxyHowto" rel="nofollow">flash proxy howto</a>.</p>

<p>These bundles contain the same hardcoded obfs2 bridge addresses as the previous bundles which may work for some jurisdictions but you are strongly advised to get new bridge addresses from BridgeDB: <a href="https://bridges.torproject.org/?transport=obfs2" rel="nofollow">https://bridges.torproject.org/?transport=obfs2</a> <a href="https://bridges.torproject.org/?transport=obfs3" rel="nofollow">https://bridges.torproject.org/?transport=obfs3</a>.</p>

<p>These bundles are signed by David Fifield (0x5CD388E5) with <a href="https://crypto.stanford.edu/flashproxy/#verify-sig" rel="nofollow">this fingerprint</a>.</p>

---
_comments:

<a id="comment-22850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2013</p>
    </div>
    <a href="#comment-22850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22850" class="permalink" rel="bookmark">Awesome! Thank you Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Awesome! Thank you Tor project!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2013</p>
    </div>
    <a href="#comment-22853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22853" class="permalink" rel="bookmark">erm you posted:
Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>erm you posted:</p>
<p>Tor Browser Bundles with FIREFOX 17.0.(4)esr</p>
<p>shoudnt it be 0.6?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22854"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22854" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2013</p>
    </div>
    <a href="#comment-22854">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22854" class="permalink" rel="bookmark">Title says &quot;Firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Title says "Firefox 17.0.6esr"</p>
<p>Then, first line says: "17.04esr"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22856"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22856" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2013</p>
    </div>
    <a href="#comment-22856">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22856" class="permalink" rel="bookmark">SSL breaks completely on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>SSL breaks completely on both:<br />
<a href="https://blog.torproject.org/crss" rel="nofollow">https://blog.torproject.org/crss</a><br />
and:<br />
<a href="https://blog.torproject.org/blog/feed" rel="nofollow">https://blog.torproject.org/blog/feed</a></p>
<p>"Your connection to this site is not encrypted."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22860"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22860" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2013</p>
    </div>
    <a href="#comment-22860">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22860" class="permalink" rel="bookmark">I use ubuntu 9.04 and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use ubuntu 9.04 and torbundle not working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-26201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-26201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 24, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22860" class="permalink" rel="bookmark">I use ubuntu 9.04 and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-26201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-26201" class="permalink" rel="bookmark">Ubuntu 9.04 ?
That&#039;s no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ubuntu 9.04 ?</p>
<p>That's no longer supported!</p>
<p>Dangerous!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-22861"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22861" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2013</p>
    </div>
    <a href="#comment-22861">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22861" class="permalink" rel="bookmark">Is my connection to your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is my connection to your site secure?</p>
<p>When I click on the (gray) padlock it says:</p>
<p>##################</p>
<p>you are connected to<br />
<b>torproject.org</b></p>
<p>which is run by<br />
<b>(unknown)</b></p>
<p>Verified by: DigiCert Inc</p>
<p>Your connection to this website is<br />
encrypted to prevent eavesdropping.</p>
<p>##################</p>
<p>Most SSL sites are like this,<br />
with the exception being grc.com<br />
and a few others which show the<br />
padlock in green with different<br />
information.</p>
<p>Am I being MiTM'd on sites where<br />
the padlock is gray and displays<br />
comparable information?</p>
<p>How do I resolve this so all SSL sites<br />
are displayed in green? Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-23001"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23001" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 05, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22861" class="permalink" rel="bookmark">Is my connection to your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-23001">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23001" class="permalink" rel="bookmark">Green and blue are only for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Green and blue are only for sites that have extended validation certs.</p>
<p>This excludes even Google and Amazon. </p>
<p>The situation is indeed confusing to many and seeing "verified by (unknown)" is rather off-putting. Worse, the Mozilla documentation even states that sensitive info should never be submitted when the padlock is gray! Imagine what the effect would be upon Amazon.com alone, if people were to follow that warning!</p>
<p>Truly outrageous.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-22863"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22863" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2013</p>
    </div>
    <a href="#comment-22863">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22863" class="permalink" rel="bookmark">GPG verify option on these</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><i>GPG verify option on these files are looking for a different key which is not listed on the signing keys page or the verification page:</i></p>
<p><a href="https://www.torproject.org/docs/signing-keys.html.en" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en</a><br />
<a href="https://www.torproject.org/docs/verifying-signatures.html" rel="nofollow">https://www.torproject.org/docs/verifying-signatures.html</a></p>
<p><b>Instead, the signing key is found at:</b></p>
<p><a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">https://crypto.stanford.edu/flashproxy/</a></p>
<p>Which is:</p>
<p>The client packages are signed with subkey 0x5CD388E5 of this key:</p>
<p>pub   8192R/<b>C11F6276</b> 2012-07-21<br />
Key fingerprint = AD1A B35C 674D F572 FBCE  8B0A 6BC7 58CB C11F 6276<br />
uid   David Fifield<br />
sub   4096R/D90A8E40 2012-07-21<br />
sub   4096R/5CD388E5 2012-07-21</p>
<p>You are looking for output like this:</p>
<p>gpg --verify flashproxy-client-version.zip.asc flashproxy-client-version.zip<br />
gpg: Signature made date using RSA key ID 5CD388E5<br />
gpg: Good signature from <b>"David Fifield "</b></p>
<p><i>Please post this change and please add release dates to your obfs page.</i></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-22869"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22869" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 03, 2013</p>
    </div>
    <a href="#comment-22869">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22869" class="permalink" rel="bookmark">Hi,
The current 2.4.12</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>The current 2.4.12 Obfsproxy TBB seems to be signed with a different key than the ones mentioned here: <a href="https://www.torproject.org/docs/signing-keys.html.en" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en</a><br />
Instead of being signed by Alexandre Allaire (0x4279F297) or Sebastian Hahn (0xC5AA446D), I am seeing the bundle signature being from David Fifield (specifically his subkey 0x5CD388E5).<br />
Is this okay?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-23094"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23094" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 06, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22869" class="permalink" rel="bookmark">Hi,
The current 2.4.12</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-23094">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23094" class="permalink" rel="bookmark">As the blog above says, OK</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As the blog above says, OK</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-22933"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22933" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2013</p>
    </div>
    <a href="#comment-22933">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22933" class="permalink" rel="bookmark">Have no secure webmail and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have no secure webmail and no idea what name or channel the webirc requires</p>
<p>Here's an idea for Tor that would help greatly. As we all no doubt have plenty of space on web bars at to of the browser. Place their the ip address and info such as..</p>
<p>Tor Cloak xxx.xxx.xxx.xxx (where x is the current IP address) now active. Though would prefer this to be movable so can place where I like it on any available tool bars.</p>
<p>This way we don't have to put up with tor etc and IP address when start the browser and can have blank page. This way we also know what the ip is at any given time and are still secure.</p>
<p>I need to open to a blank each time that I open a new window or tab both are the same.</p>
<p>Easy to move around with using toolbar customize flexible space or other if need. Though please don't make it a throbber leave it static with black letter words and numbers.</p>
<p>Please also see about making tor web site more user friendly such as details for IRC on the connecting page. Such as name or need to choose one if that is so. And what the channel number need to be entered.</p>
<p>Then also there is communication between us and Tor where have no secure email or can use IRC. A web form maybe nice idea though maybe end up been ignored to many requests. So how about a forum or request board panel or similar where user doesn't need to register. Remember about no secure email there's very few around now.</p>
<p>Those email companies that are as always with many pick a user name let say waj92q3ur97rjf91cr2218389hpcn9wrqu8rqpu9328vr90qvui2m9irw3a9ic where never would be any the same ok shorter and always comes back with sorry that name is chosen or in use how about waj92q3ur97rjf91cr2218389hpcn9wrqu8rqpu9328vr90qvui2m9irw3a9ic1 which is the same with number one at the end WTF !!!! This one is for all those big email providers out there, must be using the same old sign up script. I have lost a few email accounts of late and expect a few more with yahoo. Then again I never trusted yahoo free mail. Though now many will be disappointed as the change will mean loss of all mails for million I guess.Anyway who remembers what they put for secret questions and other details when they got the email decades ago !</p>
<p>And this post goes to show why you should not have a web form. Where someone has to read this and many others. No doubt near all companies don't read emails and such. Sure it is their job to do so but when have a few or many who cares what the email or other says. Quick glance and reply with nothing or little to do with the original email</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 05, 2013</p>
    </div>
    <a href="#comment-23022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23022" class="permalink" rel="bookmark">Feedback from China, in both</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Feedback from China, in both WINXP and WIN7, flashproxy-reg-appspot doesnt work well, here is the log file:</p>
<p>2013-06-05 19:50:22 VERSION 1<br />
2013-06-05 19:50:22 Listening remote on 0.0.0.0:9002.<br />
2013-06-05 19:50:22 Listening remote on [::]:9002.<br />
2013-06-05 19:50:22 Listening local on 127.0.0.1:3450.<br />
2013-06-05 19:50:22 CMETHOD websocket socks4 127.0.0.1:3450<br />
2013-06-05 19:50:22 CMETHODS DONE<br />
2013-06-05 19:50:22 Trying to register ":9002".<br />
2013-06-05 19:50:22 Running command: D:\xxx\APP\TBBflashnew\Tor Browser\App\flashproxy-reg-appspot :9002<br />
2013-06-05 19:50:25 Local connection from [scrubbed].<br />
2013-06-05 19:50:25 SOCKS request from [scrubbed].<br />
2013-06-05 19:50:25 Got SOCKS request for [scrubbed].<br />
2013-06-05 19:50:25 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:25 remotes (0): []<br />
2013-06-05 19:50:25 Data from unlinked local [scrubbed] (230 bytes).<br />
2013-06-05 19:50:25 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:25 remotes (0): []<br />
2013-06-05 19:50:26 flashproxy-reg-appspot: Registered "xxx.xxx.xxx.xxx:9002" with fp-reg-a.appspot.com.<br />
2013-06-05 19:50:28 Remote connection from [scrubbed].<br />
2013-06-05 19:50:28 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:28 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:28 remotes (1): ['[scrubbed]']<br />
2013-06-05 19:50:28 Linking [scrubbed] and [scrubbed].<br />
2013-06-05 19:50:28 Remote connection from [scrubbed].<br />
2013-06-05 19:50:28 Remote connection from [scrubbed].<br />
2013-06-05 19:50:28 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:28 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:28 remotes (2): ['[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:28 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (3): ['[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (4): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (5): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (6): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (7): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:29 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:29 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:29 remotes (8): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:29 Remote connection from [scrubbed].<br />
2013-06-05 19:50:30 Remote connection from [scrubbed].<br />
2013-06-05 19:50:30 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:30 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:30 remotes (8): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:30 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:30 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:30 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:30 Remote connection from [scrubbed].<br />
2013-06-05 19:50:30 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:30 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:30 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:32 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:32 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:32 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:32 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:32 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:32 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:32 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:32 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:32 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:32 Expired remote connection from [scrubbed].<br />
2013-06-05 19:50:32 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:32 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:50:39 Remote connection from [scrubbed].<br />
2013-06-05 19:50:39 Data from WebSocket-pending [scrubbed].<br />
2013-06-05 19:50:39 locals  (1): ['[scrubbed]']<br />
2013-06-05 19:50:39 remotes (10): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:51:09 Socket error from local: '[Errno 10054] An existing connection was forcibly closed by the remote host'<br />
2013-06-05 19:51:28 Data from unlinked remote [scrubbed] (6 bytes).<br />
2013-06-05 19:51:28 locals  (0): []<br />
2013-06-05 19:51:28 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:51:29 Data from unlinked remote [scrubbed] (6 bytes).<br />
2013-06-05 19:51:29 locals  (0): []<br />
2013-06-05 19:51:29 remotes (9): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:51:48 EOF from unlinked remote [scrubbed] with 6 bytes buffered.<br />
2013-06-05 19:51:48 locals  (0): []<br />
2013-06-05 19:51:48 remotes (8): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']<br />
2013-06-05 19:51:49 EOF from unlinked remote [scrubbed] with 6 bytes buffered.<br />
2013-06-05 19:51:49 locals  (0): []<br />
2013-06-05 19:51:49 remotes (7): ['[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]', '[scrubbed]']</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23132"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23132" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 06, 2013</p>
    </div>
    <a href="#comment-23132">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23132" class="permalink" rel="bookmark">The latest version (or 2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The latest version (or 2 latest versions) of alpha TBB Linux 64-bit on my system has Tor Browser 17.0.6esr crash very often (Vidalia keeps running OK).</p>
<p>Usually the browser dies on any plugin or disk interaction (Save Page As Archive with the MAFF plugin, update RSS Feeds, etc.) I tried both the "pluggable transports" and a stand-alone alpha versions - the same result. I tried disabling different plugins (except TorButton), but the crash happens regardless of any specific plugin - as long as there is a couple of them enabled (that I need to use). I didn't try disabling all the plugins yet.</p>
<p>So, it seems that the latest alpha-packaged Tor Browser is easily breakable by something. Does anyone else have this problem? Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23236"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23236" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 07, 2013</p>
    </div>
    <a href="#comment-23236">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23236" class="permalink" rel="bookmark">Hi, i got bellow errors for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, i got bellow errors for "tor-browser-2.4.12-alpha-2_en-US.exe":</p>
<p>[...] Tor Software Error - The Tor software encountered an internal bug. Please report the following error message to the Tor developers at bugs.torproject.org: "microdesc_cache_rebuild(): Bug: Discontinuity in position in microdescriptor cache.By my count, I'm at 1447850, but I should be at 1447883<br />
"<br />
[....] Tor Software Error - The Tor software encountered an internal bug. Please report the following error message to the Tor developers at bugs.torproject.org: "getinfo_helper_dir(): Bug: control.c:1715: getinfo_helper_dir: Assertion md-&gt;body failed; aborting.<br />
"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 08, 2013</p>
    </div>
    <a href="#comment-23282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23282" class="permalink" rel="bookmark">I can&#039;t still connect to tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't still connect to tor in iran. sometimes it connects, then the connection drops after 2,3 minutes... pls tor do something :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23337"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23337" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 08, 2013</p>
    </div>
    <a href="#comment-23337">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23337" class="permalink" rel="bookmark">Dear Tor Devs: Given the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear Tor Devs: Given the current situation,it probably won't be too long until the government will force a backdoor into the tor system, without allowing you to inform the tor users. Please, instead of complying, do shut down Tor instead.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-23496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-23496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 10, 2013</p>
    </div>
    <a href="#comment-23496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-23496" class="permalink" rel="bookmark">Can anyone confirm that if</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can anyone confirm that if this new version works in iran?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-25861"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-25861" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 21, 2013</p>
    </div>
    <a href="#comment-25861">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-25861" class="permalink" rel="bookmark">A Problem verifying the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A Problem verifying the win32 bundle:</p>
<p>I downloaded the win32 bundle and the corresponding sig file, after using Kleopatra's Decrypt/Verify option, I got something like this:</p>
<p>tor-pluggable-transports-browser-2.4.12-alpha-2-pt1_en-US.exe.asc :Not enough information to check signature validity.</p>
<p>Signed on 2013-06-02 07:03 with unknown certificate 0x797A326AEC4A478AF050CC3AE2B93D815CD388E5.<br />
The validity of the signature cannot be verified.</p>
<p>I have the public key of David Fiflied (Key ID C11F6276, Fingerprint AD1AB35C), I'm not sure which step is wrong, please help.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2013</p>
    </div>
    <a href="#comment-27504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27504" class="permalink" rel="bookmark">When can we expect an update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When can we expect an update to the Pluggable transports bundles considering there are 'New Tor Browser Bundles and Tor 0.2.4.14-alpha packages'?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27988"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27988" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2013</p>
    </div>
    <a href="#comment-27988">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27988" class="permalink" rel="bookmark">When I use this TBB with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I use this TBB with only hardcoded obfs 2 bridge addresses, this TBB runs fine. When I add more than 10 total additional obfs 2 and obfs 3 bridge addresses, Vidalia hangs anywhere between 'Starting Tor' and  'Connected to Tor'. I have to delete this TBB and reinstall it to get it to work. Closing TBB using Task Manager removes the 'hanging' Vidalia, obfsproxy and flash proxy but TBB wil not reopen. Only reinstalling this TBB works. Using Windows 8 Pro 32 bit, SanDisk 16 GB USB flash drive.</p>
</div>
  </div>
</article>
<!-- Comment END -->
