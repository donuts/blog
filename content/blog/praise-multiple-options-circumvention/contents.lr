title: In praise of multiple options for circumvention
---
pub_date: 2009-02-17
---
author: phobos
---
tags:

anonymity advocacy
circumvention
education
---
categories:

advocacy
circumvention
community
---
_html_body:

<p>I was asked the other day why we don't advocate for just Tor as the one tool to rule them all.  My glib answer is "of course we do, however the larger the toolbox, the better off the world."  </p>

<p>Expanding on that notion, the various anonymity, privacy, and circumvention tools target different people and use cases.  Tor advocates for <a href="https://blog.torproject.org/blog/circumvention-and-anonymity" rel="nofollow">Anonymity first, circumvention second</a>.  It would be very naive of us to think that we can solve all use cases.  In fact, it would be silly of us to try to dictate the needs of any user.  The larger the ecosystem of privacy and anonymity tools, the more options for users, and the better off we are as a whole.</p>

<p>At the core, Tor is a protocol and a set of specifications.  Others can take our documentation and build upon it for their own tool.  The EU PRIME project did this and created a fully Java implementation of Tor with a GUI.  The result was called OnionCoffee.  It's woefully out of date now, but the proof of concept stands; it can be done.  The purpose of specifying a protocol is to leave interpretation and implementation as open as possible.  Imagine if the creators of the Internet Protocol restricted implementations to exactly as they had envisioned 40-something years ago.  As for Tor, there is much more protocol work to be done, research completed, and our reference implementation polished before we can consider online anonymity solved, or even close to solved.</p>

<p>We are often asked, "I use tool X, what do you think about it?  Should I switch to Tor?"  </p>

<p>Instead of an answer, we ask a series of questions to find out why they use X; for the reasons of learning more about X, why people choose X, and what Tor may lack.  It turns out, they use X because their friends use X, and they know the strengths and weaknesses of the software well.  It may not be perfect, but they know what the software can and cannot do.  They know if a tool is compromised, or access to the service is shut off, they can switch to another.  If there was only one tool, once it is blocked or disappears, the users are screwed.  Isn't it great to have options?</p>

<p>Obviously, we state that Tor is a fine solution, and perhaps they should add the concept of anonymity and our software to their list of options.  Our goal is educating users to help themselves and others.  Anyone who suggests otherwise is trying to sell you something.</p>

---
_comments:

<a id="comment-690"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-690" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2009</p>
    </div>
    <a href="#comment-690">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-690" class="permalink" rel="bookmark">One Cell is Enough to Break</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One Cell is Enough to Break Tor’s Anonymity<br />
Xinwen Fu and Zhen Ling</p>
<p><a href="http://www.blackhat.com/html/bh-dc-09/bh-dc-09-speakers.html#Fu" rel="nofollow">http://www.blackhat.com/html/bh-dc-09/bh-dc-09-speakers.html#Fu</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-694"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-694" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 18, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-690" class="permalink" rel="bookmark">One Cell is Enough to Break</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-694">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-694" class="permalink" rel="bookmark">hardly</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>we'll have a response soon enough</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-698"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-698" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">February 18, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-690" class="permalink" rel="bookmark">One Cell is Enough to Break</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-698">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-698" class="permalink" rel="bookmark">Re: One Cell is Enough to Break</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/blog/one-cell-enough" rel="nofollow">https://blog.torproject.org/blog/one-cell-enough</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
