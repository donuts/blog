title: On recent and upcoming developments in Pluggable Transports
---
pub_date: 2014-06-15
---
author: asn
---
tags:

circumvention
pluggable transports
pt
---
categories: circumvention
---
_html_body:

<p>Hello friends,</p>

<p>this is a brief post on recent and upcoming developments of the <a href="https://www.torproject.org/docs/pluggable-transports.html.en" rel="nofollow">Pluggable Transport</a> world:</p>

<h1>What has happened</h1>

<p>Here is what has been keeping us busy during the past few months:  </p>

<h2>TBB 3.6</h2>

<p>As many of you know, the TBB team recently released the <a href="https://www.torproject.org/download/download-easy.html.en" rel="nofollow">Tor Browser Bundle 3.6</a> that features built-in PT support. This is great and has taken PT usage to <a href="https://metrics.torproject.org/users.html?graph=userstats-bridge-transport&amp;start=2014-03-17&amp;end=2014-06-15&amp;transport=obfs3#userstats-bridge-transport" rel="nofollow">new levels</a>. Maaad props to the TBB team for all their work.</p>

<p>TBB-3.6 includes obfs3 and FTE by default. If the built-in bridges are blocked for you (this is the case at least in China), try getting some more bridges from <a href="https://bridges.torproject.org" rel="nofollow">BridgeDB</a> (which also got renovated recently).</p>

<h2>obfs2 deprecation</h2>

<p>We are in the process of <a href="https://trac.torproject.org/projects/tor/ticket/10314" rel="nofollow">deprecating</a> the obfs2 pluggable transport.</p>

<p>This is because China blocks it using active probing, and because obfs3 is stictly better than obfs2. obfs3 can also be blocked using active probing, but China hasn't implemented this yet (at least as far as we know). The new upcoming line of PTs (like scramblesuit and obfs4) should be able to defend more effectively against active probing. </p>

<h2>Outgoing proxies and Pluggable Transports</h2>

<p>Yawning Angel et al. recently implemented <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/232-pluggable-transports-through-proxy.txt" rel="nofollow">outgoing proxy support for PTs</a>. This means that soon our PTs will be able to connect to an outgoing proxy using the Socks5Proxy torrc option (or the corresponding proxy field in TBB).</p>

<h2>A Childs Garden Of Pluggable Transports</h2>

<p>David Fifield created <a href="https://trac.torproject.org/projects/tor/wiki/doc/AChildsGardenOfPluggableTransports" rel="nofollow">refreshing visualizations of Pluggable Transports</a>. Take a look; it might help you understand what these damned things are doing.</p>

<h1>What will happen</h1>

<p>Now let's take a look into the short-term future (a few months ahead) of Pluggable Transports:</p>

<h2>obfs4 and ScrambleSuit</h2>

<p>Remember <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003886.html" rel="nofollow">ScrambleSuit</a>? Guess what; we are thinking of <b>not</b> deploying it after all...</p>

<p>Don't get me wrong, ScrambleSuit is great, but during the past two months Yawning has been developing a new transport called <a href="https://github.com/Yawning/obfs4" rel="nofollow">'obfs4'</a>. obfs4 is like ScrambleSuit (with regards to features and threat model), but it's faster and autofixes some of the open issues with scramblesuit (<a href="https://trac.torproject.org/projects/tor/ticket/10887" rel="nofollow">#10887</a>, <a href="https://trac.torproject.org/projects/tor/ticket/11271" rel="nofollow">#11271</a>, ...).</p>

<p>Since scramblesuit has not been entirely deployed yet, we thought that it would be a good idea to deploy obfs4 instead, and keep scramblesuit around as an emergency PT.</p>

<h2>Meek</h2>

<p>Meek is an exciting new transport by David Fifield. You can read all about it here: <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/meek</a></p>

<p>It's basically a transport that (ab)uses Firefox to do SSL in a way that makes it look like Firefox but underneath it's actually Tor. Very sneaky, and because it uses third-party services (like Google Appspot, Akamai, etc.) as proxies, the user does not need to input a bridge. Meek just works bridgeless and automagically.</p>

<p>Help us by testing the latest bundles that David made: <a href="https://lists.torproject.org/pipermail/tor-qa/2014-June/000422.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-qa/2014-June/000422.html</a></p>

<p>Also, since the <a href="https://en.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access" rel="nofollow">recent Google block in China</a>, Meek will not work with Google Appspot. However, other third-party services can be used instead of Appspot, so Meek does not lose its effectiveness.</p>

<h2>PTs and IPv6</h2>

<p>PTs are not very good at IPv6 yet. We identified some of <a href="https://trac.torproject.org/projects/tor/ticket/12138" rel="nofollow">the</a> <a href="https://trac.torproject.org/projects/tor/ticket/11211" rel="nofollow">open</a> <a href="https://trac.torproject.org/projects/tor/ticket/7961" rel="nofollow">issues</a> and hopefully we will fix them too.</p>

<p>And that's that for now.</p>

<p>Till next time, enjoy life and give thanks and praises :)</p>

<p>(For what it's worth, this was originally a post in the [tor-talk] mailing list:<br />
<a href="https://lists.torproject.org/pipermail/tor-talk/2014-June/033296.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2014-June/033296.html</a>)</p>

---
_comments:

<a id="comment-63392"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63392" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 15, 2014</p>
    </div>
    <a href="#comment-63392">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63392" class="permalink" rel="bookmark">The announcements and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The announcements and promotions made concerning obfs3, obfs4 and Meek are timely and interesting.</p>
<p>I am a businessman working and living in China for at least half a year while the rest is spent on travelling outside of it. One of my so-called leisure activities is to help Chinese people regain their freedom of expression on the internet.</p>
<p>If Tor is to achieve one of its objectives, which is to help people living under authoritarian regimes such as China, North Korea and Iran to communicate without fear and punishment with the outside world, then Tor must provide detailed "How To" guides on how to use obfs3, obfs4 and Meek. Tor developers: for your info, most Chinese dissidents are at least 40 years old and are not IT experts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63394"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63394" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63394">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63394" class="permalink" rel="bookmark">Thanks for answering my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for answering my question about Meek and China that I asked on the "Tor Challenge 2014" blog. I'm guessing you don't list all the third-party services Meek can use so China can't block all of them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-63423"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63423" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63394" class="permalink" rel="bookmark">Thanks for answering my</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-63423">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63423" class="permalink" rel="bookmark">No, they do list. Amazon</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, they do list. Amazon cloud is to work with meek, other than Google. Any other service that will ignore/redirect the front to second service can be used.<br />
You can't hide services like you think, because Someone can download meek and see its settings to see which servers it connects to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-63601"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63601" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63423" class="permalink" rel="bookmark">No, they do list. Amazon</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-63601">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63601" class="permalink" rel="bookmark">What prevents Chinese ( or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What prevents Chinese ( or other ) censors from doing exactly what you said, "Someone can download meek and see its settings to see which servers it connects to" and block whatever third party services meek is using at that time?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-63640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63640" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63601" class="permalink" rel="bookmark">What prevents Chinese ( or</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-63640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63640" class="permalink" rel="bookmark">This is fine, if they&#039;re</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is fine, if they're willing to block Google, Amazon, Cloudflare, Akamai, etc.</p>
<p>But maybe they will hesitate because of the collateral damage from such widespread blocking?</p>
<p>It won't work everywhere (sometimes China blocks all Google services for example), but there are some censored places where it will work well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-63399"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63399" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63399">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63399" class="permalink" rel="bookmark">trac.torproject.org&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>trac.torproject.org's collective cypherpunks login does work anymore, or had it's password changed.</p>
<p>I'm posting this here in the hope that someone can forward it to the right people.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63402" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63402" class="permalink" rel="bookmark">I&#039;m in Iran and use obfs3</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm in Iran and use obfs3 bridges. Obf2 works faster than obf3 here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63403" class="permalink" rel="bookmark">I&#039;m in Iran and use obfs2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm in Iran and use obfs2 bridges. Obfs2 works faster than obf3 here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63408" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63408" class="permalink" rel="bookmark">If meek makes your Firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If meek makes your Firefox connect to the Big Brother's favorite traffic info collection pots like Google and Akamai, would not that expose you to the unwanted traffic pattern learning?</p>
<p>Your IP address (identifies your ISP, your location and eventually you), plus the fingerprint of your unique Firefox, plus the time of connections (to build a profile of your habits/schedule)... And the Big Bro loves your Firefox's JavaScript doors and the Mozilla's undercover communications (Add-on/search_engine auto updates, Safebrowsing, Crash reporter, etc.)</p>
<p>We were already told that it's somehow "OK" to for the Obfsproxy bridges to have a load-screaming "I'm a Tor bridge!" domain name. Now we need meek to connect our Firefox to Google every time. <b>Hey, why not running meek via the .gov sites directly?</b></p>
<p>I appreciate your efforts and new gadgets, but... Are we turning a bit blind about the American Big Bro, (or are we working for him?), or meek is only good for the Chinese users?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63408" class="permalink" rel="bookmark">If meek makes your Firefox</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65458" class="permalink" rel="bookmark">Anyone? This seems like an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone? This seems like an legit concern. Can somone elaborate?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-65540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65540" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63408" class="permalink" rel="bookmark">If meek makes your Firefox</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65540" class="permalink" rel="bookmark">meek is a fine research</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>meek is a fine research project to explore what else is possible in terms of hiding Tor  communications in services that normal users use too. As a comparison, see the CloudTransport talk presented at PETS last week:<br />
<a href="https://www.petsymposium.org/2014/program.php" rel="nofollow">https://www.petsymposium.org/2014/program.php</a><br />
This is a nice research area that I'd love to see more work on -- the "get a bunch of bridges" arms race is no fun so we need to find some alternatives.</p>
<p>As for the unique Firefox question, meek uses the Tor Browser (with a separate profile), so in theory it won't be unique, but rather you will blend with all the other people using that browser.</p>
<p>As for JavaScript, this is a fine and interesting question. I've opened<br />
<a href="https://trac.torproject.org/projects/tor/ticket/12671" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/12671</a><br />
for David to investigate that one.</p>
<p>But yes, one of the drawbacks to meek's approach is if you're fronting through Amazon S3 and then your destination is a website hosted by Amazon -- then they're in a great position to see both sides of your circuit. That seems like a fundamental tradeoff to this approach, and resolving that contradiction is an open research problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-63415"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63415" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2014</p>
    </div>
    <a href="#comment-63415">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63415" class="permalink" rel="bookmark">The opening sentence should</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The opening sentence should be capitalized, and I'm dismayed to see you swearing in these blog posts. This post just seemed less professional than other posts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63609"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63609" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 21, 2014</p>
    </div>
    <a href="#comment-63609">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63609" class="permalink" rel="bookmark">Here China, tried to use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here China, tried to use meek but with no success (failed to establish connection...).<br />
Then, i pasted my working custom bridges and it connected normally, but saying Browser is not updated.<br />
Wasn't it supposed to be updated? (meek is based on TB 3.6.2, right?)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-63980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-63980" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 27, 2014</p>
    </div>
    <a href="#comment-63980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-63980" class="permalink" rel="bookmark">i&#039;m use tor through vpn</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i'm use tor through vpn proxy ,last month no longer work<br />
seems blocked .<br />
i'm become hate Tor and such many stupid updates.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-64538"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64538" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-63980" class="permalink" rel="bookmark">i&#039;m use tor through vpn</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-64538">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64538" class="permalink" rel="bookmark">Be glad of them, the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Be glad of them, the alternative to not being able to communicate at all is far, far worse.  Imagine if it worked, and you were compromised because one of those "stupid updates" had not been applied.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-64060"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64060" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 29, 2014</p>
    </div>
    <a href="#comment-64060">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64060" class="permalink" rel="bookmark">Blocked in saudi arabia .</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Blocked in saudi arabia .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-64286"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64286" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2014</p>
    </div>
    <a href="#comment-64286">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64286" class="permalink" rel="bookmark">Hi only a question, how to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi only a question, how to enjoy the scramblesuit by only using tor vidalia bundle?<br />
Do I need to deploy obfsproxy.exe for TP/tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-64367"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64367" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 05, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-64286" class="permalink" rel="bookmark">Hi only a question, how to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-64367">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64367" class="permalink" rel="bookmark">To use scramblesuit in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To use scramblesuit in the Vidalia bundle, you'd need to compile it all and configure it yourself. That's why the Tor Browser comes with all of it pre-configured for you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-64397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 05, 2014</p>
    </div>
    <a href="#comment-64397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64397" class="permalink" rel="bookmark">You guys are just Great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You guys are just Great</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-64426"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64426" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 05, 2014</p>
    </div>
    <a href="#comment-64426">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64426" class="permalink" rel="bookmark">Can&#039;t download any version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can't download any version of Tor on my IPhone. Please advise</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-64455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-64455" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 06, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-64426" class="permalink" rel="bookmark">Can&#039;t download any version</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-64455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-64455" class="permalink" rel="bookmark">Correct -- there aren&#039;t any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Correct -- there aren't any safe packages of Tor on the iphone -- mainly because there is no Firefox for ios, so there is no Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
