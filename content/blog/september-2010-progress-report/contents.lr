title: September 2010 Progress Report
---
pub_date: 2010-10-11
---
author: phobos
---
tags:

progress report
translation
releases
new hires
---
categories:

localization
releases
reports
---
_html_body:

<p><strong>New Hires</strong></p>

<ul>
<li>Tom Heydt-Benjamin is contracted full-time to work on NSF-related research through December 2010.</li>
<li>Erinn Clark is contracted full-time to work on improving packaging, secure updater, tor browser bundles, and continuous integration systems through December 2010.  Previously, Erinn was part-time.</li>
</ul>

<p><strong>New Releases</strong></p>

<ul>
<li>Tor Browser Bundle for Mac OS X is now available for the i386 architecture in 11 languages.  This is an alpha release to let the community test functionality. Read more at <a href="https://blog.torproject.org/blog/tor-browser-bundle-mac-os-x" rel="nofollow">https://blog.torproject.org/blog/tor-browser-bundle-mac-os-x</a></li>
<li>Orbot, Tor for Android, was released into the Android Market.  Android users can now officially download and install Orbot on their devices. <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">https://guardianproject.info/apps/orbot/</a></li>
<li>Version 0.2.10 of the graphical controller for tor, Vidalia, is now available. The main change is the use of Tor's native geoip database to map relay IP addresses to country. This removes the remote geoip resolution that occurred over Tor in previous versions of Vidalia. Read more at <a href="https://blog.torproject.org/blog/vidalia-0210-released" rel="nofollow">https://blog.torproject.org/blog/vidalia-0210-released</a></li>
<li>Tor 0.2.2.16-alpha was released, Tor 0.2.2.16-alpha fixes a variety of old stream fairness bugs (most evident at exit relays), and also continues to resolve all the little<br />
bugs that have been filling up trac lately. <a href="https://blog.torproject.org/blog/tor-02216-alpha-released" rel="nofollow">https://blog.torproject.org/blog/tor-02216-alpha-released</a></li>
<li>We released Torbutton 1.3.0-alpha.  Torbutton 1.3.0-alpha is the first release of Torbutton where most of the code has come from our community members! The full announcement is available at <a href="http://archives.seul.org/or/talk/Sep-2010/msg00140.html" rel="nofollow">http://archives.seul.org/or/talk/Sep-2010/msg00140.html</a>. </li>
<li>Tor 0.2.2.17-alpha introduces a feature to make it harder for clients to use one-hop circuits (which can put the exit relays at higher risk, plus unbalance the network); fixes a big bug in bandwidth accounting for relays that want to limit their monthly bandwidth use; fixes a big pile of bugs in how clients tolerate temporary network failure; and makes our adaptive circuit build timeout feature (which improves client performance if your network is fast while not breaking things if your network is slow) better handle bad networks. <a href="https://blog.torproject.org/blog/tor-02217-alpha-out" rel="nofollow">https://blog.torproject.org/blog/tor-02217-alpha-out</a></li>
</ul>

<p><strong>Design, develop, and implement enhancements that make Tor a better tool for users in censored countries.</strong><br />
Mike wrote up the current state of progress on a torbutton extension for Google's Chrome web browser, <a href="https://blog.torproject.org/blog/google-chrome-incognito-mode-tor-and-fingerprinting" rel="nofollow">https://blog.torproject.org/blog/google-chrome-incognito-mode-tor-and-f…</a>.</p>

<p><strong>Architecture and technical design docs for Tor enhancements related to blocking-resistance.</strong><br />
Steven submitted a proposal to automatically promote nodes to bridges.  This proposal describes how Tor clients could determine when they have sufficient bandwidth capacity and are sufficiently reliable to become either bridges or Tor relays. When they meet this criteria, they will automatically promote themselves, based on user preferences. The proposal also defines the new controller messages and options which will control this process. <a href="https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposals/175-automatic-node-promotion.txt" rel="nofollow">https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposa…</a>. </p>

<p><strong>Outreach and Advocacy</strong></p>

<ul>
<li>We released an article about the ``Ten Things to Look for in a Circumvention Tool''. As more countries crack down on Internet use, people around the world are turning to anti-censorship software that lets them reach blocked websites. Many types of software, also known as circumvention tools, have been created to answer the threat to freedom online. These tools provide different features and levels of security, and it's important for users to understand the tradeoffs. This article lays out ten features you should consider when evaluating a circumvention tool. The goal isn't to advocate for any specific tool, but to point out what kind of tools are useful for different situations. Full details at <a href="https://www.torproject.org/press/2010-09-16-ten-things-circumvention-tools.html.en" rel="nofollow">https://www.torproject.org/press/2010-09-16-ten-things-circumvention-to…</a></li>
<li>Karen attended a meeting of the United Nations Internet Governance Forum in Vilnius, Lithuania. <a href="http://igf2010.lt/" rel="nofollow">http://igf2010.lt/</a></li>
<li>Karen attended Ars Electronica in Linz, Austria.  Tor received an Honorable Mention for Digital Communities. <a href="http://aec.at" rel="nofollow">http://aec.at</a></li>
<li>Karen attended <a href="mailto:Internet@Liberty" rel="nofollow">Internet@Liberty</a> in Budapest, Hungary.  <a href="https://sites.google.com/a/pressatgoogle.com/internet-at-liberty-2010/" rel="nofollow">https://sites.google.com/a/pressatgoogle.com/internet-at-liberty-2010/</a> and  <a href="http://jilliancyork.com/tag/ial2010/" rel="nofollow">http://jilliancyork.com/tag/ial2010/</a> for Jillian's summary.</li>
<li>Sebastian gave a talk at SourceBarcelona about Anonymity, Privacy, and the real world. <a href="http://www.sourceconference.com/index.php/barcelona-2010/bcn2010-schedule#Jacob" rel="nofollow">http://www.sourceconference.com/index.php/barcelona-2010/bcn2010-schedu…</a></li>
<li>Nick and Andrew held a Boston Hackfest to explain Tor's internals, hack on some Tor simulator code, and generally be available for questions and help on topics relating to Tor.  <a href="https://blog.torproject.org/blog/boston-tor-hackers-join-us-sunday-september-19th" rel="nofollow">https://blog.torproject.org/blog/boston-tor-hackers-join-us-sunday-sept…</a>.</li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<ul>
<li>Erinn synchronized the Windows, OS X, and Linux tor browser bundles to use the same configurations and included software. </li>
<li>The TAILS team continues to improve and update their LiveCD available at <a href="https://amnesia.boum.org" rel="nofollow">https://amnesia.boum.org</a>.</li>
<li>Jacob began an audit of the TAILS LiveCD to help assess the safety and security of the software for users in highly-volatile situations.</li>
</ul>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong><br />
Created Tor 0.2.3 branch and started work on integrated bufferevents and microdescriptors.  Bufferevents are the proper way to manage heavy network i/o in Microsoft Windows.  Microdescriptors are a way to let clients on extremely low-bandwidth connections bootstrap into the Tor network.  Proposal 158, <a href="https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposals/158-microdescriptors.txt" rel="nofollow">https://gitweb.torproject.org/tor.git/blob_plain/HEAD:/doc/spec/proposa…</a>, defines microdescriptors.</p>

<p><strong>Translation work</strong></p>

<ul>
<li>We began the migration from our Pootle-based translation portal to Transifex after a discussion with their developers.  Transifex provides free hosting of their translation software for free software projects and access to thousands of translators shared between hosted projects.  All of our translations will be completed through Transifex going forward.  <a href="https://www.transifex.net/projects/p/torproject/" rel="nofollow">https://www.transifex.net/projects/p/torproject/</a>.</li>
<li>Updated translations for Russian, Arabic, Persian, Greek, Burmese, French, Italian, Swedish, Dutch, German, Spanish, Polish, and Simplified Chinese.</li>
</ul>

---
_comments:

<a id="comment-8007"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8007" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2010</p>
    </div>
    <a href="#comment-8007">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8007" class="permalink" rel="bookmark">What good is TOR if every</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What good is TOR if every tor exit is identified by "anonymizer5.torservers.net" or "tor-exit.torserver.net" and is now blocked by so many websites?</p>
<p>TOR used to be useful when those websites didn't employ means to block it, but now it's a different story... think about something or give up the illusion of anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8008"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8008" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2010</p>
    </div>
    <a href="#comment-8008">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8008" class="permalink" rel="bookmark">How will tor protect against</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How will tor protect against the FBI and other anti privacy/ anti crypto systems when if the fbi requires backdoor in crypto systems?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8015"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8015" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2010</p>
    </div>
    <a href="#comment-8015">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8015" class="permalink" rel="bookmark">Erinn Clark is contracted</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><cite>Erinn Clark is contracted full-time to work on improving packaging, secure updater, tor browser bundles, and continuous integration systems...</cite></p>
<p>That's a very good news!!!!!!!!!! Erinn is great!!!!!!! He does really useful things at the TorProject!!!!!!!!!!!! and he does them for real!!!!!!!!!!!!!!!!!!!!!!!!</p>
<p>bye!!!!!!!!!!!!!!<br />
~bee!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2010</p>
    </div>
    <a href="#comment-8022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8022" class="permalink" rel="bookmark">I need some advice.
I have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I need some advice.</p>
<p>I have installed, on a USB, vidalia bundle 0.2.1.26-0.2.9.exe (under Vista ,32 bit)</p>
<p>I have as directories:</p>
<p>Polipo<br />
Tor<br />
Torbutton<br />
Vidalia</p>
<p>I have Fireox portable installed searately.</p>
<p>I am thinking of updating this suite (Vidalia bundle), but can not fathom which bundle to use.</p>
<p>The simplest solution would be the best.</p>
<p>One of my difficulties is trying to determine which of the products are updates of my existing products!</p>
<p>Can anyone advise?</p>
<p>Thanks.</p>
<p>A</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8134"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8134" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 13, 2010</p>
    </div>
    <a href="#comment-8134">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8134" class="permalink" rel="bookmark">Is 208.53.142.39 aka</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is 208.53.142.39 aka tor-exit-proxy3-readme.formlessnetworking.net a honey pot?</p>
<p>No matter how many times I select "New Identity", TOR ends up always exiting through this tor exit (or another one in Dallas, TX).</p>
<p>What gives?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-8550"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8550" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-8134" class="permalink" rel="bookmark">Is 208.53.142.39 aka</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-8550">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8550" class="permalink" rel="bookmark">Read the docs/FAQ. Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Read the docs/FAQ. Tor reuses paths. This is good for security.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-8276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2010</p>
    </div>
    <a href="#comment-8276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8276" class="permalink" rel="bookmark">Thank you for making this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for making this tool available to people and all of your hard work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9596"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9596" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2011</p>
    </div>
    <a href="#comment-9596">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9596" class="permalink" rel="bookmark">Tor v1.0.4 not working on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor v1.0.4 not working on rooted LG Ally running Froyo. Seems to come up, with green "power" button eventually displayed after several steps -- "Connected to the Tor network" displayed. But no actual Tor connectivity -- the "Check" button pulls up Browser with "Sorry, You are not using Tor." displayed.</p>
<p>Other web apps, e.g., email, show the usual IP address of the phone, nothing changed. So it's pretty convincing that there really is no Tor connection.</p>
<p>Any help would be greatly appreciated.</p>
<p>-Ken C.</p>
</div>
  </div>
</article>
<!-- Comment END -->
