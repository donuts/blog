title: Tails 0.21 is out
---
pub_date: 2013-10-29
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.21, is out.</p>

<p>All users must upgrade as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.20.1/" rel="nofollow">numerous security issues</a>.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<ul>
<li>Security fixes
<ul>
<li>Don't grant access to the Tor control port for the desktop user. Else, an attacker able to run arbitrary code as this user could obtain the public IP.</li>
<li>Don't allow the desktop user to directly change persistence settings. Else, an attacker able to run arbitrary code as this user could leverage this feature to gain persistent root access, as long as persistence is enabled.</li>
<li>Install Iceweasel 17.0.10esr with Torbrowser patches.</li>
<li>Patch Torbutton to make window resizing closer to what the design says.</li>
</ul>
</li>
<li>New features
<ul>
<li>Add a persistence preset for printing settings.</li>
<li>Support running Tails off more types of SD cards.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Add a KeePassX launcher to the top panel.</li>
<li>Improve the bug reporting workflow.</li>
<li>Prefer stronger ciphers when encrypting data with GnuPG.</li>
<li>Exclude the version string in GnuPG's ASCII armored output.</li>
<li>Use the same custom Startpage search URL than the TBB. This apparently disables the new broken "family" filter.</li>
<li>Provide a consistent path to the persistent volume mountpoint.</li>
</ul>
</li>
<li>Localization
<ul>
<li>Many translation updates all over the place.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li>On some hardware, Vidalia does not start.</li>
<li>Longstanding <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</li>
</ul>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is scheduled for around December 11.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, <a href="https://tails.boum.org/support/" rel="nofollow">come talk to us</a>!</p>

