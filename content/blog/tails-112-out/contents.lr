title: Tails 1.1.2 is out
---
pub_date: 2014-09-25
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.1.2, is out.</p>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.1.1/" rel="nofollow">numerous security issues</a> and all users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible.</p>

<p>We prepared this release mainly to fix a <a href="https://blog.mozilla.org/security/2014/09/24/rsa-signature-forgery-in-nss/" rel="nofollow">serious flaw</a> in the Network Security Services (NSS) library used by Firefox and other products allows attackers to create forged RSA certificates.</p>

<p>Before this release, users on a compromised network could be directed to sites using a fraudulent certificate and mistake them for legitimate sites. This could deceive them into revealing personal information such as usernames and passwords. It may also deceive users into downloading malware if they believe it’s coming from a trusted site.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.8.0esr-0+tails3~bpo70+1</li>
<li>Install Linux 3.16-1</li>
<li>Numerous other software upgrades that fix security issues: GnuPG, APT, DBus, Bash, and packages built from the bind9 and libav source packages</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<p><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</p>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for October 14.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

