title: Tails 2.0 is out
---
pub_date: 2016-01-26
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>We are especially proud to present you Tails 2.0, the first version of Tails based on:</p>

<ul>
<li><i>GNOME Shell</i>, with lots of changes in the desktop environment.</li>
<li>Debian 8 (Jessie), which upgrades most included software and improves many things under the hood.</li>
</ul>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.8.2/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<p>Tails now uses the <i>GNOME Shell</i> desktop environment, in its <i>Classic</i> mode. <i>GNOME Shell</i> provides a modern, simple, and actively developed desktop environment. The <i>Classic</i> mode keeps the traditional <i>Applications</i>, <i>Places</i> menu, and windows list. Accessibility and non-Latin input sources are also better integrated.</p>

<p>To find your way around, <a href="https://tails.boum.org/doc/first_steps/introduction_to_gnome_and_the_tails_desktop/" rel="nofollow">read our introduction to GNOME and the Tails desktop.</a></p>

<h2>Upgrades and changes</h2>

<ul>
<li>Debian 8 upgrades most included software, for example:
<ul>
<li>Many core GNOME utilities from 3.4 to 3.14: <i>Files</i>, <i>Disks</i>, <i>Videos</i>, etc.</li>
<li><i>LibreOffice</i> from 3.5 to 4.3</li>
<li><i>PiTiVi</i> from 0.15 to 0.93</li>
<li><i>Git</i> from 1.7.10 to 2.1.4</li>
<li><i>Poedit</i> from 1.5.4 to 1.6.10</li>
<li><i>Liferea</i> from 1.8.6 to 1.10</li>
</ul>
</li>
<li>Update <i>Tor Browser</i> to 5.5 (based on Firefox 38.6.0 ESR):
<ul>
<li>Add Japanese support.</li>
</ul>
</li>
<li>Remove the Windows camouflage which is currently broken in GNOME Shell. We started working on <a href="https://labs.riseup.net/code/issues/10830" rel="nofollow">adding it back</a> but <a href="./windows_camouflage_jessie/" rel="nofollow">your help is needed</a>!</li>
<li>Change to <span class="geshifilter"><code class="php geshifilter-php">systemd</code></span> as init system and use it to:
<ul>
<li>Sandbox many services using Linux namespaces and make them harder to exploit.</li>
<li>Make the launching of Tor and the memory wipe on shutdown more robust.</li>
<li>Sanitize our code base by replacing many custom scripts.</li>
</ul>
</li>
<li>Update most firmware packages which might improve hardware compatibility.</li>
<li>Notify the user if Tails is running from a non-free virtualization software.</li>
<li>Remove <i>Claws Mail</i>, replaced by <i><a href="https://tails.boum.org/doc/anonymous_internet/icedove/" rel="nofollow">Icedove</a></i>, a rebranded version of <i>Mozilla Thunderbird</i>.</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>HiDPI displays are better supported. (<a href="https://labs.riseup.net/code/issues/8659" rel="nofollow">#8659</a>)</li>
<li>Remove the option to open a download with an external application in Tor Browser as this is usually impossible due to the AppArmor confinement. (<a href="https://labs.riseup.net/code/issues/9285" rel="nofollow">#9285</a>)</li>
<li>Close <i>Vidalia</i> before restarting Tor.</li>
<li>Allow <i>Videos</i> to access the DVD drive. (<a href="https://labs.riseup.net/code/issues/10455" rel="nofollow">#10455</a>, <a href="https://labs.riseup.net/code/issues/9990" rel="nofollow">#9990</a>)</li>
<li>Allow configuring printers without administration password. (<a href="https://labs.riseup.net/code/issues/8443" rel="nofollow">#8443</a>)</li>
</ul>

<h2>Known issues</h2>

<ul>
<li>Tor Browser 5.5 introduces <a href="https://trac.torproject.org/projects/tor/ticket/13313" rel="nofollow">protection against fingerprinting</a> but due to an <a href="https://labs.riseup.net/code/issues/11000" rel="nofollow">oversight</a> it is not enabled in Tails 2.0. However, this is not so bad for Tails users since each Tails system has the same fonts installed, and hence will look identical, so this only means that it's easy to distinguish whether a user of Tor Browser 5.5 uses Tails or not. That is already easy given that Tails has the AdBlock Plus extension enabled, unlike the normal Tor Browser.</li>
</ul>

<p>See the current list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</p>

<h2>Installing</h2>

<p>We also redesigned completely our download and installation instructions to make it easier to get started with Tails.</p>

<p>For example, you can now verify the ISO image automatically from Firefox using a special add-on.</p>

<p>You can also install or upgrade Tails directly from Debian or Ubuntu using the <span class="geshifilter"><code class="php geshifilter-php">tails<span style="color: #339933;">-</span>installer</code></span> package.</p>

<p><a href="https://tails.boum.org/install/" rel="nofollow">Try our new installation assistant.</a></p>

<h2>Upgrading</h2>

<p>Tails changed so much since version 1.8.2 that it is impossible to provide an automatic upgrade. We recommend you <a href="https://tails.boum.org/upgrade/" rel="nofollow">follow our new manual upgrade instructions</a> instead.</p>

<h2>What's coming up?</h2>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for March 6.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/contribute/talk/" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

