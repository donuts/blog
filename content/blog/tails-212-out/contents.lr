title: Tails 2.12 is out
---
pub_date: 2017-04-19
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.11/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>
We installed again <em>GNOME Sound Recorder</em> to provide a very simple application for recording sound in addition to the more complex <em>Audacity</em>. Sound clips recorded using <em>GNOME Sound Recorder</em> are saved to the <em>Recordings</em> folder.
</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>
We removed <a href="//geti2p.net/" rel="nofollow"><em>I2P</em></a>, an alternative anonymity network, because we unfortunately have failed to find a developer to maintain <em>I2P</em> in Tails. Maintaining software like <em>I2P</em> well-integrated in Tails takes time and effort and our team is too busy with other priorities.
</li>
<li>
Upgrade <em>Linux</em> to 4.9.13. This should improve the support for newer hardware (graphics, Wi-Fi, etc.).
</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h1>Known issues</h1>

<p>None specific to this release.</p>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1>Get Tails 2.12</h1>

<ul>
<li>To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.</li>
<li>To upgrade, automatic upgrades are available from 2.10 and 2.11 to 2.12.
<p>If you cannot do an automatic upgrade or if you fail to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.</p></li>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 2.12.</a></li>
</ul>

<h2>What's coming up?</h2>

<p>Tails 3.0 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for June 13th.</p>

<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/#2.12" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

