title: Tor 0.2.6.9 is released.
---
pub_date: 2015-06-11
---
author: nickm
---
tags:

stable
release
026
source
---
categories: releases
---
_html_body:

<p>Hi! I've just put out a new stable Tor release. It is not a high-urgency item for most clients and relays, but directory authorities should upgrade, as should any clients who rely on port-based circuit isolation. Right now, the source is available on the website, and packages should become available once their maintainers build them.</p>

<p>Tor 0.2.6.9 fixes a regression in the circuit isolation code, increases the requirements for receiving an HSDir flag, and addresses some other small bugs in the systemd and sandbox code. Clients using circuit isolation should upgrade; all directory authorities should upgrade. </p>

<h2>Changes in version 0.2.6.9 - 2015-06-11</h2>

<ul>
<li>Major bugfixes (client-side privacy):
<ul>
<li>Properly separate out each SOCKSPort when applying stream isolation. The error occurred because each port's session group was being overwritten by a default value when the listener connection was initialized. Fixes bug 16247; bugfix on 0.2.6.3-alpha. Patch by "jojelino".
  </li>
</ul>
</li>
<li>Minor feature (directory authorities, security):
<ul>
<li>The HSDir flag given by authorities now requires the Stable flag. For the current network, this results in going from 2887 to 2806 HSDirs. Also, it makes it harder for an attacker to launch a sybil attack by raising the effort for a relay to become Stable which takes at the very least 7 days to do so and by keeping the 96 hours uptime requirement for HSDir. Implements ticket 8243.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Minor bugfixes (compilation):
<ul>
<li>Build with --enable-systemd correctly when libsystemd is installed, but systemd is not. Fixes bug 16164; bugfix on 0.2.6.3-alpha. Patch from Peter Palfrader.
  </li>
</ul>
</li>
<li>Minor bugfixes (Linux seccomp2 sandbox):
<ul>
<li>Fix sandboxing to work when running as a relaymby renaming of secret_id_key, and allowing the eventfd2 and futex syscalls. Fixes bug 16244; bugfix on 0.2.6.1-alpha. Patch by Peter Palfrader.
  </li>
<li>Allow systemd connections to work with the Linux seccomp2 sandbox code. Fixes bug 16212; bugfix on 0.2.6.2-alpha. Patch by Peter Palfrader.
  </li>
</ul>
</li>
<li>Minor bugfixes (tests):
<ul>
<li>Fix a crash in the unit tests when built with MSVC2013. Fixes bug 16030; bugfix on 0.2.6.2-alpha. Patch from "NewEraCracker".
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-94863"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94863" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 11, 2015</p>
    </div>
    <a href="#comment-94863">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94863" class="permalink" rel="bookmark">I don&#039;t see how to download</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't see how to download the new version. Can it be downloaded from here?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94917"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94917" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94917">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94917" class="permalink" rel="bookmark">To: Developer(s) of Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To: Developer(s) of Tor 0.2.6.9</p>
<p>Firstly a big thank-you for your time and effort.</p>
<p>Secondly, please help us understand why you guys made a public announcement on Tor 0.2.6.9 when the next release of Tails is scheduled for June 30, 2015?</p>
<p>Your publishing the bugfixes risk unmasking our anonymity, the anonymity of Tails' users and possibly those who use Orbot.</p>
<p>Even at the time of writing this feedback, we are still using the old version of Tor (version 4.5.1) and erinn or arma have not yet released fresh install executables based on Tor 0.2.6.9.  No thanks to you guys, our anonymity is now at risk of being unmasked.</p>
<p>Did you realize that you may have inadvertently helped the NSA, Iran, China, South Sudan and North Korea?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-95720"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95720" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 21, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94917" class="permalink" rel="bookmark">To: Developer(s) of Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-95720">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95720" class="permalink" rel="bookmark">Bugs are not secret,  stop</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bugs are not secret,  stop concern trolling.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94921"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94921" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94921">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94921" class="permalink" rel="bookmark">well done! seccomp2 sandbox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well done! seccomp2 sandbox is especially welcome for relay ops</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94943" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94943" class="permalink" rel="bookmark">Wasn&#039;t Tor created by the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wasn't Tor created by the U.S. navy?<br />
Because if it was then Tor could have a backdoor that the Tor Project isn't even aware of.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94945"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94945" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94945">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94945" class="permalink" rel="bookmark">Maybe this has been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe this has been discusses on tor mailing list.<br />
I suggest listing new features on the TBB startup page.<br />
When the security set settings feature is introduced,<br />
TBB users with moderate interest in tor pseudo-anonymity, will feel motivated to choose low or medium sets. TBB users with high concern, will choose high security set.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94946"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94946" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94946">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94946" class="permalink" rel="bookmark">Can this blog.torproject.org</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can this blog.torproject.org stylesheet be edited to allow reading when browser images are disabled?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94947" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2015</p>
    </div>
    <a href="#comment-94947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94947" class="permalink" rel="bookmark">I use a 14.x laptop. Windows</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use a 14.x laptop. Windows Vista, Many websites use poor css layout, and user's fastest method to fix or  improve this error is to Maximize the browser window. (When that doesn't improve enough, I then use text menu command, View&gt;&gt;Page Style &gt;&gt; No Style.)</p>
<p>But now let's look at how TB differs from regular browsers.<br />
The anti-fingerprinting feature that prevents  TBB window maximizing to full display size is irritatingly cumbersome.<br />
I don't maintain consciously aware fear  of this feature, so sometimes I semi-automatically click TBB browser Maximize button.<br />
Then TBB window becomes ridiculously huge. If I try slightly pulling an edge, TBB window completely disappears off the region of display.<br />
I then must try multiple times to right click TBB in windows taskbar. Sometimes menu shows only Minimize command, with the other commands gray. After more right clicks, the menu shows Size and Move. But neither works correctly, so they don't fix TBB window size as I recall fix the "disappeared" window problem of other software. </p>
<p>After much trying, either Move or Size, with arrow key and mouse movement, somewhat recover TBB window visibility. But TBB window is monstrously huge. I horizontally drag the top bar of TBB window multiple times. it seems to require more than ten left or right drags to pull one of TBB vertical edges into my display region.<br />
So my first gripe is, why does TBB 'punishment' enlarge TBB window to anywhere near as large as 100 inch diameter display?</p>
<p>---------------<br />
For many users. the faster method to restore window is to shutdown  TBB Then restart.<br />
TBB window size is not genuinely maximized, yet very close to maximize dimensions. (warning: I have tried this method only once, so perhaps this method is not reliable.)</p>
<p>I think the easiest fix is to punish user bad behavior by *shrinking* the TBB window.<br />
This may not be better because it's possible that the huge size  is not origin or cause of the 'bug'. Possibly if the 'punishing' window size is small, TBB will also disappear off the region of the computer's display when user tries pulling an edge.</p>
<p>Other possible fixes:<br />
TBB ignores the Maximize window button click.<br />
TBB responds to Maximize button by toggling the site's stylesheets (the same as "No Style" command). An unfamiliar user would be puzzled, but might often just click the Maximize button again, reloading the site stylesheet.<br />
After TBB goes into ;punishment' window size, any Resize, Minimize, or Maximize command restores the window size that preceded user's clicking<br />
Maximize)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94974" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2015</p>
    </div>
    <a href="#comment-94974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94974" class="permalink" rel="bookmark">Where can I get latest</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can I get latest version for Windows ?<br />
There are only 0.2.6.7 available for us :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-95183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95183" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">June 16, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94974" class="permalink" rel="bookmark">Where can I get latest</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-95183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95183" class="permalink" rel="bookmark">https://www.torproject.org/di</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/dist/torbrowser/4.5.2/tor-win32-0.2.6.9.zip" rel="nofollow">https://www.torproject.org/dist/torbrowser/4.5.2/tor-win32-0.2.6.9.zip</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2015</p>
    </div>
    <a href="#comment-94992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94992" class="permalink" rel="bookmark">WINDOWS BUILD PLEASE.
I AM</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>WINDOWS BUILD PLEASE.<br />
I AM USING IT AS A NT SERVICE FOR LONG TIME.</p>
<p>YOUR WINDOWS BUILD TOR IS TOO OLD, SO I AM USING TOR<br />
WHICH EXTRACTED FROM TOR BROWSER BUNDLE.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95012"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95012" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 14, 2015</p>
    </div>
    <a href="#comment-95012">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95012" class="permalink" rel="bookmark">After updating to this in my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After updating to this in my centos server hidden service is not working</p>
<p>also not able to stop tor: <a href="https://i.imgur.com/nP35m5S.png" rel="nofollow">https://i.imgur.com/nP35m5S.png</a></p>
<p>i tried re-installing still the same any help?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95061"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95061" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 14, 2015</p>
    </div>
    <a href="#comment-95061">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95061" class="permalink" rel="bookmark">is tor browser affected by</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is tor browser affected by the stream isolation issue?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2015</p>
    </div>
    <a href="#comment-95398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95398" class="permalink" rel="bookmark">I have looked all over the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have looked all over the torproject website and can't, for the life of me, find instructions on how to install Tor Expert Bundle. It feels like you are pushing everyone to use TB. I thought one of the things about FOSS was choice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-97055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-97055" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-95398" class="permalink" rel="bookmark">I have looked all over the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-97055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-97055" class="permalink" rel="bookmark">+1
Also, good luck finding</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>+1</p>
<p>Also, good luck finding the change log for anything but TB.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-97116"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-97116" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 11, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-97055" class="permalink" rel="bookmark">+1
Also, good luck finding</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-97116">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-97116" class="permalink" rel="bookmark">To be fair, you are writing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To be fair, you are writing this comment about changelogs in a blog post with a changelog for something other than Tor Browser. So you must have found at least this one. :)</p>
<p>Also, on <a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a> you can click on source code, and two links to changelogs (stable, alpha) are there for Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-95565"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95565" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 19, 2015</p>
    </div>
    <a href="#comment-95565">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95565" class="permalink" rel="bookmark">Please solve the captcha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please solve the captcha picture problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-95729"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-95729" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2015</p>
    </div>
    <a href="#comment-95729">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-95729" class="permalink" rel="bookmark">hope this is noticed...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hope this is noticed... apparently the process for reporting bad relays does not always work <a href="https://chloe.re/2015/06/20/a-month-with-badonions/" rel="nofollow">https://chloe.re/2015/06/20/a-month-with-badonions/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
