title: Tor 0.2.7.4-rc is released
---
pub_date: 2015-10-22
---
author: nickm
---
tags:

tor
release candidate
software
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.7.4-rc is the second release candidate in the 0.2.7 series. It fixes some important memory leaks, and a scary-looking (but mostly harmless in practice) invalid-read bug. It also has a few small bugfixes, notably fixes for compilation and portability on different platforms. If no further significant bounds are found, the next release will the the official stable release. </p>

<p>NOTE: This is a release candidate. We think we've squashed most of the bugs, but there are probably a few more left over.</p>

<p>You can download the source from the usual place on the website.<br />
Packages should be up in a few days.</p>

<h2>Changes in version 0.2.7.4-rc - 2015-10-21</h2>

<ul>
<li>Major bugfixes (security, correctness):
<ul>
<li>Fix an error that could cause us to read 4 bytes before the beginning of an openssl string. This bug could be used to cause Tor to crash on systems with unusual malloc implementations, or systems with unusual hardening installed. Fixes bug 17404; bugfix on 0.2.3.6-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (correctness):
<ul>
<li>Fix a use-after-free bug in validate_intro_point_failure(). Fixes bug 17401; bugfix on 0.2.7.3-rc.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (memory leaks):
<ul>
<li>Fix a memory leak in ed25519 batch signature checking. Fixes bug 17398; bugfix on 0.2.6.1-alpha.
  </li>
<li>Fix a memory leak in rend_cache_failure_entry_free(). Fixes bug 17402; bugfix on 0.2.7.3-rc.
  </li>
<li>Fix a memory leak when reading an expired signing key from disk. Fixes bug 17403; bugfix on 0.2.7.2-rc.
  </li>
</ul>
</li>
<li>Minor features (geoIP):
<ul>
<li>Update geoip and geoip6 to the October 9 2015 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Repair compilation with the most recent (unreleased, alpha) vesions of OpenSSL 1.1. Fixes part of ticket 17237.
  </li>
<li>Fix an integer overflow warning in test_crypto_slow.c. Fixes bug 17251; bugfix on 0.2.7.2-alpha.
  </li>
<li>Fix compilation of sandbox.c with musl-libc. Fixes bug 17347; bugfix on 0.2.5.1-alpha. Patch from 'jamestk'.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Use libexecinfo on FreeBSD to enable backtrace support. Fixes part of bug 17151; bugfix on 0.2.5.2-alpha. Patch from Marcin Cieślak.
  </li>
</ul>
</li>
<li>Minor bugfixes (sandbox):
<ul>
<li>Add the "hidserv-stats" filename to our sandbox filter for the HiddenServiceStatistics option to work properly. Fixes bug 17354; bugfix on tor-0.2.6.2-alpha. Patch from David Goulet.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Add unit tests for get_interface_address* failure cases. Fixes bug 17173; bugfix on 0.2.7.3-rc. Patch by fk/teor.
  </li>
<li>Fix breakage when running 'make check' with BSD make. Fixes bug 17154; bugfix on 0.2.7.3-rc. Patch by Marcin Cieślak.
  </li>
<li>Make the get_ifaddrs_* unit tests more tolerant of different network configurations. (Don't assume every test box has an IPv4 address, and don't assume every test box has a non-localhost address.) Fixes bug 17255; bugfix on 0.2.7.3-rc. Patch by "teor".
  </li>
<li>Skip backtrace tests when backtrace support is not compiled in. Fixes part of bug 17151; bugfix on 0.2.7.1-alpha. Patch from Marcin Cieślak.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Fix capitalization of SOCKS in sample torrc. Closes ticket 15609.
  </li>
<li>Note that HiddenServicePorts can take a unix domain socket. Closes ticket 17364.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-112210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112210" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 23, 2015</p>
    </div>
    <a href="#comment-112210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112210" class="permalink" rel="bookmark">I need a Windows binary in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I need a Windows binary in order to test it...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-112809"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112809" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-112210" class="permalink" rel="bookmark">I need a Windows binary in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-112809">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112809" class="permalink" rel="bookmark">You can create a binary with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can create a binary with a compiler.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-112379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112379" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2015</p>
    </div>
    <a href="#comment-112379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112379" class="permalink" rel="bookmark">Keep up the great work and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Keep up the great work and with proper testing!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-112408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112408" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2015</p>
    </div>
    <a href="#comment-112408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112408" class="permalink" rel="bookmark">Does this fix Tor&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does this fix Tor's vulnerability to threat#2 (state-level adversaries) on <a href="https://weakdh.org/" rel="nofollow">https://weakdh.org/</a>?  Apparently there are standard primes (and "Oakley Groups" based on them) that have been widely used for DH.  Tor is explicity mentioned in the paper as one of the vulnerable applications.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-112808"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112808" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-112408" class="permalink" rel="bookmark">Does this fix Tor&#039;s</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-112808">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112808" class="permalink" rel="bookmark">The paper says that Tor uses</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The paper says that Tor uses Oakley groups, not that it is/was vulnerable.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-112408" class="permalink" rel="bookmark">Does this fix Tor&#039;s</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113117" class="permalink" rel="bookmark">Logjam was patched in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Logjam was patched in the Tor Browser a good while ago. Links between nodes (client-relay and relay-relay) are protected via TLS 1.2, which should use ECDHE or 2048-bit DE keys. You can confirm this by running SSL tests against them. Circuit-level crypto (the onion-like encryption) uses Curve25519, which is not vulnerable to Logjam. I cannot find a mention of Tor in their paper, and while the Tor Browser was initially vulnerable, I do not believe that the Tor network itself was vulnerable, and as I said the Tor Browser is now patched.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113120"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113120" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113117" class="permalink" rel="bookmark">Logjam was patched in the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113120">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113120" class="permalink" rel="bookmark">The 0.2.7.x series has a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The 0.2.7.x series has a hard requirement at compile time for a SSL library that supports at least one ECDHE curve.</p>
<p>1024 bit DH is still used as part of the hidden service protocol, but it is in a hybrid construct with RSA (The older TAP handshake), so should still be reasonably safe.  Naturally this will be resolved completely when the next gen HS work gets done.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113117" class="permalink" rel="bookmark">Logjam was patched in the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113165" class="permalink" rel="bookmark">My question was not about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My question was not about Logjam but about Adrian <i>et al</i>'s item#2: "threats from state-level adversaries".  They suggest that one or more of the fixed 1024-bit primes has been "cracked", and that this is what has led to the NSA building their large infrastructure for eavesdropping on VPNs.</p>
<p>Tor is mentioned in the penultimate paragraph of section 2 of the conference paper, in connection with the Oakley Groups.</p>
<p>If 1024-bit is still used in HSs then let's hope for an early transition to more secure technologies.</p>
<p>Thanks for your answers anyway.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113209"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113209" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113165" class="permalink" rel="bookmark">My question was not about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113209">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113209" class="permalink" rel="bookmark">Indeed, the current HS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed, the current HS design uses DH wrapped in RSA, so it actually resists these it's-all-one-group attacks.</p>
<p>And the next-gen HS design has stronger crypto to make it even more moot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-112597"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112597" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2015</p>
    </div>
    <a href="#comment-112597">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112597" class="permalink" rel="bookmark">having trouble using tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>having trouble using tor with my windows laptop :( it downloaded but cant connect through firewal;l</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-112695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112695" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2015</p>
    </div>
    <a href="#comment-112695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112695" class="permalink" rel="bookmark">I know This Is Not The</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I know This Is Not The Proper Spot For This Blog, But Where's Our Bi-Monthly Weekly Bog From Tor?? Something's  Wrong ?? D-Hellman Problems Etc Etc?? Just PLZ Bring The Blog Back!! I Read It A Lot To Simi Keep up todate With All That's Tor. Thank's Plz Keep Tor Safe An Thus Inturn Keep Us Safe.Tor Should Not Be Concerened With Bio-Watches Bi- Phones, And All Other Nano-bio JUNK!! Plz Just Make Tor Safe And Extremly Private, THANKS fo all your help and Good Work O And Bring Back New's Blog Weeklt,Bi Monthle  Etc .  Weekly Would be NICE  Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113210" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-112695" class="permalink" rel="bookmark">I know This Is Not The</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113210" class="permalink" rel="bookmark">The nice fellow who does Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The nice fellow who does Tor Weekly News took a few weeks off. He's back now, which is great. I'll be sure to point him to this comment so he knows he is loved. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-112851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112851" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2015</p>
    </div>
    <a href="#comment-112851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112851" class="permalink" rel="bookmark">Is there a way to use meek</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a way to use meek in TAILS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-112880"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112880" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2015</p>
    </div>
    <a href="#comment-112880">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112880" class="permalink" rel="bookmark">Does Tor work on Windows 10?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does Tor work on Windows 10?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-113001"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113001" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2015</p>
    </div>
    <a href="#comment-113001">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113001" class="permalink" rel="bookmark">With javascipt open, the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With javascipt open, the system time will be revealed by the sites you are visiting with your browser. Is that I should care about?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-113214"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113214" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113001" class="permalink" rel="bookmark">With javascipt open, the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-113214">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113214" class="permalink" rel="bookmark">https://www.torproject.org/pr</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/projects/torbrowser/design/#fingerprinting" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/#fingerprinting</a><br />
has a lot more reading for you.</p>
<p>It is indeed something you should care about (if you're worried about websites recognizing you from fingerprinting), and Tor Browser helps to make the problem less bad, but some issues still remain.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113086"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113086" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2015</p>
    </div>
    <a href="#comment-113086">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113086" class="permalink" rel="bookmark">How do I get into the deep</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do I get into the deep web</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113086" class="permalink" rel="bookmark">How do I get into the deep</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-114232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114232" class="permalink" rel="bookmark">I heard rumors about it not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I heard rumors about it not existing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-113695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113695" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2015</p>
    </div>
    <a href="#comment-113695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113695" class="permalink" rel="bookmark">Any future plan to drop the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any future plan to drop the curves. NSA is just doing it and according to some recent study and discussions everyone is advised to:<br />
<a href="http://blog.cryptographyengineering.com/2015/10/a-riddle-wrapped-in-curve.html" rel="nofollow">http://blog.cryptographyengineering.com/2015/10/a-riddle-wrapped-in-cur…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
