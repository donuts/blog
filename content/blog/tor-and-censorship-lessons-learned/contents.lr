title: Tor and Censorship:  lessons learned
---
pub_date: 2010-01-08
---
author: phobos
---
tags:

tor
ccc
censorship
26c3
roger
jacob
---
categories:

circumvention
community
network
---
_html_body:

<p>Roger recently gave a talk at <a href="http://events.ccc.de/congress/2009/Fahrplan/events/3554.en.html" rel="nofollow">26C3</a> about our experiences with various censorship technologies.  </p>

<blockquote><p>In the aftermath of the Iranian elections in June, and then the late September blockings in China, we've learned a lot about how circumvention tools work in reality for activists in tough situations. I'll give an overview of the Tor architecture, and summarize the variety of people who use it and what security it provides. Then we'll focus on the use of tools like Tor in countries like Iran and China: why anonymity is important for circumvention, why transparency in design and operation is critical for trust, the role of popular media in helping – and harming – the effectiveness of the tools, and tradeoffs between usability and security. After describing Tor's strategy for secure circumvention (what we thought would work), I'll talk about how the arms race actually seems to be going in practice.</p></blockquote>

<p>The slides of the presentation can be found at the bottom of this post.  </p>

<p>We've mirrored the full 700MB video of the presentation at <a href="http://media.torproject.org/video/26c3-3554-de-tor_and_censorship_lessons_learned.mp4" rel="nofollow">http://media.torproject.org/video/26c3-3554-de-tor_and_censorship_lesso…</a></p>

---
_comments:

<a id="comment-3911"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3911" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2010</p>
    </div>
    <a href="#comment-3911">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3911" class="permalink" rel="bookmark">Second Time: btw.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Second Time: btw. <a href="https://git.torproject.org/" rel="nofollow">https://git.torproject.org/</a> is offline since some days :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3942" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3911" class="permalink" rel="bookmark">Second Time: btw.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3942" class="permalink" rel="bookmark">See:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See: <a href="http://gitweb.torproject.org/tor" rel="nofollow">http://gitweb.torproject.org/tor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3945"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3945" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 11, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3911" class="permalink" rel="bookmark">Second Time: btw.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3945">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3945" class="permalink" rel="bookmark">Yes, due to server problems,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, due to server problems, git is offline.  We're working on bringing it back online on a new server.  The plan is this week to go live again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3921"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3921" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2010</p>
    </div>
    <a href="#comment-3921">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3921" class="permalink" rel="bookmark">Very nice.  I enjoyed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very nice.  I enjoyed watching.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3943" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2010</p>
    </div>
    <a href="#comment-3943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3943" class="permalink" rel="bookmark">I loved watching this.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I loved watching this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 16, 2010</p>
    </div>
    <a href="#comment-3984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3984" class="permalink" rel="bookmark">Maybe this is a stupid</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe this is a stupid question, as you probably want people running relays instead, but...torrent?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4014"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4014" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 18, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3984" class="permalink" rel="bookmark">Maybe this is a stupid</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4014">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4014" class="permalink" rel="bookmark">torrent what?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>torrent what?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-6170"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6170" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-6170">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6170" class="permalink" rel="bookmark">I think Anonymous means a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think Anonymous means a .torrent file of the video.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-4196"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4196" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 31, 2010</p>
    </div>
    <a href="#comment-4196">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4196" class="permalink" rel="bookmark">So, I finally got around to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So, I finally got around to watching this... very good I thought, enjoyed it a lot and it helped to explain some things.  Roger comes across really well I think, good speaker.</p>
<p>What else can I say... I'm a bridge relay... goooo bridges, woot... :D</p>
</div>
  </div>
</article>
<!-- Comment END -->
