title: Tor Browser 6.5a4-hardened is released
---
pub_date: 2016-11-16
---
author: gk
---
tags:

tor browser
tbb
tbb-6.5
---
categories:

applications
releases
---
_html_body:

<p>A new hardened Tor Browser release is available. It can be found in the <a href="https://dist.torproject.org/torbrowser/6.5a4-hardened/" rel="nofollow">6.5a4-hardened distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page for hardened builds</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr45.5" rel="nofollow">security updates</a> to Firefox. Other components got an update as well: Tor to 0.2.9.5-alpha, HTTPS-Everywhere to 5.2.7, and OpenSSL to 1.0.2j.</p>

<p>This release includes numerous bug fixes and improvements. Most notably we improved our Unix domain socket support by resolving all the issues that showed up in the previous alpha and by making sure all connections to tor (not only the control port related ones) are using this feature now.</p>

<p>Additionally, we fixed a lot of usability bugs, most notably those caused by our window resizing logic. We moved the relevant code out of Torbutton into a C++ patch which we hope to get upstreamed into Firefox. We improved the usability of our security slider as well by reducing the amount of security levels available and redesigning the custom mode.</p>

<p>Finally, we added a donation banner shown in some localized bundles starting on Nov 23 in order to point to our end-of-the-year 2016 donation campaign.</p>

<p>For those who want to know in which ways the alpha and the hardened series differ: check out the discussion we had on the <a href="https://lists.torproject.org/pipermail/tbb-dev/2016-June/000382.html" rel="nofollow">tbb-dev</a> mailing list a while back.</p>

<p><strong>Update (11/16 2213UTC):</strong> We currently have problems with our auto-updater at least on Linux systems. The updates are downloaded but don't get applied for yet unknown reasons. We therefore have decided to disable the automatic updates until we understand the problem and provide a fix for it. Progress on that task can be tracked in <a href="https://trac.torproject.org/projects/tor/ticket/20691" rel="nofollow">ticket 20691</a> in our bug tracker. We are sorry for this inconvenience. Fresh bundles are available on our <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page</a>, though.</p>

<p><strong>Update (11/18 0937UTC):</strong> We enabled the updates again with an information prompt. One of the following workarounds can be used to avoid the updater error:</p>

<ul>
<li>in <span class="geshifilter"><code class="php geshifilter-php">about<span style="color: #339933;">:</span>config</code></span>, set <span class="geshifilter"><code class="php geshifilter-php">app<span style="color: #339933;">.</span>update<span style="color: #339933;">.</span>staging<span style="color: #339933;">.</span>enabled</code></span> to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">false</span></code></span> before attempting to update</li>
<li>in <span class="geshifilter"><code class="php geshifilter-php">about<span style="color: #339933;">:</span>config</code></span>, set <span class="geshifilter"><code class="php geshifilter-php">extensions<span style="color: #339933;">.</span>torlauncher<span style="color: #339933;">.</span>control_port_use_socket</code></span> to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">false</span></code></span> (disabling the control port Unix domain socket) and restart the browser before attempting to update</li>
</ul>

<p>Here is the full changelog since 6.5a3-hardened:</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 45.5.0esr</li>
<li>Update Tor to tor-0.2.9.5-alpha</li>
<li>Update OpenSSL to 1.0.2j</li>
<li>Update Torbutton to 1.9.6.7
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20414" rel="nofollow">Bug 20414</a>: Add donation banner on about:tor for 2016 campaign</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20111" rel="nofollow">Bug 20111</a>: Use Unix domain sockets for SOCKS port by default</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19459" rel="nofollow">Bug 19459</a>: Move resizing code to tor-browser.git</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20264" rel="nofollow">Bug 20264</a>: Change security slider to 3 options</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20347" rel="nofollow">Bug 20347</a>: Enhance security slider's custom mode</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20123" rel="nofollow">Bug 20123</a>: Disable remote jar on all security levels</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20244" rel="nofollow">Bug 20244</a>: Move privacy checkboxes to about:preferences#privacy</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/17546" rel="nofollow">Bug 17546</a>: Add tooltips to explain our privacy checkboxes</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/17904" rel="nofollow">Bug 17904</a>: Allow security settings dialog to resize</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18093" rel="nofollow">Bug 18093</a>: Remove 'Restore Defaults' button</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20373" rel="nofollow">Bug 20373</a>: Prevent redundant dialogs opening</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20388" rel="nofollow">Bug 20388</a>+<a href="https://trac.torproject.org/projects/tor/ticket/20399" rel="nofollow">20399</a>+<a href="https://trac.torproject.org/projects/tor/ticket/20394" rel="nofollow">20394</a>: Code clean-up</li>
<li>Translation updates</li>
</ul>
</li>
<li>Update Tor Launcher to 0.2.11.1
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20111" rel="nofollow">Bug 20111</a>: Use Unix domain sockets for SOCKS port by default</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20185" rel="nofollow">Bug 20185</a>: Avoid using Unix domain socket paths that are too long</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20429" rel="nofollow">Bug 20429</a>: Do not open progress window if tor doesn't get started</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19646" rel="nofollow">Bug 19646</a>: Wrong location for meek browser profile on OS X</li>
<li>Translation updates</li>
</ul>
</li>
<li>Update HTTPS-Everywhere to 5.2.7</li>
<li>Update meek to 0.25
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19646" rel="nofollow">Bug 19646</a>: Wrong location for meek browser profile on OS X</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20030" rel="nofollow">Bug 20030</a>: Shut down meek-http-helper cleanly if built with Go &gt; 1.5.4</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20304" rel="nofollow">Bug 20304</a>: Support spaces and other special characters for SOCKS socket</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20490" rel="nofollow">Bug 20490</a>: Fix assertion failure due to fix for bug 20304</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19459" rel="nofollow">Bug 19459</a>: Size new windows to 1000x1000 or nearest 200x100 (Firefox patch)</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20442" rel="nofollow">Bug 20442</a>: Backport fix for local path disclosure after drag and drop</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20160" rel="nofollow">Bug 20160</a>: Backport fix for broken MP3-playback</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20043" rel="nofollow">Bug 20043</a>: Isolate SharedWorker script requests to first party</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20123" rel="nofollow">Bug 20123</a>: Always block remote jar files</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20244" rel="nofollow">Bug 20244</a>: Move privacy checkboxes to about:preferences#privacy</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19838" rel="nofollow">Bug 19838</a>: Add dgoulet's bridge and add another one commented out</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/19481" rel="nofollow">Bug 19481</a>: Point the update URL to aus1.torproject.org</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20296" rel="nofollow">Bug 20296</a>: Rotate ports again for default obfs4 bridges</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20651" rel="nofollow">Bug 20651</a>: DuckDuckGo does not work with JavaScript disabled</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20399" rel="nofollow">Bug 20399</a>+<a href="https://trac.torproject.org/projects/tor/ticket/15852" rel="nofollow">15852</a>: Code clean-up</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/15953" rel="nofollow">Bug 15953</a>: Weird resizing dance on Tor Browser startup</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20023" rel="nofollow">Bug 20023</a>: Upgrade Go to 1.7.3</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20583" rel="nofollow">Bug 20583</a>: Make the downloads.json file reproducible</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-218997"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218997" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2016</p>
    </div>
    <a href="#comment-218997">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218997" class="permalink" rel="bookmark">what would be the main</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what would be the main difference between the 'regular tor versus the 'hardened one?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-219104"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219104" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-218997" class="permalink" rel="bookmark">what would be the main</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-219104">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219104" class="permalink" rel="bookmark">See my blog post above:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See my blog post above: <a href="https://lists.torproject.org/pipermail/tbb-dev/2016-June/000382.html" rel="nofollow">https://lists.torproject.org/pipermail/tbb-dev/2016-June/000382.html</a> has the best "documentation" we have so far.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-219023"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219023" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2016</p>
    </div>
    <a href="#comment-219023">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219023" class="permalink" rel="bookmark">Is there no chance of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there no chance of enabling any of the hardening in stable builds?<br />
Breaking Firefox's HTML renderer or Javascript interpretor is easier than finding a cryptographic break in the Tor protocol. I know that selfrand and the address sanitizer are a little inconvenient because they might make huge pages take one percent longer to render but didn't Benjamin Franklin say that sacrificing privacy for national security or convenience is bad?<br />
Same with bandwidth/latency padding. Adding a few bytes/milliseconds at random might be a little inconvenient but living in dystopia really worth it just to avoid a few percent overhead?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-219114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219114" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-219023" class="permalink" rel="bookmark">Is there no chance of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-219114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219114" class="permalink" rel="bookmark">Except the ASAN everything</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Except the ASAN everything else can be used in stable builds. Now it's selfrando time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219115"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219115" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-219023" class="permalink" rel="bookmark">Is there no chance of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-219115">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219115" class="permalink" rel="bookmark">Selfrando is meant to get</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Selfrando is meant to get included in the stable series at one point. I hope to get it squeezed into the next alpha, we'll see. All the other hardening techniques are more meant to help us notice bugs earlier to fix them in the stable series as fast as possible.</p>
<p>You probably want some effective sandboxing mechanism and we are working on those as well. We hope to have something ready for the next alpha release. And, yes, working on padding mechanisms in tor is ongoing and maybe something worthwhile will even make it into 0.3.0.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-219335"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219335" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-219335">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219335" class="permalink" rel="bookmark">That is great news. Thank</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is great news. Thank you all for your work and dedication towards protecting freedom and liberty. The world needs many more people like you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-219046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219046" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2016</p>
    </div>
    <a href="#comment-219046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219046" class="permalink" rel="bookmark">XML ParseError in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>XML ParseError in UpdateCheck and OptionMenu</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 17, 2016</p>
    </div>
    <a href="#comment-219407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219407" class="permalink" rel="bookmark">Preferences not appear in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Preferences not appear in russian version,<br />
instead yellow page with error appears:</p>
<p>about:preferences</p>
<p>Ошибка синтаксического анализа XML: неопределённая сущность<br />
Адрес: about:preferences<br />
Строка 653, символ 7:</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-219482"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219482" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 18, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-219407" class="permalink" rel="bookmark">Preferences not appear in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-219482">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219482" class="permalink" rel="bookmark">Ugh, that&#039;s embarrassing.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ugh, that's embarrassing. I've put it on our agenda for this month and it will be fixed in the next release, sorry. You can follow the development in <a href="https://trac.torproject.org/projects/tor/ticket/20707" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/20707</a>. Thanks for reporting and using an alpha version!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-219933"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219933" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-219933">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219933" class="permalink" rel="bookmark">Thank you for your prompt</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your prompt reponses and engagement with the community at great risk to yourself. Please don't let the hatred of freedom and liberty get any of you down. Your work is greatly appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-219534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219534" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 18, 2016</p>
    </div>
    <a href="#comment-219534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219534" class="permalink" rel="bookmark">i can no longer use Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i can no longer use Tor Browser control port (9150) to tunnel for example curl.<br />
it used to work but broke after updating the Browser.</p>
<p>What should i do?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219541" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 18, 2016</p>
    </div>
    <a href="#comment-219541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219541" class="permalink" rel="bookmark">protip: the usual port 9150</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>protip: the usual port 9150 stops working after this update, to restore it </p>
<p>launch tor browser with this env TOR_SOCKS_PORT=9150</p>
<p>thanks to the wonderful people on #tor irc channel for this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219615"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219615" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 18, 2016</p>
    </div>
    <a href="#comment-219615">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219615" class="permalink" rel="bookmark">OpenSSL and hardened is an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OpenSSL and hardened is an oxymoron</p>
<p>You can't be serious about security when you use OpenSSL instead of LibreSSL</p>
<p>Goto youtube and watch the :LibreSSL the first 30 days" video</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219791" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2016</p>
    </div>
    <a href="#comment-219791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219791" class="permalink" rel="bookmark">现在中国可以直连Meek</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>现在中国可以直连Meek节点了, 谢谢!<br />
now china can connect meek bridge, thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219907"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219907" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2016</p>
    </div>
    <a href="#comment-219907">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219907" class="permalink" rel="bookmark">I think this is the 3rd</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think this is the 3rd edition in a row of the hardened browser and the same problem exists.  I'm running it on stable debian security at highest level, no scripts, no vid/img, plain html text pages and editing a comment box just like this one.  The whole thing begins to almost freeze up, things get really slow and irresponsive.  Task manager says that it is running out of memory, with one tab and nothing fancy.  Meanwhile on the same system regular ff45.5 may have 20 tabs loaded with stuff without a hiccup and whole bunch of other things running.  On the previous version I had taken your advise to move to a directory close to root (/tmp/torbrowser...) but no difference.  TorBr. 6.0* has never had such a thing happen.  I also run the same over VPN or without it, no difference.  I only use standalone installations. </p>
<p>Any feedback on the issue would greatly be appreciated.  Had I used hardened to edit this comment it would surely freeze-up before I would have gotten half-way through.</p>
<p>PS  I've run tigershark on the system and it has not encountered anything funny.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-220723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-220723" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-219907" class="permalink" rel="bookmark">I think this is the 3rd</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-220723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-220723" class="permalink" rel="bookmark">Not an answer but... be very</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not an answer but... be very careful with executing anything in /tmp<br />
It should really be mounted noexec.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-222122"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-222122" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2016</p>
    </div>
    <a href="#comment-222122">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-222122" class="permalink" rel="bookmark">Hi are you still producing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi are you still producing the Piratebrowser?  I like the feel of the firefox in that version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-222348"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-222348" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-222122" class="permalink" rel="bookmark">Hi are you still producing</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-222348">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-222348" class="permalink" rel="bookmark">We were never producing the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We were never producing the Piratebrowser. It was a normal firefox with Tor glued to it, which means it left out all of the security and privacy features from Tor Browser:<br />
<a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/</a></p>
<p>So from our perspective, Piratebrowser has never been a wise thing to run.</p>
<p>That said, yay them for helping to raise the profile of privacy in the world.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
