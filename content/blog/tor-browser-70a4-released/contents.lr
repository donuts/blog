title: Tor Browser 7.0a4 is released
---
pub_date: 2017-05-16
---
author: boklm
---
tags:

tor browser
tbb
tbb-7.0
---
categories:

applications
releases
---
_html_body:

<p>Tor Browser 7.0a4 is now available from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/7.0a4/">distribution directory</a>.</p>
<p>This will probably be the last alpha before the first stable release in the 7.0 series.</p>
<p>This release features a lot of improvements since the 7.0a3 release. Among other things Firefox has been updated to 52.1.1esr, fixing a <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2017-14/">security bug for Windows users</a> (although by default Tor Browser users are <strong>not</strong> affected as WebGL is put behind click-to-play placeholders, thanks to NoScript). The canvas prompt is now <a href="https://trac.torproject.org/projects/tor/ticket/21778">shown again</a>, the browser is not crashing anymore on <a href="https://trac.torproject.org/projects/tor/ticket/21962">about:addons with the security slider set to "high"</a> and <a href="https://blog.torproject.org/blog/selfrando-q-and-georg-koppen">Selfrando</a> has been integrated into the Linux 64bit build.</p>
<p>There are still some <a href="https://trac.torproject.org/projects/tor/query?status=!closed&amp;keywords=~tbb-7.0-must">unresolved issues</a> that we are working on getting fixed for the stable release. Among them, the browser is crashing (with e10s enabled) or the download is stalling (with e10s disabled) when opening/downloading files that need an external application to handle them (this is <a href="https://trac.torproject.org/projects/tor/ticket/21766">bug 21766</a> and <a href="https://trac.torproject.org/projects/tor/ticket/21886">bug 21886</a>).</p>
<p><strong>Note</strong>: comments were closed as we were <a href="https://blog.torproject.org/blog/we-are-upgrading-our-blog">upgrading our blog</a>. They are now open.</p>
<p><strong>Note for Linux users</strong>: You may get the error <cite>Directory /run/user/$uid/Tor does not exist</cite> after updating your browser. This is <a href="https://trac.torproject.org/projects/tor/ticket/22283">bug 22283</a>. A workaround for this issue is to edit the file <em>Browser/TorBrowser/Data/Tor/torrc</em> and remove the <em>ControlPort</em> and <em>SocksPort</em> lines.</p>
<p>The full changelog since Tor Browser 7.0a3 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 52.1.1esr</li>
<li>Update Tor to 0.3.0.6</li>
<li>Update Tor Launcher to 0.2.12.1
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20761">Bug 20761</a>: Don't ignore additional SocksPorts</li>
<li>Translation update</li>
</ul>
</li>
<li>Update HTTPS-Everywhere to 5.2.16</li>
<li>Update NoScript to 5.0.4</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21962">Bug 21962</a>: Fix crash on about:addons page</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21778">Bug 21778</a>: Canvas prompt is not shown in Tor Browser based on ESR52</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21569">Bug 21569</a>: Add first-party domain to Permissions key</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22165">Bug 22165</a>: Don't allow collection of local IP addresses</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/13017">Bug 13017</a>: Work around audio fingerprinting by disabling the Web Audio API</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/10286">Bug 10286</a>: Disable Touch API and add fingerprinting resistance as fallback</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/13612">Bug 13612</a>: Disable Social API</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/10283">Bug 10283</a>: Disable SpeechSynthesis API</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21675">Bug 21675</a>: Spoof window.navigator.hardwareConcurrency</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21792">Bug 21792</a>: Suppress MediaError.message</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16337">Bug 16337</a>: Round times exposed by Animation API to nearest 100ms</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21726">Bug 21726</a>: Keep Graphite support disabled</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21685">Bug 21685</a>: Disable remote new tab pages</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21790">Bug 21790</a>: Disable captive portal detection</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21686">Bug 21686</a>: Disable Microsoft Family Safety support</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22073">Bug 22073</a>: Make sure Mozilla's experiments are disabled</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21683">Bug 21683</a>: Disable newly added Safebrowsing capabilities</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22071">Bug 22071</a>: Disable Kinto-based blocklist update mechanism</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22072">Bug 22072</a>: Hide TLS error reporting checkbox</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20761">Bug 20761</a>: Don't ignore additional SocksPorts</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21340">Bug 21340</a>: Identify and backport new patches from Firefox</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22153">Bug 22153</a>: Fix broken feeds on higher security levels</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22025">Bug 22025</a>: Fix broken certificate error pages on higher security levels</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21710">Bug 21710</a>: Upgrade Go to 1.8.1</li>
</ul>
</li>
<li>Mac
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21940">Bug 21940</a>: Don't allow privilege escalation during update</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22044">Bug 22044</a>: Fix broken default search engine on macOS</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21879">Bug 21879</a>: Use our default bookmarks on OSX</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21779">Bug 21779</a>: Non-admin users can't access Tor Browser on macOS</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22041">Bug 22041</a>: Fix update error during update to 7.0a3</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22238">Bug 22238</a>: Fix use of hardened wrapper for Firefox build</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20683">Bug 20683</a>: Selfrando support for 64-bit Linux systems</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-268755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268755" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon mouse (not verified)</span> said:</p>
      <p class="date-time">May 26, 2017</p>
    </div>
    <a href="#comment-268755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268755" class="permalink" rel="bookmark">I downloaded the new updated…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded the new updated tor 7.0a4 from here on this  site  and got this from my antivirus that the file contained a virus called Virus.Gen<br />
Gen:Variant.Graftor.369260 (Traces) File E:\Tor Browser\Browser\Tor\Pluggable Transports\obfs4proxy.exe</p>
<p>the download is infected?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268768"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268768" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 28, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268755" class="permalink" rel="bookmark">I downloaded the new updated…</a> by <span>Anon mouse (not verified)</span></p>
    <a href="#comment-268768">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268768" class="permalink" rel="bookmark">Very likely no…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very likely no, not infected.</p>
<p>See <a href="https://www.torproject.org/docs/faq#VirusFalsePositives" rel="nofollow">https://www.torproject.org/docs/faq#VirusFalsePositives</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268785"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268785" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bopper (not verified)</span> said:</p>
      <p class="date-time">May 31, 2017</p>
    </div>
    <a href="#comment-268785">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268785" class="permalink" rel="bookmark">When I use https://ipleak…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I use <a href="https://ipleak.net/" rel="nofollow">https://ipleak.net/</a> or <a href="https://browserleaks.com/ip" rel="nofollow">https://browserleaks.com/ip</a> the public IP address doesn't match tor circuit report. for example the TOR circuit states<br />
Bridge :obfs4 (sweden)<br />
Netherlands (178.62.197.82)<br />
Austria (193.171.202.146)<br />
Internet</p>
<p>But the public IP information is 192.56.55.26 and 95.130.11.147 respectively. Both are in France.</p>
<p>Any idea why this occurs?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268791" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268785" class="permalink" rel="bookmark">When I use https://ipleak…</a> by <span>bopper (not verified)</span></p>
    <a href="#comment-268791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268791" class="permalink" rel="bookmark">Two possibilities:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Two possibilities:</p>
<p>A) Tor Browser isolates each tab on a different circuit, so if you go to a website and click on the green onion to see what path you're using, and then you go to a new tab and you load ipchicken or your favorite geoip tool, Tor Browser will be separating those two connections onto two different circuits, for your protection.</p>
<p>B) Some exit relays are multi-homed, meaning they advertise a different IP address than the one(s) they use for outgoing connections.</p>
<p>Without further information, I would bet on 'A' in this case.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268793" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">May 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-268793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268793" class="permalink" rel="bookmark">C) The GeoIP database…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>C) The GeoIP database shipped with Tor Browser disagrees with the GeoIP database that a random site is using.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268804" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">June 02, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268785" class="permalink" rel="bookmark">When I use https://ipleak…</a> by <span>bopper (not verified)</span></p>
    <a href="#comment-268804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268804" class="permalink" rel="bookmark">D) Have a look at the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>D) Have a look at the circuit display when you are loading those sites. You might probably see that some circuits change. If that's the case timeouts or other errors in Tor lead to picking a new circuit. Now, the thing is that the measurement (IP address detection) takes place at time t with circuit X being active but you are probably looking at time t1 at the circuit display giving you a different impression due to the above described timeouts/errors.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ms-xy (not verified)</span> said:</p>
      <p class="date-time">June 01, 2017</p>
    </div>
    <a href="#comment-268802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268802" class="permalink" rel="bookmark">Hi, before I try 7.0a4, I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, before I try 7.0a4, I would like to know one thing:<br />
What is the average download size when Tor is updated from 6.5.0 to 6.5.2 on 64 bit Linux? Is 85 MB in a normal range? Seems quite a lot. Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268806" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">June 02, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268802" class="permalink" rel="bookmark">Hi, before I try 7.0a4, I…</a> by <span>ms-xy (not verified)</span></p>
    <a href="#comment-268806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268806" class="permalink" rel="bookmark">For a full update, yes. We…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For a full update, yes. We provide smaller updates, which are called incremental updates, for updating from the previous version (sometimes even from the version before the previous version). Those are usually way smaller. For instance the incremental update from 6.5 to 6.5.1 was about 8 MByte and the one from 6.5.1 to 6.5.2 around 5 MByte.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268818"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268818" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bopper (not verified)</span> said:</p>
      <p class="date-time">June 04, 2017</p>
    </div>
    <a href="#comment-268818">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268818" class="permalink" rel="bookmark">From TOR version 6.5.2 when…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From TOR version 6.5.2 when I perform an update it doees not recognize any newer version. Is this normal ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268824"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268824" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">June 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268818" class="permalink" rel="bookmark">From TOR version 6.5.2 when…</a> by <span>bopper (not verified)</span></p>
    <a href="#comment-268824">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268824" class="permalink" rel="bookmark">Yes. 6.5.2 is the latest…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes. 6.5.2 is the latest stable Tor Browser. This is a release announcement for the alpha series of Tor Browser.</p>
<p>Incidentally, this alpha series is very close to being the new stable. Stay tuned for Tor Browser to tell you it has an update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268825" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>unFreegate (not verified)</span> said:</p>
      <p class="date-time">June 04, 2017</p>
    </div>
    <a href="#comment-268825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268825" class="permalink" rel="bookmark">A Chinese famous proxy…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A Chinese famous proxy software named Freegate, which you can make Tor Browser tunnel through with its listening port provided, but first you have to clear any types of bridges out before you can do it, without the bridges you are easier to be tracked, doesn't Tor Project go through the Freegate's trick?</p>
</div>
  </div>
</article>
<!-- Comment END -->
