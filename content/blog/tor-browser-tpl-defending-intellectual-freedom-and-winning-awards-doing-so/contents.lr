title: Tor Browser at TPL: Defending Intellectual Freedom, and Winning Awards Doing So
---
pub_date: 2019-01-25
---
author: steph
---
tags:

Library Freedom Project
tor browser
Toronto Public Library
The Tor Project
---
categories: applications
---
summary:

Public librarians can do a great deal to arm our users with the knowledge, the tools, and the confidence to navigate the surveillance society online. 
---
_html_body:

<p><em>Guest post by Jonathon Hodge, Digital Literacy Service Lead, Toronto Public Library</em></p>
<p>Every public library worker will know that person: the one who is worried about being spied on.</p>
<p>For a long time in public libraries, that person was treated with the kindness and respect we treat every person, regardless of whether we felt that their concerns may have been overblown. The difference between that bygone past and today, is that today, that person is right! The internet is <a href="https://www.newyorker.com/news/amy-davidson/the-n-s-a-verizon-scandal">spying on them</a>; it’s <a href="https://www.theatlantic.com/technology/archive/2014/08/google-knows-you-better-than-you-know-yourself/378608/">spying on all of us</a>. Even if we <a href="https://www.recode.net/2018/4/20/17254312/facebook-shadow-profiles-data-collection-non-users-mark-zuckerberg">don’t use it very much</a>. Public libraries have long offered effective guidance to the wealth of information society produces. So the question today is, ‘Are we doing enough for ‘that person’, and by extension, for all of us?’</p>
<p>‘That person’ might be Shoshanna and she’s a writer, researching bioterrorism and worried that she will be red-flagged for it. Maybe ‘that person’ is Tenzin, a teenage volunteer with a secret boyfriend he doesn’t want his family knowing about. ‘That person’ could be Sid, a local political activist trying to bring the police to account for the death of a neighbor while under police custody.</p>
<p>In Toronto, we felt that the answer was NO. Our communities let us know that they do not know enough about the actual threats they contend with on the internet, they do not know what tools to use or actions to take to protect themselves. So we have seen our users justified skepticism about using the internet grow in many cases into a paralyzing, disempowering paranoia.</p>
<p>Public librarians can do a great deal to arm our users with the knowledge, the tools, and the confidence to navigate the surveillance society online. We in Toronto felt that Tor Browser - as the most robust, and sadly, perhaps the most poorly understood privacy technology available – should be the centerpiece of a multi-vector <a href="https://torontopubliclibrary.typepad.com/news_releases/2016/11/toronto-public-library-launches-digital-privacy-initiative.html">Digital Privacy Initiative</a>, that combines privacy education, and technology training and providing privacy-enabling tools at the point of service.<br />
Once we launched Tor Browser on our public PCs (<a href="https://www.torontopubliclibrary.ca/using-the-library/computer-services/tor-browser-pilot/">as a pilot</a> currently, though with an eye to a larger deployment this year), the feedback was immediate. Users told us:</p>
<p>“I (now) use Tor Browser almost 100% at home. Thank you TPL for introducing me to it.”</p>
<p>“I am greatly appreciative that the public library has made this available… More of this in Canada please!”</p>
<p>“It’s amazing to be able to use this in the Library. I was surprised at how easy it was!”</p>
<p>We packaged Tor Browser as part of an overall effort to help the public, announcing it at <a href="https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp">a large public event</a> (with Tor Project’s co-founder Roger Dingledine, no less) that same weekend, and after having demonstrated Tor Browser in our <a href="https://www.torontopubliclibrary.ca/search.jsp?N=37867&amp;Ntt=digital+privacy">public education classes</a> for 16 months prior. With our pilot winding up in a couple of months, and with it recently winning the OLITA Technology Advancing Libraries Award, we hope that 2019 will be the year TPL provides Tor Browser to Torontonians across the city.</p>
<p>With the software being easy to install and maintain, and with a new public appetite for secure technology, I would encourage other public libraries to install Tor Browser. Our professional defense of intellectual freedom can no longer exist only at the realm of policy. In this age when our tech spies on us for the sake of massive internet companies and the State, our defense must be a technological one as well. That tech is Tor.</p>
<p> </p>
<hr />
<p><em>If you want help bringing Tor to your local library, either by providing Tor Browser or running a relay, contact the <a href="https://libraryfreedomproject.org">Library Freedom Project</a> to help get started. If you're a librarian, you can <a href="https://libraryfreedomproject.org/lfi/">apply to become a Privacy Advocate</a> in your community through the Library Freedom Institute. </em></p>
<p> </p>

---
_comments:

<a id="comment-279466"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279466" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>loskiq (not verified)</span> said:</p>
      <p class="date-time">January 25, 2019</p>
    </div>
    <a href="#comment-279466">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279466" class="permalink" rel="bookmark">this is cool news!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this is cool news!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Not Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 27, 2019</p>
    </div>
    <a href="#comment-279471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279471" class="permalink" rel="bookmark">I think it should be made…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think it should be made more clear that <strong>Tor is futile against any actor that can see both incoming and outgoing traffic</strong>, otherwise all you do is give people a false sense of security against actors who have those capabilities (quite a few)</p>
<p>Also, isn't a library already quite anonymous since multiple users are using the same computer and network? I think public libraries are the last place that needs Tor, in my opinion.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279552"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279552" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John Doe (not verified)</span> said:</p>
      <p class="date-time">January 30, 2019</p>
    </div>
    <a href="#comment-279552">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279552" class="permalink" rel="bookmark">Of course, my comment…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Of course, my comment mentioning Tor's major flaw doesn't get posted. Anyone who can see both the incoming traffic and outgoing traffic can de-anonymize Tor users, that's a known flaw of Tor. Also public libraries are already anonymous to an extent, since people share the same computer... why prioritize public libraries over other places?</p>
</div>
  </div>
</article>
<!-- Comment END -->
