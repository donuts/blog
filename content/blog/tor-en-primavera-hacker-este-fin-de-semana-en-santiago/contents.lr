title: Tor en Primavera Hacker este fin de semana en Santiago (Join Tor at Primavera Hacker in Santiago This Weekend)
---
pub_date: 2017-11-29
---
author: ilv
---
tags:

Primavera Hacker
meetup
global south
---
categories: community
---
summary: El próximo fin de semana se llevará a cabo un encuentro Tor durante el festival Primavera Hacker, un evento gratuito realizado anualmente en Santiago de Chile que aborda las relaciones entre tecnología, política y cultura. This weekend a Tor meetup will be held during the Primavera Hacker festival (hacker spring), a yearly free gathering organized in Santiago de Chile around the relationships between technology, politics and culture.
---
_html_body:

<p><em>(An English version of this post is below.)</em></p>
<p>El próximo fin de semana se llevará a cabo un encuentro Tor durante el festival Primavera Hacker, un evento gratuito realizado anualmente en Santiago de Chile que aborda las relaciones entre tecnología, política y cultura.</p>
<h3>El festival</h3>
<p>El objetivo de Primavera Hacker es discutir las implicancias políticas, sociales y culturales del uso extensivo de la tecnología en prácticamente todos los aspectos de la vida contemporánea. Esto hace necesario y urgente discutir no solo sobre el uso, pero también sobre el diseño y desarollo de la tecnología y como se incorpora en nuestra vida diaria. Esto también implica promover un enfoque crítico a preguntas actuales y nuevas en el desarrollo de soluciones alternativas que vengan de la comunidad misma.</p>
<p>El contexto en América Latina tiene muchas caras y formas. El capitalismo extremo, los feminicidios, y la corrupción generalizada son solo algunas. Lamentablemente, algunas de estas situaciones también son replicadas en el mundo digital. El festival cubrirá temas como la violencia de género en línea, medios alternativos de producción, activismo y autodefensa digital, y servidores radicales, entre muchos otros temas.</p>
<h3>Tor en contexto </h3>
<p>En este contexto, Tor ofrece un conjunto de herramientas que puede ayudar a las personas a protegerse y enfrentar algunas de estas situaciones. La interceptación de las comunicaciones, la vigilancia corporativa, y las amenazas digitales están a la vuelta de la esquina. Y no sólo eso, hay muchas otras comunidades a las que llegar y contextos que conocer, como grupos técnicos o la academia, por ejemplo.</p>
<p>El encuentro Tor en Santiago, junto con otros encuentros que se han llevado a cabo en la región, es un pequeño paso en el esfuerzo de cubrir esta brecha. Esperamos interactuar con gente local para hablar sobre Tor, construir lazos de confianza, y desarrollar formas de colaboración y soporte desde la comunidad.</p>
<h3>Las actividades </h3>
<p>Durante Primavera tendremos un espacio abierto en la tarde de ambos días. Esperamos cubir temas y actividades tales como el funcionamiento de Tor, instalación de herramientas relacionadas a Tor (Tor Browser, Orfox, Onionshare, etc.), como usar y analizar datos de OONI para detectar censura en internet, y como colaborar y unirse a la comunidad Tor al operar nodos o ayudar con traducciones. También buscamos generar discusiones sobre Tor en las universidades, Tor y los aspectos legales en América Latina, y cualquier otro tema que surja de la gente que asista. Por si fuera poco, también tendremos camisetas y stickers para regalar :-]</p>
<h3>¡Únete!</h3>
<p>Si vives en Santiago o planeas asistir a Primavera Hacker te invitamos a que te unas y seas parte del encuentro Tor. Operadores de nodos, usuarias, activistas, académicos, estudiantes, ¡todas son bienvenidas!</p>
<p>Para más información visita el sitio web de Primavera Hacker <a href="https://phacker.org/">https://phacker.org</a></p>
<p>También te puedes unir a la lista de correo <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south">https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south</a> para estar al tanto de otros encuentros y actividades.</p>
<hr />
<hr />
<p>This weekend a Tor meetup will be held during the Primavera Hacker festival (hacker spring), a yearly free gathering organized in Santiago de Chile around the relationships between technology, politics and culture.</p>
<h3>The festival</h3>
<p>The goal of Primavera Hacker is to discuss the political, social and cultural implications of the extensive use of technology in almost every aspect of contemporary life. This makes it necessary and urgent to open up the discussion not only about the use, but also the design and development of technology and how it is incorporated in our daily lives. This also implies encouraging a critical approach to current and new questions in the development of alternative solutions that come from the communities themselves. </p>
<p>The context in Latin America has many faces and shapes. Extreme capitalism, femicides, and wide spread corruption are just a few. Sadly, some of these issues are also replicated into the digital world. The festival will cover topics such as online violence against women, alternative means of production, activism and digital self defense, and radical tech, among many others.</p>
<h3>Tor in context </h3>
<p>In this context, Tor offers a set of tools that can help people protect themselves and challenge some of these issues. The interception of communications, the corporate surveillance, and digital threats are just around the corner. But there are yet many other communities to reach and contexts to be discovered, such as academic and technical groups.</p>
<p>The Tor meetup in Santiago, along with other meetups held in the region, is a tiny step in the effort to fill this gap. We hope to engage with locals to spread the word about Tor, build trust, and develop ways of collaboration and support from the community itself. </p>
<h3>The activities</h3>
<p>During Primavera, we will have an open space on the afternoon of each day. We hope to cover topics such as how Tor works, install fest of Tor related tools (Tor Browser, Orfox, Onionshare, etc.), how to use and analyze OONI data to detect network censorship, and how to collaborate and join the community by running relays or helping with translation. We also want to have discussions about Tor in universities, Tor and the law in Latin America, and any other subject that comes up from the people that will attend. We will have t-shirts and stickers :-]! </p>
<h3>Join us</h3>
<p>If you live in Santiago or you plan to attend Primavera Hacker we invite you to join us and be part of the meetup. Relay operators, users, activists, researchers, students, all are welcome!</p>
<p>For more information visit Primavera Hacker's website: <a href="https://phacker.org/">https://phacker.org</a></p>
<p>You can also join the Tor global south mailing list: <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south">https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south</a> to stay tuned about other meetups and activities.</p>

---
_comments:

<a id="comment-272763"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272763" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>salute (not verified)</span> said:</p>
      <p class="date-time">November 30, 2017</p>
    </div>
    <a href="#comment-272763">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272763" class="permalink" rel="bookmark">... 1024 rsa ... : cop toy…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>... 1024 rsa ... : cop toy for kids , nsa tool, ... not built privacy_security/anonymity  in mind ... govt compliant etc.<br />
it is not because it comes from usa that it is a way toward the freedom even if it is better than an e.u. solution.<br />
by the way , onionshare &amp; ricochet run on Tor ( ricochet updated its key , delete the old then check).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272775"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272775" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2017</p>
    </div>
    <a href="#comment-272775">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272775" class="permalink" rel="bookmark">Hey ilv! Can you have a look…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey ilv! Can you have a look at this ticket about GetTor <a href="https://trac.torproject.org/projects/tor/ticket/24447" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/24447</a> ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
