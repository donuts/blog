title: Tor Weekly News — February 26th, 2014
---
pub_date: 2014-02-26
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Velkomin to the eighth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>News from the 2014 Winter Developers’ Meeting in Reykjavík</h1>

<p>Since 2010, the Tor Project’s core contributors have tried to meet twice a year to enjoy the pleasure of each other’s company and move projects forward through face-to-face discussions and hacking time. This year, close to forty people attended the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting" rel="nofollow">2014 winter dev. meeting</a>, which was held in Reykjavík, Iceland.</p>

<p>The team discussed over forty topics ranging from organizational matters to highly technical design decisions. Among the highlights: many sessions were focused on discussing and producing roadmaps — <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/TorBrowserPlan" rel="nofollow">a mid-term vision for the Tor Browser</a>, <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/RoadmapTIMB" rel="nofollow">a step-by-step plan for the Tor Instant Messaging Bundle</a>, <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/BridgeBundles" rel="nofollow">detailed ideas for new bridge bundles</a>, <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/GuardDesign" rel="nofollow">ways to solve the research problems around guard nodes</a>, and <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/TorReleaseProcess" rel="nofollow">how to plan little-t tor releases</a>.</p>

<p>The meeting also enabled cross-team communication: pluggable transports developers were able to <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/PTTBB" rel="nofollow">discuss integration into the Tor Browser</a> and the <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/BridgeProtocolsAndTBB" rel="nofollow">lifecycle of recommended transports</a>, while the Tor Browser group and the support teams met to see how to <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/SupportAndTorBrowserTeamsMeeting" rel="nofollow">improve communications on both sides</a>. One outcome is a plan to <a href="https://bugs.torproject.org/10974" rel="nofollow">remix existing documentation to create the Tor Browser User Manual</a>.</p>

<p>Two days of intense discussion were followed by hands-on sessions which saw their <a href="https://trac.torproject.org/projects/tor/timeline?from=Feb+21%2C+2014&amp;daysback=2&amp;authors=&amp;ticket=on&amp;ticket_details=on&amp;changeset=on&amp;wiki=on&amp;update=Update" rel="nofollow">fair share of hacking and write-ups</a>.</p>

<p>Some of the minutes are still missing, but there is already plenty to read. Now that the developers have had to stop contemplating the <a href="https://en.wikipedia.org/wiki/Aurora_(astronomy)" rel="nofollow">Northern Lights</a>, some <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006288.html" rel="nofollow">discussions</a> are already continuing on the project’s various mailing lists. Stay tuned!</p>

<h1>Miscellaneous news</h1>

<p>Karsten Loesing  <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006299.html" rel="nofollow">announced</a> an IRC meeting to discuss the next steps for the rewrite of Tor Weather, to be held on the OFTC #tor-dev channel on Wednesday 26th February at 18:00 UTC. “Topics include reporting progress made since the last meeting two weeks ago, making plans for the next week, and possibly turning this project into a GSoC project”.</p>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006282.html" rel="nofollow">announced</a> the start of a weekly Tor developer’s meeting, to be held on the OFTC #tor-dev channel, “for working on the program ‘tor’. (This won’t cover all the other programs developed under the Tor umbrella.)” .</p>

<p>The Tails developers <a href="https://mailman.boum.org/pipermail/tails-dev/2014-February/004934.html" rel="nofollow">will be holding</a> their next contributors’ meeting on the OFTC #tails-dev channel at 9pm UTC on March 5th; “everyone interested in contributing to Tails is welcome.” .</p>

<p>Andrew Lewman submitted his <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000462.html" rel="nofollow">monthly status report for January</a>, and also wrote up a report of his <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000463.html" rel="nofollow">involvement in the recent Boston CryptoParty</a>.</p>

<p>Alexander Dietrich <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003942.html" rel="nofollow">published</a> a multi-instance init script for Tor (“basically the current official init script and torservers.net’s “instances” mechanism frankensteined together”, in Alexander’s words) in response to <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003913.html" rel="nofollow">a recent discussion on tor-relays</a>.</p>

<p>Responding to a message from someone interested in writing a DNS-based pluggable transport, George Kadianakis <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006250.html" rel="nofollow">suggested</a> several ways in which the existing obfsproxy code could be reworked to accommodate this.</p>

<p>George also <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003951.html" rel="nofollow">recommended</a> that operators of obfs3 or ScrambleSuit bridges install the python-gmpy package on their relays, as it can significantly increase the speed of some cryptographic operations.</p>

<p>Jens Kubieziel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006260.html" rel="nofollow">wrote up</a> the results of an attempt to determine whether the recent transition between the TAP and NTor handshake protocols is connected to some users’ reports of hidden service unavailability.</p>

<p>Max Jakob Maass <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032173.html" rel="nofollow">published</a> the preliminary results of a test in which the RIPE Atlas measurement API was used to retrieve the SSL certificate of torproject.org from as many countries as possible in order to detect attempted attacks or censorship, and wondered whether it might be worth running such a test on a regular basis.</p>

<p>With regard to a coming redesign of the “Volunteers” section on the Tor Project’s website, Moritz Bartl wrote up a list of proposed volunteer categories that was the fruit of a brainstorming session at the Tor developers’ meeting, and <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032176.html" rel="nofollow">asked for suggestions</a> of “obvious” missing sections, as well as “acceptably-licensed” graphics that could serve as icons for each category .</p>

<p>Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032174.html" rel="nofollow">wrote</a> from the Tor developers’ meeting with a request for help in compiling “<a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2014WinterDevMeeting/notes/UserStories" rel="nofollow">user stories</a>” on the Tor wiki: that is, stories of the form “a <em>type of Tor user</em> wants to <em>some feature of a Tor app</em> in order to <em>some reason related to security, privacy, etc</em>”. If you have any to add, please write them up on the dedicated wiki page!</p>

<p>Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006300.html" rel="nofollow">sent out</a> a draft of a proposal to “extend the SOCKS5 protocol when communicating with pluggable transports to allow passing more per-bridge meta-data to the transport and returning more meaningful connection failure response codes back to Tor”.</p>

<p>Josh Ayers <a href="http://opensource.dyc.edu/pipermail/tor-ramdisk/2014-February/000119.html" rel="nofollow">wrote</a> to the Tor-ramdisk list suggesting possible ways to ensure that sufficient entropy is available to the kernel by the time tor-ramdisk generates its long-term keys.</p>

<h1>Tor help desk roundup</h1>

<p>A common question the help desk receives is how to respond to the Tor Browser Bundle’s download warning message. The message indicates that the Tor Browser Bundle only routes browser traffic through Tor, not traffic from any other application. For example, a PDF file that connects automatically to a URL will not route its traffic through Tor if the file is opened with an external application. An <a href="https://bugs.torproject.org/7439" rel="nofollow">open bug ticket</a> for improving this warning message has more information about the issue.</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Lunar, Matt Pagan, Nicolas Vigier, Roger Dingledine, and George Kadianakis.</p>

<p>Want to continue reading TWN? Please help us create this newsletter.  We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

