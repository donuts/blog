title: Tor Weekly News — January 28th, 2015
---
pub_date: 2015-01-28
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fourth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>The future of Private Browsing Mode</h1>

<p>Mozilla Firefox, on which Tor Browser is based, offers users a “Private Browsing Mode” that aims solely to prevent browsing information from being saved locally. As Georg Koppen <a href="https://lists.torproject.org/pipermail/tbb-dev/2015-January/000217.html" rel="nofollow">pointed out</a>, “The question is now how to treat the other privacy-relevant areas like cross-origin linkability or fingerprinting?”</p>

<p>Georg proposed a “Private Browsing Mode+”, which would integrate the disk-avoidance, anti-tracking, anti-fingerprinting, and location-concealing elements of a privacy-preserving browser in a more logical way.</p>

<h1>Miscellaneous news</h1>

<p>After a “<a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008211.html" rel="nofollow">pretty big overhaul</a>”, Damian Johnson announced that <a href="https://stem.torproject.org/" rel="nofollow">Stem</a>, the Tor controller library, now “lazy-loads” descriptors, resulting in a <a href="https://gitweb.torproject.org/stem.git/commit/?id=3dac7c5" rel="nofollow">considerable speed increase</a> when reading network documents. “Note that descriptor validation is now opt-in rather than opt-out, so if you’d prefer validation over performance you’ll now need to include ‘validate = True’.”</p>

<p>The Tails team <a href="https://mailman.boum.org/pipermail/tails-dev/2015-January/007945.html" rel="nofollow">set out</a> the release schedule for Tails 1.3, and also published its <a href="https://tails.boum.org/contribute/working_together/code_of_conduct/" rel="nofollow">Code of Conduct</a>.</p>

<p>Arturo Filastò <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-January/000238.html" rel="nofollow">reported</a> on OONI-related activities at the Nexa Center’s <a href="http://nexa.polito.it/nntools2015" rel="nofollow">NNTools2015</a> event.</p>

<p>Patrick Schleizer <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036581.html" rel="nofollow">wondered</a> how Tor Browser could be made to act more like a “system tor”, and sketched out a <a href="https://bugs.torproject.org/14121" rel="nofollow">possible plan</a>: “What do you think about this proposal in general?”</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

