title: Tor Weekly News — June 17th, 2015
---
pub_date: 2015-06-17
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-fourth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.6.9 is out</h1>

<p>Nick Mathewson <a href="https://blog.torproject.org/blog/tor-0269-released" rel="nofollow">announced</a> a new release in Tor’s current stable series. Version 0.2.6.9 stops relays without the Stable flag from serving as onion service directories, and raises the uptime requirement for the Stable flag itself, which means that any Sybil attacks launched against the network will not become effective for at least a week. This change only affects the Tor network’s nine directory authorities, most of whom have already upgraded.</p>

<p>The other significant fix in this release concerns port-based isolation of client requests, which now functions properly; if you make use of this feature in your standalone Tor client, then please upgrade as soon as possible. For other users, writes Nick, this “is not a high-urgency item”.</p>

<h1>Tor Browser 4.5.2 and 5.0a2 are out</h1>

<p>The Tor Browser team put out new stable and alpha releases of the privacy-preserving browser. As well as updates to key software components, versions <a href="https://blog.torproject.org/blog/tor-browser-452-released" rel="nofollow">4.5.2</a> and <a href="https://blog.torproject.org/blog/tor-browser-50a2-released" rel="nofollow">5.0a2</a> both contain fixes for the “<a href="https://weakdh.org/" rel="nofollow">Logjam</a>” attack on TLS security - as Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008868.html" rel="nofollow">wrote</a> at the time of this vulnerability’s disclosure, the connections between Tor clients and relays were unlikely to have been affected by this attack, but the bug is now fixed in the browser component of Tor Browser as well.</p>

<p>These new releases also fix a possible crash in Linux, and stop the Add-ons page from breaking if Torbutton is disabled. The new alpha further improves meek’s compatibility with the automatic update process on Windows machines.</p>

<p>All users should upgrade their Tor Browser as soon as possible. Your browser might already have prompted you to do this — if not, you can always upgrade by downloading a fresh copy from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">Tor website</a>.</p>

<h1>The future of GetTor and uncensorable software distribution</h1>

<p>The <a href="https://www.torproject.org/projects/gettor" rel="nofollow">GetTor</a> service offers users who are unable to reach the Tor website an alternative method of downloading Tor Browser: any email sent to <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a> will receive an automated reply containing links to file-hosting services (such as Dropbox) for the latest Tor Browser package and its signature.</p>

<p>Israel Leiva, lead developer on the revamped GetTor project since last year’s Google Summer of Code, is back for the first-ever <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Tor Summer of Privacy</a> to continue expanding the feature set of this tool. As Israel wrote to the <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008949.html" rel="nofollow">tor-dev mailing list</a>, current plans for the summer include the addition of other file-hosting services, Tor Browser localizations, and other distribution methods (including instant messaging and Twitter).</p>

<p>However, it might also be time for a more radical change in the way GetTor works. An official distributor application or browser add-on, available through channels like the OS X or Google Chrome app stores, could automate Tor Browser downloads, as well as the vital but unintuitive process of verifying the signature to ensure the software has not been tampered with. Israel offered two suggestions for the inner workings of such a distributor: one involving a fixed (but potentially blockable) backend API with which the distributor communicates, and one in which a more complex distributor is capable of helping the user download the required software from several different sources.</p>

<p>Some related projects are already underway: the Tails team is discussing the possibility of its own browser add-on for <a href="https://tails.boum.org/blueprint/bootstrapping/extension/" rel="nofollow">ISO download and verification</a>, while Griffin Boyce <a href="https://github.com/glamrock/satori" rel="nofollow">pointed</a> to his own Satori project, a distributor application that offers torrent files and content-delivery network (CDN) links. The discussion over the possible GetTor distributor’s relationship with these projects is still to be had.</p>

<p>“I would really love to hear your comments about this idea, my work at Summer of Privacy might change depending on this discussion”, writes Israel. It’s clear that forcing users to depend on “single points of failure” for their software is bad news all round, so if you have worthwhile ideas to add to this discussion, feel free to take them to the tor-dev mailing list thread.</p>

<h1>Great progress on Orfox browser</h1>

<p>Nathan Freitas, of mobile device security specialists the Guardian Project, <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-June/004446.html" rel="nofollow">reported</a> on the status of Orfox, the Android-compatible Tor Browser build. “The goal is to get as close to the ‘real Tor Browser’ while taking into account the new, unique issues we face on Android”, he wrote. Amogh Pradeep, former Google Summer of Code student and now intern at the Guardian Project, has made significant progress getting the software to build, and you can follow his regular updates on the <a href="https://dev.guardianproject.info/projects/orfox-private-browser/news" rel="nofollow">Orfox development blog</a>. “We expect to have an alpha out this week”, wrote Nathan, “but feel free to jump in on testing of the posted builds, and file bugs or feature requests as you find them”.</p>

<h1>A persistent Tor state for Tails?</h1>

<p>The Tails team is discussing the possibility of making Tor’s state persist across sessions in the anonymous live operating system. As the team writes on the relevant <a href="https://tails.boum.org/blueprint/persistent_Tor_state/" rel="nofollow">blueprint page</a>, such a change would have several benefits: not only would Tor’s bootstrap process be faster and more efficient, but it would enable Tails to take advantage of the “<a href="https://www.torproject.org/docs/faq#EntryGuards" rel="nofollow">entry guards</a>” concept, without which Tails users are more likely to select a malicious entry node at some point over the course of their activity. Moreover, the fact that Tails selects a new entry node on every boot, while Tor Browser does not, allows an adversary to determine whether a user who remains on one network (their home or place of work, for example) is using Tails or not. This would also be solved by a persistent Tor state.</p>

<p>However, this change does of course have some drawbacks. For one thing, although entry guards in Tails would help defend against end-to-end correlation attacks, they enable a certain kind of fingerprinting: if a user makes a connection to an entry guard from their home, and an adversary later observes a connection to the same guard from an event or meeting-place that the user is suspected of attending, the adversary can draw a conclusion about the user’s geographical movement. This violates one of Tails’ threat model principles, which the team calls “AdvGoalTracking”. There are ways that Tails could request location information from the user in order to maintain different entry guards for different locations, but too many requests for information might bamboozle Tails users into accidentally worsening their own security, especially if they do not understand the threat model behind the requests, or it does not apply to them.</p>

<p>What is needed, then, is a balance between “defaults that suit the vast majority of use-cases […] for Tails’ target audience” and helping “users with different needs to avoid becoming less safe ‘thanks’ to this new feature”. The discussion continues on the <a href="https://mailman.boum.org/pipermail/tails-dev/2015-June/009095.html" rel="nofollow">tails-dev mailing list</a>.</p>

<h1>Miscellaneous news</h1>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-relays/2015-June/007179.html" rel="nofollow">recommended</a> that all relay operators upgrade their copies of OpenSSL to fix several issues that could enable remote denial-of-service attacks. As Nick makes clear, this is an “upgrade when you can”-level announcement, rather than a “run in circles freaking out”. Nick also requests that people still using OpenSSL’s 0.9.8 series upgrade to one of the more recent versions, as 0.9.8 contains several security flaws and will not be supported by Tor 0.2.7.2-alpha or later.</p>

<p>Sherief Alaa <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000854.html" rel="nofollow">reported</a> on his activities in May.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

