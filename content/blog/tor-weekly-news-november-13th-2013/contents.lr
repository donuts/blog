title: Tor Weekly News — November 13th, 2013
---
pub_date: 2013-11-13
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twentieth issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>First beta release of Tor Browser Bundle 3.0</h1>

<p>The <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">Tor Browser Bundle</a> is the Tor Project’s flagship product: an easy and straightforward way to browse the web with anonymity and privacy.</p>

<p>With previous Tor Browser Bundles, users had to interact with two different applications, Vidalia and the browser itself. Vidalia was responsible for handling and configuring the tor daemon, and the browser had no knowledge of the connection status and other details.  The result was confusing error messages, and mismatched user expectations.</p>

<p>With the 3.0 series of Tor Browser Bundle, the browser is directly responsible for configuring and handling the tor daemon. Users only see one single application. It’s clearer that only the browser will go through the Tor network. Starting and stopping the browser will take care of starting and stopping tor — no extra steps are required.</p>

<p>Mike Perry, Kathleen Brade, Mark Smith, Georg Koppen, among others, are working hard to perfect many other usability and technical improvements that are part of Tor Browser Bundle 3.0 which has now reached the “beta” stage.</p>

<p>The new <a href="https://blog.torproject.org/blog/tor-browser-bundle-30beta1-released" rel="nofollow">3.0beta1 release</a> is based on Firefox 17.0.10esr for <a href="https://www.mozilla.org/security/known-vulnerabilities/firefoxESR.html#firefox17.0.10" rel="nofollow">security updates</a>, and contains several other small improvements and corrections.</p>

<p>Current users of the 3.0 alpha series should update. Others should <a href="https://archive.torproject.org/tor-package-archive/torbrowser/3.0b1/" rel="nofollow">give it a try</a>!</p>

<h1>A critique of website traffic fingerprinting attacks</h1>

<p>For a new <a href="https://blog.torproject.org/blog/critique-website-traffic-fingerprinting-attacks" rel="nofollow">blog post</a>, Mike Perry took the time to reflect on fingerprinting attacks on website traffic. These are attacks “where the adversary attempts to recognize the encrypted traffic patterns of specific web pages without using any other information. In the case of Tor, this attack would take place between the user and the Guard node, or at the Guard node itself.”</p>

<p>In the post, Mike lays down three distinct types of adversary that could mount fingerprinting attacks: partial blocking of Tor, identification of visitors of a set of targeted pages, and identification of all web pages visited by a user.</p>

<p>In theory, such attacks could pose devastating threats to Tor users.  But in practice, “false positives matter” together with other factors that affect the classification accuracy. Mike gives a comprehensive introduction to these issues before reviewing five research papers published between 2011 and 2013. Each of them are summarized together with their shortcomings.</p>

<p>Mike concludes that “defense work has not been as conclusively studied as these papers have claimed, and that defenses are actually easier than is presently assumed by the current body of literature.” He encourages researchers to re-evaluate existing defenses “such as <a href="http://freehaven.net/anonbib/cache/LZCLCP_NDSS11.pdf" rel="nofollow">HTTPOS</a>, SPDY and pipeline randomization, <a href="https://bugs.torproject.org/7028" rel="nofollow">Guard node adaptive padding</a>, and <a href="http://freehaven.net/anonbib/cache/morphing09.pdf" rel="nofollow">Traffic Morphing</a>“, and to think about “the development of additional defenses”. Mike ends his post by mentioning that some new defenses can also be dual purpose and help with end-to-end correlation attacks.</p>

<h1>The “bananaphone” pluggable transport</h1>

<p><a href="https://www.torproject.org/docs/pluggable-transports.html" rel="nofollow">Pluggable transports</a> is how Tor traffic can be transformed from a<br />
client to a bridge in order to hide it from Deep Packet Inspection filters.</p>

<p>Improving upon the <a href="https://github.com/leif/bananaphone" rel="nofollow">initial work of Leif Ryge</a>, David Stainton has been working on the new <a href="https://github.com/david415/obfsproxy/tree/david-bananaphone" rel="nofollow">“bananaphone” pluggable transport for obfsproxy</a>. The latter implements “reverse hash encoding“, described by Leif Ryge as “a steganographic encoding scheme which transforms a stream of binary data into a stream of tokens (e.g., something resembling natural language text) such that the stream can be decoded by concatenating the hashes of the tokens.”</p>

<p>For a concrete example, that means that using <a href="http://www.gutenberg.org/cache/epub/29468/pg29468.txt" rel="nofollow">Project Gutenberg’s Don Quixote</a> as corpus, one can encode “my little poney” into “lock whisper: yellow tremendous, again suddenly breathing. master’s faces; fees, beheld convinced there calm” and back again!</p>

<p>While it’s probably not going to be the most compact pluggable transport, “bananaphone” looks like a promising project.</p>

<h1>Miscellaneous news</h1>

<p>Christian Grothoff, Matthias Wachs and Hellekin Wolf are working on getting <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005747.html" rel="nofollow">special-use domain names for P2P networks reserved</a> according to <a href="https://tools.ietf.org/html/rfc6761" rel="nofollow">RFC 6761</a>: “the goal is to reserve .onion, .exit, .i2p, .gnu and .zkey (so that they don’t become ordinary commercial TLDs at some point)”.</p>

<p>The Tails team has released their report on <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000383.html" rel="nofollow">Tails activity during the month of October</a>. Things are happening on many fronts, have a look!</p>

<p>Andrea Shepard has been working on new scheduler code for Tor. Its goal is to remove the limitation that “we can only see one channel at a time when making scheduling decisions.” Balancing between circuits without opening new attack vectors is tricky, Andrea is asking for <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005761.html" rel="nofollow">comments on potential heuristics</a>.</p>

<p>Justin Findlay has <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005762.html" rel="nofollow">recreated some of the website diagrams</a> in the versatile SVG format.</p>

<p>Roger <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031001.html" rel="nofollow">asked the community</a> to create a “Tor, king of anonymity” graphic for his presentations. Griffin Boyce made a <a href="http://i.imgur.com/PmuFz4n.jpg" rel="nofollow">“queen of anonymity” picture</a>, Lazlo Westerhof <a href="http://i.imgur.com/vYZSu6Q.png" rel="nofollow">crowned the onion</a> and Matt Pagan did the <a href="http://i.imgur.com/2yIMmcQ.png" rel="nofollow">full Tor logo</a> .</p>

<p>David Fifield released the new <a href="https://blog.torproject.org/blog/pluggable-transports-bundles-2417-rc-1-pt2-firefox-17010esr" rel="nofollow">Pluggable Transports Tor Browser Bundle version 2.4.17-rc-1-pt2 </a> based on Tor Browser Bundle 2.4.17-rc-1. The only change from the previous release of the pluggable transport bundle is a <a href="https://bugs.torproject.org/10030#comment:20" rel="nofollow">workaround</a> that makes transports resume working on Mac OS X Mavericks.</p>

<h1>Tor help desk round-up</h1>

<p>Recently users have been writing the help desk asking for assistance verifying the signature on their Tor Browser Bundle package. These users said they found the <a href="https://torproject.org/docs/verifying-signatures.html" rel="nofollow">instructions on the official Tor Project page</a> confusing. One person reported being unsure of how to open a terminal on their computer. Another person did not know how to save the package signature onto the Desktop. Yet another person reported they were able to verify the signature only after discovering that their GnuPG program was named gpg2.exe rather than gpg.exe. A ticket on <a href="https://bugs.torproject.org/10073" rel="nofollow">improving the signature verification page</a> has been opened.</p>

<p>One user mentioned wanting to use the Tor Browser Bundle as their default browser but being unable to do so because their online bank required Java. Java is disabled in the Tor Browser Bundle because it can bypass the browser proxy settings and leak the client’s real IP address over the network.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, David Stainton, sqrt2, and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

