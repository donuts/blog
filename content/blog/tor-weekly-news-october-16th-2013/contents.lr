title: Tor Weekly News — October 16th, 2013
---
pub_date: 2013-10-16
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the sixteenth issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the venerable Tor community.</p>

<h1>Making hidden services more scalable and harder to locate</h1>

<p>Christopher Baines <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005556.html" rel="nofollow">started a discussion</a> on tor-dev on the scaling issues affecting the current design of Tor hidden services. Nick Mathewson later described Christopher’s initial proposal as “single hidden service descriptor, multiple service instances per intro point” along with three other alternatives. Nick and Christopher also teamed up to produce a set of seven goals that a new hidden service design should aim for regarding scalability.</p>

<p>There’s probably more to discuss regarding which goals are the most desirable, and which designs are likely to address them, without — as always — harming anonymity.</p>

<p>George Kadianakis also <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005621.html" rel="nofollow">called for help</a> concerning “the guard enumeration attack that was described by the <a href="http://www.ieee-security.org/TC/SP2013/papers/4977a080.pdf" rel="nofollow">Trawling for Tor Hidden Services: Detection, Measurement, Deanonymization</a> paper (in section VII).”</p>

<p>The most popular solution so far seems to be enabling a client or hidden service to reuse some parts of a circuit that cannot be completed successfully in order to connect to new nodes. This should “considerably <a href="https://bugs.torproject.org/9001" rel="nofollow">slow the attack</a>”, but “might result in unexpected attacks” as George puts it.</p>

<p>These problems could benefit from everyone’s attention. Free to read the threads in full and offer your insights!</p>

<h1>Detecting malicious exit nodes</h1>

<p>Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005593.html" rel="nofollow">asked</a> for feedback on the technical architecture of “a Python-based exit relay scanner which should detect malicious and misbehaving exits.”</p>

<p>Aaron took the opportunity to <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005596.html" rel="nofollow">mention his plans</a> to leverage the work done as part of <a href="https://ooni.torproject.org/" rel="nofollow">OONI</a> to detect network interference. Aaron’s intention is to “provide Tor network tests as part of ooni-probe’s standard set of tests, so that many individuals will measure the Tor network and automatically publish their results, and so that current and future network interference tests can be easily adapted to running on the Tor network.”</p>

<p>Detecting misbehaving exits so they can be <a href="https://trac.torproject.org/projects/tor/wiki/doc/badRelays" rel="nofollow">flagged “BadExit”</a> by the operators of the directory authorities is important to make every Tor user safer. Getting more users to run tests against our many exit nodes would benefit everyone — it makes it more likely that bad behavior will be caught as soon as possible.</p>

<h1>Hiding location at the hardware level</h1>

<p>One of Tor’s goals is to hide the location of its users, and it does a reasonably good job of this for Internet connections. But when your threat model includes an adversary that can monitor which equipment is connected to a local network, or monitor Wi-Fi network probes received by many access points, extra precautions must be taken.</p>

<p>Ethernet and Wi-Fi cards ship with a factory-determined hardware address (also called a MAC address) that can uniquely identify a computer across networks. Thankfully, most devices allow their hardware address to be changed by an operating system.</p>

<p>As the Tails live operating system aims to protect the privacy and anonymity of its users, it has <a href="https://labs.riseup.net/code/issues/5421" rel="nofollow">long been suggested</a> that it should automatically randomize MAC addresses. Some important progress has been made, and this week anonym <a href="https://mailman.boum.org/pipermail/tails-dev/2013-October/003835.html" rel="nofollow">requested comments</a> on a detailed analysis of why, when and how Tails should randomize MAC addresses.</p>

<p>In this analysis, anonym describes a Tails user wanting to hide their geographical movement and not be identified as using Tails, but who also wants to “avoid alarming the local administrators (imagine a situation where security guards are sent to investigate an ‘alien computer’ at your workplace, or similar)” and “avoid network connection problems due to MAC address white-listing, hardware or driver issues, or similar”.</p>

<p>The analysis then tries to understand when MAC address should be randomized depending on several combinations of locations and devices.  The outcome is that “this feature is enabled by default, with the possibility to opt-out.” anonym then delves into user interface and implementation considerations.</p>

<p>If you are interested in the analysis, or curious about how you could help with the proposed implementation, be sure to have a look!</p>

<h1>Tor Help Desk Roundup</h1>

<p>The Tor project wishes to expand its support channels to text-based instant messaging as part of the <a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/Otter/Boisterous" rel="nofollow">Boisterous Otter</a> project. Lunar and Colin C. came up with a <a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/Otter/Boisterous/WebChat" rel="nofollow">possible implementation</a> based on the XMPP protocol, <a href="https://prosody.im/" rel="nofollow">Prosody</a> for the server side, and <a href="http://forge.webpresso.net/projects/prodromus" rel="nofollow">Prodromus</a> as the basis for the web based interface.</p>

<p>This week, multiple people asked if Tor worked well on the Raspberry Pi. Although the Tor Project does not have any documentation directed specifically at the Raspberry Pi (yet!), the <a href="http://tor.stackexchange.com/questions/242/how-to-run-tor-on-raspbian-on-the-raspberry-pi" rel="nofollow">issue was raised on Tor’s StackExchange page</a>. Tor relay operators are encouraged to share their experiences or ask for help on the <a href="https://lists.torproject.org/pipermail/tor-relays/" rel="nofollow">tor-relays public mailing list</a>.</p>

<h1>Miscellaneous news</h1>

<p>Ten years ago, on October 8th, 2003, Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-dev/2003-October/002185.html" rel="nofollow">announced</a> the first release of tor as free software on the or-dev mailing list.</p>

<p>Damian Johnson <a href="https://blog.torproject.org/blog/stem-release-11" rel="nofollow">announced</a> the release of <a href="https://stem.torproject.org/change_log.html#version-1-1" rel="nofollow">Stem 1.1.0</a>. The new version of this “Python library for interacting with Tor” adds remote descriptor fetching, connection resolution and a myriad of small improvements and fixes.</p>

<p>Arlo Breault sent out a <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005616.html" rel="nofollow">detailed plan on how Mozilla Instantbird could be turned into the Tor Messenger</a>. Feedback would be welcome, especially with regard to sandboxing, auditing, and internationalization for right-to-left languages.</p>

<p>The Spanish and German version of the Tails website are outdated and <a href="https://mailman.boum.org/pipermail/tails-dev/2013-October/003879.html" rel="nofollow">may soon be disabled </a>. Now is a good time to <a href="https://tails.boum.org/contribute/how/translate/" rel="nofollow">help</a> if you want to keep those translations running!</p>

<p>adrelanos <a href="https://lists.torproject.org/pipermail/tor-talk/2013-October/030593.html" rel="nofollow">announced</a> the release of Whonix 7, an operating system “based on Tor […], Debian GNU/Linux and the principle of security by isolation.” The new version includes tor 0.2.4, Tor Browser as the system default browser and a connection wizard, among other changes.</p>

<p>Lunar sent out a <a href="https://lists.torproject.org/pipermail/tor-reports/2013-October/000363.html" rel="nofollow">report about the Dutch OHM2013 hacker camp</a> that took place in August.</p>

<p>Philipp Winter, Justin Bull and rndm made several improvements to <a href="https://atlas.torproject.org/" rel="nofollow">Atlas</a>, the web application for learning more about currently-running Tor relays. The HSDir flag is now properly displayed (<a href="https://bugs.torproject.org/9911" rel="nofollow">#9911</a>), full country names and flags are shown instead of two-letter codes (<a href="https://bugs.torproject.org/9914" rel="nofollow">#9914</a>) and it’s now easier to find out how long a relay has been down (<a href="https://bugs.torproject.org/9814" rel="nofollow">#9814</a>).</p>

<p>ra, one of Tor’s former GSoC students, proposed a <a href="https://bugs.torproject.org/9934" rel="nofollow">patch</a> to add a command to the Tor control protocol asking tor to pick a completely new set of guards.</p>

<p>starlight <a href="https://lists.torproject.org/pipermail/tor-talk/2013-October/030607.html" rel="nofollow">shared some simple shell snippets</a> that connect to the Tor control port in order to limit the bandwidth used by a relay while running backups.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan, harmony, dope457, Damian Johnson and Philipp Winter.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

