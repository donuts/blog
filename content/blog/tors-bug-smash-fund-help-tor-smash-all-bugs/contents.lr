title: Tor’s Bug Smash Fund: Help Tor smash all the bugs!
---
pub_date: 2019-08-01
---
author: al
---
tags:

bugs
fundraising
firefox
---
categories:

applications
fundraising
releases
---
summary: The bugs are coming. We just don’t know when. This August, all donations to Tor will be earmarked towards bugs and maintenance. Your contribution can help keep us on our toes and ready to respond rapidly to critical security issues.
---
_html_body:

<p>Say hello to our Bug Smash Fund.</p>
<p>When you are building software, your work is not always about adding new features; often, it's about fixing bugs. We know that our community understands it’s very important to fix bugs we encounter in our software—especially bugs that create security problems. But it can be hard to raise money for this type of work, since maintenance is not as exciting as a cool new feature. That’s why we decided to create our Bug Smash Fund.</p>
<p>The goal of the Bug Smash Fund is to increase the Tor Project’s reserve of funds that allow us to complete maintenance work and smash the bugs necessary to keep Tor Browser, the Tor network, and the <a href="https://blog.torproject.org/strength-numbers-entire-ecosystem-relies-tor">many tools that rely on Tor</a> strong, safe, and running smoothly.</p>
<p>When we say maintenance and bugs, we are talking about work that is critical—and that we must pay for. This work includes responding quickly to security bugs, improving test coverage, and keeping up with Mozilla’s ESRs. An entire ecosystem relies on us doing so.</p>
<p>Every donation Tor receives during the month of August will count towards this fund. You can help us be prepared for what bugs may come, and in turn, we can better help keep millions of people around the world safe online and an entire ecosystem of tools healthy.</p>
<p><img alt="Tor Bug Smash Fund" src="/static/images/blog/inline-images/tor-bug-smash_3.gif" class="align-center" /></p>
<h2>Support Rapid Response &amp; Emergency Releases</h2>
<p>Security bugs happen. We can’t predict when they will happen, but it is inevitable that issues will arise. Our top priority is to smash these bugs as quickly as possible when they are discovered and respond with emergency releases. Sometimes this means diverting our attention from grant-funded projects, and we must use unrestricted funds to pay for this developer time.</p>
<p>Here are some examples of what an emergency release looks like, why these issues happen, and what we’ve done in the past to address them:</p>
<ul>
<li>Emergency security update to Tor Browser to handle a mistake in Mozilla’s signing infrastructure. (<a href="https://blog.torproject.org/noscript-temporarily-disabled-tor-browser">‘NoScript Temporarily Disabled in Tor Browser,’ 2019</a>)</li>
<li>Emergency release of Tor Browser, fixing an IP address leak issue caused by a bug in Firefox’s handling of file:// URLs. (<a href="https://blog.torproject.org/tor-browser-709-released">‘Tor Browser 7.0.9 is released,’ 2017</a>)</li>
</ul>
<p>Your donation to the Bug Smash Fund will go in part towards rapid responses to security issues like these.</p>
<h2>Support Keeping up with Firefox ESRs</h2>
<p>Because Tor Browser is built on top of Mozilla Firefox, when new <a href="https://www.mozilla.org/en-US/firefox/organizations/">ESRs</a> are released, the Tor Browser team must evaluate changes to Firefox, weigh their privacy and anonymity implications, and modify or disable features that may compromise our users.</p>
<p>You can get a feeling of how this work is welcomed by our community from the reactions on social media and in the news when we adopted the Firefox Quantum ESR.</p>
<blockquote class="twitter-tweet"><p dir="ltr" xml: xml:>We've got BIG NEWS. We gave Tor Browser a UX overhaul.</p>
<p>Tor Browser 8.0 has a new user onboarding experience, an updated landing page, additional language support, and new behaviors for bridge fetching, displaying a circuit, and visiting .onion sites.<a href="https://t.co/fpCpSTXT2L">https://t.co/fpCpSTXT2L</a> <a href="https://t.co/xbj9lKTApP">pic.twitter.com/xbj9lKTApP</a></p>
<p>— The Tor Project (@torproject) <a href="https://twitter.com/torproject/status/1037397236257366017?ref_src=twsrc%5Etfw">September 5, 2018</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet">
<p dir="ltr" xml: xml:>Ladies &amp; Gentlemen, Tor Browser "Quantum" (with NoScrpt "Quantum").<br />
Congrats to the <a href="https://twitter.com/torproject?ref_src=twsrc%5Etfw">@torproject</a> for this epic journey and release! <a href="https://t.co/61SzApEkKk">https://t.co/61SzApEkKk</a></p>
<p>— Giorgio Maone (@ma1) <a href="https://twitter.com/ma1/status/1037578465401360384?ref_src=twsrc%5Etfw">September 6, 2018</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet">
<p dir="ltr" xml: xml:>If you really don't want to be tracked on the net, the new <a href="https://twitter.com/torproject?ref_src=twsrc%5Etfw">@torproject</a> browser is out. Based on Firefox Quantum interface; new intro for new users; easier bridge mechanism for using it where it's blocked; 9 new languages supported. <a href="https://t.co/wzaFLSfjk0">https://t.co/wzaFLSfjk0</a></p>
<p>— Stephen Shankland (@stshank) <a href="https://twitter.com/stshank/status/1037420224868311040?ref_src=twsrc%5Etfw">September 5, 2018</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><p>Keeping up with ESRs means that Tor Browser users get the <a href="https://www.zdnet.com/article/tor-browser-gets-a-redesign-switches-to-new-firefox-quantum-engine/">user experience improvements</a> of Firefox, and that the Tor team can <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1430149">upstream privacy improvements to Firefox</a>. This work is a regular part of maintaining Tor Browser—and it must happen regularly.</p>
<p>Your donation to the Bug Smash Fund will go in part towards keeping Tor Browser in line with current ESRs.</p>
<p><a href="https://donate.torproject.org"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_13.png" class="align-center" /></a></p>
<p><strong>From August 1 through August 31, any donation that comes to the Tor Project will go towards the Bug Smash Fund and be earmarked for these bugs and maintenance projects.</strong></p>
<p>Want to keep up with the work we’re doing with this fund? There are three ways: (1) Follow the “BugSmashFund” trac ticket tag, (2) watch this blog for updates about the progress of these tickets, and (3) <a href="https://donate.torproject.org">make a donation</a> and opt in for our newsletter to get updates directly to your inbox.</p>
<p>Want to contribute anonymously, with <a href="https://donate.torproject.org/cryptocurrency">cryptocurrency</a>, or by mail? <a href="https://2019.www.torproject.org/donate/donate-options.html.en">Here’s how</a>.</p>
<p>Want to ask a question or learn more about supporting the Tor Project? You can email us at <a href="mailto:giving@torproject.org">giving@torproject.org</a>. </p>
<p> </p>
<hr />
<p> </p>
<p><em>Thanks to <a href="https://fontvir.us/">fontvir.us</a> for the font in our images. </em></p>

---
_comments:

<a id="comment-283341"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283341" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 01, 2019</p>
    </div>
    <a href="#comment-283341">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283341" class="permalink" rel="bookmark">This is a great idea and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a great idea and your post explains well what the new fund is all about and why it is so important for all Tor users.  I am donating!</p>
<p>Also, thanks for including the link to </p>
<p><a href="https://2019.www.torproject.org/donate/donate-options.html.en" rel="nofollow">https://2019.www.torproject.org/donate/donate-options.html.en</a></p>
<p>which has the correct address for Tor Project :-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-283386"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283386" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tabq (not verified)</span> said:</p>
      <p class="date-time">August 04, 2019</p>
    </div>
    <a href="#comment-283386">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283386" class="permalink" rel="bookmark">The bug smash fund sounds…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The bug smash fund sounds like a great idea - well done!</p>
<p>One comment though:<br />
"Our top priority is to smash these bugs as quickly as possible when they are discovered and respond with emergency releases."</p>
<p>In an ideal world the Tor Project would also have resources to pro-actively seek out bugs, not just wait for them to surface.</p>
</div>
  </div>
</article>
<!-- Comment END -->
