title: A weekend at New England Give Camp
---
pub_date: 2013-04-29
---
author: phobos
---
tags:

tor
anonymity
domestic violence
dv
ipv
new england give camp
microsoft
intimate partner violence
cambridge
cctv
---
categories:

human rights
network
---
_html_body:

<h1>Trip Report for New England Give Camp 2013</h1>

<p>I spent the entire weekend with <a href="http://newenglandgivecamp.org/" rel="nofollow">New England Give Camp</a> at <a href="http://microsoftcambridge.com" rel="nofollow">Microsoft Research</a> in Cambridge, MA. I was one of the non-profits, representing <a href="https://wiki.tpo.is" rel="nofollow">ipv tech</a>, Tor, and offering myself as a technical volunteer to help out other non-profits. Over the 48 hours, here's what I helped out doing:</p>

<ul>
<li><a href="http://www.transitionhouse.org/" rel="nofollow">Transition House</a>
<ul>
<li>Help evaluate their IT systems</li>
<li>Look at, reverse engineer, and fix their Alice database system</li>
</ul>
</li>
<li><a href="http://www.emergedv.com/" rel="nofollow">Emerge</a>
<ul>
<li>Update their wordpress installation</li>
<li>Help fix the rotating images on the site</li>
</ul>
</li>
<li><a href="https://wiki.tpo.is" rel="nofollow">ipv tech</a>
<ul>
<li>Hack on <a href="http://fuerza.is" rel="nofollow">fuerza</a> app</li>
<li>Get fuerza into a git repo, now <a href="https://gitorious.org/fuerza" rel="nofollow">here at gitorious</a></li>
<li>rewrite the app to be markdown and static files to work offline</li>
</ul>
</li>
<li>Children's Charter
<ul>
<li>Help resurrect their hacked WordPress installation and build them a new site.</li>
</ul>
</li>
</ul>

<p>I also did a 30 minute talk about technology and intimate partner violence. Over the past few years, I've seen every possible technology used to stalk, harass, and abuse people--and those that help them. I'm helping the victims and advocates use the same technologies to empower the victims and turn the tables on the abusers in most cases. The ability to be anonymous and be free from surveillance for once, even for an hour, is cherished by the victims and affected advocates.<br />
Our team was great. Kevin, Paul, John, Bob, Carmine, Adam, and Sarah did a great job at keeping motivated, making progress, and joking along the way. Microsoft, Whole Foods, and a slew of sponsors offered endless food, sugary drinks, beautiful views, and encouragement throughout the weekend.<br />
Cambridge Community Television <a href="http://cctvcambridge.org/NEGiveCamp2013" rel="nofollow">interviewed me</a> at the very end of the event. There's also a Flickr group <a href="https://secure.flickr.com/groups/negc2013/" rel="nofollow">full of pictures</a>.<br />
Overall it was a great experience. I encourage you to volunteer next year.</p>

