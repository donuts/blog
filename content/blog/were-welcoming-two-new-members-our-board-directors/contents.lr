title: We're Welcoming Two New Members to Our Board of Directors
---
pub_date: 2017-10-10
---
author: steph
---
tags:

press release
board of directors
---
summary: Today, we're welcoming two new members to our Board of Directors: Julius Mittenzwei and Ramy Raoof. “Julius and Ramy bring a wealth of diverse experience in internet activism and organizational leadership to Tor’s Board,” said Shari Steele, Executive Director of the Tor Project.
---
_html_body:

<p> </p>
<p>Today, we're welcoming two new members to our Board of Directors: Julius Mittenzwei and Ramy Raoof. </p>
<p>“Julius and Ramy bring a wealth of diverse experience in internet activism and organizational leadership to Tor’s Board,” said Shari Steele, Executive Director of the Tor Project.</p>
<p><b>About Julius Mittenzwei</b></p>
<p><a href="https://www.linkedin.com/in/julius-mittenzwei/">Julius Mittenzwei</a> is a lawyer and internet activist with 19 years of leadership experience as an Executive Director and entrepreneur in the publishing industry. He is a longtime Tor advocate with a background in the Free Software movement and member of the Chaos Computer Club (CCC), one of the oldest hacker collectives in the world. Along with CCC, he has been running Tor nodes since 2005. As a lawyer, he has represented several Tor exit node operators accused of abuse. He holds a PhD in Copyright Law from LMU Munich. </p>
<p><b>About Ramy Raoof</b></p>
<p><a href="https://www.linkedin.com/in/ramyraoof/  ">Ramy Raoof</a> is a technologist and privacy and security researcher with a passion for free/open culture. He has provided and developed digital security plans and strategies for NGOs and members of the media, emergency response in cases of physical threats, support on publishing sensitive materials, secure systems for managing sensitive information, and operational plans for human rights emergency response teams, in Egypt and the MENA region. Most recently, Ramy has been volunteering with different NGOs and civil liberty groups in Central &amp; South America, to enhance their privacy and security through means of behavioral change based on understanding surveillance and threat models in their own contexts and environments. Among different hats, Ramy is Senior Research Technologist at the Egyptian Initiative for Personal Rights (EIPR), Research Fellow with Citizen Lab, and currently a volunteer visitor with Fundación Acceso assisting collectives and networks in Central America around infosec and activism. He is also an Internet Freedom Festival Fellow on security and privacy best practices. Ramy has received multiple international awards for his important work. Most recently, Ramy received the 2017 Heroes of Human Rights and Communications Surveillance<b> </b>from Access Now earlier this month. </p>
<p><b>About the Tor Project</b></p>
<p>The Tor Project is a US 501(c)(3) non-profit organization advancing human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding. For more information, contact Stephanie A. Whited at <a href="mailto:steph@torproject.org">steph@torproject.org</a>. </p>

---
_comments:

<a id="comment-271854"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271854" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2017</p>
    </div>
    <a href="#comment-271854">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271854" class="permalink" rel="bookmark">This is good news!  The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is good news!  The qualifications of the new members are very impressive.</p>
<p>Many thanks to Shari S and the Board for continuing to solidify Tor Project's status as a human rights organization.  I believe this is essential not only for protecting some of the most endangered people in the world (human rights researchers, and perhaps, increasingly, even citizens who merely contribute financial support to human rights groups) but for reducing the likelihood that the USG and allied governments will declare Tor Project a "terrorist organization" [sic], or otherwise declare participation in the Tor movement to be "illegal" [sic].</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271874"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271874" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2017</p>
    </div>
    <a href="#comment-271874">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271874" class="permalink" rel="bookmark">Great news!  And welcome to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great news!  And welcome to the newcomers.</p>
<p>I want to make sure the Board is aware that FBI and DOJ continue to press Congress to outlaw unbackdoored citizen cryptography.  In particular, Rod Rosenberg has mentioned "OS updates" as something LEAs "must" [sic] be able to mess with in order to insert malware into the downloaded patches.  This appears to target the wonderful and invaluable initiative by Tor Project and Debian Project to provide onion mirrors for the Debian repo.  And as recent reports about Python show, this program should be expanded to CPAN (Perl), CRAN (R), SciPy repo, and other crucial tools often used by FOSS coders.  (CRAN isn't even cryptographically signed, an appalling circumstance.)</p>
<p>thehill.com<br />
GOP rep on responsible encryption: 'You can call it whatever you want'<br />
Joe Uchill<br />
12 Oct 2017</p>
<p>&gt; ...<br />
&gt; On Tuesday, during a talk about encryption at the Naval Academy, Deputy Attorney General Rod Rosenstein gave his most thorough remarks on the subject.  “Responsible encryption is achievable. Responsible encryption can involve effective, secure encryption that allows access only with judicial authorization. Such encryption already exists," he said. Rosenstein pointed to the systems used to update software as one example of "responsible" encryption. A vulnerability in that update process led to one of the largest cybersecurity incidents in the last year, when attackers attached the NotPetya malware to an update in Ukrainian accounting software. NotPetya ultimately crippled the global shipping industry and major unrelated firms.</p>
<p>In other words, Rod Rosenstein is calling for cryptographically protected software updates, *provided* that a backdoor is provided for USG (and anyone else who can figure out how to exploit the backdoor, of course).  Cybersecurity experts uniformly oppose software backdoors, of course, because these are built-in security vulnerabilities.  The physical analogy: RR is calling for all homeowners to install shiny new locks, and demanding that they all place a copy of the key under the doormat.  If such legislation were enacted, it would not take GRU or criminals very long to discover that they only need lift the doormat to access every US citizen's private information.</p>
<p>Also, Tor users in the US can join EFF members in asking their Congressional representatives to vote against the 702 reauth, particularly the "backdoor searches" by FBI and other agencies, which might actually succeed if we can ramp up grass-roots opposition:</p>
<p>thehill.com<br />
Opposition mounts against bill to renew surveillance program<br />
By Katie Bo Williams<br />
12 Oct 2017</p>
<p>&gt; ...<br />
Goodlatte’s proposal would place modest limits on the NSA by requiring officials investigating ordinary crimes to obtain a court order before viewing the content of any communications collected under that program, including those sent by Americans.  The legislation does not place the same limits on national security investigators, who are believed to use the database far more frequently. “The bill’s primary reform creates a loophole where backdoor searches of U.S. persons can continue ostensibly for ‘foreign intelligence purposes,’” civil liberties advocacy group Demand Progress wrote in a release commenting on the measure. “This makes it likely that the exception would swallow the rule.”</p>
<p>Both Republican and Democratic members oppose Goodlatte's reauth bill.</p>
<p>More evidence has recently surfaced which confirms that NSA and FBI are exploiting backdoor searches to obtain the medical and banking records of many--- perhaps all--- US citizens, for example by snagging data in transit during improperly secured backups.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271885"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271885" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>byo (not verified)</span> said:</p>
      <p class="date-time">October 13, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271874" class="permalink" rel="bookmark">Great news!  And welcome to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271885">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271885" class="permalink" rel="bookmark">thanks for your article :…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks for your article : nice &amp; clearly documented.</p>
<p>about the medical records , it is less at 100% related at the fbi (small research, more illegitimate than illegal, personal usage often, sick team ... harassment &amp; racketing policy until suicide or hospital _ mental hospital also) than a pandemic survey from private companies &amp; medical center obeying to the nsa (joint-venture) : a disease, a difference, an abnormality, a fantasy or personal behavior or an unclear gender is less &amp; less tolerated in more &amp; more countries:regions.</p>
<p>it is considered as an dangerous genetic:virus risk from ... a lot of persons ; not all are living in the high social class. i do not think they will need a backdoor in a near future , dna recorded on data center (which justice is one of the worst) are yet a famous base , jehovah center is concerned &amp; 'meetic' too.</p>
<p>about banking records , it is a very difficult subject.<br />
when you are in vacation/holiday , and go abroad : they do know how many money you have in your wallet &amp; in your bank account and it is not coming from a hidden nsa ops , it is a bank survey done with the agreement of most all govt (tourism agency policy).</p>
<p>using a backdoor or a malware could allow 'an unknown civil servant', an elected person and even<br />
someone who works in a bank to manipulate your account which transfer, number, destination.</p>
<p>i do not know how you could avoid medical &amp; banking recorded exploited by back-door  when the same things are yet used since several years by legitimate &amp; legal communities, firms, honorable societies, foundations,  (yesterday it was for promoting space tomorrow it will be for experimenting cloning human being) ... </p>
<p>implementing a backdoor sounds as a sadism measure without real justification.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271935"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271935" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sujoy (not verified)</span> said:</p>
      <p class="date-time">October 18, 2017</p>
    </div>
    <a href="#comment-271935">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271935" class="permalink" rel="bookmark">I am happy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am happy</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271937"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271937" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>k.w gorka (not verified)</span> said:</p>
      <p class="date-time">October 18, 2017</p>
    </div>
    <a href="#comment-271937">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271937" class="permalink" rel="bookmark">Very nice and smart :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very nice and smart :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272035"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272035" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 23, 2017</p>
    </div>
    <a href="#comment-272035">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272035" class="permalink" rel="bookmark">Hi guys ... ladies…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi guys ... ladies..... Thanx for being......great thing...merciiii ,!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272263"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272263" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2017</p>
    </div>
    <a href="#comment-272263">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272263" class="permalink" rel="bookmark">Question for Ramy Raoof …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Question for Ramy Raoof (formerly of Citizen Lab in Canada):</p>
<p>I need to get in touch with Citizen Lab but cannot use email.  Years ago, CL had a phone number in their website, but it has been removed.  Is it possible to snail mail or call CL?</p>
<p>TIA.</p>
</div>
  </div>
</article>
<!-- Comment END -->
