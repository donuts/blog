title: Usable security for end-users: How Tor improves usability without compromising user privacy (LibrePlanet 2021)
---
author: ggus
---
start_date: 2021-03-20
---
summary:

During this presentation we will share our human-centered design methodologies and the impact they had in releasing usable free software PET's.



---
body:

Saturday, March 20, 14:45 - 15:30 EDT / LibrePlanet 2021

The Tor network, used by 2.5 million users every day, protects their privacy via “onion routing,” which directs Internet traffic – email, instant messages, online posts, Web form visits, and more – through a multilayered network that obfuscates who the user is, thus concealing their identity and location.

Because our design prioritizes privacy, our tools gather very little information about our users. Early in 2018, we began our User Research Program, where we incorporated user research as part of our outreach security trainings by doing interviews and collecting user feedback on the tools we taught participants during the training. So far, this program has reached an audience of over 800 people in countries like Brazil, Colombia, Mexico, India, Indonesia, Kenya, and Uganda. The methods of conducting open user research and collecting usability feedback mentioned above have been informing design decisions in every stable Tor Browser release since 7.5 (and we’re up to 10.0.2!). 

During this presentation we will share our human-centered design methodologies and the impact they had in releasing usable free software PET's.

**Speaker**

#### Antonela Debiasi

Antonela is a lead product designer, practicing ethical user research and free, human-centered, participatory design. She is interested in critical internet infrastructure, feminism as an intersectional practice, free software communities, privacy, and Russian avant-garde art. She holds a bachelor's degree in Communications Design from the National University in Rosario, Argentina, and a master's degree in Graphic and Media Design from the ISD in Napoli, Italy. She currently leads User Experience and Design at The Tor Project. Before joining a nonprofit for the first time, she designed products within for-profit industries including live betting, fintech, e-commerce, and AR/VR labs. She has been nomading all over the world between 2012-2020.



Website:
[LibrePlanet Schedule](https://libreplanet.org/2021/speakers/#4841)
---
tags:

UX
usability
