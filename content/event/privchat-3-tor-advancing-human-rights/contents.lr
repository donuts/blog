title: PrivChat #3 - Tor Advancing Human Rights (livestream)
---
author: ggus
---
start_date: 2020-12-11
---
summary:

Edward Snowden will host Tor Project’s third PrivChat, a fundraising livestream event and conversation with human rights defenders + real-life Tor users



---
body:

**12/11  18:00 UTC - 13:00 Eastern  10:00 Pacific, [@torproject](https://www.youtube.com/watch?v=S2N3GoewgC8) YouTube channel**

![Edward Snowden](/sites/default/files/inline-images/ed_0.jpeg)

For our third edition of PrivChat, we are bringing you some real-life Tor users who will share how Tor has been important for them and their work to defend human rights and freedoms around the world.

The Tor Project's main mission is to advance human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies. People use our technology, namely the Tor network and Tor Browser, in diverse ways. Tor is used by whistleblowers who need a safe way to bring to light information about wrongdoing -- information that is crucial for society to know -- without sharing their identity. Tor is used by activists around the world who are fighting against authoritarian governments and to defend human rights, not only for their safety and anonymity, but also to circumvent internet censorship so their voices can be heard. Tor allows millions of people to protect themselves online, no matter what privilege they have or don't have.

**Hosted by**  

Edward Snowden  

President, Freedom of the Press Foundation



**Participants**  

Alison Macrina  

Founder, Library Freedom Project

Berhan Taye  

Africa Policy Manager and Global Internet Shutdowns Lead, Access Now

Ramy Raoof  

Security Labs Technologist, Amnesty International



PrivChat is free to attend. If you get value out of these events and you like Tor, please consider [becoming a donor](https://torproject.org/donate/donate-usetor-bp-fot).

Website:
[PrivChat chapter #3](https://www.torproject.org/privchat/)
---
tags:

human rights
whistleblowers
privchat
