title: PrivChat with Tor - "Online Privacy in 2020: Activism & COVID-19" (live stream)
---
author: ggus
---
start_date: 2020-06-23
---
summary:

This edition topic: Privacy in today’s context (covid-19 and protests)



---
body:

**June 23, Tuesday, 2pm EDT / 1800 UTC | [@thetorproject](https://youtu.be/gSyDvG4Z308) YouTube channel.**

*PrivChat: a conversation about tech, human rights, and internet freedom brought to you by the Tor Project. The topic for this edition: privacy in the context of COVID-19 and protesting.*

When the COVID-19 pandemic hit most countries around the world, many governments looked for technology to trace the spread of the virus in order to fight the pandemic. Contact tracing practices and technologies raised many questions about privacy, particularly: is it possible to trace the virus while respecting people's privacy? Now amidst the uprising in the U.S. against systemic racism, followed by protests all around the world, the central question about contact tracing, privacy, and surveillance becomes critical. Can the technology used for tracking the virus be used to track protesters? Will it be? For our first ever PrivChat, the Tor Project is bringing you three amazing guests to chat with us about privacy in this context.

#### * Carmela Troncoso

#### * Daniel Kahn Gillmor

#### * Matt Mitchell

#### * Roger Dingledine



**About PrivChat** 

PrivChat is a fundraising event series held to raise donations for the Tor Project. Our goal is to bring you important information related to what is happening in tech, human rights, and internet freedom by convening experts for a chat with our community. PrivChat events are free to attend. If you find value in this content and Tor, please consider becoming a monthly donor: <https://donate.torproject.org/monthly-giving>



**About the Tor Project** 

The Tor Project is a US 501(c)(3) non-profit organization advancing human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding.

**More information: <https://torproject.org/privchat/>**

Website:
[YouTube channel](https://youtu.be/gSyDvG4Z308)
---
tags:

privacy
video
